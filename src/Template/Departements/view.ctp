<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Departement $departement
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Departement'), ['action' => 'edit', $departement->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Departement'), ['action' => 'delete', $departement->id], ['confirm' => __('Are you sure you want to delete # {0}?', $departement->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Departements'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Departement'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Regions'), ['controller' => 'Regions', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Region'), ['controller' => 'Regions', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Communes'), ['controller' => 'Communes', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Commune'), ['controller' => 'Communes', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="departements view large-9 medium-8 columns content">
    <h3><?= h($departement->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Departementscol') ?></th>
            <td><?= h($departement->departementscol) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Region') ?></th>
            <td><?= $departement->has('region') ? $this->Html->link($departement->region->id, ['controller' => 'Regions', 'action' => 'view', $departement->region->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($departement->id) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Communes') ?></h4>
        <?php if (!empty($departement->communes)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Nom') ?></th>
                <th scope="col"><?= __('Departement Id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($departement->communes as $communes): ?>
            <tr>
                <td><?= h($communes->id) ?></td>
                <td><?= h($communes->nom) ?></td>
                <td><?= h($communes->departement_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Communes', 'action' => 'view', $communes->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Communes', 'action' => 'edit', $communes->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Communes', 'action' => 'delete', $communes->id], ['confirm' => __('Are you sure you want to delete # {0}?', $communes->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
