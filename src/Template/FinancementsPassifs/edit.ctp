<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\FinancementsPassif $financementsPassif
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $financementsPassif->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $financementsPassif->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Financements Passifs'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Dossiers'), ['controller' => 'Dossiers', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Dossier'), ['controller' => 'Dossiers', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="financementsPassifs form large-9 medium-8 columns content">
    <?= $this->Form->create($financementsPassif) ?>
    <fieldset>
        <legend><?= __('Edit Financements Passif') ?></legend>
        <?php
            echo $this->Form->control('libelle');
            echo $this->Form->control('annee_moins_trois');
            echo $this->Form->control('annee_moins_deux');
            echo $this->Form->control('annee_moins_un');
            echo $this->Form->control('annee');
            echo $this->Form->control('prevision');
            echo $this->Form->control('variation');
            echo $this->Form->control('dossiers_id', ['options' => $dossiers]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
