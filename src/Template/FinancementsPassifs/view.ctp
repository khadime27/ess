<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\FinancementsPassif $financementsPassif
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Financements Passif'), ['action' => 'edit', $financementsPassif->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Financements Passif'), ['action' => 'delete', $financementsPassif->id], ['confirm' => __('Are you sure you want to delete # {0}?', $financementsPassif->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Financements Passifs'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Financements Passif'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Dossiers'), ['controller' => 'Dossiers', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Dossier'), ['controller' => 'Dossiers', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="financementsPassifs view large-9 medium-8 columns content">
    <h3><?= h($financementsPassif->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Libelle') ?></th>
            <td><?= h($financementsPassif->libelle) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Dossier') ?></th>
            <td><?= $financementsPassif->has('dossier') ? $this->Html->link($financementsPassif->dossier->id, ['controller' => 'Dossiers', 'action' => 'view', $financementsPassif->dossier->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($financementsPassif->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Annee Moins Trois') ?></th>
            <td><?= $this->Number->format($financementsPassif->annee_moins_trois) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Annee Moins Deux') ?></th>
            <td><?= $this->Number->format($financementsPassif->annee_moins_deux) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Annee Moins Un') ?></th>
            <td><?= $this->Number->format($financementsPassif->annee_moins_un) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Annee') ?></th>
            <td><?= $this->Number->format($financementsPassif->annee) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Prevision') ?></th>
            <td><?= $this->Number->format($financementsPassif->prevision) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Variation') ?></th>
            <td><?= $this->Number->format($financementsPassif->variation) ?></td>
        </tr>
    </table>
</div>
