<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Banniere $banniere
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Banniere'), ['action' => 'edit', $banniere->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Banniere'), ['action' => 'delete', $banniere->id], ['confirm' => __('Are you sure you want to delete # {0}?', $banniere->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Bannieres'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Banniere'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="bannieres view large-9 medium-8 columns content">
    <h3><?= h($banniere->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Photo') ?></th>
            <td><?= h($banniere->photo) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($banniere->id) ?></td>
        </tr>
    </table>
</div>
