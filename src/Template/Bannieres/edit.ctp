<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Banniere $banniere
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $banniere->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $banniere->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Bannieres'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="bannieres form large-9 medium-8 columns content">
    <?= $this->Form->create($banniere) ?>
    <fieldset>
        <legend><?= __('Edit Banniere') ?></legend>
        <?php
            echo $this->Form->control('photo');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
