<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\FinancesParametre $financesParametre
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Finances Parametres'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="financesParametres form large-9 medium-8 columns content">
    <?= $this->Form->create($financesParametre) ?>
    <fieldset>
        <legend><?= __('Add Finances Parametre') ?></legend>
        <?php
            echo $this->Form->control('libelle');
            echo $this->Form->control('type');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
