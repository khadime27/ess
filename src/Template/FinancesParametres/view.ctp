<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\FinancesParametre $financesParametre
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Finances Parametre'), ['action' => 'edit', $financesParametre->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Finances Parametre'), ['action' => 'delete', $financesParametre->id], ['confirm' => __('Are you sure you want to delete # {0}?', $financesParametre->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Finances Parametres'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Finances Parametre'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="financesParametres view large-9 medium-8 columns content">
    <h3><?= h($financesParametre->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Libelle') ?></th>
            <td><?= h($financesParametre->libelle) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Type') ?></th>
            <td><?= h($financesParametre->type) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($financesParametre->id) ?></td>
        </tr>
    </table>
</div>
