<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Filiere $filiere
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $filiere->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $filiere->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Filieres'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Dossiers'), ['controller' => 'Dossiers', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Dossier'), ['controller' => 'Dossiers', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="filieres form large-9 medium-8 columns content">
    <?= $this->Form->create($filiere) ?>
    <fieldset>
        <legend><?= __('Edit Filiere') ?></legend>
        <?php
            echo $this->Form->control('nom');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
