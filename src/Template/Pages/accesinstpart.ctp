
<main id="main">
<section id="featured-services" class="featured-services" style="margin-top:8%">
 
    <div class="container" data-aos="fade-up">
    <div class="section-title">
          <h2>Devenir Membre de l'ESS Senegal</h2>
          </div>
        <div class="row">
            <div class="col-md-6 col-lg-3 d-flex align-items-stretch mb-5 mb-lg-0">
                <div id="personne" class="icon-box cursor-pointer" data-aos="fade-up" data-aos-delay="100">
                    <div class="icon"><i class="bx bx-male"></i></div>
                    <h4 class="title"><a href="<?php echo $this->Url->build(array('controller' => 'Users', 'action' => 'add')); ?>">Entrepreneur(es)</a></h4>
                    <p class="description">Inscrivez-vous en tant que Acteur</p>
                </div>
            </div>

            <div class="col-md-6 col-lg-3 d-flex align-items-stretch mb-5 mb-lg-0">
                <div id="entreprise" class="icon-box cursor-pointer" data-aos="fade-up" data-aos-delay="200">
                    <div class="icon"><i class="bx bx-building"></i></div>
                    <h4 class="title"><a href="<?php echo $this->Url->build(array('controller' => 'Users', 'action' => 'add')); ?>">Société Coopérative et Participative (SCOP)</a></h4>
                    <p class="description">Vous êtes une entreprise ( SA, SARL ou SAS dont les salariés sont les associés majoritaires) ?</p>
                </div>
            </div>

            <div class="col-md-6 col-lg-3 d-flex align-items-stretch mb-5 mb-lg-0">
                <div id="entite" class="icon-box cursor-pointer" data-aos="fade-up" data-aos-delay="300">
                    <div class="icon"><i class="bx bxs-institution"></i></div>
                    <h4 class="title"><a href="<?php echo $this->Url->build(array('controller' => 'Users', 'action' => 'add')); ?>">Association</a></h4>
                    <p class="description">Vous êtes une association, groupement, GIE, fondation</p>
                </div>
            </div>
            <div class="col-md-6 col-lg-3 d-flex align-items-stretch mb-5 mb-lg-0">
                <div id="autre" class="icon-box cursor-pointer" data-aos="fade-up" data-aos-delay="300">
                    <div class="icon"><i class="bx bx-group"></i></div>
                    <h4 class="title"><a href="<?php echo $this->Url->build(array('controller' => 'Users', 'action' => 'add')); ?>">Autres</a></h4>
                    <p class="description">Vous êtes autres acteurs de l'ESS</p>
                </div>
            </div>
        </div>
    </div>
    </div>
    <div class="container" data-aos="fade-up" style="margin-top:2%">
    <div class="section-title">
          <h2>ACCES ACTEURS PUBLICS ET PARTENAIRES FINANCIERS</h2>
          </div>
        <div class="row">
            <div class="col-md-6 col-lg-3 d-flex align-items-stretch mb-5 mb-lg-0">
                <div id="personne" class="icon-box cursor-pointer" data-aos="fade-up" data-aos-delay="100">
                    <div class="icon"><i class="bx bx-male"></i></div>
                    <h4 class="title"><a href="<?php echo $this->Url->build(array('controller' => 'Users', 'action' => 'add')); ?>">Autorité administrative déconcentrée</a></h4>
                    <p class="description">Vous êtes une autorité administrative déconcentrée</p>
                </div>
            </div>

            <div class="col-md-6 col-lg-3 d-flex align-items-stretch mb-5 mb-lg-0">
                <div id="entreprise" class="icon-box cursor-pointer" data-aos="fade-up" data-aos-delay="200">
                    <div class="icon"><i class="bx bx-buildings"></i></div>
                    <h4 class="title"><a href="<?php echo $this->Url->build(array('controller' => 'Users', 'action' => 'add')); ?>">Collectivités territoriales</a></h4>
                    <p class="description">Vous êtes une Collectivités territoriales ?</p>
                </div>
            </div>

            <div class="col-md-6 col-lg-3 d-flex align-items-stretch mb-5 mb-lg-0">
                <div id="entite" class="icon-box cursor-pointer" data-aos="fade-up" data-aos-delay="300">
                    <div class="icon"><i class="bx bx-building-house"></i></div>
                    <h4 class="title"><a href="<?php echo $this->Url->build(array('controller' => 'Users', 'action' => 'add')); ?>">Sociétés nationales,Etablissement publics Agences d’exécution</a></h4>
                    <p class="description">Vous êtes une Sociétés nationales,Etablissement publics Agences d’exécution ?</p>
                </div>
            </div>
            <div class="col-md-6 col-lg-3 d-flex align-items-stretch mb-5 mb-lg-0">
                <div id="autre" class="icon-box cursor-pointer" data-aos="fade-up" data-aos-delay="300">
                    <div class="icon"><i class="bx bx-money"></i></div>
                    <h4 class="title"><a href="<?php echo $this->Url->build(array('controller' => 'Users', 'action' => 'add')); ?>">Partenaires Financiers</a></h4>
                    <p class="description">Vous êtes Partenaires Financiers</p>
                </div>
            </div>
        </div>
    </div>
</section>
</main>