<main id="main">

<!-- ======= Featured Services Section ======= -->
<section id="featured-services" class="contact"  style="margin-top:8%">
  <div class="container" data-aos="fade-up">
  
  <div class="section-title">
          <h2><span>Médiathèque</span></h2>
          <h3>Actualité et liens utiles de l' <span>Economie Sociale et Solidaire</span></h3>
        </div>

        <div class="row" data-aos="fade-up" data-aos-delay="100">
          <div class="col-lg-6">
            <div class="info-box mb-4">
              <i class="bx bxs-camera-movie"></i>
              <h3>Vidéos Actualité</h3>
              <table class="mb-0 table">
                                            
                <tbody>
                    <tr>
                    <td> STESS/Bilan: « IL y’a une forte volonté de promouvoir l’économie sociale et solidaire au Sénégal</td>

                    <td scope="row"><a href="https://www.youtube.com/watch?v=6UCAxxez-aA&ab_channel=DakaractuTVHD" 
                        class="venobox btn-watch-video"
                        data-vbtype="video" data-autoplay="true"> 
                        <i class="icofont-play-alt-2"></i></a>
                       </td>
                      
                    </tr>
                    <tr>
                    
                    <td>L'économie sociale et solidaire, le business autrement</td>
                        <td scope="row"><a href="https://www.youtube.com/watch?v=KA0ykHuy9Nk&ab_channel=FRANCE24" 
                        class="venobox btn-watch-video"
                        data-vbtype="video" data-autoplay="true"> 
                        
                        <i class="icofont-play-alt-2"></i></a></td>
                                                
                    </tr>

                    <tr>
                    
                    <td>
                        L’Économie sociale et solidaire est enseigné dans les université du Sénégal</td>
                    <td scope="row"><a href="https://www.youtube.com/watch?v=yipXZRbm-Q0&ab_channel=DakaractuTVHD" 
                        class="venobox btn-watch-video"
                        data-vbtype="video" data-autoplay="true"> 
                        <i class="icofont-play-alt-2"></i></a></td>
                    </tr>

                    
                </tbody>
            </table>
            </div>
          </div>

          <div class="col-lg-6 col-md-6">
            <div class="info-box  mb-4">
              <i class="bx bxs-file-doc"></i>

              <h3>Documents ESS </h3>
              <table class="mb-0 table">                
                <tbody>
                    <tr>
                    <td>
                        <a href="http://polecng.sn/IMG/pdf/14-savoirs-communs.pdf">
                        L'ESS, un atout pour la coopération ...
                        </a>
                    </td>
                    </tr>
                    <tr>
                    <td>
                        <a href="http://lartes-ifan.org/wp-content/uploads/2019/12/salon_ess_dakar.pdf">
                        Évolution historique de l'ESS...
                        </a>
                    </td>
                    </tr>
                    <tr>
                    <td>
                        <a href="https://www.eclosio.ong/wp-content/uploads/2018/09/economie-sociale-entreprendre-avec-et-pour-les-autres.pdf">
                        L'ESS, au bénéfice des populations rurales...
                        </a>
                    </td>
                    </tr>
                </tbody>
                </table>
            </div>
          </div>

  </div>
</section>
</main>