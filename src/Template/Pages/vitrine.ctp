<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
  </ol>
  <div class="carousel-inner">
    <div class="carousel-item active">
      <!-- ======= Hero Section ======= -->
      <section class="d-flex align-items-center testimonial-item" id="hero" >
        <div class="container" data-aos="zoom-out" data-aos-delay="100">
          <h1>Bienvenue à <span style="color:#672211;">ESS</span>
          </h1>
          <h2>Formation Accompagnement Innovations dans les Terroirs du Sénégal <strong>(FAITS)</strong> </h2>
          <div class="d-flex">
            <a href="#about" class="btn-get-started scrollto" style="background-color:#672211;">ESS c’est quoi</a>
            <a href="https://youtu.be/oQQySulvkm8?t=15" class="venobox btn-watch-video"
            data-vbtype="video" data-autoplay="true"> Mot du ministre <i class="icofont-play-alt-2" style="color:#672211;"></i></a>

            <a href="https://youtu.be/GhydFP3Ccsc?t=109" class="venobox btn-watch-video"
            data-vbtype="video" data-autoplay="true"style=" margin-left: 25%"> 
            Mot du ministre (Wolof)<i class="icofont-play-alt-2" style="color:#672211"></i></a>
          </div>
        </div>
      </section><!-- End Hero -->
    </div>
    <div class="carousel-item">
      <!-- ======= bg-1 Section ======= -->
   <section  class="d-flex align-items-center testimonial-item" id="bg-1">
    <div class="container" data-aos="zoom-out" data-aos-delay="100">
    <h1>SUNU LABEL <span >E.S.S</span>
      </h1>
      <h2>Formation Accompagnement Innovations dans les Terroirs du Sénégal <strong>(FAITS)</strong> </h2>
      <div class="d-flex">
        <a href="#about" class="btn-get-started scrollto">ESS c’est quoi</a>
        <a href="https://youtu.be/oQQySulvkm8?t=15" class="venobox btn-watch-video"
         data-vbtype="video" data-autoplay="true"> Mot du ministre <i class="icofont-play-alt-2"></i></a>

         <a href="https://youtu.be/GhydFP3Ccsc?t=109" class="venobox btn-watch-video"
            data-vbtype="video" data-autoplay="true"style=" margin-left: 25%"> 
            Mot du ministre (Wolof)<i class="icofont-play-alt-2" style="color:#672211"></i></a>
      </div>
    </div>
  </section><!-- End bg-1 -->
    </div>

  </div>
  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Precedent</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Suivant</span>
  </a>
</div>  

  <main id="main">

  <section id="featured-services" class="featured-services">
  <div class="section-title">
          <h2>Devenir Membre de l'ESS Senegal</h2>
          </div>
    <div class="container" data-aos="fade-up">
        <div class="row">
            <div class="col-md-6 col-lg-3 d-flex align-items-stretch mb-5 mb-lg-0">
                <div id="personne" class="icon-box cursor-pointer" data-aos="fade-up" data-aos-delay="100">
                    <div class="icon"><i class="bx bx-male"></i></div>
                    <h4 class="title"><a href="<?php echo $this->Url->build(array('controller' => 'Users', 'action' => 'add')); ?>">Entrepreneur(es)</a></h4>
                    <p class="description">Inscrivez-vous en tant que Acteur</p>
                </div>
            </div>

            <div class="col-md-6 col-lg-3 d-flex align-items-stretch mb-5 mb-lg-0">
                <div id="entreprise" class="icon-box cursor-pointer" data-aos="fade-up" data-aos-delay="200">
                    <div class="icon"><i class="bx bx-building"></i></div>
                    <h4 class="title"><a href="<?php echo $this->Url->build(array('controller' => 'Users', 'action' => 'add')); ?>">Société Coopérative et Participative (SCOP)</a></h4>
                    <p class="description">Vous êtes une entreprise ( SA, SARL ou SAS dont les salariés sont les associés majoritaires) ?</p>
                </div>
            </div>

            <div class="col-md-6 col-lg-3 d-flex align-items-stretch mb-5 mb-lg-0">
                <div id="entite" class="icon-box cursor-pointer" data-aos="fade-up" data-aos-delay="300">
                    <div class="icon"><i class="bx bxs-institution"></i></div>
                    <h4 class="title"><a href="<?php echo $this->Url->build(array('controller' => 'Users', 'action' => 'add')); ?>">Association</a></h4>
                    <p class="description">Vous êtes une association, groupement, GIE, fondation</p>
                </div>
            </div>
            <div class="col-md-6 col-lg-3 d-flex align-items-stretch mb-5 mb-lg-0">
                <div id="autre" class="icon-box cursor-pointer" data-aos="fade-up" data-aos-delay="300">
                    <div class="icon"><i class="bx bx-group"></i></div>
                    <h4 class="title"><a href="<?php echo $this->Url->build(array('controller' => 'Users', 'action' => 'add')); ?>">Autres</a></h4>
                    <p class="description">Vous êtes autres acteurs de l'ESS</p>
                </div>
            </div>
        </div>
    </div>
</section>

    <!-- ======= Featured Services Section ======= 
    <section id="featured-services" class="featured-services">
      <div class="container" data-aos="fade-up">

        <div class="row">
          <div class="col-md-6 col-lg-3 d-flex align-items-stretch mb-5 mb-lg-0">
            <div class="icon-box" data-aos="fade-up" data-aos-delay="100">
              <div class="icon"><i class="bx bxl-dribbble"></i></div>
              <h4 class="title"><a href="">Valeur ESS</a></h4>
              <p class="description">Faire  de la coopération une valeur aussi puissante que la compétitivité et la concurrence</p>
            </div>
          </div>

          <div class="col-md-6 col-lg-3 d-flex align-items-stretch mb-5 mb-lg-0">
            <div class="icon-box" data-aos="fade-up" data-aos-delay="200">
              <div class="icon"><i class="bx bx-file"></i></div>
              <h4 class="title"><a href="">Devenir Membre</a></h4>
              <p class="description">L’adhésion aux projets et aux structures est ouverte et volontaire</p>
            </div>
          </div>

          <div class="col-md-6 col-lg-3 d-flex align-items-stretch mb-5 mb-lg-0">
            <div class="icon-box" data-aos="fade-up" data-aos-delay="300">
              <div class="icon"><i class="bx bx-tachometer"></i></div>
              <h4 class="title"><a href="">Le Baord</a></h4>
              <p class="description">Adopter une démarche favorisant la coopération ou le partenariat au service de projets économiques et sociaux innovants.</p>
            </div>
          </div>

          <div class="col-md-6 col-lg-3 d-flex align-items-stretch mb-5 mb-lg-0">
            <div class="icon-box" data-aos="fade-up" data-aos-delay="400">
              <div class="icon"><i class="bx bx-world"></i></div>
              <h4 class="title"><a href="">Vision</a></h4>
              <p class="description">Soutenir  les efforts territoriaux face aux grands défis sociaux et environnementaux du XXIe siècle</p>
            </div>
          </div>

        </div>

      </div>
    </section>--><!-- End Featured Services Section -->

    <!-- ======= About Section ======= -->
    <section id="about" class="about section-bg">
      <div class="container" data-aos="fade-up">

        <div class="section-title">
          <h3>Construire collectivement <span>le Sénégal que nous aimons </span></h3>
          <h4><span> Bâtir l'avenir à l'unission </span></h4>
          <p>Avoir l'Agrement ESS, c'est trouver des partenaires à vos projets</p>
        </div>

        <!-- <div class="row">
          <div class="col-lg-6 pt-4 pt-lg-0 content d-flex flex-column" data-aos="zoom-out" data-aos-delay="100">
            <?= $this->Html->image("about.png", ['alt' => '', 'class' => 'img-fluid']) ?>
            
          </div>
        </div> -->

        <div class="row">
          <div class="col-lg-1"></div>
          <div class="col-lg-4 pt-4 pt-lg-0 content d-flex flex-column" data-aos="zoom-out" data-aos-delay="100">
            <?= $this->Html->image("about2.png", ['alt' => '', 'class' => 'img-fluid']) ?>
            
          </div>
          <div class="col-lg-6 " data-aos="fade-up" data-aos-delay="100">
          <h5>Objectifs et Plaidoyer</h5>
          <p>En apportant  des réponses aux besoins économiques, sociaux, et environnementaux des territoires, la
             démarche <b>ESS</b> permet de : <br>
<p><b>•</b>	 Favoriser l’insertion sociale et professionnelle, lutter contre l’exclusion, la pauvreté, les discriminations</p>
<p><b>•</b>  Créer de nouveaux outils dans le développement de l’ESS</p>
<p><b>•</b>	 Promouvoir un patrimoine pour lui permettre d’assumer les missions de service public qui sont confiées par lui ou par la loi aux différents acteurs de l’ESS</p>
<p><b>•</b>	 Appuyer les acteurs de l’ESS dans les instances d’élaboration et de suivi des politiques territoriales</p>
          </div>
        </div>

        <div class="row" style="padding-top:20px; padding-left:14px;">
        <div class="x_panel">
<div class="x_content">
<ul class="nav nav-tabs bar_tabs" id="myTab" role="tablist">
<li class="nav-item">
<a class="nav-link active" id="role-tab" data-toggle="tab" href="#role" role="tab" aria-controls="role" aria-selected="true"> <b>Role et responsabilité</b></a>
</li>
<li class="nav-item" >
<a class="nav-link" id="acteur-tab" data-toggle="tab" href="#acteur" role="tab" aria-controls="acteur" aria-selected="false"> <b>Acteurs</b></a>
</li>
<li class="nav-item">
<a class="nav-link" id="cness-tab" data-toggle="tab" href="#cness" role="tab" aria-controls="cness" aria-selected="false"><b>CNESS</b></a>
</li>
<li class="nav-item">
<a class="nav-link" id="ess-tab" data-toggle="tab" href="#ess" role="tab" aria-controls="ess" aria-selected="false"><b>ESS dans le monde</b></a>
</li>
<li class="nav-item">
<a class="nav-link" id="dossier-tab" data-toggle="tab" href="#dossier" role="tab" aria-controls="dossier" aria-selected="false"><b>Dossiers thématiques</b></a>
</li>
<li class="nav-item">
<a class="nav-link" id="agrement-tab" data-toggle="tab" href="#agrement" role="tab" aria-controls="agrement" aria-selected="false"><b>L’agrément ESS</b></a>
</li>
<li class="nav-item">
<a class="nav-link" id="avantage-tab" data-toggle="tab" href="#avantage" role="tab" aria-controls="avantage" aria-selected="false"><b>Avantages</b></a>
</li>

</ul>
<div class="tab-content" id="myTabContent">
<div class="tab-pane fade show active" id="role" role="tabpanel" aria-labelledby="role-tab">
<strong>Ministère de ESS</strong> <br>
<p>
Met en œuvre la politique nationale de promotion et de développement de l’économie sociale et solidaire. <br>
-	Elabore la stratégie nationale de promotion et de développement de l’économie sociale et solidaire. <br>
-	Promeut et développe l’économie sociale et solidaire <br>
-	Facilite l’accès au financement aux acteurs de l’E.S.S. <br>
<a href="<?php echo $this->Url->build(array('controller' => 'Pages', 'action' => 'role')); ?>"> En savoir plus...</a>

</p>

</div>
<div class="tab-pane fade" id="acteur" role="tabpanel" aria-labelledby="acteur-tab">

</div>
<div class="tab-pane fade" id="cness" role="tabpanel" aria-labelledby="cness-tab">
<p>texte</p>
<a href="<?php echo $this->Url->build(array('controller' => 'Pages', 'action' => 'cness')); ?>" target="_blank" class="btn-buy">Plus d'Info...</a>

</div>
<div class="tab-pane fade" id="ess" role="tabpanel" aria-labelledby="ess-tab">
texte ESS dans le monde
</div>
<div class="tab-pane fade" id="dossier" role="tabpanel" aria-labelledby="dossier-tab">
Publication/communication (espace ouvert à tous renvoi à l’espace)
</div>
<div class="tab-pane fade" id="agrement" role="tabpanel" aria-labelledby="agrement-tab">
<p>Une structure est considérée <b>d’utilité sociale</b>  si elle a pour objectif d’apporter un soutien 
  à des personnes en situation de <b>fragilité</b> , « contribuer à la lutte contre les exclusions et les
   inégalités sanitaires, sociales, économiques et culturelles, à l’éducation à la citoyenneté, à la
    préservation et au développement du lien social ou au maintien et au renforcement de la cohésion 
    territoriale », ou encore concourir au développement durable.</p>
    <p> <b>L'agrément</b> est la reconnaissance officielle délivré par l’autorité XXXXXXXXXXX, attestant 
    qu’une personne physique ou morale  possède la formation et les qualités nécessaires pour recevoir un 
    titre d’organisme ESS et qu’elle vérifie les critères d’utilités sociales et associés à la pratique de 
    son activité dans son domaine.</p>
</div>

<div class="tab-pane fade" id="avantage" role="tabpanel" aria-labelledby="avantage-tab">
<p>Cet agrément permet  aux structures éligibles d'obtenir des fonds issus de l'épargne salariale solidaire et 
  de bénéficier de dispositifs de financement spécifiques</p>
  <p><b>1 - </b>Elles ont pour objectif d'apporter, à travers leur activité, un soutien à des personnes en 
  situation de fragilité</p>
  <p><b>2 - </b>Elles ont pour objectif de contribuer à la lutte contre les exclusions et les inégalités 
  sanitaires, sociales, économiques et culturelles, à l'éducation à la citoyenneté</p>
  <p><b>3 - </b>Elles concourent au développement durable dans ses dimensions économique, sociale, 
  environnementale et participative, à la transition énergétique ou à la solidarité internationale</p>
</div>
</div>

</div>
</div>
        </div>







        
      </div>
    </section><!-- End About Section -->

    <!-- ======= Skills Section ======= -->
    <section id="skills" class="skills">
      <div class="container" data-aos="fade-up">

        <div class="row skills-content">

          <div class="col-lg-6">

            <div class="progress">
              <span class="skill">GIE <i class="val">85%</i></span>
              <div class="progress-bar-wrap">
                <div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="85"></div>
              </div>
            </div>

            <div class="progress">
              <span class="skill">Groupement de femme <i class="val">90%</i></span>
              <div class="progress-bar-wrap">
                <div class="progress-bar" role="progressbar" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100"></div>
              </div>
            </div>

            <div class="progress">
              <span class="skill">Association <i class="val">75%</i></span>
              <div class="progress-bar-wrap">
                <div class="progress-bar" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
              </div>
            </div>

          </div>

          <div class="col-lg-6">

            <div class="progress">
              <span class="skill">Entreprenariat <i class="val">80%</i></span>
              <div class="progress-bar-wrap">
                <div class="progress-bar" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"></div>
              </div>
            </div>

            <div class="progress">
              <span class="skill">PME <i class="val">90%</i></span>
              <div class="progress-bar-wrap">
                <div class="progress-bar" role="progressbar" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100"></div>
              </div>
            </div>

            <div class="progress">
              <span class="skill">Micro Credit <i class="val">55%</i></span>
              <div class="progress-bar-wrap">
                <div class="progress-bar" role="progressbar" aria-valuenow="55" aria-valuemin="0" aria-valuemax="100"></div>
              </div>
            </div>

          </div>

        </div>

      </div>
    </section><!-- End Skills Section -->

    <!-- ======= Counts Section ======= -->
    <section id="counts" class="counts">
      <div class="container" data-aos="fade-up">

        <div class="row">

          <div class="col-lg-3 col-md-6">
            <div class="count-box">
              <i class="icofont-simple-smile"></i>
              <span data-toggle="counter-up">2302</span>
              <p>Acteurs</p>
            </div>
          </div>

          <div class="col-lg-3 col-md-6 mt-5 mt-md-0">
            <div class="count-box">
              <i class="icofont-document-folder"></i>
              <span data-toggle="counter-up">521</span>
              <p>Projets</p>
            </div>
          </div>

          <div class="col-lg-3 col-md-6 mt-5 mt-lg-0">
            <div class="count-box">
              <i class="icofont-live-support"></i>
              <span data-toggle="counter-up">263</span>
              <p>Financement</p>
            </div>
          </div>

          <div class="col-lg-3 col-md-6 mt-5 mt-lg-0">
            <div class="count-box">
              <i class="icofont-users-alt-5"></i>
              <span data-toggle="counter-up">15</span>
              <p>Labellisés ESS</p>
            </div>
          </div>

        </div>

      </div>
    </section><!-- End Counts Section -->

    <!-- ======= Clients Section ======= -->
    <section id="clients" class="clients section-bg">
      <div class="container" data-aos="zoom-in">

      <marquee>
        <div class="row">
          <div class="col-lg-2 col-md-4 col-6 d-flex align-items-center justify-content-center">
            <?= $this->Html->image("clients/mfb.png", ['alt' => '', 'class' => 'img-fluid']) ?>
          </div>

          <div class="col-lg-2 col-md-4 col-6 d-flex align-items-center justify-content-center">
            <?= $this->Html->image("clients/afd.jpg", ['alt' => '', 'class' => 'img-fluid']) ?>
          </div>

          <div class="col-lg-2 col-md-4 col-6 d-flex align-items-center justify-content-center">
            <?= $this->Html->image("clients/mepc.png" ,['alt' => '', 'class' => 'img-fluid']) ?>
          </div>

          <div class="col-lg-2 col-md-4 col-6 d-flex align-items-center justify-content-center">
            <?= $this->Html->image("clients/plasepri1.png", ['alt' => '', 'class' => 'img-fluid']) ?>
          </div>

          <div class="col-lg-2 col-md-4 col-6 d-flex align-items-center justify-content-center">
            <?= $this->Html->image("clients/ractes.png", ['alt' => '', 'class' => 'img-fluid']) ?>
          </div>

          <div class="col-lg-2 col-md-4 col-6 d-flex align-items-center justify-content-center">
            <?= $this->Html->image("clients/promise1.jpg", ['alt' => '', 'class' => 'img-fluid']) ?>
          </div>
        </div>
        </marquee>

      </div>
    </section><!-- End Clients Section -->

    <!-- ======= Services Section ======= -->
    <section id="services" class="services">
      <div class="container" data-aos="fade-up">

        <div class="section-title">
          <h3>Le <span>Ministère</span></h3>
          <?= $this->Html->image("zahra1.jpg", ['alt' => '', 'class' => 'img-fluid']) ?>


        </div>

        <div class="row">
          
          <div class="col-lg-3 col-md-3 d-flex align-items-stretch mt-4 mt-md-0" data-aos="zoom-in" data-aos-delay="100">
            <div class="icon-box">
              <div class="icon"><i class="bx bx-file"></i></div>
              <h4><a href="">Mission</a></h4>
              <p>Le Ministre de la Microfinance, de l’Economie sociale et solidaire 
              </p>
            </div>
          </div>

          <div class="col-lg-3 col-md-3 d-flex align-items-stretch mt-4 mt-md-0" data-aos="zoom-in" data-aos-delay="300">
            <div class="icon-box">
              <div class="icon"><i class="bx bx-tachometer"></i></div>
              <h4><a href="">Les Services</a></h4>
              <p>Le Ministère : Madame Zahra Iyane THIAM Madame Zahra Iyane </p>
            </div>
          </div>

          <div class="col-lg-3 col-md-3 d-flex align-items-stretch mt-4 mt-md-0" data-aos="zoom-in" data-aos-delay="300">
            <div class="icon-box">
              <div class="icon"><i class="bx bx-world"></i></div>
              <h4><a href="">Rapports d'activités</a></h4>
              <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis</p>
            </div>
          </div>

          <div class="col-lg-3 col-md-3 d-flex align-items-stretch mt-4 mt-md-0" data-aos="zoom-in" data-aos-delay="400">
            <div class="icon-box">
              <div class="icon"><i class="bx bx-slideshow"></i></div>
              <h4><a href="">Coronavirus</a></h4>
              <p>Quis consequatur saepe eligendi voluptatem consequatur dolor consequuntur</p>
            </div>
          </div>

          <!-- <div class="col-lg-4 col-md-4 d-flex align-items-stretch mt-4" data-aos="zoom-in" data-aos-delay="300">
            <div class="icon-box">
              <div class="icon"><i class="bx bx-arch"></i></div>
              <h4><a href="">Divera don</a></h4>
              <p>Modi nostrum vel laborum. Porro fugit error sit minus sapiente sit aspernatur</p>
            </div>
          </div> -->

        </div>

      </div>
    </section><!-- End Services Section -->

    <!-- ======= Testimonials Section ======= -->
    <section id="testimonials" class="testimonials">
      <div class="container" data-aos="zoom-in">

        <div class="owl-carousel testimonials-carousel">
          
        <div class="testimonial-item">
          <?= $this->Html->image("testimonials/testimonials-3.jpg", ['alt' => '', 'class' => 'testimonial-img']) ?>
            
            <h3>Kofi Annan</h3>
            <h4>SG de l'ONU</h4>
            <p>
              <i class="bx bxs-quote-alt-left quote-icon-left"></i>
              Les maux économiques et sociaux dont notre monde est affligé ne sont que trop réels – tout comme la nécessité de faire en sorte que la mondialisation profite à tous les peuples, en ancrant la nouvelle économie mondiale dans une société mondiale qui repose sur des valeurs globales communes de solidarité, de justice sociale et de respect des droits de l'homme
              <i class="bx bxs-quote-alt-right quote-icon-right"></i>
            </p>
          </div>

          <div class="testimonial-item">
            
            <?= $this->Html->image("testimonials/testimonials-1.jpg", ['alt' => '', 'class' => 'testimonial-img']) ?>
            <h3>Saul Goodman</h3>
            <h4>Ceo &amp; Founder</h4>
            <p>
              <i class="bx bxs-quote-alt-left quote-icon-left"></i>
              Le domaine de l’ESS est une branche très vaste et assez particulière. Il a une vocation d’utilité sociale. Il requiert une polyvalence dans le sens où il associe l’activité d’entreprise et le domaine économique. Il s’agit également d’un secteur en perpétuelle mobilité et où les acteurs sont très diversifiés (mutuelles, associations, coopératives, fondations...). 
              <i class="bx bxs-quote-alt-right quote-icon-right"></i>
            </p>
          </div>

          <div class="testimonial-item">
          <?= $this->Html->image("testimonials/testimonials-2.jpg", ['alt' => '', 'class' => 'testimonial-img']) ?>
            
            <h3>Mme Zahra Iyane Thiam</h3>
            <h4>Ministre ESS</h4>
            <p>
              <i class="bx bxs-quote-alt-left quote-icon-left"></i>
              En économie sociale et solidaire, entreprendre est une forme d'action politique. Il s'agit de transformer le monde.
              <i class="bx bxs-quote-alt-right quote-icon-right"></i>
            </p>
          </div>

          <div class="testimonial-item">
          <?= $this->Html->image("testimonials/testimonials-4.jpg", ['alt' => '', 'class' => 'testimonial-img']) ?>
            
            <h3>Matt Brandon</h3>
            <h4>Freelancer</h4>
            <p>
              <i class="bx bxs-quote-alt-left quote-icon-left"></i>
              Face aux excès et à la violence dangereuse de la financiarisation d'une économie mondialisée, l'économie sociale prend tout son sens
              <i class="bx bxs-quote-alt-right quote-icon-right"></i>
            </p>
          </div>

          <div class="testimonial-item">
          <?= $this->Html->image("testimonials/testimonials-5.jpg", ['alt' => '', 'class' => 'testimonial-img']) ?>
           <h3>Mireille Bertrand Lhérisson</h3>
            <h4>Entrepreneure Sociale, Poète Engagée</h4>
            <p>
              <i class="bx bxs-quote-alt-left quote-icon-left"></i>
              L'économie sociale et solidaire ESS, une économie morale, une solution aux défis posés par les crises économique, sociale et environnementale.
              <i class="bx bxs-quote-alt-right quote-icon-right"></i>
            </p>
          </div>

        </div>

      </div>
    </section><!-- End Testimonials Section -->

    <!-- ======= Portfolio Section ======= -->
    <section id="portfolio" class="portfolio">
      <div class="container" data-aos="fade-up">

        <div class="section-title">
          <h3>Activité ESS au <span>SENEGAL</span></h3>
          <!-- <p>Ut possimus qui ut temporibus culpa velit eveniet modi omnis est adipisci expedita at voluptas atque vitae autem.</p>
        -->
        </div>

        <div class="row" data-aos="fade-up" data-aos-delay="100">
          <div class="col-lg-12 d-flex justify-content-center">
            <ul id="portfolio-flters">
              <li data-filter="*" class="filter-active">Tout</li>
              <li data-filter=".filter-app">AGRO</li>
              <li data-filter=".filter-card"><Table>TRANSFOM</Table></li>
              <li data-filter=".filter-web">TECH</li>
            </ul>
          </div>
        </div>

        <div class="row portfolio-container" data-aos="fade-up" data-aos-delay="200">
        <div class="col-lg-4 col-md-6 portfolio-item filter-web">
            <?= $this->Html->image("portfolio2/portfolio-13.jpg", ['alt' => '', 'class' => 'img-fluid']) ?>
            <div class="portfolio-info">
              <h4>Web 3</h4>
              <p>Web</p>
              <a href="front/img/portfolio2/portfolio-13.jpg" data-gall="portfolioGallery" class="venobox preview-link" title="Web 3"><i class="bx bx-plus"></i></a>
              <a href="portfolio-details.html" class="details-link" title="More Details"><i class="bx bx-link"></i></a>
            </div>
          </div>
          <div class="col-lg-4 col-md-6 portfolio-item filter-app">
          <?= $this->Html->image("portfolio2/portfolio-1.jpg", ['alt' => '', 'class' => 'img-fluid']) ?>
            <div class="portfolio-info">
              <h4>App 1</h4>
              <p>App</p>
              <a href="front/img/portfolio2/portfolio-1.jpg" data-gall="portfolioGallery" class="venobox preview-link" title="App 1"><i class="bx bx-plus"></i></a>
              <a href="portfolio-details.html" class="details-link" title="More Details"><i class="bx bx-link"></i></a>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 portfolio-item filter-web">
            <?= $this->Html->image("portfolio2/portfolio-2.jpg" ,['alt' => '', 'class' => 'img-fluid']) ?>
            <div class="portfolio-info">
              <h4>Web 3</h4>
              <p>Web</p>
              <a href="front/img/portfolio2/portfolio-2.jpg" data-gall="portfolioGallery" class="venobox preview-link" title="Web 3"><i class="bx bx-plus"></i></a>
              <a href="portfolio-details.html" class="details-link" title="More Details"><i class="bx bx-link"></i></a>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 portfolio-item filter-app">
            <?= $this->Html->image("portfolio2/portfolio-3.jpg", ['alt' => '', 'class' => 'img-fluid']) ?>
            <div class="portfolio-info">
              <h4>App 2</h4>
              <p>App</p>
              <a href="front/img/portfolio2/portfolio-3.jpg" data-gall="portfolioGallery" class="venobox preview-link" title="App 2"><i class="bx bx-plus"></i></a>
              <a href="portfolio-details.html" class="details-link" title="More Details"><i class="bx bx-link"></i></a>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 portfolio-item filter-card">
            <?= $this->Html->image("portfolio2/portfolio-4.jpg", ['alt' => '', 'class' => 'img-fluid']) ?>
            <div class="portfolio-info">
              <h4>Card 2</h4>
              <p>Card</p>
              <a href="front/img/portfolio2/portfolio-4.jpg" data-gall="portfolioGallery" class="venobox preview-link" title="Card 2"><i class="bx bx-plus"></i></a>
              <a href="portfolio-details.html" class="details-link" title="More Details"><i class="bx bx-link"></i></a>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 portfolio-item filter-web">
            <?= $this->Html->image("portfolio2/portfolio-5.jpg", ['alt' => '', 'class' => 'img-fluid']) ?>
            <div class="portfolio-info">
              <h4>Web 2</h4>
              <p>Web</p>
              <a href="front/img/portfolio2/portfolio-5.jpg" data-gall="portfolioGallery" class="venobox preview-link" title="Web 2"><i class="bx bx-plus"></i></a>
              <a href="portfolio-details.html" class="details-link" title="More Details"><i class="bx bx-link"></i></a>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 portfolio-item filter-app">
            <?= $this->Html->image("portfolio2/portfolio-12.jpg", ['alt' => '', 'class' => 'img-fluid']) ?>
            <div class="portfolio-info">
              <h4>App 3</h4>
              <p>App</p>
              <a href="front/img/portfolio2/portfolio-12.jpg" data-gall="portfolioGallery" class="venobox preview-link" title="App 3"><i class="bx bx-plus"></i></a>
              <a href="portfolio-details.html" class="details-link" title="More Details"><i class="bx bx-link"></i></a>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 portfolio-item filter-card">
            <?= $this->Html->image("portfolio2/portfolio-11.jpg", ['alt' => '', 'class' => 'img-fluid']) ?>
            <div class="portfolio-info">
              <h4>Card 1</h4>
              <p>Card</p>
              <a href="front/img/portfolio2/portfolio-11.jpg" data-gall="portfolioGallery" class="venobox preview-link" title="Card 1"><i class="bx bx-plus"></i></a>
              <a href="portfolio-details.html" class="details-link" title="More Details"><i class="bx bx-link"></i></a>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 portfolio-item filter-card">
            <?= $this->Html->image("portfolio2/portfolio-8.jpg", ['alt' => '', 'class' => 'img-fluid']) ?>
            <div class="portfolio-info">
              <h4>Card 3</h4>
              <p>Card</p>
              <a href="front/img/portfolio2/portfolio-8.jpg" data-gall="portfolioGallery" class="venobox preview-link" title="Card 3"><i class="bx bx-plus"></i></a>
              <a href="portfolio-details.html" class="details-link" title="More Details"><i class="bx bx-link"></i></a>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 portfolio-item filter-web">
            <?= $this->Html->image("portfolio2/portfolio-9.jpg", ['alt' => '', 'class' => 'img-fluid']) ?>
            <div class="portfolio-info">
              <h4>Web 3</h4>
              <p>Web</p>
              <a href="front/img/portfolio2/portfolio-9.jpg" data-gall="portfolioGallery" class="venobox preview-link" title="Web 3"><i class="bx bx-plus"></i></a>
              <a href="portfolio-details.html" class="details-link" title="More Details"><i class="bx bx-link"></i></a>
            </div>
          </div>

          

        </div>

      </div>
    </section><!-- End Portfolio Section -->

    <!-- ======= Team Section ======= -->
    <section id="team" class="team section-bg">
      <div class="container" data-aos="fade-up">

        <div class="section-title">
          <h3>GALERIE PROJETS <span>ESS</span></h3>
          <!-- <p>Ut possimus qui ut temporibus culpa velit eveniet modi omnis est adipisci expedita at voluptas atque vitae autem.</p> -->
        </div>

        <div class="row">

          <div class="col-lg-3 col-md-6 d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="100">
            <div class="member">
              <div class="member-img">
              
                <?= $this->Html->image("portfolio2/portfolio-7.jpg", ['alt' => '', 'class' => 'img-fluid']) ?>
                <!-- <div class="social">
                  <a href=""><i class="icofont-twitter"></i></a>
                  <a href=""><i class="icofont-facebook"></i></a>
                  <a href=""><i class="icofont-instagram"></i></a>
                  <a href=""><i class="icofont-linkedin"></i></a>
                </div> -->
              </div>
              <div class="member-info">
                <h4><a href="<?php echo $this->Url->build(array('controller' => 'Pages', 'action' => 'agriculture')); ?>">AGRICULTURE </a></h4>
                <!-- <span>Chief Executive Officer</span> -->
              </div>
            </div>
          </div>

          <div class="col-lg-3 col-md-6 d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="300">
            <div class="member">
              <div class="member-img">
              
                <!-- <?= $this->Html->image("team/team-3.jpg", ['alt' => '', 'class' => 'img-fluid']) ?> -->
                <?= $this->Html->image("portfolio2/portfolio-3.jpg", ['alt' => '', 'class' => 'img-fluid']) ?>
                <!-- <div class="social">
                  <a href=""><i class="icofont-twitter"></i></a>
                  <a href=""><i class="icofont-facebook"></i></a>
                  <a href=""><i class="icofont-instagram"></i></a>
                  <a href=""><i class="icofont-linkedin"></i></a>
                </div> -->
              </div>
              <div class="member-info">
                <h4><a href="<?php echo $this->Url->build(array('controller' => 'Pages', 'action' => 'elevage')); ?>">ELEVAGE</a></h4>
                <!-- <span>CTO</span> -->
              </div>
            </div>
          </div>

          <div class="col-lg-3 col-md-6 d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="200">
            <div class="member">
              <div class="member-img">
                <!-- <?= $this->Html->image("team/team-2.jpg", ['alt' => '', 'class' => 'img-fluid']) ?> -->
                <?= $this->Html->image("portfolio2/portfolio-6.jpg", ['alt' => '', 'class' => 'img-fluid']) ?>
                <!-- <div class="social">
                  <a href=""><i class="icofont-twitter"></i></a>
                  <a href=""><i class="icofont-facebook"></i></a>
                  <a href=""><i class="icofont-instagram"></i></a>
                  <a href=""><i class="icofont-linkedin"></i></a>
                </div> -->
              </div>
              <div class="member-info">
                <h4><a href="<?php echo $this->Url->build(array('controller' => 'Pages', 'action' => 'peche')); ?>">PÊCHE</a></h4>
                <!-- <span>Product Manager</span> -->
              </div>
            </div>
          </div>

          

          <div class="col-lg-3 col-md-6 d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="400">
            <div class="member">
              <div class="member-img">
                <?= $this->Html->image("portfolio2/portfolio-7.jpg", ['alt' => '', 'class' => 'img-fluid']) ?>
                <!-- <div class="social">
                  <a href=""><i class="icofont-twitter"></i></a>
                  <a href=""><i class="icofont-facebook"></i></a>
                  <a href=""><i class="icofont-instagram"></i></a>
                  <a href=""><i class="icofont-linkedin"></i></a>
                </div> -->
              </div>
              <div class="member-info">
                <h4><a href="<?php echo $this->Url->build(array('controller' => 'Pages', 'action' => 'artisanat')); ?>">ARTISANAT</a></h4>
                <!-- <span>Accountant</span> -->
              </div>
            </div>
          </div>

        </div>

      </div>
    </section><!-- End Team Section -->

    <!-- ======= ECO-SYSTEME ESS Section ======= -->
    <section id="pricing" class="pricing">
      <div class="container" data-aos="fade-up">

        <div class="section-title">
          <h3>ECO-SYSTEME <span>ESS</span> 
          
          </h3>
          <!-- <p>Ut possimus qui ut temporibus culpa velit eveniet modi omnis est adipisci expedita at voluptas atque vitae autem.</p>
        -->
        </div>

        <div class="row">

          <div class="col-lg-3 col-md-6" data-aos="fade-up" data-aos-delay="100">
            <div class="box">
              <h3>Cadre juridique </h3>
              <p>
              Au Sénégal, la Loi a été adoptée par l’Assemblée Nationale du Sénégal le 21/07/08 et par le Sénat le 22/08/08. Elle a été promulguée le 03 septembre 2008 (Loi n°2008-47) et son décret d’application...
              </p>
              <div class="btn-wrap">
                <a href="http://www.microfinance.sn/la-microfinance/cadre-legal-reglementaire-et-institutionnel/cadre-legal-et-reglementaire/loi-2008-47-regissant-les-sfd/" target="_blank" class="btn-buy">Plus d'Info...</a>
              </div>
            </div>
          </div>

          <div class="col-lg-3 col-md-6 mt-4 mt-md-0" data-aos="fade-up" data-aos-delay="200">
            <div class="box">  <!--featured -->
              <h3>La microfinance au Sénégal</h3>
              
              <p>
              Le secteur de la microfinance a connu un développement fulgurant au cours de la dernière décennie en rapport avec l’essor de la dynamique associative et la lutte contre la pauvreté. En décembre 2005...
              </p>
              <div class="btn-wrap">
                <a href="http://www.microfinance.sn/la-microfinance/la-microfinance-au-senegal/" target="_blank" class="btn-buy">Plus d'Info...</a>
              </div>
            </div>
          </div>

          <div class="col-lg-3 col-md-6 mt-4 mt-lg-0" data-aos="fade-up" data-aos-delay="300">
            <div class="box">
              <h3>Partenaires Techniques et Financiers </h3>
           <p>
           Regroupés au sein du sous-groupe thématique microfinance, les bailleurs de fonds (ou partenaires techniques et financiers) appuient fortement le secteur. L’essentiel de leurs actions s’inscrivent...
           </p>
              <div class="btn-wrap">
                <a href="http://www.microfinance.sn/cat/avis/emploi/"  target="_blank" class="btn-buy">Plus d'Info...</a>
              </div>
            </div>
          </div>

          <div class="col-lg-3 col-md-6 mt-4 mt-lg-0" data-aos="fade-up" data-aos-delay="400">
            <div class="box">
              <!-- <span class="advanced">Advanced</span> -->
              <h3>Evolution du Secteur</h3>
              <p>
              La progression continue du nombre de membres & clients (+1,3 %) des SFD qui passe à 3 395 925 comptes , soit un taux de pénétration de la population totale de 17,03% au 30 Semptembre 2019. <br> Les usagers...
              </p>
              <div class="btn-wrap">
                <a href="http://www.microfinance.sn/la-microfinance/evolution-du-secteur/" target="_blank" class="btn-buy">Plus d'Info...</a>
              </div>
            </div>
          </div> 
        </div>

      </div>
    </section><!-- End ECO-SYSTEME ESS Section -->

    <!-- ======= Frequently Asked Questions Section ======= -->
    <section id="faq" class="faq section-bg">
      <div class="container" data-aos="fade-up">

        <div class="section-title">
          <h3>Foire Aux <span>Questions</span></h3>
        </div>
        <div>
        <div class="section-title">
        <a href="<?php echo $this->Url->build(array('controller' => 'Pages', 'action' => 'faq')); ?>"
        class="btn btn-get-started scrollto" style ="background-color:#672211;color:#fff">
             <h3>Consulter</h3> 
        </a>
        </div>
       </div>

      </div>
    </section><!-- End Frequently Asked Questions Section -->

    <!-- ======= Contact Section ======= -->
    <section id="contact" class="contact">
      <div class="container" data-aos="fade-up">

        <div class="section-title">
          <h3><span>Contacter nous</span></h3>
        </div>

        <div class="row" data-aos="fade-up" data-aos-delay="100">
          <div class="col-lg-6">
            <div class="info-box mb-4">
              <i class="bx bx-map"></i>
              <h3>Notre Adresse</h3>
              <p>Sphère ministérielle Ousmane Tanor Dieng</p>
              <p>1er Arrondissement </p>
              <p>Pole urbain de Diamniadio</p>
            </div>
          </div>

          <div class="col-lg-3 col-md-6">
            <div class="info-box  mb-4">
              <i class="bx bx-envelope"></i>

              <h3>Email </h3>
              <p>contact@example.com</p>
              <br>
              <p>info@example.com</p>
            </div>
          </div>

          <div class="col-lg-3 col-md-6">
            <div class="info-box mb-4">
              <i class="bx bx-phone-call"></i>
              <h3>Téléphone</h3>
              <p>+221 33 860 26 52</p><br>
              <p>+221 33 860 59 71</p>

            </div>
          </div>

        </div>

        <div class="row" data-aos="fade-up" data-aos-delay="100">

          <div class="col-lg-6 ">
           <iframe class="mb-4 mb-lg-0" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3770.356019313571!2d-17.198654225714904!3d14.741763604615059!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xd5e4e3b5b26c8784!2sSph%C3%A8re%20Minist%C3%A9rielle%20Ousmane%20Tanor%20Dieng!5e0!3m2!1sfr!2ssn!4v1601558868301!5m2!1sfr!2ssn" frameborder="0" style="border:0; width: 100%; height: 384px;" allowfullscreen></iframe>
          </div>

          <div class="col-lg-6">
            <form action="forms/contact.php" method="post" role="form" class="php-email-form">
              <div class="form-row">
                <div class="col form-group">
                  <input type="text" name="name" class="form-control" id="name" placeholder="Votre Nom" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                  <div class="validate"></div>
                </div>
                <div class="col form-group">
                  <input type="email" class="form-control" name="email" id="email" placeholder="Votre Email" data-rule="email" data-msg="Please enter a valid email" />
                  <div class="validate"></div>
                </div>
              </div>
              <div class="form-group">
                <input type="text" class="form-control" name="subject" id="subject" placeholder="Objet" data-rule="minlen:4" data-msg="Please enter at least 8 chars of subject" />

                <div class="validate"></div>
              </div>
              <div class="form-group">
                <textarea class="form-control" name="message" rows="5" data-rule="required" data-msg="Please write something for us" placeholder="Message"></textarea>
                <div class="validate"></div>
              </div>
              <div class="mb-3">
                <div class="loading">Chargement</div>
                <div class="error-message"></div>
                <div class="sent-message">Votre message a ete envoyé. Merci!</div>
              </div>
              <div class="text-center"><button type="submit">Envoyer Message</button></div>
            </form>
          </div>

        </div>

      </div>
    </section><!-- End Contact Section -->

  </main><!-- End #main -->

  <!-- JQuery -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
