<main id="main">
    <!-- ======= Frequently Asked Questions Section ======= -->
    <section id="faq" class="faq section-bg" style="margin-top:6%">
      <div class="container" data-aos="fade-up">

        <div class="section-title">
          <h3>Role & <span>Responsabilité</span></h3>
          </div>

        <ul class="faq-list" data-aos="fade-up" data-aos-delay="100">

          <li>
            <a data-toggle="collapse" class="" href="#faq1"> Ministère de ESS <i class="icofont-simple-up"></i></a>
            <div id="faq1" class="collapse show" data-parent=".faq-list">
              <p>
                -	 Met en œuvre de la politique nationale de promotion et de développement de l’économie sociale et solidaire. <br>
                -	Elabore la stratégie nationale de promotion et de développement de l’économie sociale et solidaire.<br>
                -	Promeut et développe l’économie sociale et solidaire <br>
                -	Facilite l’accès au financement aux acteurs de l’E.S.S. <br>
                -	Développe des mesures fiscales de soutien aux acteurs de l’E.S.S. <br>
                -	Développe des mesures sociales de soutien aux acteurs de l’E.S.S. <br>
                -	Veille à la disponibilité de données statistique pour mesurer l’évolution de l’E.S.S. <br>

              </p>
            </div>
          </li>

          <li>
            <a data-toggle="collapse" href="#faq2" class="collapsed">Ministère du travail, Ministère de la famille & Ministère finances <i class="icofont-simple-up"></i></a>
            <div id="faq2" class="collapse" data-parent=".faq-list">
              <p>
              Chaque département ministériel assure la promotion et le développement de l’économie sociale et solidaire, pour les acteurs de son secteur, dans le cadre de la stratégie nationale.
              </p>
            </div>
          </li>

          <!-- <li>
            <a data-toggle="collapse" href="#faq3" class="collapsed">Dolor sit amet consectetur adipiscing elit pellentesque habitant morbi? <i class="icofont-simple-up"></i></a>
            <div id="faq3" class="collapse" data-parent=".faq-list">
              <p>
                Eleifend mi in nulla posuere sollicitudin aliquam ultrices sagittis orci. Faucibus pulvinar elementum integer enim. Sem nulla pharetra diam sit amet nisl suscipit. Rutrum tellus pellentesque eu tincidunt. Lectus urna duis convallis convallis tellus. Urna molestie at elementum eu facilisis sed odio morbi quis
              </p>
            </div>
          </li> -->

          <li>
            <a data-toggle="collapse" href="#faq4" class="collapsed">Autorité administrative (préfet/maire)<i class="icofont-simple-up"></i></a>
            <div id="faq4" class="collapse" data-parent=".faq-list">
              <p>
                -	La coordination de la déconcentration des mécanismes de suivi de la politique de l’E.S.S. <br>
                -	La tenue des registres de l’économie sociale et solidaire <br>
                -	L’identification des acteurs de l’économie populaire  <br>
                -   Fédération nationale de la mutualité Senegalaise (FNMS) <br>
              </p>
            </div>
          </li>

          <li>
            <a data-toggle="collapse" href="#faq5" class="collapsed">Conseils régionaux, Conseils départementaux, Intercommunalités et Communes<i class="icofont-simple-up"></i></a>
            <div id="faq5" class="collapse" data-parent=".faq-list">
              <p>
            -	La planification locale de la promotion de l’E.S.S. <br>
            -	La participation au financement des acteurs de l’E.S.S. <br>
            -	La participation des associations fédératives de Collectivités territoriales <br>

              </p>
            </div>
          </li>

          <li>
            <a data-toggle="collapse" href="#faq6" class="collapsed">Partenaires techniques et financiers, Société civile, représentants des Chambres de Métiers & Conseil économique, social et environnemental  <i class="icofont-simple-up"></i></a>
            <div id="faq6" class="collapse" data-parent=".faq-list">
              <p>
            -	La contribution au développement de l’E.S.S. <br>
            -	La participation aux dispositions facilitant l’accès au financement aux acteurs de l’E.S.S. <br>
            -	L’intégration dans leurs stratégies d’intervention, des mesures spécifiques destinées aux acteurs de l’économie sociale et solidaire. <br>
        </p>
            </div>
          </li>


        </ul>

      </div>
    </section><!-- End Frequently Asked Questions Section -->
   </main> 