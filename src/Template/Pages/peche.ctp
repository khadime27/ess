<main id="main">
<section id="cness" class="cness section-bg">
      <div class="container" data-aos="fade-up">

        <div class="section-title">
          <h2>Peche</h2>
          <h3>Nos projets de Peche dans l'<span>Economie Sociale et Solidaire</span></h3>
          </div>

          <div class="row">
                    <div class="col-4 col-12-mobile">
                      <article class="item">
                        <a href="#" class="image fit">
                        <?= $this->Html->image("Images/pic02.jpg", ['alt' => '', 'class' => 'image fit']) ?></a>
                        <header>
                          <h3>Kayar</h3>
                        </header>
                      </article>
                      <article class="item">
                        <a href="#" class="image fit">
                        <?= $this->Html->image("Images/pic03.jpg", ['alt' => '', 'class' => 'image fit']) ?></a>
                        <header>
                          <h3>Soumbedioune</h3>
                        </header>
                      </article>
                    </div>
                    <div class="col-4 col-12-mobile">
                      <article class="item">
                        <a href="#" class="image fit">
                        <?= $this->Html->image("Images/pic04.jpg", ['alt' => '', 'class' => 'image fit']) ?></a>
                        <header>
                          <h3>Thiaroye</h3>
                        </header>
                      </article>
                      <article class="item">
                        <a href="#" class="image fit">
                        <?= $this->Html->image("Images/pic05.jpg", ['alt' => '', 'class' => 'image fit']) ?></a>
                        <header>
                          <h3>Mbour</h3>
                        </header>
                      </article>
                    </div>
                    <div class="col-4 col-12-mobile">
                      <article class="item">
                        <a href="#" class="image fit"><?= $this->Html->image("Images/pic06.jpg", ['alt' => '', 'class' => 'image fit']) ?></a>
                        <header>
                          <h3>St Louis</h3>
                        </header>
                      </article>
                      <article class="item">
                        <a href="#" class="image fit"><?= $this->Html->image("Images/pic07.jpg", ['alt' => '', 'class' => 'image fit']) ?></a>
                        <header>
                          <h3>Bargnie</h3>
                        </header>
                      </article>
                    </div>
                  </div>

                </div>
              </div>
    </section>

</main>