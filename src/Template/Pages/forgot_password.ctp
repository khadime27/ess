<?php
use Cake\Cache\Cache;
use Cake\Core\Configure;
use Cake\Core\Plugin;
use Cake\Datasource\ConnectionManager;
use Cake\Error\Debugger;
use Cake\Http\Exception\NotFoundException;

$this->layout = 'default';

?>

<div class="row" style="margin-top: 30px;">
    <!-- Default form login -->
    <div class="col-md-4 offset-md-4 col-xs-12">
        <div class="users text-center border border-light p-5">
            <div id="form">

                <p class="h4 mb-4">Réinitialisation du mot de passe</p>

                <!-- Email -->
                <input type="email" name="email" id="email" class="form-control mb-4" placeholder="E-mail">

                <!-- Sign in button -->
                <button id="submit" class="btn btn-info btn-block my-4">Envoyer</button>

                <div class="d-flex justify-content-around">
                    <div>
                        <a href="<?php echo $this->Url->build(array('controller' => 'users', 'action' => 'connecter')); ?>">Se connecter</a>
                    </div>
                </div><br/>

                <!-- Register -->
                <p>Pas de compte ?
                    <a href="<?php echo $this->Url->build(array('controller' => 'users', 'action' => 'add')); ?>">S'inscrir ici</a>
                </p>

            </div>
            <div id="rep"></div>
        </div>
    <!-- Default form login -->
    </div>
</div>


<?= $this->Html->script('jquery3.3.1/jquery') ?>
<script>
    $("#submit").click(function(){
        var email = $("#email").val();
        if(email == '') {
            alert("Merci de renseigner votre email !")
            return;
        }
        <?php $url=$this->Url->build(['controller' => 'Users', 'action'=>'resetPassword']); ?>
        $.ajax({
            type:"GET",
            url:"<?= $url; ?>/"+email,
            //async: false,
            success : function(data, statut) {
            //success: function(data) {
                console.log(data);
                var rep = JSON.parse(data);
                if(rep=='ok'){
                    $("#form").hide();
                    $('#rep').html('<div class="card card-success"><div class="card-header">Mot de passe réinitialisé avec succès. Un mail vous a été envoyé.</div></div>');
                    $("#rep").append("<br/><a href='<?php echo $this->Url->build(array('controller' => 'users', 'action' => 'connecter')); ?>' class='btn btn-info'>Retour</a>");
                }else{
                    $('#rep').html('<div class="card card-danger"><div class="card-header">E-mail non identifié. Merci de réessayer.</div></div>');
                }
            },
            error : function(resultat, statut, erreur){
            //error: function(){
            },
            complete: function(resultat, statut){
                //$('.loader-container').hide();
            }
        });
        $("body").click(function(){
            $('.message').hide();
        });

    });

</script>