<main id="main">
    <!-- ======= Frequently Asked Questions Section ======= -->
    <section id="faq" class="faq section-bg" style="margin-top:6%">
      <div class="container" data-aos="fade-up">

        <div class="section-title">
          <h2>F.A.Q</h2>
          <h3>Foire Aux <span>Questions</span></h3>
          </div>

        <ul class="faq-list" data-aos="fade-up" data-aos-delay="100">

          <li>
            <a data-toggle="collapse" class="" href="#faq1">Quels sont les principes de l'économie sociale et solidaire ? <i class="icofont-simple-up"></i></a>
            <div id="faq1" class="collapse show" data-parent=".faq-list">
              <p>
              Le concept d'économie sociale et solidaire (ESS) désigne un ensemble d'entreprises organisées sous forme de coopératives, mutuelles, associations, ou fondations, dont le fonctionnement interne et les activités sont fondés sur un principe de solidarité et d'utilité sociale.
              </p>
            </div>
          </li>

          <li>
            <a data-toggle="collapse" href="#faq2" class="collapsed">Quelles sont les valeurs de l'ESS ? <i class="icofont-simple-up"></i></a>
            <div id="faq2" class="collapse" data-parent=".faq-list">
              <p>
              La lucrativité limitée : constitution de fonds propres impartageables, la majeure partie des excédents est non redistribuable ; La gestion autonome et indépendante des pouvoirs publics, mais la coopération y est développée ; Les principes de solidarité et de responsabilité guident la mise en place des actions.
              </p>
            </div>
          </li>

          <!-- <li>
            <a data-toggle="collapse" href="#faq3" class="collapsed">Dolor sit amet consectetur adipiscing elit pellentesque habitant morbi? <i class="icofont-simple-up"></i></a>
            <div id="faq3" class="collapse" data-parent=".faq-list">
              <p>
                Eleifend mi in nulla posuere sollicitudin aliquam ultrices sagittis orci. Faucibus pulvinar elementum integer enim. Sem nulla pharetra diam sit amet nisl suscipit. Rutrum tellus pellentesque eu tincidunt. Lectus urna duis convallis convallis tellus. Urna molestie at elementum eu facilisis sed odio morbi quis
              </p>
            </div>
          </li> -->

          <li>
            <a data-toggle="collapse" href="#faq4" class="collapsed">Qui sont les acteurs de l'économie sociale et solidaire ? <i class="icofont-simple-up"></i></a>
            <div id="faq4" class="collapse" data-parent=".faq-list">
              <p>
              <strong> Les acteurs de l'Économie Sociale et Solidaire (ESS) <br /></strong>
                > Le Conseil national des chambres régionales de l'économie sociale (CNCRES) <br />
                > Le Mouvement associatif. <br />
                > Coop Sn (coopératives) <br />
                > Le Mouvement des entrepreneurs sociaux (Mouves) <br />
                > La Fédération nationale de la mutualité Senegalaise (FNMS)
              </p>
            </div>
          </li>

          <li>
            <a data-toggle="collapse" href="#faq5" class="collapsed">Quels sont les grands principes de l'économie sociale ? <i class="icofont-simple-up"></i></a>
            <div id="faq5" class="collapse" data-parent=".faq-list">
              <p>
              L'économie sociale et solidaire s'articule autour de trois grands principes révélateurs de la philosophe et de la vision du monde qu'il prône : le volontariat, la démocratie, ma primauté de l'homme sur le capital. ... C'est en ce sens que l'on peut parler de "démocratie" dans l'entreprise.
              </p>
            </div>
          </li>

          <li>
            <a data-toggle="collapse" href="#faq6" class="collapsed">Qu'est-ce qui constitue les particularités des organisations de l'économie sociale ? <i class="icofont-simple-up"></i></a>
            <div id="faq6" class="collapse" data-parent=".faq-list">
              <p>
              Les organisations de l'économie sociale adhèrent à des principes fondateurs, parmi lesquels3 : la recherche d'une utilité collective, la non-lucrativité ou la lucrativité limitée (bénéfices réinvestis au service du projet collectif), une gouvernance démocratique (primauté des personnes sur le capital : « 1 personne = 1 voix », implication des parties prenantes)
              </p>
            </div>
          </li>

          <li>
            <a data-toggle="collapse" href="#faq7" class="collapsed">Quel est le but de l'ESS ? <i class="icofont-simple-up"></i></a>
            <div id="faq7" class="collapse" data-parent=".faq-list">
              <p>
              L'Économie Sociale et Solidaire (ESS) est une économie dont la finalité sociale est fondamentale. L'entreprise va utiliser ses bénéfices au service d'une cause. Son objectif premier n'est pas lucratif. Elle porte un projet collectif le plus souvent à finalité sociale et/ou environnementale.
              </p>
            </div>
          </li>

          <li>
            <a data-toggle="collapse" href="#faq8" class="collapsed">Quelles sont les structures juridiques spécifiques des entreprises de l'économie sociale et solidaire ? <i class="icofont-simple-up"></i></a>
            <div id="faq8" class="collapse" data-parent=".faq-list">
              <p>
              L'économie sociale regroupe principalement trois familles : mutuelles, coopératives, associations auxquelles s'ajoutent les "marges" (comité d'entreprise et d'établissement, caisse d'épargne et de prévoyance, organisations culturelles, etc.).
              </p>
            </div>
          </li>

          
          <li>
            <a data-toggle="collapse" href="#faq9" class="collapsed">Une entreprise informelle peut elle être une actrice ESS? <i class="icofont-simple-up"></i></a>
            <div id="faq9" class="collapse" data-parent=".faq-list">
              <p>
              Tout  citoyen est potentiellement un acteur de  l'ESS. Il suffit de s'inscrire dans la plateforme ess-senegal et se laisser guider. Vous trouverez des agents à votre écoute pour vous aider à concrétiser votre rêve d'être un citoyen économiquement et socialement engagé.
              </p>
            </div>
          </li>

        </ul>

      </div>
    </section><!-- End Frequently Asked Questions Section -->
   </main> 