    <!-- Main -->
<div id="main">

    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
      <ol class="carousel-indicators">
        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
      </ol>
      <div class="carousel-inner">
        <div class="carousel-item active">
          <!-- ======= Hero Section ======= -->
          <section class="one d-flex align-items-center testimonial-item" id="hero"
          style="width: 100%;
            height: 75vh;
            background: url('<?php echo $this->request->webroot; ?>img/real-bg.jpg') top left;
            background-size: cover;
            position: relative;">
            <div class="container" data-aos="zoom-out" data-aos-delay="100">
              <h1>Bienvenue à <span style="color:#04135E;">ESS</span>
              </h1>
              <h2>Formation Accompagnement Innovations dans les Terroirs du Sénégal <strong>(FAITS)</strong> </h2>
              <div class="d-flex">
                <a href="#about" class="btn-get-started scrollto" style="background-color:#04135E;" class="button scrolly">ESS c’est quoi</a>
                <a href="https://youtu.be/oQQySulvkm8?t=15" class="venobox btn-watch-video"
                data-vbtype="video" data-autoplay="true"> Mot du ministre <i class="icofont-play-alt-2" style="color:#04135E;"></i></a>

                <a href="https://youtu.be/GhydFP3Ccsc?t=109" class="venobox btn-watch-video"
                data-vbtype="video" data-autoplay="true"style=" margin-left: 25%"> 
                Mot du ministre (Wolof)<i class="icofont-play-alt-2" style="color:#04135E"></i></a>
              </div>
            </div>
          </section><!-- End Hero -->
        </div>
        <div class="carousel-item">
          <!-- ======= bg-1 Section ======= -->
      <section  class="one d-flex align-items-center testimonial-item" id="bg-1"
      style="width: 100%;
      height: 75vh;
      background: url('<?php echo $this->request->webroot; ?>img/real-bg1.jpg') top left;
      background-size: cover;
      position: relative;">

        <div class="container" data-aos="zoom-out" data-aos-delay="100">
        <h1>SUNU LABEL <span >E.S.S</span>
          </h1>
          <h2>Formation Accompagnement Innovations dans les Terroirs du Sénégal <strong>(FAITS)</strong> </h2>
          <div class="d-flex">
            <a href="#about" class="btn-get-started scrollto" class="button scrolly">ESS c’est quoi</a>
            <a href="https://youtu.be/oQQySulvkm8?t=15" class="venobox btn-watch-video"
            data-vbtype="video" data-autoplay="true"> Mot du ministre <i class="icofont-play-alt-2"></i></a>

            <a href="https://youtu.be/GhydFP3Ccsc?t=109" class="venobox btn-watch-video"
                data-vbtype="video" data-autoplay="true"style=" margin-left: 25%"> 
                Mot du ministre (Wolof)<i class="icofont-play-alt-2" style="color:#04135E"></i></a>
          </div>
        </div>
      </section><!-- End bg-1 -->
        </div>

      </div>
      <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Precedent</span>
      </a>
      <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Suivant</span>
      </a>
    </div> 


    <section id="top" class="two about section-bg">
      <div class="container" data-aos="fade-up">

      <header>
        <h2><h3>GALERIE PROJETS <span>ESS</span></h3></h2>
      </header>

          <div class="row">
            <?php foreach($filieres as $filiere): ?>
            <div class="col-sm-6">
              <article class="item">
                  <a href="<?php echo $this->Url->build(array('controller' => 'Dossiers', 'action' => 'projectBySecteur', $filiere->id)); ?>" class="image fit">
                  <?= $this->Html->image("filieres/".$filiere->photo, ['alt' => h($filiere->nom), 'class' => 'img-fluid']) ?></a>
                  <header>
                  <h3 class="text-uppercase"><?= $filiere->nom ?></h3>
                  </header>
              </article>
            </div>
            <?php endforeach; ?>

          </div>

      </div>
    </section>


    <!-- ======= Counts Section ======= -->
    <section id="counts" class="counts">
      <div class="container" data-aos="fade-up">
      <header>
        <h2><h3>NOS PERFORMANCES</h3></h2>
      </header>
        <div class="row">

          <div class="col-lg-3 col-md-6">
            <div class="count-box">
              <i class="icofont-simple-smile"></i>
              <span data-toggle="counter-up"><?= $nbEmployeTotal ?></span>
              <p>Acteurs</p>
            </div>
          </div>

          <div class="col-lg-3 col-md-6 mt-5 mt-md-0">
            <div class="count-box">
              <i class="icofont-document-folder"></i>
              <span data-toggle="counter-up"><?= $nbProjet ?></span>
              <p>Projets</p>
            </div>
          </div>


          <div class="col-lg-3 col-md-6 mt-5 mt-lg-0">
            <div class="count-box">
              <i class="icofont-users-alt-5"></i>
              <span data-toggle="counter-up"><?= $nbDossiersLabelises ?></span>
              <p>Labellisés ESS</p>
            </div>
          </div>

          <div class="col-lg-3 col-md-6 mt-5 mt-lg-0">
            <div class="count-box">
              <i class="icofont-live-support"></i>
              <span data-toggle="counter-up"><?= $nbDossiersFinances ?></span>
              <p>Financement</p>
            </div>
          </div>

        </div>

      </div>
    </section><!-- End Counts Section -->

    <!-- ======= Skills Section ======= -->
    <section id="skills" class="skills">
      <div class="container" data-aos="fade-up">
      <header>
        <h2><h3>LES RESULTATS DES ACTEURS DE L'<span>ESS</span></h3></h2>
      </header>
        <div class="row skills-content">

          <div class="col-lg-6">

            <div class="progress">
              <span class="skill">GIE <i class="val">85%</i></span>
              <div class="progress-bar-wrap">
                <div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="85"></div>
              </div>
            </div>

            <div class="progress">
              <span class="skill">Groupement de femme <i class="val">90%</i></span>
              <div class="progress-bar-wrap">
                <div class="progress-bar" role="progressbar" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100"></div>
              </div>
            </div>

            <div class="progress">
              <span class="skill">Association <i class="val">75%</i></span>
              <div class="progress-bar-wrap">
                <div class="progress-bar" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
              </div>
            </div>

          </div>

          <div class="col-lg-6">

            <div class="progress">
              <span class="skill">Entreprenariat <i class="val">80%</i></span>
              <div class="progress-bar-wrap">
                <div class="progress-bar" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"></div>
              </div>
            </div>

            <div class="progress">
              <span class="skill">PME <i class="val">90%</i></span>
              <div class="progress-bar-wrap">
                <div class="progress-bar" role="progressbar" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100"></div>
              </div>
            </div>

            <div class="progress">
              <span class="skill">Micro Credit <i class="val">55%</i></span>
              <div class="progress-bar-wrap">
                <div class="progress-bar" role="progressbar" aria-valuenow="55" aria-valuemin="0" aria-valuemax="100"></div>
              </div>
            </div>

          </div>

        </div>

      </div>
    </section><!-- End Skills Section -->


    <!-- ======= Testimonials Section ======= -->
    <section id="about" class="three testimonials">
      <div class="container" data-aos="zoom-in">
      <header>
        <h2><h3>UNE IDEE SUR L'<span>ESS ?</span></h3></h2>
      </header>

        <div class="owl-carousel testimonials-carousel">
          
        <div class="testimonial-item">
          <?= $this->Html->image("testimonials/testimonials-3.jpg", ['alt' => '', 'class' => 'testimonial-img']) ?>
            
            <h3>Kofi Annan</h3>
            <h4>SG de l'ONU</h4>
            <p>
              <i class="bx bxs-quote-alt-left quote-icon-left"></i>
              Les maux économiques et sociaux dont notre monde est affligé ne sont que trop réels – tout comme la nécessité de faire en sorte que la mondialisation profite à tous les peuples, en ancrant la nouvelle économie mondiale dans une société mondiale qui repose sur des valeurs globales communes de solidarité, de justice sociale et de respect des droits de l'homme
              <i class="bx bxs-quote-alt-right quote-icon-right"></i>
            </p>
          </div>

          <div class="testimonial-item">
            
            <?= $this->Html->image("testimonials/testimonials-1.jpg", ['alt' => '', 'class' => 'testimonial-img']) ?>
            <h3>Saul Goodman</h3>
            <h4>Ceo &amp; Founder</h4>
            <p>
              <i class="bx bxs-quote-alt-left quote-icon-left"></i>
              Le domaine de l’ESS est une branche très vaste et assez particulière. Il a une vocation d’utilité sociale. Il requiert une polyvalence dans le sens où il associe l’activité d’entreprise et le domaine économique. Il s’agit également d’un secteur en perpétuelle mobilité et où les acteurs sont très diversifiés (mutuelles, associations, coopératives, fondations...). 
              <i class="bx bxs-quote-alt-right quote-icon-right"></i>
            </p>
          </div>

          <div class="testimonial-item">
          <?= $this->Html->image("testimonials/testimonials-2.jpg", ['alt' => '', 'class' => 'testimonial-img']) ?>
            
            <h3>Mme Zahra Iyane Thiam</h3>
            <h4>Ministre ESS</h4>
            <p>
              <i class="bx bxs-quote-alt-left quote-icon-left"></i>
              En économie sociale et solidaire, entreprendre est une forme d'action politique. Il s'agit de transformer le monde.
              <i class="bx bxs-quote-alt-right quote-icon-right"></i>
            </p>
          </div>

          <div class="testimonial-item">
          <?= $this->Html->image("testimonials/testimonials-4.jpg", ['alt' => '', 'class' => 'testimonial-img']) ?>
            
            <h3>Matt Brandon</h3>
            <h4>Freelancer</h4>
            <p>
              <i class="bx bxs-quote-alt-left quote-icon-left"></i>
              Face aux excès et à la violence dangereuse de la financiarisation d'une économie mondialisée, l'économie sociale prend tout son sens
              <i class="bx bxs-quote-alt-right quote-icon-right"></i>
            </p>
          </div>

          <div class="testimonial-item">
          <?= $this->Html->image("testimonials/testimonials-5.jpg", ['alt' => '', 'class' => 'testimonial-img']) ?>
          <h3>Mireille Bertrand Lhérisson</h3>
            <h4>Entrepreneure Sociale, Poète Engagée</h4>
            <p>
              <i class="bx bxs-quote-alt-left quote-icon-left"></i>
              L'économie sociale et solidaire ESS, une économie morale, une solution aux défis posés par les crises économique, sociale et environnementale.
              <i class="bx bxs-quote-alt-right quote-icon-right"></i>
            </p>
          </div>

        </div>

      </div>

    </section>
    <!-- End Testimonials Section -->

    <!-- ======= Frequently Asked Questions Section ======= -->
    <section id="faq" class="faq section-bg">
          <div class="container" data-aos="zoom-in">
          <header>
              <h2><h3>NOS PARTENAIRES <span></span></h3></h2>
            </header>
          <marquee>
          <div class="row clients section-bg">
          <div class="col-lg-2 col-md-4 col-6 d-flex align-items-center justify-content-center">
            <?= $this->Html->image("clients/mfb.png", ['alt' => '', 'class' => 'img-fluid']) ?>
          </div>

          <div class="col-lg-2 col-md-4 col-6 d-flex align-items-center justify-content-center">
            <?= $this->Html->image("clients/afd.jpg", ['alt' => '', 'class' => 'img-fluid']) ?>
          </div>

          <div class="col-lg-2 col-md-4 col-6 d-flex align-items-center justify-content-center">
            <?= $this->Html->image("clients/mepc.png" ,['alt' => '', 'class' => 'img-fluid']) ?>
          </div>

          <div class="col-lg-2 col-md-4 col-6 d-flex align-items-center justify-content-center">
            <?= $this->Html->image("clients/plasepri1.png", ['alt' => '', 'class' => 'img-fluid']) ?>
          </div>

          <div class="col-lg-2 col-md-4 col-6 d-flex align-items-center justify-content-center">
            <?= $this->Html->image("clients/ractes.png", ['alt' => '', 'class' => 'img-fluid']) ?>
          </div>

          <div class="col-lg-2 col-md-4 col-6 d-flex align-items-center justify-content-center">
            <?= $this->Html->image("clients/promise1.jpg", ['alt' => '', 'class' => 'img-fluid']) ?>
          </div>
          </div>
          </marquee>

          </div>
            <div class="container" data-aos="fade-up">
            <header>
              <h2>Foire Aux <span>Questions</span></h2>
            </header>
            
              <div>
              <div class="section-title">
              
              <a href="<?php echo $this->Url->build(array('controller' => 'Pages', 'action' => 'faq')); ?>" class="button scrolly"
              style="background-color:#04135E;"> Consulter   <i class="fas fa-arrow-circle-right"></i></a>
              </div>
            </div>

          </div>
    </section><!-- End Frequently Asked Questions Section -->

        <!-- Contact -->          
    <section id="contact" class="four contact">
      <div class="container" data-aos="fade-up">
      
        <div class="section-title">
          <h3><span>Contacter nous</span></h3>
        </div>

        <div class="row" data-aos="fade-up" data-aos-delay="100">
          <div class="col-lg-6">
            <div class="info-box mb-4">
              <i class="bx bx-map"></i>
              <h3>Notre Adresse</h3>
              <p>Sphère ministérielle Ousmane Tanor Dieng</p>
              <p>1er Arrondissement </p>
              <p>Pole urbain de Diamniadio</p>
            </div>
          </div>

          <div class="col-lg-3 col-md-6">
            <div class="info-box  mb-4">
              <i class="bx bx-envelope"></i>

              <h3>Email </h3>
              <p>contact@senlabeless.com</p>
              <br>
              <p>info@senlabeless.com</p>
            </div>
          </div>

          <div class="col-lg-3 col-md-6">
            <div class="info-box mb-4">
              <i class="bx bx-phone-call"></i>
              <h3>Téléphone</h3>
              <p>+221 33 860 26 52</p><br>
              <p>+221 33 860 59 71</p>

            </div>
          </div>

        </div>

        <div class="row" data-aos="fade-up" data-aos-delay="100">

          <div class="col-lg-6 ">
          <iframe class="mb-4 mb-lg-0" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3770.356019313571!2d-17.198654225714904!3d14.741763604615059!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xd5e4e3b5b26c8784!2sSph%C3%A8re%20Minist%C3%A9rielle%20Ousmane%20Tanor%20Dieng!5e0!3m2!1sfr!2ssn!4v1601558868301!5m2!1sfr!2ssn" frameborder="0" style="border:0; width: 100%; height: 384px;" allowfullscreen></iframe>
          </div>

          <div class="col-lg-6">
            <form action="forms/contact.php" method="post" role="form" class="php-email-form">
              <div class="form-row">
                <div class="col form-group">
                  <input type="text" name="name" class="form-control" id="name" placeholder="Votre Nom" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                  <div class="validate"></div>
                </div>
                <div class="col form-group">
                  <input type="email" class="form-control" name="email" id="email" placeholder="Votre Email" data-rule="email" data-msg="Please enter a valid email" />
                  <div class="validate"></div>
                </div>
              </div>
              <div class="form-group">
                <input type="text" class="form-control" name="subject" id="subject" placeholder="Objet" data-rule="minlen:4" data-msg="Please enter at least 8 chars of subject" />

                <div class="validate"></div>
              </div>
              <div class="form-group">
                <textarea class="form-control" name="message" rows="5" data-rule="required" data-msg="Please write something for us" placeholder="Message"></textarea>
                <div class="validate"></div>
              </div>
              <div class="mb-3">
                <div class="loading">Chargement</div>
                <div class="error-message"></div>
                <div class="sent-message">Votre message a ete envoyé. Merci!</div>
              </div>
              <div class="text-center"><button type="submit">Envoyer Message</button></div>
            </form>
          </div>

        </div>

      </div>
    </section><!-- End Contact Section -->

</div>
