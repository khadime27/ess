<?php $this->setLayout('back_office') ?>

<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            <div class="page-title-icon">
                <i class="pe-7s-compass icon-gradient bg-mean-fruit">
                </i>
            </div>
            <div class="page-title-subheading">
                Modification du profil
            </div>
        </div>   
    </div>
</div>            
<div class="container">
    <?= $this->Form->create('Users', array('enctype'=>'multipart/form-data', 'url'=>array('action'=> 'edit', $user->id))) ?>
        <div class="form-row mb-4">
            <div class="col">
                <label id="prenom">Prénom</label>
                <input type="text" name="prenom" id="defaultRegisterFormFirstName" class="form-control" placeholder="Prénom" required>
            </div>
            <div class="col">
                <label id="nom">Nom</label>
                <input type="text" name="nom" id="defaultRegisterFormLastName" class="form-control" placeholder="Nom" required>
            </div>
        </div>
        <div class="form-row mb-4">
            <div class="col">
                <label>Email</label>
                <input type="email" name="email" class="form-control" placeholder="Adresse email" required>
            </div>
            <div class="col">
                <label>Mot de passe</label>
                <input type="password" name="password" class="form-control" placeholder="Mot de passe" required>
            </div>
        </div>
        <div class="form-row mb-4">
            <div class="col">
                <label>Téléphone</label>
                <input type="text" name="telephone" placeholder="Numéro de téléphone" class="form-control">
            </div>

            <div class="col">
                <label>Photo de profile</label>
                <input type="file" name="photo" class="form-control mb-4" />
            </div>
        </div>
        <div class="form-row mb-4">
            <div class="col">
                <label>Date de naissance</label>
                <input type="date" name="date_naissance" class="form-control">
            </div>
            <div class="col">
                <label>Lieu de naissance</label>
                <input type="text" name="lieu_naissance" placeholder="Votre lieu de naissance" class="form-control">
            </div>
            <div class="col">
                <label>Lieu de résidence</label>
                <input type="text" name="lieu_residence" class="form-control" placeholder="Lieu de résidence">
            </div>
        </div>
        
    <?= $this->Form->end() ?>
</div>