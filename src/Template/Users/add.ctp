<style>
    label {
        font-size: 20px;
    }
    .card-register {
        border-radius: 2% !important;
        box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2) !important;
        transition: 0.3s !important;
    }
    .card-register:hover {
        box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2) !important;
    }
</style>
<div id="main">
<section id="featured-services" class="featured-services mb-150">
    <div class="container" data-aos="fade-up">

        <div class="row">
            <div class="col-md-6 col-lg-6 d-flex align-items-stretch mb-5 mb-lg-0">
                <div id="personne" class="icon-box cursor-pointer w-100" data-aos="fade-up" data-aos-delay="100">
                    <div class="icon"><i class="bx bx-male"></i></div>
                    <h4 class="title"><a href="">Entrepreneur(es)</a></h4>
                    <p class="description">Inscrivez-vous en tant que Acteur</p>
                </div>
            </div>

            <div class="col-md-6 col-lg-6 d-flex align-items-stretch mb-5 mb-lg-0">
                <div id="entreprise" class="icon-box cursor-pointer w-100" data-aos="fade-up" data-aos-delay="200">
                    <div class="icon"><i class="bx bx-building"></i></div>
                    <h4 class="title"><a href="">Société Coopérative et Participative (SCOP)</a></h4>
                    <p class="description">Vous êtes une entreprise ( SA, SARL ou SAS dont les salariés sont les associés majoritaires) ?</p>
                </div>
            </div>

            <div class="col-md-6 col-lg-6 d-flex align-items-stretch mb-5 mb-lg-0">
                <div id="entite" class="icon-box cursor-pointer w-100" data-aos="fade-up" data-aos-delay="300">
                    <div class="icon"><i class="bx bxs-institution"></i></div>
                    <h4 class="title"><a href="">Association</a></h4>
                    <p class="description">Vous êtes une association, groupement, GIE, fondation</p>
                </div>
            </div>
            <div class="col-md-6 col-lg-6 d-flex align-items-stretch mb-5 mb-lg-0">
                <div id="autre" class="icon-box cursor-pointer w-100" data-aos="fade-up" data-aos-delay="300">
                    <div class="icon"><i class="bx bx-group"></i></div>
                    <h4 class="title"><a href="">Autres</a></h4>
                    <p class="description">Vous êtes autres acteurs de l'ESS</p>
                </div>
            </div>
        </div>

    </div>
</section> 


<div id="form-register" class="row" style="margin-top: 100px">
    <div class="card card-register col-md-10 offset-md-1 col-12 p-5">
        <div class="card-header bg-white">
            <div class="col-sm-12">
                <h4 class="font-weight-bold color">Inscription
                    <button id="goBack" class="btn bg-color box-shadow-0 border-rad-0 btn-medium btn-block float-right ml-5 waves-effect waves-light" type="button"
                    style="background-color:#04135E;">
                        <i class="bx bx-arrow-back"></i> Retour                        
                    </button>
                </h4>
                
            </div>
        </div>
        <div class="card-body">
            <!-- Default form register -->
            <?= $this->Form->create('Users', array('enctype'=>'multipart/form-data', 'url'=>array('action'=> 'add'))) ?>

                <div class="form-row mb-4">
                    <div class="col entite-entreprise">
                        <label>Entité</label>
                        <input type="text" name="nomEntreprise" placeholder="Nom de l'entité" class="form-control">
                    </div>
                    <div class="col">
                        <label id="prenom">Prénom</label>
                        <input type="text" name="prenom" id="defaultRegisterFormFirstName" class="form-control" placeholder="Prénom" required>
                    </div>
                    <div class="col">
                        <label id="nom">Nom</label>
                        <input type="text" name="nom" id="defaultRegisterFormLastName" class="form-control" placeholder="Nom" required>
                    </div>
                </div>

                <div class="form-group mb-4 personne">
                    <label>Genre / Civilité</label>
                    <div class="custom-control custom-radio d-flex">
                        <input type="radio" class="custom-control-input sexe" value="homme" id="homme" checked name="sexe">
                        <label class="custom-control-label" for="homme">Homme</label>

                        <span class="ml-5">
                            <input type="radio" class="custom-control-input sexe" value="femme" id="femme" name="sexe">
                            <label class="custom-control-label" for="femme">Femme</label>
                        </span>
                        <input type="hidden" id="genre" name="genre">
                    </div>
                </div>
                
                <div class="form-row">
                    <div class="col">
                        <!-- E-mail -->
                        <input type="email" name="email" id="defaultRegisterFormEmail" class="form-control mb-4" placeholder="E-mail">
                    </div>
                    <div class="col">
                        <!-- Password -->
                        <input type="password" name="password" id="defaultRegisterFormPassword" class="form-control mb-4" placeholder="Mot de passe" aria-describedby="defaultRegisterFormPasswordHelpBlock">
                    </div>
                </div>
                <div class="form-row mb-4 personne">
                    <div class="col">
                        <label>Date de naissance</label>
                        <input type="date" name="date_naissance" max="<?= date('Y-m-d') ?>" class="form-control">
                    </div>
                    <div class="col">
                        <label>Lieu de naissance</label>
                        <input type="text" name="lieu_naissance" placeholder="Votre lieu de naissance" class="form-control">
                    </div>
                    <div class="col">
                        <label>Lieu de résidence</label>
                        <input type="text" name="lieu_residence" class="form-control" placeholder="Votre lieu de résidence">
                    </div>
                </div>

                <div class="form-row mb-4 entite-entreprise">
                    <div class="col">
                        <label>Date de création</label>
                        <input type="date" name="date_creation" class="form-control">
                    </div>
                    <div class="col">
                        <label>Nombre de personnes</label>
                        <input type="number" name="nb_employe" class="form-control" placeholder="Nombre">
                    </div>
                    <div class="col entite">
                        <label>Immatriculation</label>
                        <input type="text" name="immatriculation" class="form-control" placeholder="Numéro">
                    </div>
                    <div class="col entite-entreprise">
                        <div class="form-group mb-2">
                            <label>Statut juridique</label>
                            <select name="statut-juridique" id="statut-juridique" class="form-control">
                                <option value="">Choisissez le statut</option>
                                <option value="Formel">Formel</option>
                                <option value="Informel">Informel<option>
                            </select>
                        </div>
                    </div>

                </div>

                <div class="form-row mb-4">
                    <div class="col personne">
                        <div class="form-group mb-2">
                            <div class="custom-control custom-radio d-flex">
                                <input type="radio" class="custom-control-input identification" value="CNI" id="cni" checked name="identification">
                                <label class="custom-control-label" for="cni">Numéro CNI</label>

                                <span class="ml-5">
                                    <input type="radio" class="custom-control-input identification" value="passport" id="passport" name="identification">
                                    <label class="custom-control-label" for="passport">Passport</label>
                                </span>
                            </div>
                            <input type="hidden" id="type_identification" name="type_identification">
                        </div>
                        <input type="number" maxlength="13" name="num_identification" placeholder="Numéro d'identification" class="form-control">
                    </div>
                    <div class="col entite">
                        <div class="form-group mb-2">
                            <div class="custom-control custom-radio d-flex">
                                <input type="radio" class="custom-control-input" id="whatsapp" value="WhatsApp" checked name="type_social">
                                <label class="custom-control-label" for="whatsapp">WhatsApp</label>

                                <span class="ml-5">
                                    <input type="radio" class="custom-control-input" id="facebook" value="Facebook" name="type_social">
                                    <label class="custom-control-label" for="facebook">Facebook</label>
                                </span>
                            </div>
                        </div>
                        <input type="text" name="social_link" placeholder="Nom de domaine" class="form-control">
                    </div>
                    <div class="col entite-entreprise formel hide">
                        <label>Type de l'entité</label>
                        <select name="type_entreprise_id" class="form-control">
                            <option value="">Choisissez un type</option>
                            <?php foreach($typesEntreprises as $type): ?>
                                <option value="<?= $type->id ?>"><?= $type->nom ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>

                    <div class="col entite-entreprise formel hide">
                        <label>Ninea</label>
                        <input type="text" name="ninea" class="form-control" placeholder="Votre ninea">
                    </div>

                </div>
                
                <div class="form-row mb-4">
                    <div class="col">
                        <label>Téléphone</label>
                        <input type="text" name="telephone" placeholder="Numéro de téléphone" class="form-control">
                    </div>

                    <div class="col">
                        <label>Photo de profile</label>
                        <input type="file" name="photo" class="form-control mb-4" />
                    </div>
                </div>

                <div class="form-row mb-4 entite-entreprise">
                    <div class="col">
                        <label>Région</label>
                        <select id="region_id" class="form-control">
                            <option value="">Veuillez choisir la région</option>
                            <?php foreach($regions as $region): ?>
                                <option value="<?= $region->id ?>"><?= $region->nom ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="col">
                        <label for="name">Département</label>
                        <select type="text" id ="departement"  name="departement_id" class="form-control js-example-basic">
                            <option selected="selected" disabled required value="">Liste des départements</option>'
                        </select>
                    </div>
                    <div class="col commune">
                        <label>Commune</label>
                        <select name="communes_id" id="commun" class="form-control">
                            <option disabled value="">Liste des communes</option>
                        </select>
                    </div>
                </div>

                <div class="form-row entite-entreprise mb-4">
                    <div class="col">
                        <label>Nombre de salariés</label>
                        <input type="number" name="nb_salarie" class="form-control">
                    </div>
                    <div class="col">
                        <label>Nombre de stagiaires</label>
                        <input type="number" name="nb_stageaire" class="form-control">
                    </div>
                    <div class="col">
                        <label>Nombre de saisonniers</label>
                        <input type="number" name="nb_saisonnier" class="form-control">
                    </div>
                </div>
                <input type="hidden" id="type-entite" name="type-entite">

                <div class="row">
                    <div class="col-lg-12">
                        <button class="btn bg-color box-shadow-0 border-rad-0 btn-medium  btn-block  float-right ml-5 waves-effect waves-light" type="submit"
                        style="background-color:#04135E;">
                            Inscrire                        
                        </button>
                        <a href="<?php echo $this->Url->build(array('controller' => 'Users', 'action' => 'connecter')); ?>" class="float-right mt-1 text-black demi-opac float-left">J'ai déjà un compte</a>
                    </div>
                </div>

            <?= $this->Form->end() ?>
            <!-- Default form register -->
        </div>
    </div>
</div>
</div>

<?= $this->Html->script('front/vendor/jquery/jquery.min') ?>
<script>
    $(function() {

        $('#form-register').hide();

        var el = $('.col');
        
        if ($(window).width() < 723) {
            el.addClass('col-12');
            el.removeClass('col');
        }

        $('.cursor-pointer').click(function(){
            $('#featured-services').hide();
            $('#form-register').show(1000);

            if($(this).attr('id') == 'personne') {
                $('.personne').show();
                $('.entreprise').hide();
                $('.entite-entreprise').hide();
                $('.entite').hide();
                $('#genre').val('homme');
                $('#type_identification').val('CNI');
                $('#type-entite').val('');
            }else {
                $('.personne').hide();
            }

            if($(this).attr('id') == 'entreprise') {
                $('.entreprise').show();
                $('.entite-entreprise').show();
                $('.personne').hide();
                $('.entite').hide();
                $('#prenom').text('Prénom du responsable');
                $('#nom').text('Nom du responsable');
                $('#genre').val('');
                $('#type_identification').val('');
                $('#type-entite').val('entreprise');
            }else{
                $('.entreprise').hide();
            }

            if($(this).attr('id') == 'entite') {
                $('.entite').show();
                $('.entite-entreprise').show();
                $('.entreprise').hide();
                $('.personne').hide();
                $('#prenom').text('Prénom du responsable');
                $('#nom').text('Nom du responsable');
                $('#genre').val('');
                $('#type_identification').val('');
                $('#type-entite').val('association');
            }else{
                $('.entite').hide();
            }

        });

        $('#goBack').click(function(){
            $('#featured-services').show(1000)
            $('#form-register').hide();

        });

        $('.sexe').change(function(){
            $('#genre').val($(this).val());
        });
        $('.identification').change(function(){
            $('#type_identification').val($(this).val())
        });

        $("#region_id").change(function () {

            var region_id = $('#region_id option:selected').val();
            
            <?php $url = $this->Url->build(['controller' => 'Regions', 'action' => 'view']); ?>
            $.ajax(
                    {type: 'GET',
                        url: "<?= $url; ?>/" + region_id,
                        async: false,
                        success: function (json) {
                        var data = JSON.parse(json);
                        //alert(data);
                        var infos;
                        var listdept = [];
                        infos += '<option selected="selected" value=""> Veuillez choisir le département</option>';
                        for (var i in data) {

                            listdept['nom'] = data[i]['departementscol'];
                            infos += '<option required value="' + data[i]['id'] + '">' + listdept['nom'] + '</option>';
                        }
                        $("#departement").html(infos);
                        }
                    });
            });


        $("#departement").change(function () {
            var departement = $('#departement option:selected').val();
            // alert(departement);

            <?php $url1 = $this->Url->build(['controller' => 'Departements', 'action' => 'view']); ?>
                $.ajax(
                {type: 'GET',
                    url: "<?= $url1; ?>/" + departement,
                    async: false,
                    success: function (json) {
                    var data = JSON.parse(json);
                    
                    var infos;
                    var listcommun = [];
                    infos += '<option selected="selected" value=""> Veuillez choisir la commune</option>';
                    for (var i in data) {

                        listcommun['nom'] = data[i]['nom'];
                        infos += '<option required value="' + data[i]['id'] + '">' + listcommun['nom'] + '</option>';
                    }
                    $("#commun").html(infos);
                    }
                });
            });


        $("#departement").change(function () {
            $(".commune").show(1000);
        });

        $('#statut-juridique').change(function(){
            if($(this).val() == "Formel") {
                $('.formel').removeClass('hide');
            }else {
                $('.formel').addClass('hide');
            }
        });

    });
</script>