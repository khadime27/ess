<style>
    .card-login {
        border-radius: 5% !important;
        box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2) !important;
        transition: 0.3s !important;
    }
    .card-login:hover {
        box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2) !important;
    }
</style>
<div id="main">
<div class="row mt-5">
    <!-- Default form login -->
    <div class="col-md-6 offset-md-3 col-xs-12">
        <div class="card users text-center p-5 card-login">
            <?= $this->Form->create() ?>

                <h2 class="mb-4 color font-weight-bold">Connexion</h2>

                <?= $this->Flash->render() ?><br/>
                <!-- Email -->
                <input type="email" name="email" id="defaultLoginFormEmail" class="form-control mb-4" placeholder="E-mail">

                <!-- Password -->
                <input type="password" name="password" id="defaultLoginFormPassword" class="form-control mb-4" placeholder="Password">

                <div class="d-flex justify-content-around">
                    <div>
                        <!-- Remember me -->
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="defaultLoginFormRemember">
                            <label class="custom-control-label" for="defaultLoginFormRemember">Se souvenir de moi.</label>
                        </div>
                    </div>
                    <div>
                        <!-- Forgot password -->
                        <a href="<?php echo $this->Url->build(array('controller' => 'pages', 'action' => 'forgot_password')); ?>">Mot de passe oublié ?</a>
                    </div>
                </div>

                <!-- Sign in button -->
                <button class="btn bg-color btn-block my-4" type="submit">Connexion</button>

                <!-- Register -->
                <p>Pas de compte ?
                    <a href="<?php echo $this->Url->build(array('controller' => 'users', 'action' => 'add')); ?>">S'inscrire ici</a>
                </p>


            <?= $this->Form->end() ?>
        </div>
    <!-- Default form login -->
    </div>
</div>
</div>