<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\QualificationDossier $qualificationDossier
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Qualification Dossier'), ['action' => 'edit', $qualificationDossier->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Qualification Dossier'), ['action' => 'delete', $qualificationDossier->id], ['confirm' => __('Are you sure you want to delete # {0}?', $qualificationDossier->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Qualification Dossiers'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Qualification Dossier'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Dossiers'), ['controller' => 'Dossiers', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Dossier'), ['controller' => 'Dossiers', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="qualificationDossiers view large-9 medium-8 columns content">
    <h3><?= h($qualificationDossier->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Dossier') ?></th>
            <td><?= $qualificationDossier->has('dossier') ? $this->Html->link($qualificationDossier->dossier->id, ['controller' => 'Dossiers', 'action' => 'view', $qualificationDossier->dossier->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($qualificationDossier->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($qualificationDossier->created) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Desc Etat Civil') ?></h4>
        <?= $this->Text->autoParagraph(h($qualificationDossier->desc_etat_civil)); ?>
    </div>
    <div class="row">
        <h4><?= __('Desc Dossier') ?></h4>
        <?= $this->Text->autoParagraph(h($qualificationDossier->desc_dossier)); ?>
    </div>
    <div class="row">
        <h4><?= __('Desc Impact') ?></h4>
        <?= $this->Text->autoParagraph(h($qualificationDossier->desc_impact)); ?>
    </div>
    <div class="row">
        <h4><?= __('Raison Rejet') ?></h4>
        <?= $this->Text->autoParagraph(h($qualificationDossier->raison_rejet)); ?>
    </div>
</div>
