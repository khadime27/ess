<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\QualificationDossier $qualificationDossier
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $qualificationDossier->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $qualificationDossier->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Qualification Dossiers'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Dossiers'), ['controller' => 'Dossiers', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Dossier'), ['controller' => 'Dossiers', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="qualificationDossiers form large-9 medium-8 columns content">
    <?= $this->Form->create($qualificationDossier) ?>
    <fieldset>
        <legend><?= __('Edit Qualification Dossier') ?></legend>
        <?php
            echo $this->Form->control('dossiers_id', ['options' => $dossiers]);
            echo $this->Form->control('desc_etat_civil');
            echo $this->Form->control('desc_dossier');
            echo $this->Form->control('desc_impact');
            echo $this->Form->control('raison_rejet');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
