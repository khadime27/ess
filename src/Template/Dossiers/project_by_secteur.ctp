<main id="main">
  <section id="cness" class="cness section-bg">
    <div class="container" data-aos="fade-up">

      <div class="section-title">
        <h2><?= $filiere->nom ?></h2>
        <h3>Nos projets sous le secteur <?= $filiere->nom ?> dans l'<span>Economie Sociale et Solidaire</span></h3>
      </div>

        <div class="row">
          <?php foreach($dossiers as $dossier): ?>
            <div class="col-4 col-12-mobile">
              <article class="item">
                <a href="<?php echo $this->Url->build(array('controller' => 'Dossiers', 'action' => 'detailproject', $dossier->id)); ?>" class="image fit">
                  <?= $this->Html->image("Images/pic02.jpg", ['alt' => '', 'class' => 'image fit']) ?>
                </a>
                <header>
                  <h3><?= $dossier->intitule ?></h3>
                </header>
              </article>
            </div>
          <?php endforeach; ?>
        </div>

    </div>
  </section>

</main>