<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Dossier $dossier
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Dossier'), ['action' => 'edit', $dossier->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Dossier'), ['action' => 'delete', $dossier->id], ['confirm' => __('Are you sure you want to delete # {0}?', $dossier->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Dossiers'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Dossier'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Nature Demandes'), ['controller' => 'NatureDemandes', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Nature Demande'), ['controller' => 'NatureDemandes', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Porteurs'), ['controller' => 'Porteurs', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Porteur'), ['controller' => 'Porteurs', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Filieres'), ['controller' => 'Filieres', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Filiere'), ['controller' => 'Filieres', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Communes'), ['controller' => 'Communes', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Commune'), ['controller' => 'Communes', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Agents'), ['controller' => 'Agents', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Agent'), ['controller' => 'Agents', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Assignations'), ['controller' => 'Assignations', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Assignation'), ['controller' => 'Assignations', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Financement Credits'), ['controller' => 'FinancementCredits', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Financement Credit'), ['controller' => 'FinancementCredits', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Imputations'), ['controller' => 'Imputations', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Imputation'), ['controller' => 'Imputations', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Messages'), ['controller' => 'Messages', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Message'), ['controller' => 'Messages', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Notations'), ['controller' => 'Notations', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Notation'), ['controller' => 'Notations', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Piece Jointes'), ['controller' => 'PieceJointes', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Piece Jointe'), ['controller' => 'PieceJointes', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Statut Dossiers'), ['controller' => 'StatutDossiers', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Statut Dossier'), ['controller' => 'StatutDossiers', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Suretes'), ['controller' => 'Suretes', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Surete'), ['controller' => 'Suretes', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="dossiers view large-9 medium-8 columns content">
    <h3><?= h($dossier->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Intitule') ?></th>
            <td><?= h($dossier->intitule) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Numero') ?></th>
            <td><?= h($dossier->numero) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Nature Demande') ?></th>
            <td><?= $dossier->has('nature_demande') ? $this->Html->link($dossier->nature_demande->id, ['controller' => 'NatureDemandes', 'action' => 'view', $dossier->nature_demande->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Porteur') ?></th>
            <td><?= $dossier->has('porteur') ? $this->Html->link($dossier->porteur->id, ['controller' => 'Porteurs', 'action' => 'view', $dossier->porteur->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Filiere') ?></th>
            <td><?= $dossier->has('filiere') ? $this->Html->link($dossier->filiere->id, ['controller' => 'Filieres', 'action' => 'view', $dossier->filiere->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Commune') ?></th>
            <td><?= $dossier->has('commune') ? $this->Html->link($dossier->commune->id, ['controller' => 'Communes', 'action' => 'view', $dossier->commune->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Soumis') ?></th>
            <td><?= h($dossier->soumis) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Agent') ?></th>
            <td><?= $dossier->has('agent') ? $this->Html->link($dossier->agent->id, ['controller' => 'Agents', 'action' => 'view', $dossier->agent->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($dossier->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($dossier->created) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Descrip Projet') ?></h4>
        <?= $this->Text->autoParagraph(h($dossier->descrip_projet)); ?>
    </div>
    <div class="row">
        <h4><?= __('Descrip Marche') ?></h4>
        <?= $this->Text->autoParagraph(h($dossier->descrip_marche)); ?>
    </div>
    <div class="row">
        <h4><?= __('Demarche Cmerc') ?></h4>
        <?= $this->Text->autoParagraph(h($dossier->demarche_cmerc)); ?>
    </div>
    <div class="related">
        <h4><?= __('Related Assignations') ?></h4>
        <?php if (!empty($dossier->assignations)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Ot Id') ?></th>
                <th scope="col"><?= __('Agence Id') ?></th>
                <th scope="col"><?= __('Structure Id') ?></th>
                <th scope="col"><?= __('Agent Id') ?></th>
                <th scope="col"><?= __('Dossier Id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($dossier->assignations as $assignations): ?>
            <tr>
                <td><?= h($assignations->id) ?></td>
                <td><?= h($assignations->ot_id) ?></td>
                <td><?= h($assignations->agence_id) ?></td>
                <td><?= h($assignations->structure_id) ?></td>
                <td><?= h($assignations->agent_id) ?></td>
                <td><?= h($assignations->dossier_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Assignations', 'action' => 'view', $assignations->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Assignations', 'action' => 'edit', $assignations->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Assignations', 'action' => 'delete', $assignations->id], ['confirm' => __('Are you sure you want to delete # {0}?', $assignations->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Financement Credits') ?></h4>
        <?php if (!empty($dossier->financement_credits)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Investissement') ?></th>
                <th scope="col"><?= __('Fond Roulement') ?></th>
                <th scope="col"><?= __('Apport Valeur') ?></th>
                <th scope="col"><?= __('Mtnt Solicite') ?></th>
                <th scope="col"><?= __('Intitution Financiere Id') ?></th>
                <th scope="col"><?= __('Apport Prcent') ?></th>
                <th scope="col"><?= __('Dossier Id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($dossier->financement_credits as $financementCredits): ?>
            <tr>
                <td><?= h($financementCredits->id) ?></td>
                <td><?= h($financementCredits->investissement) ?></td>
                <td><?= h($financementCredits->fond_roulement) ?></td>
                <td><?= h($financementCredits->apport_valeur) ?></td>
                <td><?= h($financementCredits->mtnt_solicite) ?></td>
                <td><?= h($financementCredits->intitution_financiere_id) ?></td>
                <td><?= h($financementCredits->apport_prcent) ?></td>
                <td><?= h($financementCredits->dossier_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'FinancementCredits', 'action' => 'view', $financementCredits->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'FinancementCredits', 'action' => 'edit', $financementCredits->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'FinancementCredits', 'action' => 'delete', $financementCredits->id], ['confirm' => __('Are you sure you want to delete # {0}?', $financementCredits->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Imputations') ?></h4>
        <?php if (!empty($dossier->imputations)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Dossier Id') ?></th>
                <th scope="col"><?= __('Agent Imput Id') ?></th>
                <th scope="col"><?= __('Agent Imputateur Id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($dossier->imputations as $imputations): ?>
            <tr>
                <td><?= h($imputations->id) ?></td>
                <td><?= h($imputations->dossier_id) ?></td>
                <td><?= h($imputations->agent_imput_id) ?></td>
                <td><?= h($imputations->agent_imputateur_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Imputations', 'action' => 'view', $imputations->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Imputations', 'action' => 'edit', $imputations->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Imputations', 'action' => 'delete', $imputations->id], ['confirm' => __('Are you sure you want to delete # {0}?', $imputations->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Messages') ?></h4>
        <?php if (!empty($dossier->messages)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Date') ?></th>
                <th scope="col"><?= __('Resume') ?></th>
                <th scope="col"><?= __('Type Message Id') ?></th>
                <th scope="col"><?= __('Agent Id') ?></th>
                <th scope="col"><?= __('Dossier Id') ?></th>
                <th scope="col"><?= __('Porteur Id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($dossier->messages as $messages): ?>
            <tr>
                <td><?= h($messages->id) ?></td>
                <td><?= h($messages->date) ?></td>
                <td><?= h($messages->resume) ?></td>
                <td><?= h($messages->type_message_id) ?></td>
                <td><?= h($messages->agent_id) ?></td>
                <td><?= h($messages->dossier_id) ?></td>
                <td><?= h($messages->porteur_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Messages', 'action' => 'view', $messages->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Messages', 'action' => 'edit', $messages->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Messages', 'action' => 'delete', $messages->id], ['confirm' => __('Are you sure you want to delete # {0}?', $messages->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Notations') ?></h4>
        <?php if (!empty($dossier->notations)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Sect Filier') ?></th>
                <th scope="col"><?= __('Prod Serv') ?></th>
                <th scope="col"><?= __('Imp Emploi') ?></th>
                <th scope="col"><?= __('Imp Terri') ?></th>
                <th scope="col"><?= __('Imp Filier') ?></th>
                <th scope="col"><?= __('Formation') ?></th>
                <th scope="col"><?= __('Experience') ?></th>
                <th scope="col"><?= __('Rep Bank') ?></th>
                <th scope="col"><?= __('Bp') ?></th>
                <th scope="col"><?= __('Stad Avanc') ?></th>
                <th scope="col"><?= __('Equi Ct') ?></th>
                <th scope="col"><?= __('Dossier Id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($dossier->notations as $notations): ?>
            <tr>
                <td><?= h($notations->id) ?></td>
                <td><?= h($notations->sect_filier) ?></td>
                <td><?= h($notations->prod_serv) ?></td>
                <td><?= h($notations->imp_emploi) ?></td>
                <td><?= h($notations->imp_terri) ?></td>
                <td><?= h($notations->imp_filier) ?></td>
                <td><?= h($notations->formation) ?></td>
                <td><?= h($notations->experience) ?></td>
                <td><?= h($notations->rep_bank) ?></td>
                <td><?= h($notations->bp) ?></td>
                <td><?= h($notations->stad_avanc) ?></td>
                <td><?= h($notations->equi_ct) ?></td>
                <td><?= h($notations->dossier_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Notations', 'action' => 'view', $notations->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Notations', 'action' => 'edit', $notations->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Notations', 'action' => 'delete', $notations->id], ['confirm' => __('Are you sure you want to delete # {0}?', $notations->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Piece Jointes') ?></h4>
        <?php if (!empty($dossier->piece_jointes)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Url') ?></th>
                <th scope="col"><?= __('Message Id') ?></th>
                <th scope="col"><?= __('Dossier Id') ?></th>
                <th scope="col"><?= __('Titre') ?></th>
                <th scope="col"><?= __('Mediation Id') ?></th>
                <th scope="col"><?= __('Animation Id') ?></th>
                <th scope="col"><?= __('Suivi Evaluation Id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($dossier->piece_jointes as $pieceJointes): ?>
            <tr>
                <td><?= h($pieceJointes->id) ?></td>
                <td><?= h($pieceJointes->url) ?></td>
                <td><?= h($pieceJointes->message_id) ?></td>
                <td><?= h($pieceJointes->dossier_id) ?></td>
                <td><?= h($pieceJointes->titre) ?></td>
                <td><?= h($pieceJointes->mediation_id) ?></td>
                <td><?= h($pieceJointes->animation_id) ?></td>
                <td><?= h($pieceJointes->suivi_evaluation_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'PieceJointes', 'action' => 'view', $pieceJointes->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'PieceJointes', 'action' => 'edit', $pieceJointes->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'PieceJointes', 'action' => 'delete', $pieceJointes->id], ['confirm' => __('Are you sure you want to delete # {0}?', $pieceJointes->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Statut Dossiers') ?></h4>
        <?php if (!empty($dossier->statut_dossiers)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Dossier Id') ?></th>
                <th scope="col"><?= __('Agent Id') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Etat') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($dossier->statut_dossiers as $statutDossiers): ?>
            <tr>
                <td><?= h($statutDossiers->id) ?></td>
                <td><?= h($statutDossiers->dossier_id) ?></td>
                <td><?= h($statutDossiers->agent_id) ?></td>
                <td><?= h($statutDossiers->created) ?></td>
                <td><?= h($statutDossiers->etat) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'StatutDossiers', 'action' => 'view', $statutDossiers->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'StatutDossiers', 'action' => 'edit', $statutDossiers->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'StatutDossiers', 'action' => 'delete', $statutDossiers->id], ['confirm' => __('Are you sure you want to delete # {0}?', $statutDossiers->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Suretes') ?></h4>
        <?php if (!empty($dossier->suretes)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Libelle') ?></th>
                <th scope="col"><?= __('Valeur') ?></th>
                <th scope="col"><?= __('Cotation') ?></th>
                <th scope="col"><?= __('Type Id') ?></th>
                <th scope="col"><?= __('Dossier Id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($dossier->suretes as $suretes): ?>
            <tr>
                <td><?= h($suretes->id) ?></td>
                <td><?= h($suretes->libelle) ?></td>
                <td><?= h($suretes->valeur) ?></td>
                <td><?= h($suretes->cotation) ?></td>
                <td><?= h($suretes->type_id) ?></td>
                <td><?= h($suretes->dossier_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Suretes', 'action' => 'view', $suretes->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Suretes', 'action' => 'edit', $suretes->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Suretes', 'action' => 'delete', $suretes->id], ['confirm' => __('Are you sure you want to delete # {0}?', $suretes->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
