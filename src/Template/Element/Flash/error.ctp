<?php
if (!isset($params['escape']) || $params['escape'] !== false) {
    $message = h($message);
}
?>

<?= $this->Html->css('growl/jquery.growl.css');?>

<!-- <div class="message error" onclick="this.classList.add('hidden')"><?= $message ?></div> -->


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js" type="text/javascript"></script>
<?php echo $this->Html->script('growl/jquery.growl'); ?>

<script type="text/javascript">
  $.growl.warning({ title: "Erreur !", message: "<strong><?php echo $message ?></strong>" });
</script>
