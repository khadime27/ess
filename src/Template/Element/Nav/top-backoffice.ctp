<style>
    .small-imag-profil {
        width: 35px;
        height: 35px;
        border-radius: 50%;
    }
</style>
<div class="app-header header-shadow">
    <div class="app-header__logo">
        <div class="">
            <a href="<?php echo $this->Url->build('/'); ?>" style="text-decoration: none">
                <!-- <h5 class="color font-weight-bold">ESS Sénégal</h5> -->
                <?= $this->Html->image("portfolio/icon.jpg", ['alt' => 'Icon', 'class' => 'img-fluid', 'width' => '55px']) ?>
            </a>
        </div>
        <div class="header__pane ml-auto">
            <div>
                <button type="button" class="hamburger close-sidebar-btn hamburger--elastic" data-class="closed-sidebar">
                    <span class="hamburger-box">
                        <span class="hamburger-inner"></span>
                    </span>
                </button>
            </div>
        </div>
    </div>
    <div class="app-header__mobile-menu">
        <div>
            <button type="button" class="hamburger hamburger--elastic mobile-toggle-nav">
                <span class="hamburger-box">
                    <span class="hamburger-inner"></span>
                </span>
            </button>
        </div>
    </div>
    <div class="app-header__menu">
        <span>
            <button type="button" class="btn-icon btn-icon-only btn btn-primary btn-sm mobile-toggle-header-nav">
                <span class="btn-icon-wrapper">
                    <i class="fa fa-ellipsis-v fa-w-6"></i>
                </span>
            </button>
        </span>
    </div>    
    <div class="app-header__content">
        <div class="app-header-left">
            <div class="search-wrapper">
                <div class="input-holder">
                    <input type="text" class="search-input" placeholder="Rechercher...">
                    <button class="search-icon"><span></span></button>
                </div>
                <button class="close"></button>
            </div>
            <ul class="header-menu nav">
                <?php if($userConnect->profiles_id == 2): ?>
                <li class="dropdown nav-item">
                    <a href="<?php echo $this->Url->build(array('controller' => 'Parametres', 'action' => 'index')); ?>" class="nav-link">
                        <i class="nav-link-icon fa fa-cog"></i>
                        Parametres
                    </a>
                </li>
                <?php endif; ?>
                <?php if($userConnect->profiles_id == 1): ?>
                    <li class="dropdown nav-item">
                        <a href="<?php echo $this->Url->build(array('controller' => 'Dossiers', 'action' => 'add')); ?>" class="nav-link">
                            <i class="nav-link-icon fa fa-plus"></i>
                            Demande labélisation
                        </a>
                    </li>
                <?php endif; ?>
                <?php if($userConnect->profiles_id != 4 && $userConnect->profiles_id != 6): ?>
                    <li class="dropdown nav-item">
                        <a href="<?php echo $this->Url->build(array('controller' => 'Chats', 'action' => 'index')); ?>" class="nav-link">
                            <i class="nav-link-icon fa fa-comments"></i>
                            Messages
                        </a>
                    </li>
                <?php endif; ?>
            </ul>        
        </div>
        <div class="app-header-right">
            <div class="header-btn-lg pr-0">
                <div class="widget-content p-0">
                    <div class="widget-content-wrapper">
                        <div class="widget-content-left  ml-3 header-user-info">
                            <div class="widget-heading">
                                <?php if($userConnect->profiles_id == 1) echo $userConnect->porteurs[0]->prenom." ".$userConnect->porteurs[0]->nom;
                                else echo $userConnect->agents[0]->prenom." ".$userConnect->agents[0]->nom ?>
                            </div>
                            <div class="widget-subheading">
                                <?= $userConnect->profile->libelle ?><br>
                                <?php if($userConnect->porteurs): ?>
                                    <strong><?= $userConnect->porteurs[0]->entreprise ? $userConnect->porteurs[0]->entreprise->nom : '' ?></strong>
                                <?php endif; ?>
                            </div>
                        </div>
                        <div class="widget-content-left">
                            <div class="btn-group">
                                
                                <div tabindex="-1" role="menu" aria-hidden="true" class="dropdown-menu dropdown-menu-right">
                                    <button type="button" tabindex="0" class="dropdown-item">
                                        <i class="fa fa-user text-primary mr-1"></i>
                                        <a href="<?php echo $this->Url->build(array('controller' => 'Users', 'action' => 'edit', $userConnect->id)); ?>">Mon profil</a>
                                    </button>
                                    <button type="button" tabindex="0" class="dropdown-item">
                                        <i class="fa fa-sign-out-alt text-primary mr-1"></i>
                                        <a href="<?php echo $this->Url->build(array('prefix' => false, 'controller' => 'Users', 'action' => 'deconnexion')); ?>">Déconnexion</a>
                                    </button>
                                    
                                </div>
                                <a data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="p-0 btn ml-2">
                                    <?php if($userConnect->photo != null):
                                        $chemin = null;
                                        if($userConnect->profiles_id == 1) $chemin = "porteurs/".$userConnect->photo;
                                        else $chemin = "agents/".$userConnect->photo; ?>
                                        <?= $this->Html->image($chemin, ['alt' => '', 'class' => 'small-imag-profil']) ?> 
                                    <?php else: ?>
                                        <?= $this->Html->image('portfolio/icon.jpg', ['alt' => '', 'class' => 'small-imag-profil']) ?> 
                                    <?php endif; ?>
                                    <i class="fa fa-angle-down ml-2 opacity-8"></i>
                                </a>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>        
        </div>
    </div>
</div>