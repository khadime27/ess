
<div class="app-sidebar sidebar-shadow">
    <div class="app-header__logo">
        <div class="logo-src"></div>
        <div class="header__pane ml-auto">
            <div>
                <button type="button" class="hamburger close-sidebar-btn hamburger--elastic" data-class="closed-sidebar">
                    <span class="hamburger-box">
                        <span class="hamburger-inner"></span>
                    </span>
                </button>
            </div>
        </div>
    </div>
    <div class="app-header__mobile-menu">
        <div>
            <button type="button" class="hamburger hamburger--elastic mobile-toggle-nav">
                <span class="hamburger-box">
                    <span class="hamburger-inner"></span>
                </span>
            </button>
        </div>
    </div>
    <div class="app-header__menu">
        <span>
            <button type="button" class="btn-icon btn-icon-only btn btn-primary btn-sm mobile-toggle-header-nav">
                <span class="btn-icon-wrapper">
                    <i class="fa fa-ellipsis-v fa-w-6"></i>
                </span>
            </button>
        </span>
    </div>    
    
    <div class="scrollbar-sidebar scrollbar-container">
        <div class="app-sidebar__inner">
            <ul class="vertical-nav-menu">
                <li class="app-sidebar__heading">Tableau de Bord</li>
                <li>
                    <a href="<?php echo $this->Url->build(array('controller' => 'Dashbord', 'action' => 'index')); ?>" class="mm-active">
                        <i class="metismenu-icon pe-7s-compass"></i>
                        Dashboard
                    </a>
                </li>
                <li>
                    <a href="<?php echo $this->Url->build(array('controller' => 'Bibliotheques', 'action' => 'index')); ?>" class="btn btn-success text-white font-weight-bold">
                        <i class="metismenu-icon pe-7s-cloud-download"></i>
                        Modèles de documents
                    </a>
                </li>
                <li class="app-sidebar__heading">Labélisation</li>
                
                <li>
                    <a href="#">
                        <i class="metismenu-icon pe-7s-folder"></i>
                        Dossier
                        <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
                    </a>
                    <ul class="mm-show">
                        <li>
                            <a href="<?php echo $this->Url->build(array('controller' => 'Dossiers', 'action' => 'add')); ?>" class="btn btn-warning text-white font-weight-bold">
                                <i class="fa fa-plus"></i>
                                Demande labélisation
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo $this->Url->build(array('controller' => 'Labelisations', 'action' => 'nonsoumis')); ?>">
                                <i class="metismenu-icon"></i> Non soumis
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo $this->Url->build(array('controller' => 'Labelisations', 'action' => 'encours')); ?>">
                                <i class="metismenu-icon"></i> En cours
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo $this->Url->build(array('controller' => 'Labelisations', 'action' => 'acceptes')); ?>">
                                <i class="metismenu-icon"></i> Acceptés
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo $this->Url->build(array('controller' => 'Labelisations', 'action' => 'rejetes')); ?>">
                                <i class="metismenu-icon"></i> Rejetés
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="app-sidebar__heading">Financement</li>
                <li>
                    <a href="#">
                        <i class="metismenu-icon pe-7s-folder"></i>
                        Dossier
                        <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
                    </a>
                    <ul class="mm-show">
                        <li>
                            <a href="<?php echo $this->Url->build(array('controller' => 'Financements', 'action' => 'nontraites')); ?>">
                                <i class="metismenu-icon"></i> Non traités
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo $this->Url->build(array('controller' => 'Financements', 'action' => 'encours')); ?>">
                                <i class="metismenu-icon"></i> En cours
                            </a>
                        </li>
                    </ul>
                </li>
                
            </ul>
        </div>
    </div>
</div>