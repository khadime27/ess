
<div class="app-sidebar sidebar-shadow">
    <div class="app-header__logo">
        <div class="logo-src"></div>
        <div class="header__pane ml-auto">
            <div>
                <button type="button" class="hamburger close-sidebar-btn hamburger--elastic" data-class="closed-sidebar">
                    <span class="hamburger-box">
                        <span class="hamburger-inner"></span>
                    </span>
                </button>
            </div>
        </div>
    </div>
    <div class="app-header__mobile-menu">
        <div>
            <button type="button" class="hamburger hamburger--elastic mobile-toggle-nav">
                <span class="hamburger-box">
                    <span class="hamburger-inner"></span>
                </span>
            </button>
        </div>
    </div>
    <div class="app-header__menu">
        <span>
            <button type="button" class="btn-icon btn-icon-only btn btn-primary btn-sm mobile-toggle-header-nav">
                <span class="btn-icon-wrapper">
                    <i class="fa fa-ellipsis-v fa-w-6"></i>
                </span>
            </button>
        </span>
    </div>    
    
    <div class="scrollbar-sidebar scrollbar-container">
        <div class="app-sidebar__inner">
            <ul class="vertical-nav-menu">
                <li class="app-sidebar__heading">Tableau de Bord</li>
                <li>
                    <a href="<?php echo $this->Url->build(array('controller' => 'Dashbord', 'action' => 'index')); ?>" class="mm-active">
                        <i class="metismenu-icon pe-7s-compass"></i>
                        Dashboard
                    </a>
                </li>
                <li class="app-sidebar__heading">Dossiers</li>
                <li>
                    <a href="#">
                        <i class="metismenu-icon pe-7s-folder"></i>
                        Labélisation
                        <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
                    </a>
                    <ul class="mm-show">
                        <li>
                            <a href="<?php echo $this->Url->build(array('controller' => 'Labelisations', 'action' => 'acceptes')); ?>">
                                <i class="metismenu-icon"></i> En attentes
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo $this->Url->build(array('controller' => 'Labelisations', 'action' => 'confirmes')); ?>">
                                <i class="metismenu-icon"></i> Confirmés
                            </a>
                        </li>
                    </ul>

                    <a href="#">
                        <i class="metismenu-icon pe-7s-folder"></i>
                        Financement
                        <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
                    </a>
                    <ul class="mm-show">
                        <li>
                            <a href="<?php echo $this->Url->build(array('controller' => 'Financements', 'action' => 'acceptes')); ?>">
                                <i class="metismenu-icon"></i> En attentes
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo $this->Url->build(array('controller' => 'Financements', 'action' => 'confirmes')); ?>">
                                <i class="metismenu-icon"></i> Confirmés
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="app-sidebar__heading">Gestion</li>
                <li>
                    <ul class="mm-show">
                        <li>
                            <a href="<?php echo $this->Url->build(array('controller' => 'Agents', 'action' => 'index')); ?>">
                                <i class="metismenu-icon"></i> Gérer les agents
                            </a>
                        </li>
                    </ul>
                </li>
                
            </ul>
        </div>
    </div>
</div>