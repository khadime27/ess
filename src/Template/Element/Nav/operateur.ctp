
<div class="app-sidebar sidebar-shadow">
    <div class="app-header__logo">
        <div class="logo-src"></div>
        <div class="header__pane ml-auto">
            <div>
                <button type="button" class="hamburger close-sidebar-btn hamburger--elastic" data-class="closed-sidebar">
                    <span class="hamburger-box">
                        <span class="hamburger-inner"></span>
                    </span>
                </button>
            </div>
        </div>
    </div>
    <div class="app-header__mobile-menu">
        <div>
            <button type="button" class="hamburger hamburger--elastic mobile-toggle-nav">
                <span class="hamburger-box">
                    <span class="hamburger-inner"></span>
                </span>
            </button>
        </div>
    </div>
    <div class="app-header__menu">
        <span>
            <button type="button" class="btn-icon btn-icon-only btn btn-primary btn-sm mobile-toggle-header-nav">
                <span class="btn-icon-wrapper">
                    <i class="fa fa-ellipsis-v fa-w-6"></i>
                </span>
            </button>
        </span>
    </div>    
    
    <div class="scrollbar-sidebar scrollbar-container">
        <div class="app-sidebar__inner">
            <ul class="vertical-nav-menu">
                <li class="app-sidebar__heading">Tableau de Bord</li>
                <li>
                    <a href="<?php echo $this->Url->build(array('controller' => 'Dashbord', 'action' => 'index')); ?>" class="mm-active">
                        <i class="metismenu-icon pe-7s-compass"></i>
                        Dashboard
                    </a>
                </li>
                <li class="app-sidebar__heading">Traitement</li>
                <li>
                    <?php if($userConnect->agents[0]->comites_id == 1): ?>
                    <a href="#">
                        <i class="metismenu-icon pe-7s-folder"></i>
                        Qualification
                        <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
                    </a>
                    <ul class="mm-show">
                        <li>
                            <a href="<?php echo $this->Url->build(array('controller' => 'Labelisations', 'action' => 'regions')); ?>">
                                <i class="metismenu-icon"></i> Non assignés
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo $this->Url->build(array('controller' => 'Labelisations', 'action' => 'encours')); ?>">
                                <i class="metismenu-icon"></i> En cours
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo $this->Url->build(array('controller' => 'Labelisations', 'action' => 'acceptes')); ?>">
                                <i class="metismenu-icon"></i> Acceptés
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo $this->Url->build(array('controller' => 'Labelisations', 'action' => 'rejetes')); ?>">
                                <i class="metismenu-icon"></i> Rejetées
                            </a>
                        </li>
                    </ul>
                    <a href="#">
                        <i class="metismenu-icon pe-7s-folder"></i>
                        Financement
                        <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
                    </a>
                    <ul class="mm-show">
                        <li>
                            <a href="<?php echo $this->Url->build(array('controller' => 'FinancesQualifications', 'action' => 'notraites')); ?>">
                                <i class="metismenu-icon"></i> Non assignés
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo $this->Url->build(array('controller' => 'FinancesQualifications', 'action' => 'encours')); ?>">
                                <i class="metismenu-icon"></i> En cours
                            </a>
                        </li>
                    </ul>
                    <?php elseif($userConnect->agents[0]->comites_id == 2): ?>
                        <a href="#">
                            <i class="metismenu-icon pe-7s-diamond"></i>
                            Analyse labélisation
                            <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
                        </a>
                        <ul class="mm-show">
                            <li>
                                <a href="<?php echo $this->Url->build(array('controller' => 'Analyses', 'action' => 'regions', 'label')); ?>">
                                    <i class="metismenu-icon"></i> Non assignés
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo $this->Url->build(array('controller' => 'Analyses', 'action' => 'encours', 'label')); ?>">
                                    <i class="metismenu-icon"></i> En cours
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo $this->Url->build(array('controller' => 'Analyses', 'action' => 'acceptes', 'label')); ?>">
                                    <i class="metismenu-icon"></i> Acceptés
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo $this->Url->build(array('controller' => 'Analyses', 'action' => 'rejetes', 'label')); ?>">
                                    <i class="metismenu-icon"></i> Rejetés
                                </a>
                            </li>
                        </ul>

                        <a href="#">
                            <i class="metismenu-icon pe-7s-diamond"></i>
                            Analyse financement
                            <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
                        </a>
                        <ul class="mm-show">
                            <li>
                                <a href="<?php echo $this->Url->build(array('controller' => 'Analyses', 'action' => 'regions', "finance")); ?>">
                                    <i class="metismenu-icon"></i> Non assignés
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo $this->Url->build(array('controller' => 'Analyses', 'action' => 'encours', "finance")); ?>">
                                    <i class="metismenu-icon"></i> En cours
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo $this->Url->build(array('controller' => 'Analyses', 'action' => 'acceptes', "finance")); ?>">
                                    <i class="metismenu-icon"></i> Acceptés
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo $this->Url->build(array('controller' => 'Analyses', 'action' => 'rejetes', "finance")); ?>">
                                    <i class="metismenu-icon"></i> Rejetés
                                </a>
                            </li>
                        </ul>
                    <?php endif; ?>

                </li>
                <li class="app-sidebar__heading">Gestion</li>
                <li>
                    <a href="<?php echo $this->Url->build(array('controller' => 'Agents', 'action' => 'index')); ?>">
                        <i class="metismenu-icon pe-7s-users"></i>
                        Gérer les agents
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>
