<!-- Header -->
<header id="header">

      <div class="top">

      <!-- Logo -->
        <div id="logo">


          <span class="title">

            <?= $this->Html->image("portfolio/icon.jpg", ['alt' => 'Icon', 'class' => 'img-fluid']) ?>
          </span>
        <h1 id="title">Sen Label ESS</h1>
        <!-- <p>Hyperspace Engineer</p> -->
        </div>

      <!-- Nav -->
        <nav id="nav1">
          <ul class="menu">
            <li><a href="<?php echo $this->Url->build('/#hero'); ?>" id="top-link"><span class="icon solid fa-home">À PROPOS</span></a></li>
            <li><a href="<?php echo $this->Url->build('/#top'); ?>" id="projet-link"><span class="icon solid fas fa-folder-open">PROJETS</span></a></li>
            <li><a href="<?php echo $this->Url->build('/#Actualites'); ?>" id="portfolio-link"><span class="icon solid fa-th">ACTUALITÉS</span></a></li>
            <li><a href="<?php echo $this->Url->build('/#about'); ?>" id="about-link"><span class="icon solid fa-user">ÉVÈNEMENTS</span></a></li>
            <li><a href="<?php echo $this->Url->build('/#contact'); ?>" id="contact-link"><span class="icon solid fa-envelope">CONTACT</span></a></li>
            <?php if(!$userConnect): ?>
                      <li>
                          <a href="<?php echo $this->Url->build(array('controller' => 'Users', 'action' => 'add')); ?>">
                          <span class="icon solid fas fa-sign-in-alt"></span>  Inscripton
                          </a>
                      </li>
                      <li>
                          <a href="<?php echo $this->Url->build(array('controller' => 'Users', 'action' => 'connecter')); ?>">
                              Connexion
                          </a>
                      </li>
                    <?php else: ?>
                      <li class="drop-down">
                          <a href="">
                            <?php if($userConnect->profiles_id == 1) echo $userConnect->porteurs[0]->prenom." ".$userConnect->porteurs[0]->nom;
                            else echo $userConnect->agents[0]->prenom." ".$userConnect->agents[0]->nom ?>
                          </a>
                          <ul>
                            <?php $prefix = "";
                              if($userConnect->profiles_id == 1) $prefix = "porteur";
                              elseif($userConnect->profiles_id == 2) $prefix = "structure";
                              elseif($userConnect->profiles_id == 3) $prefix = "operateur";
                              elseif($userConnect->profiles_id == 4) $prefix = "validateur";
                              elseif($userConnect->profiles_id == 5) $prefix = "comite";
                              elseif($userConnect->profiles_id == 6) $prefix = "superviseur";
                            ?>
                            <li><a href="<?php echo $this->Url->build(array('prefix' => $prefix, 'controller' => 'Dashbord', 'action' => 'index')); ?>">Mon espace</a></li>
                            <li><a href="<?php echo $this->Url->build(array('controller' => 'Users', 'action' => 'deconnexion')); ?>">Se déconnecter</a></li>
                            <?php if($userConnect->profiles_id == 2): ?>
                              <li><a href="<?php echo $this->Url->build(array('controller' => 'Admin', 'action' => 'index')); ?>">Admin Vitrine</a></li>
                            <?php endif; ?>
                          </ul>
                      </li>
                    <?php endif; ?>
                      
          </ul>
        </nav>

      </div>

    <div class="bottom">

      <!-- Social Icons -->
        <ul class="icons">
          <li><a href="#" class="icon brands fa-twitter"><span class="label">Twitter</span></a></li>
          <li><a href="https://web.facebook.com/Ministère-de-la-Microfinance-et-de-lEconomie-Sociale-et-Solidaire-114475760104661/" class="icon brands fa-facebook-f"><span class="label">Facebook</span></a></li>
          <li><a href="#" class="icon brands fa-github"><span class="label">Github</span></a></li>
          <li><a href="#" class="icon brands fa-dribbble"><span class="label">Dribbble</span></a></li>
          <li><a href="#" class="icon solid fa-envelope"><span class="label">Email</span></a></li>
        
        </ul>

    </div>

</header>

  <script>
    $(function(){

$('nav').coreNavigation();

});
    $('nav').coreNavigation({

// default, brand-center, fullscreen, sidebar, sidebar-toggle, section, side-icon
layout: "default",

// menu selector
menu: ".menu",

// full width or not
menuFullWidth: false,

// header selector
header: ".nav-header",

// left, right, center, bottom
menuPosition: "left",

// true or false
container: true,

// selector of toggle button
toggle<a href="https://www.jqueryscript.net/menu/">Menu</a>: ".toggle-bar",

// Only for "side-icon" layout mode
toggleHoverSidebar: false,

// true or false
responsideSlide: false,

// click, hover, accordion
dropdownEvent: "hover",

// default, fixed, sticky
mode: "default",

// enable animations
// requires animate.css
animated: false,
animatedIn: "bounceIn",
animatedOut: "bounceOut"

});
  </script>