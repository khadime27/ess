<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\SituationsCompte $situationsCompte
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $situationsCompte->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $situationsCompte->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Situations Comptes'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Dossiers'), ['controller' => 'Dossiers', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Dossier'), ['controller' => 'Dossiers', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="situationsComptes form large-9 medium-8 columns content">
    <?= $this->Form->create($situationsCompte) ?>
    <fieldset>
        <legend><?= __('Edit Situations Compte') ?></legend>
        <?php
            echo $this->Form->control('libelle');
            echo $this->Form->control('annee_moins_trois');
            echo $this->Form->control('annee_moins_deux');
            echo $this->Form->control('annee_moins_un');
            echo $this->Form->control('annee');
            echo $this->Form->control('prevision');
            echo $this->Form->control('dossiers_id', ['options' => $dossiers, 'empty' => true]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
