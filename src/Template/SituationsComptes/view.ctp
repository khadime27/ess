<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\SituationsCompte $situationsCompte
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Situations Compte'), ['action' => 'edit', $situationsCompte->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Situations Compte'), ['action' => 'delete', $situationsCompte->id], ['confirm' => __('Are you sure you want to delete # {0}?', $situationsCompte->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Situations Comptes'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Situations Compte'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Dossiers'), ['controller' => 'Dossiers', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Dossier'), ['controller' => 'Dossiers', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="situationsComptes view large-9 medium-8 columns content">
    <h3><?= h($situationsCompte->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Libelle') ?></th>
            <td><?= h($situationsCompte->libelle) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Dossier') ?></th>
            <td><?= $situationsCompte->has('dossier') ? $this->Html->link($situationsCompte->dossier->id, ['controller' => 'Dossiers', 'action' => 'view', $situationsCompte->dossier->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($situationsCompte->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Annee Moins Trois') ?></th>
            <td><?= $this->Number->format($situationsCompte->annee_moins_trois) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Annee Moins Deux') ?></th>
            <td><?= $this->Number->format($situationsCompte->annee_moins_deux) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Annee Moins Un') ?></th>
            <td><?= $this->Number->format($situationsCompte->annee_moins_un) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Annee') ?></th>
            <td><?= $this->Number->format($situationsCompte->annee) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Prevision') ?></th>
            <td><?= $this->Number->format($situationsCompte->prevision) ?></td>
        </tr>
    </table>
</div>
