<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\SituationsCompte[]|\Cake\Collection\CollectionInterface $situationsComptes
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Situations Compte'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Dossiers'), ['controller' => 'Dossiers', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Dossier'), ['controller' => 'Dossiers', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="situationsComptes index large-9 medium-8 columns content">
    <h3><?= __('Situations Comptes') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('libelle') ?></th>
                <th scope="col"><?= $this->Paginator->sort('annee_moins_trois') ?></th>
                <th scope="col"><?= $this->Paginator->sort('annee_moins_deux') ?></th>
                <th scope="col"><?= $this->Paginator->sort('annee_moins_un') ?></th>
                <th scope="col"><?= $this->Paginator->sort('annee') ?></th>
                <th scope="col"><?= $this->Paginator->sort('prevision') ?></th>
                <th scope="col"><?= $this->Paginator->sort('dossiers_id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($situationsComptes as $situationsCompte): ?>
            <tr>
                <td><?= $this->Number->format($situationsCompte->id) ?></td>
                <td><?= h($situationsCompte->libelle) ?></td>
                <td><?= $this->Number->format($situationsCompte->annee_moins_trois) ?></td>
                <td><?= $this->Number->format($situationsCompte->annee_moins_deux) ?></td>
                <td><?= $this->Number->format($situationsCompte->annee_moins_un) ?></td>
                <td><?= $this->Number->format($situationsCompte->annee) ?></td>
                <td><?= $this->Number->format($situationsCompte->prevision) ?></td>
                <td><?= $situationsCompte->has('dossier') ? $this->Html->link($situationsCompte->dossier->id, ['controller' => 'Dossiers', 'action' => 'view', $situationsCompte->dossier->id]) : '' ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $situationsCompte->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $situationsCompte->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $situationsCompte->id], ['confirm' => __('Are you sure you want to delete # {0}?', $situationsCompte->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
