<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Impacte $impacte
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Impactes'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="impactes form large-9 medium-8 columns content">
    <?= $this->Form->create($impacte) ?>
    <fieldset>
        <legend><?= __('Add Impacte') ?></legend>
        <?php
            echo $this->Form->control('libelle');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
