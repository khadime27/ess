<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Impacte $impacte
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Impacte'), ['action' => 'edit', $impacte->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Impacte'), ['action' => 'delete', $impacte->id], ['confirm' => __('Are you sure you want to delete # {0}?', $impacte->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Impactes'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Impacte'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="impactes view large-9 medium-8 columns content">
    <h3><?= h($impacte->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Libelle') ?></th>
            <td><?= h($impacte->libelle) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($impacte->id) ?></td>
        </tr>
    </table>
</div>
