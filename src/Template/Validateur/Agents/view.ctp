<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Agent $agent
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Agent'), ['action' => 'edit', $agent->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Agent'), ['action' => 'delete', $agent->id], ['confirm' => __('Are you sure you want to delete # {0}?', $agent->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Agents'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Agent'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Ots'), ['controller' => 'Ots', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Ot'), ['controller' => 'Ots', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Agences'), ['controller' => 'Agences', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Agence'), ['controller' => 'Agences', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Structures'), ['controller' => 'Structures', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Structure'), ['controller' => 'Structures', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Agents'), ['controller' => 'Agents', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Agent'), ['controller' => 'Agents', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Animations'), ['controller' => 'Animations', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Animation'), ['controller' => 'Animations', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Assignations'), ['controller' => 'Assignations', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Assignation'), ['controller' => 'Assignations', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Dossiers'), ['controller' => 'Dossiers', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Dossier'), ['controller' => 'Dossiers', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Messages'), ['controller' => 'Messages', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Message'), ['controller' => 'Messages', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Statut Dossiers'), ['controller' => 'StatutDossiers', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Statut Dossier'), ['controller' => 'StatutDossiers', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Suivi Evaluations'), ['controller' => 'SuiviEvaluations', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Suivi Evaluation'), ['controller' => 'SuiviEvaluations', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="agents view large-9 medium-8 columns content">
    <h3><?= h($agent->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Prenom') ?></th>
            <td><?= h($agent->prenom) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Nom') ?></th>
            <td><?= h($agent->nom) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Telephone') ?></th>
            <td><?= h($agent->telephone) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Adresse') ?></th>
            <td><?= h($agent->adresse) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Ot') ?></th>
            <td><?= $agent->has('ot') ? $this->Html->link($agent->ot->id, ['controller' => 'Ots', 'action' => 'view', $agent->ot->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('User') ?></th>
            <td><?= $agent->has('user') ? $this->Html->link($agent->user->id, ['controller' => 'Users', 'action' => 'view', $agent->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Agence') ?></th>
            <td><?= $agent->has('agence') ? $this->Html->link($agent->agence->id, ['controller' => 'Agences', 'action' => 'view', $agent->agence->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Structure') ?></th>
            <td><?= $agent->has('structure') ? $this->Html->link($agent->structure->id, ['controller' => 'Structures', 'action' => 'view', $agent->structure->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($agent->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Agent Id') ?></th>
            <td><?= $this->Number->format($agent->agent_id) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Agents') ?></h4>
        <?php if (!empty($agent->agents)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Prenom') ?></th>
                <th scope="col"><?= __('Nom') ?></th>
                <th scope="col"><?= __('Telephone') ?></th>
                <th scope="col"><?= __('Adresse') ?></th>
                <th scope="col"><?= __('Ot Id') ?></th>
                <th scope="col"><?= __('User Id') ?></th>
                <th scope="col"><?= __('Agent Id') ?></th>
                <th scope="col"><?= __('Agence Id') ?></th>
                <th scope="col"><?= __('Structure Id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($agent->agents as $agents): ?>
            <tr>
                <td><?= h($agents->id) ?></td>
                <td><?= h($agents->prenom) ?></td>
                <td><?= h($agents->nom) ?></td>
                <td><?= h($agents->telephone) ?></td>
                <td><?= h($agents->adresse) ?></td>
                <td><?= h($agents->ot_id) ?></td>
                <td><?= h($agents->user_id) ?></td>
                <td><?= h($agents->agent_id) ?></td>
                <td><?= h($agents->agence_id) ?></td>
                <td><?= h($agents->structure_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Agents', 'action' => 'view', $agents->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Agents', 'action' => 'edit', $agents->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Agents', 'action' => 'delete', $agents->id], ['confirm' => __('Are you sure you want to delete # {0}?', $agents->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Animations') ?></h4>
        <?php if (!empty($agent->animations)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Titre') ?></th>
                <th scope="col"><?= __('Description') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Type Animation Id') ?></th>
                <th scope="col"><?= __('Agent Id') ?></th>
                <th scope="col"><?= __('Lieu') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($agent->animations as $animations): ?>
            <tr>
                <td><?= h($animations->id) ?></td>
                <td><?= h($animations->titre) ?></td>
                <td><?= h($animations->description) ?></td>
                <td><?= h($animations->created) ?></td>
                <td><?= h($animations->type_animation_id) ?></td>
                <td><?= h($animations->agent_id) ?></td>
                <td><?= h($animations->lieu) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Animations', 'action' => 'view', $animations->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Animations', 'action' => 'edit', $animations->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Animations', 'action' => 'delete', $animations->id], ['confirm' => __('Are you sure you want to delete # {0}?', $animations->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Assignations') ?></h4>
        <?php if (!empty($agent->assignations)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Ot Id') ?></th>
                <th scope="col"><?= __('Agence Id') ?></th>
                <th scope="col"><?= __('Structure Id') ?></th>
                <th scope="col"><?= __('Agent Id') ?></th>
                <th scope="col"><?= __('Dossier Id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($agent->assignations as $assignations): ?>
            <tr>
                <td><?= h($assignations->id) ?></td>
                <td><?= h($assignations->ot_id) ?></td>
                <td><?= h($assignations->agence_id) ?></td>
                <td><?= h($assignations->structure_id) ?></td>
                <td><?= h($assignations->agent_id) ?></td>
                <td><?= h($assignations->dossier_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Assignations', 'action' => 'view', $assignations->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Assignations', 'action' => 'edit', $assignations->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Assignations', 'action' => 'delete', $assignations->id], ['confirm' => __('Are you sure you want to delete # {0}?', $assignations->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Dossiers') ?></h4>
        <?php if (!empty($agent->dossiers)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Intitule') ?></th>
                <th scope="col"><?= __('Numero') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Descrip Projet') ?></th>
                <th scope="col"><?= __('Nature Demande Id') ?></th>
                <th scope="col"><?= __('Descrip Marche') ?></th>
                <th scope="col"><?= __('Demarche Cmerc') ?></th>
                <th scope="col"><?= __('Porteur Id') ?></th>
                <th scope="col"><?= __('Filiere Id') ?></th>
                <th scope="col"><?= __('Commune Id') ?></th>
                <th scope="col"><?= __('Soumis') ?></th>
                <th scope="col"><?= __('Agent Id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($agent->dossiers as $dossiers): ?>
            <tr>
                <td><?= h($dossiers->id) ?></td>
                <td><?= h($dossiers->intitule) ?></td>
                <td><?= h($dossiers->numero) ?></td>
                <td><?= h($dossiers->created) ?></td>
                <td><?= h($dossiers->descrip_projet) ?></td>
                <td><?= h($dossiers->nature_demande_id) ?></td>
                <td><?= h($dossiers->descrip_marche) ?></td>
                <td><?= h($dossiers->demarche_cmerc) ?></td>
                <td><?= h($dossiers->porteur_id) ?></td>
                <td><?= h($dossiers->filiere_id) ?></td>
                <td><?= h($dossiers->commune_id) ?></td>
                <td><?= h($dossiers->soumis) ?></td>
                <td><?= h($dossiers->agent_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Dossiers', 'action' => 'view', $dossiers->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Dossiers', 'action' => 'edit', $dossiers->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Dossiers', 'action' => 'delete', $dossiers->id], ['confirm' => __('Are you sure you want to delete # {0}?', $dossiers->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Messages') ?></h4>
        <?php if (!empty($agent->messages)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Date') ?></th>
                <th scope="col"><?= __('Resume') ?></th>
                <th scope="col"><?= __('Type Message Id') ?></th>
                <th scope="col"><?= __('Agent Id') ?></th>
                <th scope="col"><?= __('Dossier Id') ?></th>
                <th scope="col"><?= __('Porteur Id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($agent->messages as $messages): ?>
            <tr>
                <td><?= h($messages->id) ?></td>
                <td><?= h($messages->date) ?></td>
                <td><?= h($messages->resume) ?></td>
                <td><?= h($messages->type_message_id) ?></td>
                <td><?= h($messages->agent_id) ?></td>
                <td><?= h($messages->dossier_id) ?></td>
                <td><?= h($messages->porteur_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Messages', 'action' => 'view', $messages->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Messages', 'action' => 'edit', $messages->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Messages', 'action' => 'delete', $messages->id], ['confirm' => __('Are you sure you want to delete # {0}?', $messages->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Statut Dossiers') ?></h4>
        <?php if (!empty($agent->statut_dossiers)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Dossier Id') ?></th>
                <th scope="col"><?= __('Agent Id') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Etat') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($agent->statut_dossiers as $statutDossiers): ?>
            <tr>
                <td><?= h($statutDossiers->id) ?></td>
                <td><?= h($statutDossiers->dossier_id) ?></td>
                <td><?= h($statutDossiers->agent_id) ?></td>
                <td><?= h($statutDossiers->created) ?></td>
                <td><?= h($statutDossiers->etat) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'StatutDossiers', 'action' => 'view', $statutDossiers->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'StatutDossiers', 'action' => 'edit', $statutDossiers->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'StatutDossiers', 'action' => 'delete', $statutDossiers->id], ['confirm' => __('Are you sure you want to delete # {0}?', $statutDossiers->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Suivi Evaluations') ?></h4>
        <?php if (!empty($agent->suivi_evaluations)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Titre') ?></th>
                <th scope="col"><?= __('Description') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Type Evaluation Id') ?></th>
                <th scope="col"><?= __('Agent Id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($agent->suivi_evaluations as $suiviEvaluations): ?>
            <tr>
                <td><?= h($suiviEvaluations->id) ?></td>
                <td><?= h($suiviEvaluations->titre) ?></td>
                <td><?= h($suiviEvaluations->description) ?></td>
                <td><?= h($suiviEvaluations->created) ?></td>
                <td><?= h($suiviEvaluations->type_evaluation_id) ?></td>
                <td><?= h($suiviEvaluations->agent_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'SuiviEvaluations', 'action' => 'view', $suiviEvaluations->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'SuiviEvaluations', 'action' => 'edit', $suiviEvaluations->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'SuiviEvaluations', 'action' => 'delete', $suiviEvaluations->id], ['confirm' => __('Are you sure you want to delete # {0}?', $suiviEvaluations->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
