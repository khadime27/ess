<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Dossier[]|\Cake\Collection\CollectionInterface $dossiers
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Dossier'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Nature Demandes'), ['controller' => 'NatureDemandes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Nature Demande'), ['controller' => 'NatureDemandes', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Porteurs'), ['controller' => 'Porteurs', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Porteur'), ['controller' => 'Porteurs', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Filieres'), ['controller' => 'Filieres', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Filiere'), ['controller' => 'Filieres', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Communes'), ['controller' => 'Communes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Commune'), ['controller' => 'Communes', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Agents'), ['controller' => 'Agents', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Agent'), ['controller' => 'Agents', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Assignations'), ['controller' => 'Assignations', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Assignation'), ['controller' => 'Assignations', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Financement Credits'), ['controller' => 'FinancementCredits', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Financement Credit'), ['controller' => 'FinancementCredits', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Imputations'), ['controller' => 'Imputations', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Imputation'), ['controller' => 'Imputations', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Messages'), ['controller' => 'Messages', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Message'), ['controller' => 'Messages', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Notations'), ['controller' => 'Notations', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Notation'), ['controller' => 'Notations', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Piece Jointes'), ['controller' => 'PieceJointes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Piece Jointe'), ['controller' => 'PieceJointes', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Statut Dossiers'), ['controller' => 'StatutDossiers', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Statut Dossier'), ['controller' => 'StatutDossiers', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Suretes'), ['controller' => 'Suretes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Surete'), ['controller' => 'Suretes', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="dossiers index large-9 medium-8 columns content">
    <h3><?= __('Dossiers') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('intitule') ?></th>
                <th scope="col"><?= $this->Paginator->sort('numero') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                <th scope="col"><?= $this->Paginator->sort('nature_demande_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('porteur_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('filiere_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('commune_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('soumis') ?></th>
                <th scope="col"><?= $this->Paginator->sort('agent_id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($dossiers as $dossier): ?>
            <tr>
                <td><?= $this->Number->format($dossier->id) ?></td>
                <td><?= h($dossier->intitule) ?></td>
                <td><?= h($dossier->numero) ?></td>
                <td><?= h($dossier->created) ?></td>
                <td><?= $dossier->has('nature_demande') ? $this->Html->link($dossier->nature_demande->id, ['controller' => 'NatureDemandes', 'action' => 'view', $dossier->nature_demande->id]) : '' ?></td>
                <td><?= $dossier->has('porteur') ? $this->Html->link($dossier->porteur->id, ['controller' => 'Porteurs', 'action' => 'view', $dossier->porteur->id]) : '' ?></td>
                <td><?= $dossier->has('filiere') ? $this->Html->link($dossier->filiere->id, ['controller' => 'Filieres', 'action' => 'view', $dossier->filiere->id]) : '' ?></td>
                <td><?= $dossier->has('commune') ? $this->Html->link($dossier->commune->id, ['controller' => 'Communes', 'action' => 'view', $dossier->commune->id]) : '' ?></td>
                <td><?= h($dossier->soumis) ?></td>
                <td><?= $dossier->has('agent') ? $this->Html->link($dossier->agent->id, ['controller' => 'Agents', 'action' => 'view', $dossier->agent->id]) : '' ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $dossier->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $dossier->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $dossier->id], ['confirm' => __('Are you sure you want to delete # {0}?', $dossier->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
