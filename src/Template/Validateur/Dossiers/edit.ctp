<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Dossier $dossier
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $dossier->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $dossier->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Dossiers'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Nature Demandes'), ['controller' => 'NatureDemandes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Nature Demande'), ['controller' => 'NatureDemandes', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Porteurs'), ['controller' => 'Porteurs', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Porteur'), ['controller' => 'Porteurs', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Filieres'), ['controller' => 'Filieres', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Filiere'), ['controller' => 'Filieres', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Communes'), ['controller' => 'Communes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Commune'), ['controller' => 'Communes', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Agents'), ['controller' => 'Agents', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Agent'), ['controller' => 'Agents', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Assignations'), ['controller' => 'Assignations', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Assignation'), ['controller' => 'Assignations', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Financement Credits'), ['controller' => 'FinancementCredits', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Financement Credit'), ['controller' => 'FinancementCredits', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Imputations'), ['controller' => 'Imputations', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Imputation'), ['controller' => 'Imputations', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Messages'), ['controller' => 'Messages', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Message'), ['controller' => 'Messages', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Notations'), ['controller' => 'Notations', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Notation'), ['controller' => 'Notations', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Piece Jointes'), ['controller' => 'PieceJointes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Piece Jointe'), ['controller' => 'PieceJointes', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Statut Dossiers'), ['controller' => 'StatutDossiers', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Statut Dossier'), ['controller' => 'StatutDossiers', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Suretes'), ['controller' => 'Suretes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Surete'), ['controller' => 'Suretes', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="dossiers form large-9 medium-8 columns content">
    <?= $this->Form->create($dossier) ?>
    <fieldset>
        <legend><?= __('Edit Dossier') ?></legend>
        <?php
            echo $this->Form->control('intitule');
            echo $this->Form->control('numero');
            echo $this->Form->control('descrip_projet');
            echo $this->Form->control('nature_demande_id', ['options' => $natureDemandes, 'empty' => true]);
            echo $this->Form->control('descrip_marche');
            echo $this->Form->control('demarche_cmerc');
            echo $this->Form->control('porteur_id', ['options' => $porteurs]);
            echo $this->Form->control('filiere_id', ['options' => $filieres, 'empty' => true]);
            echo $this->Form->control('commune_id', ['options' => $communes]);
            echo $this->Form->control('soumis');
            echo $this->Form->control('agent_id', ['options' => $agents, 'empty' => true]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
