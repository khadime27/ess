<?php $this->extend('/Cell/dossiers/list') ?>
<?php if($type == "finance") $titre = "Demandes de financement en cours"; else $titre = "Demandes de labélisation en cours"; ?>
<?php $this->assign('titre', $titre); ?>
<?php $this->assign('typeAnalyse', $type); ?>
<?php $this->assign('phrase', 'Les dossiers qui sont en cours de traitement.'); ?>
<?php $this->end(); ?>


