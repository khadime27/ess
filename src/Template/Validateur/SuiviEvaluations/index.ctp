<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\SuiviEvaluation[]|\Cake\Collection\CollectionInterface $suiviEvaluations
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Suivi Evaluation'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Type Evaluations'), ['controller' => 'TypeEvaluations', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Type Evaluation'), ['controller' => 'TypeEvaluations', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Agents'), ['controller' => 'Agents', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Agent'), ['controller' => 'Agents', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Piece Jointes'), ['controller' => 'PieceJointes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Piece Jointe'), ['controller' => 'PieceJointes', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="suiviEvaluations index large-9 medium-8 columns content">
    <h3><?= __('Suivi Evaluations') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('titre') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                <th scope="col"><?= $this->Paginator->sort('type_evaluation_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('agent_id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($suiviEvaluations as $suiviEvaluation): ?>
            <tr>
                <td><?= $this->Number->format($suiviEvaluation->id) ?></td>
                <td><?= h($suiviEvaluation->titre) ?></td>
                <td><?= h($suiviEvaluation->created) ?></td>
                <td><?= $suiviEvaluation->has('type_evaluation') ? $this->Html->link($suiviEvaluation->type_evaluation->id, ['controller' => 'TypeEvaluations', 'action' => 'view', $suiviEvaluation->type_evaluation->id]) : '' ?></td>
                <td><?= $suiviEvaluation->has('agent') ? $this->Html->link($suiviEvaluation->agent->id, ['controller' => 'Agents', 'action' => 'view', $suiviEvaluation->agent->id]) : '' ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $suiviEvaluation->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $suiviEvaluation->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $suiviEvaluation->id], ['confirm' => __('Are you sure you want to delete # {0}?', $suiviEvaluation->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
