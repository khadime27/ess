<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\SuiviEvaluation $suiviEvaluation
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $suiviEvaluation->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $suiviEvaluation->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Suivi Evaluations'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Type Evaluations'), ['controller' => 'TypeEvaluations', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Type Evaluation'), ['controller' => 'TypeEvaluations', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Agents'), ['controller' => 'Agents', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Agent'), ['controller' => 'Agents', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Piece Jointes'), ['controller' => 'PieceJointes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Piece Jointe'), ['controller' => 'PieceJointes', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="suiviEvaluations form large-9 medium-8 columns content">
    <?= $this->Form->create($suiviEvaluation) ?>
    <fieldset>
        <legend><?= __('Edit Suivi Evaluation') ?></legend>
        <?php
            echo $this->Form->control('titre');
            echo $this->Form->control('description');
            echo $this->Form->control('type_evaluation_id', ['options' => $typeEvaluations, 'empty' => true]);
            echo $this->Form->control('agent_id', ['options' => $agents, 'empty' => true]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
