<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Message $message
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Messages'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Type Messages'), ['controller' => 'TypeMessages', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Type Message'), ['controller' => 'TypeMessages', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Agents'), ['controller' => 'Agents', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Agent'), ['controller' => 'Agents', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Dossiers'), ['controller' => 'Dossiers', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Dossier'), ['controller' => 'Dossiers', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Porteurs'), ['controller' => 'Porteurs', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Porteur'), ['controller' => 'Porteurs', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Piece Jointes'), ['controller' => 'PieceJointes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Piece Jointe'), ['controller' => 'PieceJointes', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="messages form large-9 medium-8 columns content">
    <?= $this->Form->create($message) ?>
    <fieldset>
        <legend><?= __('Add Message') ?></legend>
        <?php
            echo $this->Form->control('date', ['empty' => true]);
            echo $this->Form->control('resume');
            echo $this->Form->control('type_message_id', ['options' => $typeMessages]);
            echo $this->Form->control('agent_id', ['options' => $agents, 'empty' => true]);
            echo $this->Form->control('dossier_id', ['options' => $dossiers, 'empty' => true]);
            echo $this->Form->control('porteur_id', ['options' => $porteurs, 'empty' => true]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
