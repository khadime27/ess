<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\FinancesEngagement[]|\Cake\Collection\CollectionInterface $financesEngagements
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Finances Engagement'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Dossiers'), ['controller' => 'Dossiers', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Dossier'), ['controller' => 'Dossiers', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="financesEngagements index large-9 medium-8 columns content">
    <h3><?= __('Finances Engagements') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('libelle') ?></th>
                <th scope="col"><?= $this->Paginator->sort('montant') ?></th>
                <th scope="col"><?= $this->Paginator->sort('encours') ?></th>
                <th scope="col"><?= $this->Paginator->sort('differe_mois') ?></th>
                <th scope="col"><?= $this->Paginator->sort('date_last_echeance') ?></th>
                <th scope="col"><?= $this->Paginator->sort('duree_mois') ?></th>
                <th scope="col"><?= $this->Paginator->sort('banque') ?></th>
                <th scope="col"><?= $this->Paginator->sort('taux_interet') ?></th>
                <th scope="col"><?= $this->Paginator->sort('dossiers_id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($financesEngagements as $financesEngagement): ?>
            <tr>
                <td><?= $this->Number->format($financesEngagement->id) ?></td>
                <td><?= h($financesEngagement->libelle) ?></td>
                <td><?= $this->Number->format($financesEngagement->montant) ?></td>
                <td><?= $this->Number->format($financesEngagement->encours) ?></td>
                <td><?= $this->Number->format($financesEngagement->differe_mois) ?></td>
                <td><?= h($financesEngagement->date_last_echeance) ?></td>
                <td><?= $this->Number->format($financesEngagement->duree_mois) ?></td>
                <td><?= h($financesEngagement->banque) ?></td>
                <td><?= $this->Number->format($financesEngagement->taux_interet) ?></td>
                <td><?= $financesEngagement->has('dossier') ? $this->Html->link($financesEngagement->dossier->id, ['controller' => 'Dossiers', 'action' => 'view', $financesEngagement->dossier->id]) : '' ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $financesEngagement->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $financesEngagement->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $financesEngagement->id], ['confirm' => __('Are you sure you want to delete # {0}?', $financesEngagement->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
