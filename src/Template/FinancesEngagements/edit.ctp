<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\FinancesEngagement $financesEngagement
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $financesEngagement->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $financesEngagement->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Finances Engagements'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Dossiers'), ['controller' => 'Dossiers', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Dossier'), ['controller' => 'Dossiers', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="financesEngagements form large-9 medium-8 columns content">
    <?= $this->Form->create($financesEngagement) ?>
    <fieldset>
        <legend><?= __('Edit Finances Engagement') ?></legend>
        <?php
            echo $this->Form->control('libelle');
            echo $this->Form->control('montant');
            echo $this->Form->control('encours');
            echo $this->Form->control('differe_mois');
            echo $this->Form->control('date_last_echeance', ['empty' => true]);
            echo $this->Form->control('duree_mois');
            echo $this->Form->control('banque');
            echo $this->Form->control('taux_interet');
            echo $this->Form->control('dossiers_id', ['options' => $dossiers]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
