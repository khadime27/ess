<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\FinancesEngagement $financesEngagement
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Finances Engagement'), ['action' => 'edit', $financesEngagement->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Finances Engagement'), ['action' => 'delete', $financesEngagement->id], ['confirm' => __('Are you sure you want to delete # {0}?', $financesEngagement->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Finances Engagements'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Finances Engagement'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Dossiers'), ['controller' => 'Dossiers', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Dossier'), ['controller' => 'Dossiers', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="financesEngagements view large-9 medium-8 columns content">
    <h3><?= h($financesEngagement->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Libelle') ?></th>
            <td><?= h($financesEngagement->libelle) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Banque') ?></th>
            <td><?= h($financesEngagement->banque) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Dossier') ?></th>
            <td><?= $financesEngagement->has('dossier') ? $this->Html->link($financesEngagement->dossier->id, ['controller' => 'Dossiers', 'action' => 'view', $financesEngagement->dossier->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($financesEngagement->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Montant') ?></th>
            <td><?= $this->Number->format($financesEngagement->montant) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Encours') ?></th>
            <td><?= $this->Number->format($financesEngagement->encours) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Differe Mois') ?></th>
            <td><?= $this->Number->format($financesEngagement->differe_mois) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Duree Mois') ?></th>
            <td><?= $this->Number->format($financesEngagement->duree_mois) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Taux Interet') ?></th>
            <td><?= $this->Number->format($financesEngagement->taux_interet) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Date Last Echeance') ?></th>
            <td><?= h($financesEngagement->date_last_echeance) ?></td>
        </tr>
    </table>
</div>
