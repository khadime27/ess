<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Animation $animation
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Animation'), ['action' => 'edit', $animation->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Animation'), ['action' => 'delete', $animation->id], ['confirm' => __('Are you sure you want to delete # {0}?', $animation->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Animations'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Animation'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Type Animations'), ['controller' => 'TypeAnimations', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Type Animation'), ['controller' => 'TypeAnimations', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Agents'), ['controller' => 'Agents', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Agent'), ['controller' => 'Agents', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Participants'), ['controller' => 'Participants', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Participant'), ['controller' => 'Participants', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Piece Jointes'), ['controller' => 'PieceJointes', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Piece Jointe'), ['controller' => 'PieceJointes', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="animations view large-9 medium-8 columns content">
    <h3><?= h($animation->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Titre') ?></th>
            <td><?= h($animation->titre) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Type Animation') ?></th>
            <td><?= $animation->has('type_animation') ? $this->Html->link($animation->type_animation->id, ['controller' => 'TypeAnimations', 'action' => 'view', $animation->type_animation->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Agent') ?></th>
            <td><?= $animation->has('agent') ? $this->Html->link($animation->agent->id, ['controller' => 'Agents', 'action' => 'view', $animation->agent->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Lieu') ?></th>
            <td><?= h($animation->lieu) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($animation->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($animation->created) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Description') ?></h4>
        <?= $this->Text->autoParagraph(h($animation->description)); ?>
    </div>
    <div class="related">
        <h4><?= __('Related Participants') ?></h4>
        <?php if (!empty($animation->participants)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Nom') ?></th>
                <th scope="col"><?= __('Prenom') ?></th>
                <th scope="col"><?= __('Adresse') ?></th>
                <th scope="col"><?= __('Telephone') ?></th>
                <th scope="col"><?= __('Cni') ?></th>
                <th scope="col"><?= __('Animation Id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($animation->participants as $participants): ?>
            <tr>
                <td><?= h($participants->id) ?></td>
                <td><?= h($participants->nom) ?></td>
                <td><?= h($participants->prenom) ?></td>
                <td><?= h($participants->adresse) ?></td>
                <td><?= h($participants->telephone) ?></td>
                <td><?= h($participants->cni) ?></td>
                <td><?= h($participants->animation_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Participants', 'action' => 'view', $participants->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Participants', 'action' => 'edit', $participants->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Participants', 'action' => 'delete', $participants->id], ['confirm' => __('Are you sure you want to delete # {0}?', $participants->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Piece Jointes') ?></h4>
        <?php if (!empty($animation->piece_jointes)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Url') ?></th>
                <th scope="col"><?= __('Message Id') ?></th>
                <th scope="col"><?= __('Dossier Id') ?></th>
                <th scope="col"><?= __('Titre') ?></th>
                <th scope="col"><?= __('Mediation Id') ?></th>
                <th scope="col"><?= __('Animation Id') ?></th>
                <th scope="col"><?= __('Suivi Evaluation Id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($animation->piece_jointes as $pieceJointes): ?>
            <tr>
                <td><?= h($pieceJointes->id) ?></td>
                <td><?= h($pieceJointes->url) ?></td>
                <td><?= h($pieceJointes->message_id) ?></td>
                <td><?= h($pieceJointes->dossier_id) ?></td>
                <td><?= h($pieceJointes->titre) ?></td>
                <td><?= h($pieceJointes->mediation_id) ?></td>
                <td><?= h($pieceJointes->animation_id) ?></td>
                <td><?= h($pieceJointes->suivi_evaluation_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'PieceJointes', 'action' => 'view', $pieceJointes->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'PieceJointes', 'action' => 'edit', $pieceJointes->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'PieceJointes', 'action' => 'delete', $pieceJointes->id], ['confirm' => __('Are you sure you want to delete # {0}?', $pieceJointes->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
