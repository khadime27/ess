<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Animation $animation
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Animations'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Type Animations'), ['controller' => 'TypeAnimations', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Type Animation'), ['controller' => 'TypeAnimations', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Agents'), ['controller' => 'Agents', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Agent'), ['controller' => 'Agents', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Participants'), ['controller' => 'Participants', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Participant'), ['controller' => 'Participants', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Piece Jointes'), ['controller' => 'PieceJointes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Piece Jointe'), ['controller' => 'PieceJointes', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="animations form large-9 medium-8 columns content">
    <?= $this->Form->create($animation) ?>
    <fieldset>
        <legend><?= __('Add Animation') ?></legend>
        <?php
            echo $this->Form->control('titre');
            echo $this->Form->control('description');
            echo $this->Form->control('type_animation_id', ['options' => $typeAnimations]);
            echo $this->Form->control('agent_id', ['options' => $agents, 'empty' => true]);
            echo $this->Form->control('lieu');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
