<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Animation[]|\Cake\Collection\CollectionInterface $animations
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Animation'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Type Animations'), ['controller' => 'TypeAnimations', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Type Animation'), ['controller' => 'TypeAnimations', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Agents'), ['controller' => 'Agents', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Agent'), ['controller' => 'Agents', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Participants'), ['controller' => 'Participants', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Participant'), ['controller' => 'Participants', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Piece Jointes'), ['controller' => 'PieceJointes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Piece Jointe'), ['controller' => 'PieceJointes', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="animations index large-9 medium-8 columns content">
    <h3><?= __('Animations') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('titre') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                <th scope="col"><?= $this->Paginator->sort('type_animation_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('agent_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('lieu') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($animations as $animation): ?>
            <tr>
                <td><?= $this->Number->format($animation->id) ?></td>
                <td><?= h($animation->titre) ?></td>
                <td><?= h($animation->created) ?></td>
                <td><?= $animation->has('type_animation') ? $this->Html->link($animation->type_animation->id, ['controller' => 'TypeAnimations', 'action' => 'view', $animation->type_animation->id]) : '' ?></td>
                <td><?= $animation->has('agent') ? $this->Html->link($animation->agent->id, ['controller' => 'Agents', 'action' => 'view', $animation->agent->id]) : '' ?></td>
                <td><?= h($animation->lieu) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $animation->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $animation->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $animation->id], ['confirm' => __('Are you sure you want to delete # {0}?', $animation->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
