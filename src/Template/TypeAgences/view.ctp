<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\TypeAgence $typeAgence
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Type Agence'), ['action' => 'edit', $typeAgence->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Type Agence'), ['action' => 'delete', $typeAgence->id], ['confirm' => __('Are you sure you want to delete # {0}?', $typeAgence->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Type Agences'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Type Agence'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="typeAgences view large-9 medium-8 columns content">
    <h3><?= h($typeAgence->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Libelle') ?></th>
            <td><?= h($typeAgence->libelle) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($typeAgence->id) ?></td>
        </tr>
    </table>
</div>
