<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\TypeAgence $typeAgence
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $typeAgence->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $typeAgence->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Type Agences'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="typeAgences form large-9 medium-8 columns content">
    <?= $this->Form->create($typeAgence) ?>
    <fieldset>
        <legend><?= __('Edit Type Agence') ?></legend>
        <?php
            echo $this->Form->control('libelle');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
