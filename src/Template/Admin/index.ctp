<!-- Modal Baniere 1 -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
    <?= $this->Form->create(false,array('enctype'=>'multipart/form-data', 'url'=>array('controller'=> 'Bannieres', 'action'=> 'edit'))) ?>
   
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modifier l'image Baniere 1</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <?= $this->Html->image("real-bg.jpg", ['alt' => '', 'class' => 'img-fluid']) ?>
       

      <div class="form-group">
    <label for="exampleFormControlFile1">Choisir une Image</label>
    <input type="file" class="form-control-file" id="exampleFormControlFile1">
    </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
        <button type="submit" class="btn btn-primary">Enregistrer</button>
      </div>
      
      <?= $this->Form->end() ?>
    </div>
  </div>
</div>
   
   <!-- Modal Baniere 1 -->
<div class="modal fade" id="exampleModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
    <form method="post" action="/admin/ban2">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modifier l'image Baniere 2</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <?= $this->Html->image("real-bg1.jpg", ['alt' => '', 'class' => 'img-fluid']) ?>
       

        <div class="input-group">
            <div class="custom-file">
                <input type="file" class="custom-file-input" name="image2">
                <label class="custom-file-label" for="inputGroupFile04">Choisissez un fichier</label>
            </div>
        </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
        <button type="button" class="btn btn-primary">Enregistrer</button>
      </div>
      
      </form>
    </div>
  </div>
</div>
   
   <!-- Main -->
<div id="main">

<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
  </ol>
  <div class="carousel-inner">
    <div class="carousel-item active">
      <!-- ======= Hero Section ======= -->
      <section class="one d-flex align-items-center testimonial-item" id="hero" >
          
        <div class="container" data-aos="zoom-out" data-aos-delay="100">

            <div class="float-right">
                <button type="button" data-toggle="modal" data-target="#exampleModal">Modifier <i class="fas fa-edit"></i></button>
            </div>

            <h1>Bienvenue à <span style="color:#04135E;">ESS</span>
            </h1>
            <h2>Formation Accompagnement Innovations dans les Terroirs du Sénégal <strong>(FAITS)</strong> </h2>
            <div class="d-flex">
                <a href="#about" class="btn-get-started scrollto" style="background-color:#04135E;" class="button scrolly">ESS c’est quoi</a>
                <a href="https://youtu.be/oQQySulvkm8?t=15" class="venobox btn-watch-video"
                data-vbtype="video" data-autoplay="true"> Mot du ministre <i class="icofont-play-alt-2" style="color:#04135E;"></i></a>

                <a href="https://youtu.be/GhydFP3Ccsc?t=109" class="venobox btn-watch-video"
                data-vbtype="video" data-autoplay="true"style=" margin-left: 25%"> 
                Mot du ministre (Wolof)<i class="icofont-play-alt-2" style="color:#04135E"></i></a>
            </div>
         
        </div>
      </section><!-- End Hero -->
    </div>
    <div class="carousel-item">
      <!-- ======= bg-1 Section ======= -->
        <section  class="one d-flex align-items-center testimonial-item" id="bg-1">
                
            <div class="container" data-aos="zoom-out" data-aos-delay="100">
                
                <div class="float-right">
                    <button type="button" data-toggle="modal" data-target="#exampleModal1">Modifier <i class="fas fa-edit"></i></button>
                </div>
            <h1>SUNU LABEL <span >E.S.S</span>
            </h1>
            <h2>Formation Accompagnement Innovations dans les Terroirs du Sénégal <strong>(FAITS)</strong> </h2>
            <div class="d-flex">
                <a href="#about" class="btn-get-started scrollto" class="button scrolly">ESS c’est quoi</a>
                <a href="https://youtu.be/oQQySulvkm8?t=15" class="venobox btn-watch-video"
                data-vbtype="video" data-autoplay="true"> Mot du ministre <i class="icofont-play-alt-2"></i></a>

                <a href="https://youtu.be/GhydFP3Ccsc?t=109" class="venobox btn-watch-video"
                    data-vbtype="video" data-autoplay="true"style=" margin-left: 25%"> 
                    Mot du ministre (Wolof)<i class="icofont-play-alt-2" style="color:#04135E"></i></a>
            </div>
            </div>
        </section><!-- End bg-1 -->
    </div>

  </div>
  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Precedent</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Suivant</span>
  </a>
</div> 


<section id="top" class="two about section-bg">
      <div class="container" data-aos="fade-up">

      <header>
        <h2><h3>GALERIE PROJETS <span>ESS</span></h3></h2>
      </header>

          <div class="row">
            <?php foreach($filieres as $filiere): ?>
            <div class="col-sm-6">
              <article class="item">
                  <a href="<?php echo $this->Url->build(array('controller' => 'Dossiers', 'action' => 'projectBySecteur', $filiere->id)); ?>" class="image fit">
                  <?= $this->Html->image("filieres/".$filiere->photo, ['alt' => h($filiere->nom), 'class' => 'img-fluid']) ?></a>
                  <header>
                  <h3 class="text-uppercase"><?= $filiere->nom ?></h3>
                  </header>
              </article>
            </div>
            <?php endforeach; ?>

          </div>

      </div>
    </section>

<!-- ======= Frequently Asked Questions Section ======= -->
<section id="faq" class="faq section-bg">
      <div class="container" data-aos="zoom-in">
      <header>
          <h2><h3>NOS PARTENAIRES <span></span></h3></h2>
        </header>
    <marquee>
      <div class="row clients section-bg">
        <div class="col-lg-2 col-md-4 col-6 d-flex align-items-center justify-content-center">
            <?= $this->Html->image("clients/mfb.png", ['alt' => '', 'class' => 'img-fluid']) ?>
        </div>

        <div class="col-lg-2 col-md-4 col-6 d-flex align-items-center justify-content-center">
            <?= $this->Html->image("clients/afd.jpg", ['alt' => '', 'class' => 'img-fluid']) ?>
        </div>

        <div class="col-lg-2 col-md-4 col-6 d-flex align-items-center justify-content-center">
            <?= $this->Html->image("clients/mepc.png" ,['alt' => '', 'class' => 'img-fluid']) ?>
        </div>

        <div class="col-lg-2 col-md-4 col-6 d-flex align-items-center justify-content-center">
            <?= $this->Html->image("clients/plasepri1.png", ['alt' => '', 'class' => 'img-fluid']) ?>
        </div>

        <div class="col-lg-2 col-md-4 col-6 d-flex align-items-center justify-content-center">
            <?= $this->Html->image("clients/ractes.png", ['alt' => '', 'class' => 'img-fluid']) ?>
        </div>

        <div class="col-lg-2 col-md-4 col-6 d-flex align-items-center justify-content-center">
            <?= $this->Html->image("clients/promise1.jpg", ['alt' => '', 'class' => 'img-fluid']) ?>
        </div>
      </div>
    </marquee>
        <div class="float-right">
            <button>Modifier <i class="fas fa-edit"></i></button>
        </div>

      </div>
</section><!-- End Frequently Asked Questions Section -->

    <!-- Contact -->          
<section id="contact" class="four contact">
  <div class="container" data-aos="fade-up">
  
    <div class="section-title">
      <h3><span>Contacter nous</span></h3>
    </div>

    <div class="row" data-aos="fade-up" data-aos-delay="100">
      <div class="col-lg-6">
        <div class="info-box mb-4">
          <i class="bx bx-map"></i>
          <h3>Notre Adresse</h3>
          <p>Sphère ministérielle Ousmane Tanor Dieng</p>
          <p>1er Arrondissement </p>
          <p>Pole urbain de Diamniadio</p>
        </div>
      </div>

      <div class="col-lg-3 col-md-6">
        <div class="info-box  mb-4">
          <i class="bx bx-envelope"></i>

          <h3>Email </h3>
          <p>contact@example.com</p>
          <br>
          <p>info@example.com</p>
        </div>
      </div>

      <div class="col-lg-3 col-md-6">
        <div class="info-box mb-4">
          <i class="bx bx-phone-call"></i>
          <h3>Téléphone</h3>
          <p>+221 33 860 26 52</p><br>
          <p>+221 33 860 59 71</p>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
      </div>

    </div>

    <div class="row" data-aos="fade-up" data-aos-delay="100">

      <div class="col-lg-6 ">
      <iframe class="mb-4 mb-lg-0" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3770.356019313571!2d-17.198654225714904!3d14.741763604615059!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xd5e4e3b5b26c8784!2sSph%C3%A8re%20Minist%C3%A9rielle%20Ousmane%20Tanor%20Dieng!5e0!3m2!1sfr!2ssn!4v1601558868301!5m2!1sfr!2ssn" frameborder="0" style="border:0; width: 100%; height: 384px;" allowfullscreen></iframe>
      </div>

      <div class="col-lg-6">
        <form action="forms/contact.php" method="post" role="form" class="php-email-form">
          <div class="form-row">
            <div class="col form-group">
              <input type="text" name="name" class="form-control" id="name" placeholder="Votre Nom" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
              <div class="validate"></div>
            </div>
            <div class="col form-group">
              <input type="email" class="form-control" name="email" id="email" placeholder="Votre Email" data-rule="email" data-msg="Please enter a valid email" />
              <div class="validate"></div>
            </div>
          </div>
          <div class="form-group">
            <input type="text" class="form-control" name="subject" id="subject" placeholder="Objet" data-rule="minlen:4" data-msg="Please enter at least 8 chars of subject" />

            <div class="validate"></div>
          </div>
          <div class="form-group">
            <textarea class="form-control" name="message" rows="5" data-rule="required" data-msg="Please write something for us" placeholder="Message"></textarea>
            <div class="validate"></div>
          </div>
          <div class="mb-3">
            <div class="loading">Chargement</div>
            <div class="error-message"></div>
            <div class="sent-message">Votre message a ete envoyé. Merci!</div>
          </div>
          <div class="text-center"><button type="submit">Envoyer Message</button></div>
        </form>
      </div>

    </div>

  </div>
</section><!-- End Contact Section -->

</div>
