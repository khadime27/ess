<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Notation $notation
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Notation'), ['action' => 'edit', $notation->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Notation'), ['action' => 'delete', $notation->id], ['confirm' => __('Are you sure you want to delete # {0}?', $notation->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Notations'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Notation'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Dossiers'), ['controller' => 'Dossiers', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Dossier'), ['controller' => 'Dossiers', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Agents'), ['controller' => 'Agents', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Agent'), ['controller' => 'Agents', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="notations view large-9 medium-8 columns content">
    <h3><?= h($notation->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Dossier') ?></th>
            <td><?= $notation->has('dossier') ? $this->Html->link($notation->dossier->id, ['controller' => 'Dossiers', 'action' => 'view', $notation->dossier->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Agent') ?></th>
            <td><?= $notation->has('agent') ? $this->Html->link($notation->agent->id, ['controller' => 'Agents', 'action' => 'view', $notation->agent->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($notation->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Nat Struc Juridique') ?></th>
            <td><?= $this->Number->format($notation->nat_struc_juridique) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Eligibilite Filiere') ?></th>
            <td><?= $this->Number->format($notation->eligibilite_filiere) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Partenariat Relation') ?></th>
            <td><?= $this->Number->format($notation->partenariat_relation) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Pertinence Activite') ?></th>
            <td><?= $this->Number->format($notation->pertinence_activite) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Coherence Activite') ?></th>
            <td><?= $this->Number->format($notation->coherence_activite) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Positionnement Activite') ?></th>
            <td><?= $this->Number->format($notation->positionnement_activite) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Aspect Technique') ?></th>
            <td><?= $this->Number->format($notation->aspect_technique) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Sante Financiere') ?></th>
            <td><?= $this->Number->format($notation->sante_financiere) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Croissance Reultat') ?></th>
            <td><?= $this->Number->format($notation->croissance_reultat) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Situation Actif') ?></th>
            <td><?= $this->Number->format($notation->situation_actif) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Situation Passif') ?></th>
            <td><?= $this->Number->format($notation->situation_passif) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Croissance En Vue') ?></th>
            <td><?= $this->Number->format($notation->croissance_en_vue) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Aspect Financier') ?></th>
            <td><?= $this->Number->format($notation->aspect_financier) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Eligibilite Ess') ?></th>
            <td><?= $this->Number->format($notation->eligibilite_ess) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Impact Social Env') ?></th>
            <td><?= $this->Number->format($notation->impact_social_env) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Influence Territoriale') ?></th>
            <td><?= $this->Number->format($notation->influence_territoriale) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Autre Critere Ess1') ?></th>
            <td><?= $this->Number->format($notation->autre_critere_ess1) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Autre Critere Ess2') ?></th>
            <td><?= $this->Number->format($notation->autre_critere_ess2) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Pourcentage Revenu Dirigeants') ?></th>
            <td><?= $this->Number->format($notation->pourcentage_revenu_dirigeants) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Aspect Env') ?></th>
            <td><?= $this->Number->format($notation->aspect_env) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Comment Aspect Tech') ?></h4>
        <?= $this->Text->autoParagraph(h($notation->comment_aspect_tech)); ?>
    </div>
    <div class="row">
        <h4><?= __('Comment Aspect Financ') ?></h4>
        <?= $this->Text->autoParagraph(h($notation->comment_aspect_financ)); ?>
    </div>
    <div class="row">
        <h4><?= __('Comment Aspect Env') ?></h4>
        <?= $this->Text->autoParagraph(h($notation->comment_aspect_env)); ?>
    </div>
</div>
