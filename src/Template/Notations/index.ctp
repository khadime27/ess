<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Notation[]|\Cake\Collection\CollectionInterface $notations
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Notation'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Dossiers'), ['controller' => 'Dossiers', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Dossier'), ['controller' => 'Dossiers', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Agents'), ['controller' => 'Agents', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Agent'), ['controller' => 'Agents', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="notations index large-9 medium-8 columns content">
    <h3><?= __('Notations') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('dossiers_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('agents_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('nat_struc_juridique') ?></th>
                <th scope="col"><?= $this->Paginator->sort('eligibilite_filiere') ?></th>
                <th scope="col"><?= $this->Paginator->sort('partenariat_relation') ?></th>
                <th scope="col"><?= $this->Paginator->sort('pertinence_activite') ?></th>
                <th scope="col"><?= $this->Paginator->sort('coherence_activite') ?></th>
                <th scope="col"><?= $this->Paginator->sort('positionnement_activite') ?></th>
                <th scope="col"><?= $this->Paginator->sort('aspect_technique') ?></th>
                <th scope="col"><?= $this->Paginator->sort('sante_financiere') ?></th>
                <th scope="col"><?= $this->Paginator->sort('croissance_reultat') ?></th>
                <th scope="col"><?= $this->Paginator->sort('situation_actif') ?></th>
                <th scope="col"><?= $this->Paginator->sort('situation_passif') ?></th>
                <th scope="col"><?= $this->Paginator->sort('croissance_en_vue') ?></th>
                <th scope="col"><?= $this->Paginator->sort('aspect_financier') ?></th>
                <th scope="col"><?= $this->Paginator->sort('eligibilite_ess') ?></th>
                <th scope="col"><?= $this->Paginator->sort('impact_social_env') ?></th>
                <th scope="col"><?= $this->Paginator->sort('influence_territoriale') ?></th>
                <th scope="col"><?= $this->Paginator->sort('autre_critere_ess1') ?></th>
                <th scope="col"><?= $this->Paginator->sort('autre_critere_ess2') ?></th>
                <th scope="col"><?= $this->Paginator->sort('pourcentage_revenu_dirigeants') ?></th>
                <th scope="col"><?= $this->Paginator->sort('aspect_env') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($notations as $notation): ?>
            <tr>
                <td><?= $this->Number->format($notation->id) ?></td>
                <td><?= $notation->has('dossier') ? $this->Html->link($notation->dossier->id, ['controller' => 'Dossiers', 'action' => 'view', $notation->dossier->id]) : '' ?></td>
                <td><?= $notation->has('agent') ? $this->Html->link($notation->agent->id, ['controller' => 'Agents', 'action' => 'view', $notation->agent->id]) : '' ?></td>
                <td><?= $this->Number->format($notation->nat_struc_juridique) ?></td>
                <td><?= $this->Number->format($notation->eligibilite_filiere) ?></td>
                <td><?= $this->Number->format($notation->partenariat_relation) ?></td>
                <td><?= $this->Number->format($notation->pertinence_activite) ?></td>
                <td><?= $this->Number->format($notation->coherence_activite) ?></td>
                <td><?= $this->Number->format($notation->positionnement_activite) ?></td>
                <td><?= $this->Number->format($notation->aspect_technique) ?></td>
                <td><?= $this->Number->format($notation->sante_financiere) ?></td>
                <td><?= $this->Number->format($notation->croissance_reultat) ?></td>
                <td><?= $this->Number->format($notation->situation_actif) ?></td>
                <td><?= $this->Number->format($notation->situation_passif) ?></td>
                <td><?= $this->Number->format($notation->croissance_en_vue) ?></td>
                <td><?= $this->Number->format($notation->aspect_financier) ?></td>
                <td><?= $this->Number->format($notation->eligibilite_ess) ?></td>
                <td><?= $this->Number->format($notation->impact_social_env) ?></td>
                <td><?= $this->Number->format($notation->influence_territoriale) ?></td>
                <td><?= $this->Number->format($notation->autre_critere_ess1) ?></td>
                <td><?= $this->Number->format($notation->autre_critere_ess2) ?></td>
                <td><?= $this->Number->format($notation->pourcentage_revenu_dirigeants) ?></td>
                <td><?= $this->Number->format($notation->aspect_env) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $notation->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $notation->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $notation->id], ['confirm' => __('Are you sure you want to delete # {0}?', $notation->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
