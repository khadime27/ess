<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Notation $notation
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Notations'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Dossiers'), ['controller' => 'Dossiers', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Dossier'), ['controller' => 'Dossiers', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Agents'), ['controller' => 'Agents', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Agent'), ['controller' => 'Agents', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="notations form large-9 medium-8 columns content">
    <?= $this->Form->create($notation) ?>
    <fieldset>
        <legend><?= __('Add Notation') ?></legend>
        <?php
            echo $this->Form->control('dossiers_id', ['options' => $dossiers]);
            echo $this->Form->control('agents_id', ['options' => $agents]);
            echo $this->Form->control('nat_struc_juridique');
            echo $this->Form->control('eligibilite_filiere');
            echo $this->Form->control('partenariat_relation');
            echo $this->Form->control('pertinence_activite');
            echo $this->Form->control('coherence_activite');
            echo $this->Form->control('positionnement_activite');
            echo $this->Form->control('comment_aspect_tech');
            echo $this->Form->control('aspect_technique');
            echo $this->Form->control('sante_financiere');
            echo $this->Form->control('croissance_reultat');
            echo $this->Form->control('situation_actif');
            echo $this->Form->control('situation_passif');
            echo $this->Form->control('croissance_en_vue');
            echo $this->Form->control('comment_aspect_financ');
            echo $this->Form->control('aspect_financier');
            echo $this->Form->control('eligibilite_ess');
            echo $this->Form->control('impact_social_env');
            echo $this->Form->control('influence_territoriale');
            echo $this->Form->control('autre_critere_ess1');
            echo $this->Form->control('autre_critere_ess2');
            echo $this->Form->control('pourcentage_revenu_dirigeants');
            echo $this->Form->control('comment_aspect_env');
            echo $this->Form->control('aspect_env');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
