<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Filiere $filiere
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Filiere'), ['action' => 'edit', $filiere->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Filiere'), ['action' => 'delete', $filiere->id], ['confirm' => __('Are you sure you want to delete # {0}?', $filiere->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Filieres'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Filiere'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Dossiers'), ['controller' => 'Dossiers', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Dossier'), ['controller' => 'Dossiers', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="filieres view large-9 medium-8 columns content">
    <h3><?= h($filiere->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Nom') ?></th>
            <td><?= h($filiere->nom) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($filiere->id) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Dossiers') ?></h4>
        <?php if (!empty($filiere->dossiers)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Intitule') ?></th>
                <th scope="col"><?= __('Numero') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Descrip Projet') ?></th>
                <th scope="col"><?= __('Nature Demande Id') ?></th>
                <th scope="col"><?= __('Descrip Marche') ?></th>
                <th scope="col"><?= __('Demarche Cmerc') ?></th>
                <th scope="col"><?= __('Porteur Id') ?></th>
                <th scope="col"><?= __('Filiere Id') ?></th>
                <th scope="col"><?= __('Commune Id') ?></th>
                <th scope="col"><?= __('Soumis') ?></th>
                <th scope="col"><?= __('Agent Id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($filiere->dossiers as $dossiers): ?>
            <tr>
                <td><?= h($dossiers->id) ?></td>
                <td><?= h($dossiers->intitule) ?></td>
                <td><?= h($dossiers->numero) ?></td>
                <td><?= h($dossiers->created) ?></td>
                <td><?= h($dossiers->descrip_projet) ?></td>
                <td><?= h($dossiers->nature_demande_id) ?></td>
                <td><?= h($dossiers->descrip_marche) ?></td>
                <td><?= h($dossiers->demarche_cmerc) ?></td>
                <td><?= h($dossiers->porteur_id) ?></td>
                <td><?= h($dossiers->filiere_id) ?></td>
                <td><?= h($dossiers->commune_id) ?></td>
                <td><?= h($dossiers->soumis) ?></td>
                <td><?= h($dossiers->agent_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Dossiers', 'action' => 'view', $dossiers->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Dossiers', 'action' => 'edit', $dossiers->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Dossiers', 'action' => 'delete', $dossiers->id], ['confirm' => __('Are you sure you want to delete # {0}?', $dossiers->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
