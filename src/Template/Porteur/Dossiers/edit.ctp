<?php $this->setLayout('back_office'); ?>

<section class="my-5 mt-150 content-add-folder" style="margin-bottom:20px">
    <div class="card" style="border: none;">
      <div class="card-header bg-color text-white">
          <div class="col-sm-12 text-center">
            <h3 class="mb-0">MODIFICATION DU DOSSIER</h3>
          </div>
      </div>
      <input type="hidden" id="verifEntreprise" value="<?= $checkEntreprise ? '1' : '0' ?>">
      <!--Card content-->
      <div class="card-body pt-3" style="margin-bottom: 100px;">
        <!-- Form -->
        <?= $this->Form->create('Dossiers', array('enctype' => 'multipart/form-data', 'url'=>array('action'=> 'edit', $id))) ?>
        <div class="input-group md-form" id="form-dossier">
          <div class="container">
            <div class="row">
                <div class="col-sm-6 mb-5">
                    <div class="form-row mb-4">
                        <label for="name">L'activité est pour le compte de ?</label>
                        <select id="owner_type" disabled class="form-control" required>
                            <option value="physique" <?php if($userConnect->porteurs[0]->entreprise_id == null) echo 'selected'; ?>>Mon propre compte</option>
                            <option value="moral" <?php if($userConnect->porteurs[0]->entreprise_id != null) echo 'selected'; ?>>Mon entreprise</option>
                            <option value="moral" <?php if($checkAssociation) echo 'selected'; ?>>Association</option>                       
                        </select>
                    </div>
                    
                    <?php if(!$checkEntreprise): ?>
                    <div class="form-row mb-4" id="bloc-cni">
                        <label><?= $userConnect->porteurs[0]->type_identification ?></label>
                        <input type="number" maxlength="13" name="num_identification" value="<?= $userConnect->porteurs[0]->num_identification ?>" placeholder="Numéro d'identification" class="form-control">
                    </div>
                    <?php else: ?>
                    <div class="bloc-entreprise">
                        <div class="form-row mb-4">
                            <label><?php if(!$checkAssociation) echo 'Entreprise'; else echo "Nom de l'asociation"; ?></label>
                            <input type="text" name="nomEntreprise" value="<?= $userConnect->porteurs[0]->entreprise->nom ?>" placeholder="Saisir le nom" class="form-control" />
                        </div>
                        <div class="form-row mb-4">
                            <label><?php if(!$checkAssociation) echo "Type d'entreprise"; else echo "Type d'association"; ?></label>
                            <select name="type_entreprise_id" class="form-control">
                                <?php foreach($typeEntreprises as $typeEntreprise): ?>
                                    <option value="<?= $typeEntreprise->id ?>" <?php if($checkEntreprise): if($userConnect->porteurs[0]->entreprise->type_entreprise_id == $typeEntreprise->id) echo 'selected'; endif; ?>><?= $typeEntreprise->nom ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="form-row mb-4">
                            <label>Ninéa</label>
                            <input type="text" name="ninea" value="<?php if($checkEntreprise) echo $userConnect->porteurs[0]->entreprise->ninea; ?>" class="form-control" placeholder="Votre ninéa" />
                        </div>
                        <div class="form-row mb-4">
                            <label>Nombre d'employé</label>
                            <input type="number" name="nb_employe" value="<?= $userConnect->porteurs[0]->entreprise->nb_employe ?>" class="form-control" placeholder="Saisir le nombre d'employé" />
                        </div>
                    </div>
                    <?php endif; ?>

                    <div class="form-row mb-4">
                        <label>Nombre de personne impacté</label>
                        <input type="number" name="nb_perso_impact" value="<?= $dossier->nb_perso_impact ?>" class="form-control" placeholder="Saisir le nombre" />
                    </div>

                    <div class="form-row mb-4">
                        <label>Secteur du l'activité</label>
                        <select name="filiere_id" class="form-control">
                            <?php foreach($filieres as $filiere): ?>
                                <option value="<?= $filiere->id ?>" <?php if($dossier->filiere_id == $filiere->id) echo 'selected'; ?>><?= $filiere->nom ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>

                    <div class="form-row mb-4">
                        <label>Autres secteurs impactés</label>
                        <select name="secteurs_interesses[]" class="form-control select-multiple" multiple>
                            <option value="">Choisissez des secteurs</option>
                            <?php foreach($filieres as $filiere): ?>
                                <option value="<?= $filiere->id ?>" <?php if($secteursInteresses): foreach($secteursInteresses as $secteur): if($filiere->id == $secteur->filieres_id) echo 'selected'; endforeach; endif; ?>>
                                    <?= $filiere->nom ?>
                                </option>
                            <?php endforeach; ?>
                        </select>
                    </div>


                    <div class="form-row mb-4">
                        <label>Sous secteurs impactés</label>
                        <select name="secteurs_relations[]" class="form-control select-multiple" multiple>
                            <option value="">Choisissez des secteurs</option>
                            <?php foreach($filieres as $filiere): ?>
                                <option value="<?= $filiere->id ?>" <?php if($secteursEnRelations): foreach($secteursEnRelations as $secteur): if($filiere->id == $secteur->filieres_id) echo 'selected'; endforeach; endif; ?>>
                                    <?= $filiere->nom ?>
                                </option>
                            <?php endforeach; ?>
                        </select>
                    </div>

                    <div class="form-row mb-4">
                        <label>L'activité sera exécuté dans quelle région ?</label>
                        <select id="region_id" class="form-control">
                            <option value="">Veuillez choisir la région</option>
                            <?php foreach($regions as $region): ?>
                                <option value="<?= $region->id ?>" <?php if($dossier->commune && $dossier->commune->departement->region_id == $region->id) echo 'selected'; ?>><?= $region->nom ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>

                    <div class="form-row mb-4">
                        <label for="name">Veuillez choisir le département de la région</label>
                        <select type="text" id ="departement"  name="departement_id" class="form-control js-example-basic">
                            <option selected="selected" disabled required value="">Liste des départements</option>
                            <option value="<?php if($dossier->commune) echo $dossier->commune->departement_id; ?>" <?php if($dossier->commune && $dossier->commune->departement) echo 'selected'; ?>><?php if($dossier->commune) echo $dossier->commune->departement->departementscol; ?></option>
                        </select>
                    </div>

                    <div class="form-row mb-4 commune">
                        <label>Veuillez donner le nom de la commune du département</label>
                        <select name="nomcom" id="commun" class="form-control">
                            <option disabled value="">Liste des communes</option>
                            <option value="<?php if($dossier->commune) echo $dossier->commune_id; ?>" <?php if($dossier->commune) echo 'selected'; ?>><?php if($dossier->commune) echo $dossier->commune->nom; ?></option>
                        </select>
                    </div>

                    <div class="form-row mb-4">
                        <label class="font-weight-bold">Impact social</label>
                        <select name="impacts_sociales[]" class="form-control select-multiple" multiple>
                            <?php foreach($typesImpacts as $typeImpact): 
                                if($typeImpact->impactes_id == 1): ?>
                                    <option value="<?= $typeImpact->id ?>" <?php if($dossiersImpacts): foreach($dossiersImpacts as $dossierImpact): if($typeImpact->id == $dossierImpact->types_impacts_id) echo 'selected'; endforeach; endif; ?> >
                                        <?= $typeImpact->libelle ?>
                                    </option>
                            <?php endif; endforeach; ?>
                        </select>
                    </div>

                    <div class="form-row mb-4">
                        <label class="font-weight-bold">Impact environnemental</label>
                        <select name="impacts_environes[]" class="form-control select-multiple" multiple>
                            <?php foreach($typesImpacts as $typeImpact):
                                if($typeImpact->impactes_id == 2): ?>
                                    <option value="<?= $typeImpact->id ?>" <?php if($dossiersImpacts): foreach($dossiersImpacts as $dossierImpact): if($typeImpact->id == $dossierImpact->types_impacts_id) echo 'selected'; endforeach; endif; ?>>
                                        <?= $typeImpact->libelle ?>
                                    </option>
                            <?php endif; endforeach; ?>
                        </select>
                    </div>

                    

                </div>
                <!-- Col -->
                <div class="col-sm-6">
                    <div class="form-row mb-4">
                        <label>Intitulé de l'activité</label>
                        <input type="text" name="intitule" value="<?= $dossier->intitule ?>" class="form-control" />
                    </div>

                    <?php if($checkEntreprise): ?>
                    <div class="form-row mb-4">
                        <label>Statut juridique</label>
                        <select name="statut-juridique" class="form-control">
                            <option value="">Choisissez le statut</option>
                            <option value="Formel" <?php if($userConnect->porteurs[0]->entreprise && $userConnect->porteurs[0]->entreprise->formel) echo 'selected'; ?>>Formel</option>
                            <option value="Informel" <?php if($userConnect->porteurs[0]->entreprise && !$userConnect->porteurs[0]->entreprise->formel) echo 'selected'; ?>>Informel<option>
                        </select>
                    </div>
                    <div class="form-row mb-4">
                        <label>Immatriculation</label>
                        <input type="text" name="immatriculation" value="<?= $userConnect->porteurs[0]->entreprise->immatriculation ?>" class="form-control" />
                    </div>
                    <?php endif; ?>

                    <label>Avez vous des documents concernant l'activité ?</label>
                    <div class="form-row mb-4 ml-1">
                        <span class="mr-2">OUI <input type="radio" name="doc" <?php if($dossier->piece_jointes) echo 'checked'; ?> value="oui" id="docoui" /></span>
                        <span>NON <input type="radio" name="doc" value="nom" id="docnon" /></span>
                    </div>

                    <?php if($dossier->piece_jointes != null): ?>
                    <div class="form-row form-group multiple-form-group mb-4 doc" data-max=3>

                        <?php foreach($dossier->piece_jointes as $piece): ?>
                            <div class="md-form input-group mb-3">
                                <label>La nature du document</label>
                                <div class="row">
                                    <div class="col-md-6">
                                        <select name="url[]" class="form-control">
                                            <option value="" selected="">--Nature du document--</option>
                                            <?php foreach($typesPieces as $type): ?>
                                                <option value="<?= $type->libelle ?>" <?php if($piece->titre == $type->libelle) echo 'selected'; ?>>
                                                    <?= $type->libelle ?>
                                                </option>
                                            <?php endforeach; ?>
                                        </select>
                                        <?= $this->Html->link(__(' Voir'),'/documents/dossier/'.$piece->url,
                                            array('target'=>'_blank','escape'=>false, 'class'=>'fa fa-eye btn btn-warning btn-sm')) ?>
                                    </div>
                                    <div class="col-md-5" style="display: flex;">
                                        <input type="file" name="fields[]" class="form-control inputFileNatureDoc">
                                        <span class="input-group-btn">
                                            <button type="button" class="btn btn-primary btn-add">+</button>
                                        </span>
                                    </div>
                                </div>  
                            </div>
                        <?php endforeach; ?>
                    </div>
                    <?php endif; ?>

                    <div class="form-row multiple-form-group mb-4 fiche" data-max=3> 
                        <div class="md-form input-group mb-3">
                            <label for="name">Fiche d'intention</label>
                            <div class="row">
                                <div class="col-md-6">
                                    <select name="url[]" class="form-control">
                                        <option value="Fiche d'indention"> Fiche d'intention</option>
                                    </select>
                                </div>
                                <div class="col-md-6">
                                    <input type="file" name="fields[]" class="form-control">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-row mb-4">
                        <label>Description activité <?= $dossier->numero ? 'ou raison demande de renouvellement' : '' ?></label>
                        <textarea class="form-control" id="editor1" name="descrip_projet" rows="2" cols="30" placeholder="Description du projet"><?= $dossier->descrip_projet ?></textarea>
                    </div>
                    
                </div>
            </div>
            <div class="row text-center">
              <div class="col-sm-12">
                <button type="submit" id="btn-save" class="_bg _shadow btn btn-primary btnRound" style="width: 250px; height: 40px;">Enregistrer</button>
              </div>
            </div>
          </div>

        </div>

        <?= $this->Form->end() ?>
        <!-- Form -->
      </div>

  </div>

</section>

<?= $this->Html->script('front/vendor/jquery/jquery.min') ?>


<script>

  $(document).ready(function () {
      //bootstrap WYSIHTML5 - text editor
      CKEDITOR.replace('editor1');
      

      if($('#verifEntreprise').val() == '0') {
          $('.bloc-entreprise').hide();
      }
      //validation

      $("#owner_type").change(function () {
        var data = $('#owner_type option:selected').val();
        if (data == "moral") {
          $(".bloc-entreprise").show(1000);
          $("#bloc-cni").hide();
        } else if (data == "physique") {
          $("#bloc-cni").show(1000);
          $(".bloc-entreprise").hide();
        }
      });

      $('.verif_demande').change(function(){
          if($(this).val() == "renouvel") {
              $('#row-code').removeClass('hide');
              $('#btn-save').attr('disabled', true);
              $('#renew').val('1');
          }else {
              $('#row-code').addClass('hide');
              $('#btn-save').attr('disabled', false);
              $('#code-dossier').val('');
              $('#renew').val('0');
          }
      });

      if($('#checkRenew').val() == '1') {
          $('#btn-save').attr('disabled', false);
      }

      $("#region_id").change(function () {

        var region_id = $('#region_id option:selected').val();
        
        <?php $url = $this->Url->build(['controller' => 'Regions', 'action' => 'view']); ?>
          $.ajax(
                  {type: 'GET',
                    url: "<?= $url; ?>/" + region_id,
                    async: false,
                    success: function (json) {
                      var data = JSON.parse(json);
                      //alert(data);
                      var infos;
                      var listdept = [];
                      infos += '<option selected="selected" value=""> Veuillez choisir le département</option>';
                      for (var i in data) {

                        listdept['nom'] = data[i]['departementscol'];
                        infos += '<option required value="' + data[i]['id'] + '">' + listdept['nom'] + '</option>';
                      }
                      $("#departement").html(infos);
                    }
                  });
        });


      $("#departement").change(function () {
          var departement = $('#departement option:selected').val();
        // alert(departement);

          <?php $url1 = $this->Url->build(['controller' => 'Departements', 'action' => 'view']); ?>
            $.ajax(
              {type: 'GET',
                url: "<?= $url1; ?>/" + departement,
                async: false,
                success: function (json) {
                  var data = JSON.parse(json);
                
                  var infos;
                  var listcommun = [];
                  infos += '<option selected="selected" value=""> Veuillez choisir la commune</option>';
                  for (var i in data) {

                    listcommun['nom'] = data[i]['nom'];
                    infos += '<option required value="' + data[i]['id'] + '">' + listcommun['nom'] + '</option>';
                  }
                  $("#commun").html(infos);
                }
            });
        });


      $("#departement").change(function () {
        $(".commune").show(1000);
      });


      $("#docoui").click(function () {
        $(".doc").show(1000);
        $(".fiche").hide(1000);
      });

      $("#docnon").click(function () {
        $(".doc").hide(1000);
        $(".fiche").show(1000);
      });
  });

(function ($) {
$(function () {

    var addFormGroup = function (event) {
        event.preventDefault();

        var $formGroup = $(this).closest('.form-group');
        var $multipleFormGroup = $formGroup.closest('.multiple-form-group');
        var $formGroupClone = $formGroup.clone();

        $(this)
            .toggleClass('btn-default btn-add btn-danger btn-remove')
            .html('–');

        $formGroupClone.find('input').val('');
        $formGroupClone.insertAfter($formGroup);

        var $lastFormGroupLast = $multipleFormGroup.find('.form-group:last');
        if ($multipleFormGroup.data('max') <= countFormGroup($multipleFormGroup)) {
            //$lastFormGroupLast.find('.btn-add').attr('disabled', true);
        }
    };

    var removeFormGroup = function (event) {
        event.preventDefault();

        var $formGroup = $(this).closest('.form-group');
        var $multipleFormGroup = $formGroup.closest('.multiple-form-group');

        var $lastFormGroupLast = $multipleFormGroup.find('.form-group:last');
        if ($multipleFormGroup.data('max') >= countFormGroup($multipleFormGroup)) {
            $lastFormGroupLast.find('.btn-add').attr('disabled', false);
        }

        $formGroup.remove();
    };

    var countFormGroup = function ($form) {
        return $form.find('.form-group').length;
    };

    $(document).on('click', '.btn-add', addFormGroup);
    $(document).on('click', '.btn-remove', removeFormGroup);

});
})(jQuery);
</script>
