<?php $this->setLayout('back_office'); ?>

<section class="my-5 mt-150 content-add-folder" style="margin-bottom:20px">
    
    <div class="card" style="border: none;">
      <div class="card-header bg-color text-white">
          <div class="col-sm-12 text-center">
            <h3 class="mb-0">SOUMISSION POUR LABÉLISATION</h3>
          </div>
      </div>
      <input type="hidden" id="verifEntreprise" value="<?= $checkEntreprise ? '1' : '0' ?>">
      <input type="hidden" id="checkStatutJuri" value="<?php if($userConnect->porteurs[0]->entreprise && $userConnect->porteurs[0]->entreprise->formel) echo '1'; else echo '0'; ?>">
      <!--Card content-->
      <div class="card-body pt-3" style="margin-bottom: 100px;">
        <?php // dd($dossier); ?>
        <input type="hidden" id="checkRenew" value="<?php if($dossier->numero) echo '1'; else echo '0'; ?>">
        <div class="container">
            <div class="form-row mb-4">
                <div class="col-md-6 form-group mb-2">
                    <div class="custom-control custom-radio">
                        <input type="radio" class="custom-control-input verif_demande" id="nv_demande" value="nv_demande" checked name="verif_demande">
                        <label class="custom-control-label" for="nv_demande">Nouvelle demande</label>

                        <span class="ml-5">
                            <input type="radio" class="custom-control-input verif_demande" id="renouvel" <?php if($dossier->numero) echo 'checked'; ?> value="renouvel" name="verif_demande">
                            <label class="custom-control-label" for="renouvel">Renouvellement</label>
                        </span>
                    </div>
                </div>
                <div class="col-md-6">
                    <?= $this->Form->create('Dossiers', array('url'=>array('controller' => 'Dossiers', 'action'=> 'add'))) ?>
                        <div class="form-row w-100 <?= !$dossier->numero ? 'hide' : '' ?>" id="row-code">
                            <div class="col-8">
                                <input type="text" name="code-dossier" id="code-dossier" value="<?= $dossier->numero ? $dossier->numero : '' ?>" placeholder="Saisir le numéro du dossier" class="form-control">
                            </div>
                            <div class="col-4">
                                <button type="submit" class="btn btn-success btn-xs">Valider</button>
                            </div>
                        </div>
                    <?= $this->Form->end() ?>
                </div>
            </div>
        </div>
        <!-- Form -->
        <?= $this->Form->create('Dossiers', array('enctype' => 'multipart/form-data', 'url'=>array('action'=> 'add'))) ?>
        <div class="input-group md-form" id="form-dossier">
          <div class="container">
            <input type="hidden" id="renew" name="renew" value="<?= $dossier->numero ? '1' : '0' ?>">
            <input type="hidden" name="code-dossier">
            <input type="hidden" name="idDossier" value="<?= $dossier->id ?>">
            <div class="row">
                <div class="col-sm-6 mb-5">
                    <div class="form-row mb-4">
                        <label for="name">L'activité est pour le compte de ? <strong class="text-danger">*</strong></label>
                        <select id="owner_type" disabled class="form-control" required>
                            <option value="physique" <?php if($userConnect->porteurs[0]->entreprise_id == null) echo 'selected'; ?>>Mon propre compte</option>
                            <option value="moral" <?php if($userConnect->porteurs[0]->entreprise_id != null) echo 'selected'; ?>>Mon entreprise</option>
                            <option value="moral" <?php if($checkAssociation) echo 'selected'; ?>>Association</option>                       
                        </select>
                    </div>
                    
                    <?php if(!$checkEntreprise): ?>
                    <div class="form-row mb-4" id="bloc-cni">
                        <label><?= $userConnect->porteurs[0]->type_identification ?><strong class="text-danger">*</strong></label>
                        <input type="number" maxlength="13" name="num_identification" value="<?= $userConnect->porteurs[0]->num_identification ?>" placeholder="Numéro d'identification" class="form-control" required>
                    </div>
                    <?php else: ?>
                    <div class="bloc-entreprise">
                        <div class="form-row mb-4">
                            <label><?php if(!$checkAssociation) echo 'Entreprise'; else echo "Nom de l'asociation"; ?> <strong class="text-danger">*</strong></label>
                            <input type="text" name="nomEntreprise" value="<?= $userConnect->porteurs[0]->entreprise->nom ?>" placeholder="Saisir le nom" class="form-control" required />
                        </div>
                        <div class="form-row mb-4 formel">
                            <label><?php if(!$checkAssociation) echo "Type d'entreprise"; else echo "Type d'association"; ?> <strong class="text-danger">*</strong></label>
                            <select name="type_entreprise_id" class="form-control" required>
                                <?php foreach($typeEntreprises as $typeEntreprise): ?>
                                    <option value="<?= $typeEntreprise->id ?>" <?php if($checkEntreprise): if($userConnect->porteurs[0]->entreprise->type_entreprise_id == $typeEntreprise->id) echo 'selected'; endif; ?>><?= $typeEntreprise->nom ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="form-row mb-4 formel">
                            <label>Ninéa <strong class="text-danger">*</strong></label>
                            <input type="text" name="ninea" required value="<?php if($checkEntreprise) echo $userConnect->porteurs[0]->entreprise->ninea; ?>" class="form-control" placeholder="Votre ninéa" />
                        </div>
                        <div class="form-row mb-4">
                            <label>Nombre d'employé <strong class="text-danger">*</strong></label>
                            <input type="number" name="nb_employe" required value="<?= $userConnect->porteurs[0]->entreprise->nb_employe ?>" class="form-control" placeholder="Saisir le nombre d'employé" />
                        </div>
                    </div>
                    <?php endif; ?>

                    <div class="form-row mb-4">
                        <div class="col-sm-4 col-12">
                            <label>Nombre de personnes concernées<strong class="text-danger">*</strong></label>
                            <input type="number" id="nbPersoConcerne" name="nb_perso_impact" min="0" required value="<?= $dossier->nb_perso_impact ?>" class="form-control" placeholder="Nombre" />
                        </div>
                        <div class="col-sm-4 col-12">
                            <label>Nombre de femmes concernées<strong class="text-danger">*</strong></label>
                            <input type="number" id="nbFemmeConcerne" name="nb_femme_impact" min="0" required value="<?= $dossier->nb_femme_impact ?>" class="form-control" placeholder="Nombre" />
                        </div>
                        <div class="col-sm-4 col-12">
                            <label>Nombre d'hommes concernés<strong class="text-danger">*</strong></label>
                            <input type="number" id="nbHommeConcerne" name="nb_homme_impact" min="0" required value="<?= $dossier->nb_homme_impact ?>" class="form-control" placeholder="Nombre" />
                        </div>
                        <small id="error-nombres" class="text-danger hide">
                            <strong>Les nombres ne sont pas corrects.</strong>
                        </small>
                    </div>
                    

                    <div class="form-row mb-4">
                        <label>Secteur du l'activité <strong class="text-danger">*</strong></label>
                        <select name="filiere_id" class="form-control" required>
                            <?php foreach($filieres as $filiere): ?>
                                <option value="<?= $filiere->id ?>" <?php if($dossier->filiere_id == $filiere->id) echo 'selected'; ?>><?= $filiere->nom ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>

                    <div class="form-row mb-4">
                        <label>Autres secteurs impactés <strong class="text-danger">*</strong></label>
                        <select name="secteurs_interesses[]" class="form-control select-multiple" multiple required>
                            <option value="">Choisissez des secteurs</option>
                            <?php foreach($filieres as $filiere): ?>
                                <option value="<?= $filiere->id ?>" <?php if($secteursInteresses): foreach($secteursInteresses as $secteur): if($filiere->id == $secteur->filieres_id) echo 'selected'; endforeach; endif; ?>>
                                    <?= $filiere->nom ?>
                                </option>
                            <?php endforeach; ?>
                        </select>
                    </div>


                    <div class="form-row mb-4">
                        <label>Sous secteurs impactés <strong class="text-danger">*</strong></label>
                        <select name="secteurs_relations[]" class="form-control select-multiple" multiple required>
                            <option value="">Choisissez des secteurs</option>
                            <?php foreach($filieres as $filiere): ?>
                                <option value="<?= $filiere->id ?>" <?php if($secteursEnRelations): foreach($secteursEnRelations as $secteur): if($filiere->id == $secteur->filieres_id) echo 'selected'; endforeach; endif; ?>>
                                    <?= $filiere->nom ?>
                                </option>
                            <?php endforeach; ?>
                        </select>
                    </div>

                    <div class="form-row mb-4">
                        <label>L'activité sera exécuté dans quelle région ?<strong class="text-danger">*</strong></label>
                        <select id="region_id" class="form-control" required>
                            <option value="">Veuillez choisir la région</option>
                            <?php foreach($regions as $region): ?>
                                <option value="<?= $region->id ?>" <?php if($dossier->commune && $dossier->commune->departement->region_id == $region->id) echo 'selected'; ?>><?= $region->nom ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>

                    <div class="form-row mb-4">
                        <label for="name">Veuillez choisir le département de la région<strong class="text-danger">*</strong></label>
                        <select type="text" id ="departement"  name="departement_id" class="form-control js-example-basic" required>
                            <option selected="selected" disabled value="">Liste des départements</option>
                            <option value="<?php if($dossier->commune) echo $dossier->commune->departement_id; ?>" <?php if($dossier->commune && $dossier->commune->departement) echo 'selected'; ?>><?php if($dossier->commune) echo $dossier->commune->departement->departementscol; ?></option>
                        </select>
                    </div>

                    <div class="form-row mb-4 commune">
                        <label>Veuillez donner le nom de la commune du département<strong class="text-danger">*</strong></label>
                        <select name="nomcom" id="commun" class="form-control" required>
                            <option disabled value="">Liste des communes</option>
                            <option value="<?php if($dossier->commune) echo $dossier->commune_id; ?>" <?php if($dossier->commune) echo 'selected'; ?>><?php if($dossier->commune) echo $dossier->commune->nom; ?></option>
                        </select>
                    </div>

                    <div class="form-row mb-4">
                        <label class="font-weight-bold">Impact social<strong class="text-danger">*</strong></label>
                        <select name="impacts_sociales[]" class="form-control select-multiple" multiple required>
                            <?php foreach($typesImpacts as $typeImpact):
                                if($typeImpact->impactes_id == 1): ?>
                                    <option value="<?= $typeImpact->id ?>" <?php if($dossiersImpacts): foreach($dossiersImpacts as $dossierImpact): if($typeImpact->id == $dossierImpact->types_impacts_id) echo 'selected'; endforeach; endif; ?> >
                                        <?= $typeImpact->libelle ?>
                                    </option>
                            <?php endif; endforeach; ?>
                        </select>
                    </div>

                    <div class="form-row mb-4">
                        <label class="font-weight-bold">Impact environnemental<strong class="text-danger">*</strong></label>
                        <select name="impacts_environes[]" class="form-control select-multiple" multiple required>
                            <?php foreach($typesImpacts as $typeImpact):
                                if($typeImpact->impactes_id == 2): ?>
                                    <option value="<?= $typeImpact->id ?>" <?php if($dossiersImpacts): foreach($dossiersImpacts as $dossierImpact): if($typeImpact->id == $dossierImpact->types_impacts_id) echo 'selected'; endforeach; endif; ?>>
                                        <?= $typeImpact->libelle ?>
                                    </option>
                            <?php endif; endforeach; ?>
                        </select>
                    </div>

                    

                </div>
                <!-- Col -->
                <div class="col-sm-6">
                    <div class="form-row mb-4">
                        <label>Intitulé de l'activité<strong class="text-danger">*</strong></label>
                        <input type="text" name="intitule" value="<?= $dossier->intitule ?>" class="form-control" required />
                    </div>

                    <?php if($checkEntreprise): ?>
                    <div class="form-row mb-4">
                        <label>Statut juridique<strong class="text-danger">*</strong></label>
                        <select name="statut-juridique" class="form-control" id="statut-juridique" required>
                            <option value="">Choisissez le statut</option>
                            <option value="Formel" <?php if($userConnect->porteurs[0]->entreprise && $userConnect->porteurs[0]->entreprise->formel) echo 'selected'; ?>>Formel</option>
                            <option value="Informel" <?php if($userConnect->porteurs[0]->entreprise && !$userConnect->porteurs[0]->entreprise->formel) echo 'selected'; ?>>Informel<option>
                        </select>
                    </div>
                    <div class="form-row mb-4">
                        <label>Immatriculation<strong class="text-danger">*</strong></label>
                        <input type="text" name="immatriculation" required value="<?= $userConnect->porteurs[0]->entreprise->immatriculation ?>" class="form-control" />
                    </div>
                    <?php endif; ?>

                    <label>Avez vous des documents concernant l'activité ?</label>
                    <div class="form-row mb-4 ml-1">
                        <span class="mr-2">OUI <input type="radio" name="doc" <?php if($dossier->piece_jointes) echo 'checked'; ?> value="oui" id="docoui" /></span>
                        <span>NON <input type="radio" name="doc" value="nom" id="docnon" /></span>
                    </div>

                    <div class="form-row form-group multiple-form-group mb-4 doc" data-max=3>

                        <?php if(!$dossier->piece_jointes): ?>
                        <div class="md-form input-group mb-3">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>La nature du document</label>
                                    <select name="url[]" class="form-control">
                                        <option value="" selected="">--Nature du document--</option>
                                        <?php foreach($typesPieces as $type): ?>
                                            <option value="<?= $type->libelle ?>">
                                                <?= $type->libelle ?>
                                            </option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="col-md-6">
                                    <label>Ajout doc (+)</label>
                                    <span style="display: flex;">
                                        <input type="file" name="fields[]" class="form-control inputFileNatureDoc">
                                        <span class="input-group-btn">
                                            <button type="button" class="btn btn-primary btn-add">+</button>
                                        </span>
                                    </span>
                                </div>
                            </div>  
                        </div>
                        <?php else: 
                            foreach($dossier->piece_jointes as $piece): ?>
                                <div class="md-form input-group mb-3">
                                    <label>La nature du document</label>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <select name="url[]" class="form-control">
                                                <option value="" selected="">--Nature du document--</option>
                                                <?php foreach($typesPieces as $type): ?>
                                                    <option value="<?= $type->libelle ?>" <?php if($dossier->piece_jointes): if($piece->titre == $type->libelle) echo 'selected'; endif; ?>>
                                                        <?= $type->libelle ?>
                                                    </option>
                                                <?php endforeach; ?>
                                            </select>
                                            <?= $this->Html->link(__(' Voir'),'/documents/dossier/'.$piece->url,
                                                array('target'=>'_blank','escape'=>false, 'class'=>'fa fa-eye btn btn-warning btn-sm')) ?>
                                        </div>
                                        <div class="col-md-5" style="display: flex;">
                                            <input type="file" name="fields[]" class="form-control inputFileNatureDoc">
                                            <span class="input-group-btn">
                                                <button type="button" class="btn btn-primary btn-add">+</button>
                                            </span>
                                        </div>
                                    </div>  
                                </div>
                        <?php endforeach; endif; ?>
                    </div>

                    <div class="form-row multiple-form-group mb-4 fiche" data-max=3> 
                        <div class="md-form input-group mb-3">
                            <label for="name">Fiche d'intention</label>
                            <div class="row">
                                <div class="col-md-6">
                                    <select name="url[]" class="form-control">
                                        <option value="Fiche d'indention"> Fiche d'intention</option>
                                    </select>
                                </div>
                                <div class="col-md-6">
                                    <input type="file" name="fields[]" class="form-control">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-row mb-4">
                        <label>Description activité <?= $dossier->numero ? 'ou raison demande de renouvellement' : '' ?></label>
                        <textarea class="form-control" id="editor1" name="descrip_projet" rows="2" cols="30" placeholder="Description du projet"><?= $dossier->descrip_projet ?></textarea>
                        <br><small class="text-success"><strong>Nombre de mots autorisés (infini)</strong></small>
                    </div>
                    
                </div>
            </div>
            <div class="row text-center">
              <div class="col-sm-12">
                <button type="submit" id="btn-save" disabled class="_bg _shadow btn btn-primary btnRound" style="width: 250px; height: 40px;">Enregistrer</button>
              </div>
            </div>
          </div>

        </div>

        <?= $this->Form->end() ?>
        <!-- Form -->
      </div>

  </div>

</section>


<?= $this->Html->script('front/vendor/jquery/jquery.min') ?>

<script type="text/javascript">
    $(window).on('load',function(){
        if($('#checkResult').val() == 0){
            $('#modalSuccess').modal({
                backdrop: 'static',
                keyboard: false 
            });
        }
    });
</script>

<script>

  $(document).ready(function () {
      //bootstrap WYSIHTML5 - text editor
      CKEDITOR.replace('editor1');

      $('#nbPersoConcerne').keyup(function(){
          if(parseInt($(this).val()) == parseInt($('#nbFemmeConcerne').val()) + parseInt($('#nbHommeConcerne').val())) {
              $('#error-nombres').addClass('hide');
              $('#btn-save').removeAttr('disabled');
          }else {
              $('#error-nombres').removeClass('hide');
              $('#btn-save').attr('disabled', true);
          }
      });
      $('#nbFemmeConcerne').keyup(function(){
          if(parseInt($(this).val()) + parseInt($('#nbHommeConcerne').val()) == parseInt($('#nbPersoConcerne').val())) {
              $('#error-nombres').addClass('hide');
              $('#btn-save').removeAttr('disabled');
          }else {
              $('#error-nombres').removeClass('hide');
              $('#btn-save').attr('disabled', true);
          }
      });
      $('#nbHommeConcerne').keyup(function(){
          if(parseInt($(this).val()) + parseInt($('#nbFemmeConcerne').val()) == parseInt($('#nbPersoConcerne').val())) {
              $('#error-nombres').addClass('hide');
              $('#btn-save').removeAttr('disabled');
          }else {
              $('#error-nombres').removeClass('hide');
              $('#btn-save').attr('disabled', true);
          }
      });

      if($('#checkStatutJuri').val() == '1') {
          $('.formel').show();
      }else {
          $('.formel').hide();
      }

      $('#statut-juridique').change(function(){
         if($(this).val() == "Formel") {
            $('.formel').show();
         }else {
            $('.formel').hide();
         }
      }); 
      
      if($('#checkRenew').val() != '1') {
          $('.commune').hide();
          $(".doc").hide();
      }

      if($('#verifEntreprise').val() == '0') {
          $('.bloc-entreprise').hide();
      }
      //validation

      $("#owner_type").change(function () {
        var data = $('#owner_type option:selected').val();
        if (data == "moral") {
          $(".bloc-entreprise").show(1000);
          $("#bloc-cni").hide();
        } else if (data == "physique") {
          $("#bloc-cni").show(1000);
          $(".bloc-entreprise").hide();
        }
      });

      $('.verif_demande').change(function(){
          if($(this).val() == "renouvel") {
              $('#row-code').removeClass('hide');
              $('#btn-save').attr('disabled', true);
              $('#renew').val('1');
          }else {
              $('#row-code').addClass('hide');
              $('#btn-save').attr('disabled', false);
              $('#code-dossier').val('');
              $('#renew').val('0');
          }
      });

      if($('#checkRenew').val() == '1') {
          $('#btn-save').attr('disabled', false);
      }

      $("#region_id").change(function () {

        var region_id = $('#region_id option:selected').val();
        
        <?php $url = $this->Url->build(['controller' => 'Regions', 'action' => 'view']); ?>
          $.ajax(
                  {type: 'GET',
                    url: "<?= $url; ?>/" + region_id,
                    async: false,
                    success: function (json) {
                      var data = JSON.parse(json);
                      //alert(data);
                      var infos;
                      var listdept = [];
                      infos += '<option selected="selected" value=""> Veuillez choisir le département</option>';
                      for (var i in data) {

                        listdept['nom'] = data[i]['departementscol'];
                        infos += '<option required value="' + data[i]['id'] + '">' + listdept['nom'] + '</option>';
                      }
                      $("#departement").html(infos);
                    }
                  });
        });


      $("#departement").change(function () {
          var departement = $('#departement option:selected').val();
        // alert(departement);

          <?php $url1 = $this->Url->build(['controller' => 'Departements', 'action' => 'view']); ?>
            $.ajax(
              {type: 'GET',
                url: "<?= $url1; ?>/" + departement,
                async: false,
                success: function (json) {
                  var data = JSON.parse(json);
                
                  var infos;
                  var listcommun = [];
                  infos += '<option selected="selected" value=""> Veuillez choisir la commune</option>';
                  for (var i in data) {

                    listcommun['nom'] = data[i]['nom'];
                    infos += '<option required value="' + data[i]['id'] + '">' + listcommun['nom'] + '</option>';
                  }
                  $("#commun").html(infos);
                }
            });
        });


      $("#departement").change(function () {
        $(".commune").show(1000);
      });


      $("#docoui").click(function () {
        $(".doc").show(1000);
        $(".fiche").hide(1000);
      });

      $("#docnon").click(function () {
        $(".doc").hide(1000);
        $(".fiche").show(1000);
      });
  });

(function ($) {
$(function () {

    var addFormGroup = function (event) {
        event.preventDefault();

        var $formGroup = $(this).closest('.form-group');
        var $multipleFormGroup = $formGroup.closest('.multiple-form-group');
        var $formGroupClone = $formGroup.clone();

        $(this)
            .toggleClass('btn-default btn-add btn-danger btn-remove')
            .html('–');

        $formGroupClone.find('input').val('');
        $formGroupClone.insertAfter($formGroup);

        var $lastFormGroupLast = $multipleFormGroup.find('.form-group:last');
        if ($multipleFormGroup.data('max') <= countFormGroup($multipleFormGroup)) {
            //$lastFormGroupLast.find('.btn-add').attr('disabled', true);
        }
    };

    var removeFormGroup = function (event) {
        event.preventDefault();

        var $formGroup = $(this).closest('.form-group');
        var $multipleFormGroup = $formGroup.closest('.multiple-form-group');

        var $lastFormGroupLast = $multipleFormGroup.find('.form-group:last');
        if ($multipleFormGroup.data('max') >= countFormGroup($multipleFormGroup)) {
            $lastFormGroupLast.find('.btn-add').attr('disabled', false);
        }

        $formGroup.remove();
    };

    var countFormGroup = function ($form) {
        return $form.find('.form-group').length;
    };

    $(document).on('click', '.btn-add', addFormGroup);
    $(document).on('click', '.btn-remove', removeFormGroup);

});
})(jQuery);
</script>
