<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Bibliotheque $bibliotheque
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $bibliotheque->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $bibliotheque->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Bibliotheques'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="bibliotheques form large-9 medium-8 columns content">
    <?= $this->Form->create($bibliotheque) ?>
    <fieldset>
        <legend><?= __('Edit Bibliotheque') ?></legend>
        <?php
            echo $this->Form->control('libelle');
            echo $this->Form->control('photo');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
