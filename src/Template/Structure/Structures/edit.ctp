<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Structure $structure
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $structure->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $structure->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Structures'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Agences'), ['controller' => 'Agences', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Agence'), ['controller' => 'Agences', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Agents'), ['controller' => 'Agents', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Agent'), ['controller' => 'Agents', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Assignations'), ['controller' => 'Assignations', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Assignation'), ['controller' => 'Assignations', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Ots'), ['controller' => 'Ots', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Ot'), ['controller' => 'Ots', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="structures form large-9 medium-8 columns content">
    <?= $this->Form->create($structure) ?>
    <fieldset>
        <legend><?= __('Edit Structure') ?></legend>
        <?php
            echo $this->Form->control('nom');
            echo $this->Form->control('telephone');
            echo $this->Form->control('adresse');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
