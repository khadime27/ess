<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Structure $structure
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Structure'), ['action' => 'edit', $structure->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Structure'), ['action' => 'delete', $structure->id], ['confirm' => __('Are you sure you want to delete # {0}?', $structure->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Structures'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Structure'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Agences'), ['controller' => 'Agences', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Agence'), ['controller' => 'Agences', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Agents'), ['controller' => 'Agents', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Agent'), ['controller' => 'Agents', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Assignations'), ['controller' => 'Assignations', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Assignation'), ['controller' => 'Assignations', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Ots'), ['controller' => 'Ots', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Ot'), ['controller' => 'Ots', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="structures view large-9 medium-8 columns content">
    <h3><?= h($structure->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Nom') ?></th>
            <td><?= h($structure->nom) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Telephone') ?></th>
            <td><?= h($structure->telephone) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Adresse') ?></th>
            <td><?= h($structure->adresse) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($structure->id) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Agences') ?></h4>
        <?php if (!empty($structure->agences)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Nom') ?></th>
                <th scope="col"><?= __('Structure Id') ?></th>
                <th scope="col"><?= __('Telephone') ?></th>
                <th scope="col"><?= __('Adresse') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($structure->agences as $agences): ?>
            <tr>
                <td><?= h($agences->id) ?></td>
                <td><?= h($agences->nom) ?></td>
                <td><?= h($agences->structure_id) ?></td>
                <td><?= h($agences->telephone) ?></td>
                <td><?= h($agences->adresse) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Agences', 'action' => 'view', $agences->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Agences', 'action' => 'edit', $agences->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Agences', 'action' => 'delete', $agences->id], ['confirm' => __('Are you sure you want to delete # {0}?', $agences->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Agents') ?></h4>
        <?php if (!empty($structure->agents)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Prenom') ?></th>
                <th scope="col"><?= __('Nom') ?></th>
                <th scope="col"><?= __('Telephone') ?></th>
                <th scope="col"><?= __('Adresse') ?></th>
                <th scope="col"><?= __('Ot Id') ?></th>
                <th scope="col"><?= __('User Id') ?></th>
                <th scope="col"><?= __('Agent Id') ?></th>
                <th scope="col"><?= __('Agence Id') ?></th>
                <th scope="col"><?= __('Structure Id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($structure->agents as $agents): ?>
            <tr>
                <td><?= h($agents->id) ?></td>
                <td><?= h($agents->prenom) ?></td>
                <td><?= h($agents->nom) ?></td>
                <td><?= h($agents->telephone) ?></td>
                <td><?= h($agents->adresse) ?></td>
                <td><?= h($agents->ot_id) ?></td>
                <td><?= h($agents->user_id) ?></td>
                <td><?= h($agents->agent_id) ?></td>
                <td><?= h($agents->agence_id) ?></td>
                <td><?= h($agents->structure_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Agents', 'action' => 'view', $agents->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Agents', 'action' => 'edit', $agents->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Agents', 'action' => 'delete', $agents->id], ['confirm' => __('Are you sure you want to delete # {0}?', $agents->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Assignations') ?></h4>
        <?php if (!empty($structure->assignations)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Ot Id') ?></th>
                <th scope="col"><?= __('Agence Id') ?></th>
                <th scope="col"><?= __('Structure Id') ?></th>
                <th scope="col"><?= __('Agent Id') ?></th>
                <th scope="col"><?= __('Dossier Id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($structure->assignations as $assignations): ?>
            <tr>
                <td><?= h($assignations->id) ?></td>
                <td><?= h($assignations->ot_id) ?></td>
                <td><?= h($assignations->agence_id) ?></td>
                <td><?= h($assignations->structure_id) ?></td>
                <td><?= h($assignations->agent_id) ?></td>
                <td><?= h($assignations->dossier_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Assignations', 'action' => 'view', $assignations->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Assignations', 'action' => 'edit', $assignations->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Assignations', 'action' => 'delete', $assignations->id], ['confirm' => __('Are you sure you want to delete # {0}?', $assignations->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Ots') ?></h4>
        <?php if (!empty($structure->ots)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Nom') ?></th>
                <th scope="col"><?= __('Adresse') ?></th>
                <th scope="col"><?= __('Telephone') ?></th>
                <th scope="col"><?= __('Structure Id') ?></th>
                <th scope="col"><?= __('Type') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($structure->ots as $ots): ?>
            <tr>
                <td><?= h($ots->id) ?></td>
                <td><?= h($ots->nom) ?></td>
                <td><?= h($ots->adresse) ?></td>
                <td><?= h($ots->telephone) ?></td>
                <td><?= h($ots->structure_id) ?></td>
                <td><?= h($ots->type) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Ots', 'action' => 'view', $ots->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Ots', 'action' => 'edit', $ots->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Ots', 'action' => 'delete', $ots->id], ['confirm' => __('Are you sure you want to delete # {0}?', $ots->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
