<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Structure[]|\Cake\Collection\CollectionInterface $structures
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Structure'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Agences'), ['controller' => 'Agences', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Agence'), ['controller' => 'Agences', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Agents'), ['controller' => 'Agents', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Agent'), ['controller' => 'Agents', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Assignations'), ['controller' => 'Assignations', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Assignation'), ['controller' => 'Assignations', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Ots'), ['controller' => 'Ots', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Ot'), ['controller' => 'Ots', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="structures index large-9 medium-8 columns content">
    <h3><?= __('Structures') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('nom') ?></th>
                <th scope="col"><?= $this->Paginator->sort('telephone') ?></th>
                <th scope="col"><?= $this->Paginator->sort('adresse') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($structures as $structure): ?>
            <tr>
                <td><?= $this->Number->format($structure->id) ?></td>
                <td><?= h($structure->nom) ?></td>
                <td><?= h($structure->telephone) ?></td>
                <td><?= h($structure->adresse) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $structure->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $structure->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $structure->id], ['confirm' => __('Are you sure you want to delete # {0}?', $structure->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
