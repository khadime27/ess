<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Agence $agence
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Agence'), ['action' => 'edit', $agence->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Agence'), ['action' => 'delete', $agence->id], ['confirm' => __('Are you sure you want to delete # {0}?', $agence->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Agences'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Agence'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Structures'), ['controller' => 'Structures', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Structure'), ['controller' => 'Structures', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Agents'), ['controller' => 'Agents', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Agent'), ['controller' => 'Agents', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Assignations'), ['controller' => 'Assignations', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Assignation'), ['controller' => 'Assignations', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="agences view large-9 medium-8 columns content">
    <h3><?= h($agence->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Nom') ?></th>
            <td><?= h($agence->nom) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Structure') ?></th>
            <td><?= $agence->has('structure') ? $this->Html->link($agence->structure->id, ['controller' => 'Structures', 'action' => 'view', $agence->structure->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Telephone') ?></th>
            <td><?= h($agence->telephone) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Adresse') ?></th>
            <td><?= h($agence->adresse) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($agence->id) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Agents') ?></h4>
        <?php if (!empty($agence->agents)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Prenom') ?></th>
                <th scope="col"><?= __('Nom') ?></th>
                <th scope="col"><?= __('Telephone') ?></th>
                <th scope="col"><?= __('Adresse') ?></th>
                <th scope="col"><?= __('Ot Id') ?></th>
                <th scope="col"><?= __('User Id') ?></th>
                <th scope="col"><?= __('Agent Id') ?></th>
                <th scope="col"><?= __('Agence Id') ?></th>
                <th scope="col"><?= __('Structure Id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($agence->agents as $agents): ?>
            <tr>
                <td><?= h($agents->id) ?></td>
                <td><?= h($agents->prenom) ?></td>
                <td><?= h($agents->nom) ?></td>
                <td><?= h($agents->telephone) ?></td>
                <td><?= h($agents->adresse) ?></td>
                <td><?= h($agents->ot_id) ?></td>
                <td><?= h($agents->user_id) ?></td>
                <td><?= h($agents->agent_id) ?></td>
                <td><?= h($agents->agence_id) ?></td>
                <td><?= h($agents->structure_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Agents', 'action' => 'view', $agents->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Agents', 'action' => 'edit', $agents->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Agents', 'action' => 'delete', $agents->id], ['confirm' => __('Are you sure you want to delete # {0}?', $agents->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Assignations') ?></h4>
        <?php if (!empty($agence->assignations)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Ot Id') ?></th>
                <th scope="col"><?= __('Agence Id') ?></th>
                <th scope="col"><?= __('Structure Id') ?></th>
                <th scope="col"><?= __('Agent Id') ?></th>
                <th scope="col"><?= __('Dossier Id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($agence->assignations as $assignations): ?>
            <tr>
                <td><?= h($assignations->id) ?></td>
                <td><?= h($assignations->ot_id) ?></td>
                <td><?= h($assignations->agence_id) ?></td>
                <td><?= h($assignations->structure_id) ?></td>
                <td><?= h($assignations->agent_id) ?></td>
                <td><?= h($assignations->dossier_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Assignations', 'action' => 'view', $assignations->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Assignations', 'action' => 'edit', $assignations->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Assignations', 'action' => 'delete', $assignations->id], ['confirm' => __('Are you sure you want to delete # {0}?', $assignations->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
