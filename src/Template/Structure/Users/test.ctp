<?php
use Cake\Cache\Cache;
use Cake\Core\Configure;
use Cake\Core\Plugin;
use Cake\Datasource\ConnectionManager;
use Cake\Error\Debugger;
use Cake\Http\Exception\NotFoundException;

$this->layout = 'default';

$cakeDescription = 'CakePHP: the rapid development PHP framework';
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $cakeDescription ?>
    </title>
    <?= $this->Html->css('test.css') ?>

    <link href="https://fonts.googleapis.com/css?family=Raleway:500i|Roboto:300,400,700|Roboto+Mono" rel="stylesheet">
</head>
<body>

    <div class="row">
        <h1>Plugin pgwSlider</h1> <a href="https://pgwjs.com/pgwslider/" target="_blank">Documentation</a>
        <ul class="pgwSlider" style="margin-bottom: 20px;">
            <li><img src="https://static.pgwjs.com/img/pg/slider/paris.jpg" alt="Paris, France" data-description="Eiffel Tower and Champ de Mars"></li>
            <li><img src="https://static.pgwjs.com/img/pg/slider/shanghai.jpg" alt="Shangai" data-description="Lorem ipsum dolor sit amet, consectetur adipiscing" data-large-src="https://static.pgwjs.com/img/pg/slider/shanghai.jpg"></li>
            <li>
                <img src="https://static.pgwjs.com/img/pg/slider/new-york.jpg" data-description="Lorem ipsum dolor sit amet, consectetur adipiscing">
                <span>New York, USA</span>
            </li>
            <li>
                <a href="#">
                    <img src="https://static.pgwjs.com/img/pg/slider/montreal.jpg" data-description="Lorem ipsum dolor sit amet, consectetur adipiscing">
                    <span>Montréal, Canada</span>
                </a>
            </li>
        </ul>
    </div>

    <div class="row" style="margin-bottom: 25px;">
        <div class="col-md-1 rowArrowsInCarousel">
            <a class="prev" role="button" style="cursor: pointer;">
              <?= $this->Html->image('https://img.icons8.com/flat_round/64/000000/back.png', ['style' => 'width: 50px;']) ?>
              <span class="sr-only">Précédent</span>
            </a>
        </div>
        <div class="col-md-10">
            <h1>Plugin Slick</h1> <a href="http://kenwheeler.github.io/slick/" target="blanck">Documentation : Slick</a>
            <div class='carouselSlick'>
                <div><?= $this->Html->image('https://upload.wikimedia.org/wikipedia/commons/d/d9/Big_Bear_Valley%2C_California.jpg', ['alt' => 'Non disponible', 'class' => 'img-responsive', 'style' => 'width: 100%; height: 350px']) ?></div>
                <div><?= $this->Html->image('https://upload.wikimedia.org/wikipedia/commons/thumb/3/36/Sonoma_Lake_aerial_view.jpg/640px-Sonoma_Lake_aerial_view.jpg', ['alt' => 'Non disponible', 'class' => 'img-responsive', 'style' => 'width: 100%; height: 350px']) ?></div>
                <div><?= $this->Html->image('https://d12dkjq56sjcos.cloudfront.net/pub/media/wysiwyg/paris/01-city-landing/Paris-Skyline-Summer-Big-Bus-Tours.jpg', ['alt' => 'Non disponible', 'class' => 'img-responsive', 'style' => 'width: 100%; height: 350px']) ?></div>
                <div><?= $this->Html->image('http://valencuba.com/wp-content/uploads/2017/12/hawai-skyview.jpeg', ['alt' => 'Non disponible', 'class' => 'img-responsive', 'style' => 'width: 100%; height: 350px']) ?></div>
            </div>
        </div>
        <div class="col-md-1 rowArrowsInCarousel">
            <a class="next" role="button" style="cursor: pointer;">
                <?= $this->Html->image('https://img.icons8.com/flat_round/64/000000/circled-chevron-right.png', ['style' => 'width: 50px;']) ?>
                <span class="sr-only">Suivant</span>
            </a>
        </div>

    </div>

    <div class="row">
        <div class="col-sm-10 offset-1">
            <h1>Plugin FancyBox</h1> <a href="https://fancyapps.com/fancybox/3/" target="blanck">Documentation : FancyBox</a>
            <p class="imglist">
                <a href="https://source.unsplash.com/IvfoDk30JnI/1500x1000" data-fancybox data-caption="&lt;b&gt;Single photo&lt;/b&gt;&lt;br /&gt;Caption can contain &lt;em&gt;any&lt;/em&gt; HTML elements">
                    <img src="https://source.unsplash.com/IvfoDk30JnI/240x160" />
                </a>

                <a href="https://source.unsplash.com/0JYgd2QuMfw/1500x1000" data-fancybox data-caption="This image has a simple caption">
                    <img src="https://source.unsplash.com/0JYgd2QuMfw/240x160" />
                </a>

                <a href="https://source.unsplash.com/xAgvgQpYsf4/1500x1000" data-fancybox>
                    <img src="https://source.unsplash.com/xAgvgQpYsf4/240x160" />
                </a>
            </p>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-10 offset-1">
            <h1>FancyBox (Groupe d'éléments)</h1>
            <p class="imglist" style="max-width: 1000px;">
                <a href="https://source.unsplash.com/juHayWuaaoQ/1500x1000" data-fancybox="images" data-caption="Backpackers following a dirt trail">
                    <img src="https://source.unsplash.com/juHayWuaaoQ/240x160" />
                </a>

                <a href="https://source.unsplash.com/eWFdaPRFjwE/1500x1000" data-fancybox="images" data-caption="Mallorca, Llubí, Spain">
                    <img src="https://source.unsplash.com/eWFdaPRFjwE/240x160" />
                </a>
                
                <a href="https://source.unsplash.com/c1JxO-uAZd0/1500x1000" data-fancybox="images" data-caption="Danish summer">
                    <img src="https://source.unsplash.com/c1JxO-uAZd0/240x160" />
                </a>

            </p>
        </div>
    </div>

    <div class="row" style="margin-bottom: 20px;">
        <div class="col-sm-10 offset-1">
            <h1>Plugin sweetAlert</h1>
            <a href="https://sweetalert.js.org/guides/" taget="blanck">Documentation sweetAlert</a>
            <p>
                <a id="test" href="javascript:;" class="btn btn-primary">Ouvrir iframe</a>
                <a id="alertSuccess" href="javascript:;" class="btn btn-success">Alert success</a>
                <a id="alertDanger" href="javascript:;" class="btn btn-danger">Alert warning</a>
            </p>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-10 offset-1">
            <table id="table_id" class="display">
                <thead>
                    <tr>
                        <th>Column 1</th>
                        <th>Column 2</th>
                        <th>Column 3</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Row 1 Data 1</td>
                        <td>Row 1 Data 2</td>
                        <td>Row 1 Data 3</td>
                    </tr>
                    <tr>
                        <td>Row 2 Data 1</td>
                        <td>Row 2 Data 2</td>
                        <td>Row 1 Data 3</td>
                    </tr>
                    <tr>
                        <td>Row 3 Data 1</td>
                        <td>Row 3 Data 2</td>
                        <td>Row 1 Data 3</td>
                    </tr>
                    
                </tbody>
            </table>
        </div>
        
    </div>

    <div class="row" style="margin-bottom: 30px;">
        <div class="col-sm-10 offset-1">
            <h1>jQuery UI (Autocomplete) </h1> <a href="http://jqueryui.com/autocomplete/" target="blank"> Documentation : jQuery UI</a>
            <div class="ui-widget">
                <input id="tags" class="form-control form-control-lg" type="text" placeholder="Langage de programmation">
            </div>
        </div>
    </div>

    <div class="row" style="margin-bottom: 20px;">
        <div class="col-sm-10 offset-1">
            <h1>jQuery UI (Resizable) </h1> <a href="http://jqueryui.com/resizable/" target="blank"> Documentation : jQuery UI</a>
            <div id="resizable" class="ui-widget-content">
                <h3 class="ui-widget-header">Resizable</h3>
            </div>
        </div>
    </div>

    <div class="row" style="margin-bottom: 20px;">
        <div class="col-sm-10 offset-1">
            <h1>Notify.js </h1> <a href="https://notifyjs.jpillora.com/" target="blank"> Documentation</a><br/>
            <button type="button" class="btn btn-purple pos-demo" style="border-radius: 25px;">
                <i class="fas fa-thumbs-up pr-2" aria-hidden="true"></i>Click for notification
            </button>
        </div>
    </div>

    <div class="row" style="margin-bottom: 20px;">
        <div class="col-sm-10 offset-1">
            <h1>Plugin Ckeditor</h1>
            <textarea class="form-control" id="editor1" name="description" rows="2" cols="30" placeholder="Description"></textarea>
        </div>
    </div>

    <div class="row" style="margin-bottom: 20px;">
        <div class="col-sm-10 offset-1">
            <h1>Slider</h1>  
            <div id="slider"></div>
        </div>
    </div>

    <div class="row" style="margin-bottom: 200px;">
        <div class="col-sm-10 offset-1">
            <h1>Progressbar</h1> <a href="http://api.jqueryui.com/progressbar/" target="_blank" >Documentation</a>
            <div id="progressbar"></div>
        </div>
    </div>

</body>
</html>
