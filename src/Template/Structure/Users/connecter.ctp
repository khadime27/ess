
<div class="row" style="margin-top: 30px;">
    <!-- Default form login -->
    <div class="col-md-4 offset-md-4 col-xs-12">
        <div class="users text-center border border-light p-5">
            <?= $this->Form->create() ?>

                <p class="h4 mb-4">Connexion</p>

                <?= $this->Flash->render() ?><br/>
                <!-- Email -->
                <input type="email" name="email" id="defaultLoginFormEmail" class="form-control mb-4" placeholder="E-mail">

                <!-- Password -->
                <input type="password" name="password" id="defaultLoginFormPassword" class="form-control mb-4" placeholder="Password">

                <div class="d-flex justify-content-around">
                    <div>
                        <!-- Remember me -->
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="defaultLoginFormRemember">
                            <label class="custom-control-label" for="defaultLoginFormRemember">Se souvenir de moi.</label>
                        </div>
                    </div>
                    <div>
                        <!-- Forgot password -->
                        <a href="<?php echo $this->Url->build(array('controller' => 'pages', 'action' => 'forgot_password')); ?>">Mot de passe oublié ?</a>
                    </div>
                </div>

                <!-- Sign in button -->
                <button class="btn btn-info btn-block my-4" type="submit">Connexion</button>

                <!-- Register -->
                <p>Pas de compte ?
                    <a href="<?php echo $this->Url->build(array('controller' => 'users', 'action' => 'add')); ?>">S'inscrire ici</a>
                </p>

                <!-- Social login -->
                <p>Ou s'inscrir avec :</p>

                <a type="button" class="light-blue-text mx-2">
                    <i class="fab fa-facebook-f"></i>
                </a>
                <a type="button" class="light-blue-text mx-2">
                    <i class="fab fa-twitter"></i>
                </a>
                <a type="button" class="light-blue-text mx-2">
                    <i class="fab fa-linkedin-in"></i>
                </a>
                <a type="button" class="light-blue-text mx-2">
                    <i class="fab fa-github"></i>
                </a>

            <?= $this->Form->end() ?>
        </div>
    <!-- Default form login -->
    </div>
</div>