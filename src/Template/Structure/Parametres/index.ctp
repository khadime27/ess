<?php $this->setLayout('back_office'); ?>
<div class="app-page-title">
    
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            <div class="page-title-icon">
                <i class="fa fa-cog"></i>
            </div>
            <div>PARAMETRAGE :
                <span class="font-weight-bold text-success text-uppercase">Espace superadmin</span>
                <div class="page-title-subheading"></div>
            </div>
        </div>
        
        <div class="page-title-actions">
            <button onclick="window.history.go(-1)" class="btn btn-primary">
                <i class="fa fa-arrow-left fa-w-20"></i> Retour
            </button>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-3">
        <a href="<?php echo $this->Url->build(array('controller' => 'Parametres', 'action' => 'filieres')); ?>">
            <div class="card card-param">
                <div class="card-body text-center">
                    <h6><i class="fa fa-cog"></i> <br>SECTEURS D'ACTIVITÉS</h6>
                </div>
            </div>
        </a>
    </div>
    <div class="col-sm-3">
        <a href="<?php echo $this->Url->build(array('controller' => 'Agences', 'action' => 'index')); ?>">
            <div class="card card-param">
                <div class="card-body text-center">
                    <h6><i class="fa fa-home"></i> <br>GESTION DES SERVICES</h6>
                </div>
            </div>
        </a>
    </div>
    <div class="col-sm-3">
        <a href="<?php echo $this->Url->build(array('controller' => 'Agences', 'action' => 'comites')); ?>">
            <div class="card card-param">
                <div class="card-body text-center">
                    <h6><i class="fa fa-map-marker"></i> <br>GESTION DES COMITÉS</h6>
                </div>
            </div>
        </a>
    </div>
    <div class="col-sm-3">
        <a href="#">
            <div class="card card-param">
                <div class="card-body text-center">
                    <h6><i class="fa fa-building"></i> <br>TYPES DE STRUCTURES</h6>
                </div>
            </div>
        </a>
    </div>
    
</div>

<div class="row mt-4">
    <div class="col-sm-3">
        <a href="<?php echo $this->Url->build(array('controller' => 'Bibliotheques', 'action' => 'index')); ?>">
            <div class="card card-param">
                <div class="card-body text-center">
                    <h6><i class="fa fa-file-upload"></i> <br>BIBLIOTHÈQUE</h6>
                </div>
            </div>
        </a>
    </div>
</div>