<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\SuiviEvaluation $suiviEvaluation
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Suivi Evaluation'), ['action' => 'edit', $suiviEvaluation->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Suivi Evaluation'), ['action' => 'delete', $suiviEvaluation->id], ['confirm' => __('Are you sure you want to delete # {0}?', $suiviEvaluation->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Suivi Evaluations'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Suivi Evaluation'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Type Evaluations'), ['controller' => 'TypeEvaluations', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Type Evaluation'), ['controller' => 'TypeEvaluations', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Agents'), ['controller' => 'Agents', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Agent'), ['controller' => 'Agents', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Piece Jointes'), ['controller' => 'PieceJointes', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Piece Jointe'), ['controller' => 'PieceJointes', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="suiviEvaluations view large-9 medium-8 columns content">
    <h3><?= h($suiviEvaluation->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Titre') ?></th>
            <td><?= h($suiviEvaluation->titre) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Type Evaluation') ?></th>
            <td><?= $suiviEvaluation->has('type_evaluation') ? $this->Html->link($suiviEvaluation->type_evaluation->id, ['controller' => 'TypeEvaluations', 'action' => 'view', $suiviEvaluation->type_evaluation->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Agent') ?></th>
            <td><?= $suiviEvaluation->has('agent') ? $this->Html->link($suiviEvaluation->agent->id, ['controller' => 'Agents', 'action' => 'view', $suiviEvaluation->agent->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($suiviEvaluation->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($suiviEvaluation->created) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Description') ?></h4>
        <?= $this->Text->autoParagraph(h($suiviEvaluation->description)); ?>
    </div>
    <div class="related">
        <h4><?= __('Related Piece Jointes') ?></h4>
        <?php if (!empty($suiviEvaluation->piece_jointes)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Url') ?></th>
                <th scope="col"><?= __('Message Id') ?></th>
                <th scope="col"><?= __('Dossier Id') ?></th>
                <th scope="col"><?= __('Titre') ?></th>
                <th scope="col"><?= __('Mediation Id') ?></th>
                <th scope="col"><?= __('Animation Id') ?></th>
                <th scope="col"><?= __('Suivi Evaluation Id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($suiviEvaluation->piece_jointes as $pieceJointes): ?>
            <tr>
                <td><?= h($pieceJointes->id) ?></td>
                <td><?= h($pieceJointes->url) ?></td>
                <td><?= h($pieceJointes->message_id) ?></td>
                <td><?= h($pieceJointes->dossier_id) ?></td>
                <td><?= h($pieceJointes->titre) ?></td>
                <td><?= h($pieceJointes->mediation_id) ?></td>
                <td><?= h($pieceJointes->animation_id) ?></td>
                <td><?= h($pieceJointes->suivi_evaluation_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'PieceJointes', 'action' => 'view', $pieceJointes->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'PieceJointes', 'action' => 'edit', $pieceJointes->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'PieceJointes', 'action' => 'delete', $pieceJointes->id], ['confirm' => __('Are you sure you want to delete # {0}?', $pieceJointes->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
