<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Message $message
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Message'), ['action' => 'edit', $message->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Message'), ['action' => 'delete', $message->id], ['confirm' => __('Are you sure you want to delete # {0}?', $message->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Messages'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Message'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Type Messages'), ['controller' => 'TypeMessages', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Type Message'), ['controller' => 'TypeMessages', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Agents'), ['controller' => 'Agents', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Agent'), ['controller' => 'Agents', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Dossiers'), ['controller' => 'Dossiers', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Dossier'), ['controller' => 'Dossiers', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Porteurs'), ['controller' => 'Porteurs', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Porteur'), ['controller' => 'Porteurs', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Piece Jointes'), ['controller' => 'PieceJointes', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Piece Jointe'), ['controller' => 'PieceJointes', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="messages view large-9 medium-8 columns content">
    <h3><?= h($message->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Type Message') ?></th>
            <td><?= $message->has('type_message') ? $this->Html->link($message->type_message->id, ['controller' => 'TypeMessages', 'action' => 'view', $message->type_message->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Agent') ?></th>
            <td><?= $message->has('agent') ? $this->Html->link($message->agent->id, ['controller' => 'Agents', 'action' => 'view', $message->agent->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Dossier') ?></th>
            <td><?= $message->has('dossier') ? $this->Html->link($message->dossier->id, ['controller' => 'Dossiers', 'action' => 'view', $message->dossier->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Porteur') ?></th>
            <td><?= $message->has('porteur') ? $this->Html->link($message->porteur->id, ['controller' => 'Porteurs', 'action' => 'view', $message->porteur->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($message->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Date') ?></th>
            <td><?= h($message->date) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Resume') ?></h4>
        <?= $this->Text->autoParagraph(h($message->resume)); ?>
    </div>
    <div class="related">
        <h4><?= __('Related Piece Jointes') ?></h4>
        <?php if (!empty($message->piece_jointes)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Url') ?></th>
                <th scope="col"><?= __('Message Id') ?></th>
                <th scope="col"><?= __('Dossier Id') ?></th>
                <th scope="col"><?= __('Titre') ?></th>
                <th scope="col"><?= __('Mediation Id') ?></th>
                <th scope="col"><?= __('Animation Id') ?></th>
                <th scope="col"><?= __('Suivi Evaluation Id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($message->piece_jointes as $pieceJointes): ?>
            <tr>
                <td><?= h($pieceJointes->id) ?></td>
                <td><?= h($pieceJointes->url) ?></td>
                <td><?= h($pieceJointes->message_id) ?></td>
                <td><?= h($pieceJointes->dossier_id) ?></td>
                <td><?= h($pieceJointes->titre) ?></td>
                <td><?= h($pieceJointes->mediation_id) ?></td>
                <td><?= h($pieceJointes->animation_id) ?></td>
                <td><?= h($pieceJointes->suivi_evaluation_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'PieceJointes', 'action' => 'view', $pieceJointes->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'PieceJointes', 'action' => 'edit', $pieceJointes->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'PieceJointes', 'action' => 'delete', $pieceJointes->id], ['confirm' => __('Are you sure you want to delete # {0}?', $pieceJointes->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
