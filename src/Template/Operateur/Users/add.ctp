<?php
use Cake\Cache\Cache;
use Cake\Core\Configure;
use Cake\Core\Plugin;
use Cake\Datasource\ConnectionManager;
use Cake\Error\Debugger;
use Cake\Http\Exception\NotFoundException;

$this->layout = 'default';

?>

<div class="row">
    <div class="text-center border border-light p-5" style="margin-top: 20px">
        <!-- Default form register -->
        <?= $this->Form->create($user) ?>

            <p class="h4 mb-4">Inscription</p>

            <div class="form-row mb-4">
                <div class="col">
                    <!-- First name -->
                    <input type="text" name="firstname" id="defaultRegisterFormFirstName" class="form-control" placeholder="Prénom" required>
                </div>
                <div class="col">
                    <!-- Last name -->
                    <input type="text" name="lastname" id="defaultRegisterFormLastName" class="form-control" placeholder="Nom" required>
                </div>
            </div>

            <!-- E-mail -->
            <input type="email" name="email" id="defaultRegisterFormEmail" class="form-control mb-4" placeholder="E-mail">

            <!-- Password -->
            <input type="password" name="password" id="defaultRegisterFormPassword" class="form-control" placeholder="Mot de passe" aria-describedby="defaultRegisterFormPasswordHelpBlock">
            <small id="defaultRegisterFormPasswordHelpBlock" class="form-text text-muted mb-4">
                Au moins 8 caractères
            </small>

            <!-- Phone number -->
            <input type="text" name="phone" id="defaultRegisterPhonePassword" class="form-control" placeholder="Numéro de téléphone" aria-describedby="defaultRegisterFormPhoneHelpBlock">
            <small id="defaultRegisterFormPhoneHelpBlock" class="form-text text-muted mb-4">
                Optionel
            </small>

            <!-- Sign up button -->
            <button class="btn btn-info my-4 btn-block" type="submit">Inscrire</button>

            <hr>

            <!-- Register -->
            <p>Vous avez un compte ?
                <a href="<?php echo $this->Url->build(array('prefix'=>'auth','controller' => 'users', 'action' => 'connecter')); ?>">Se connecter</a>
            </p>

        <?= $this->Form->end() ?>
        <!-- Default form register -->
    </div>
</div>