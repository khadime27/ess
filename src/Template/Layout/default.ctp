<?php
    $cakeDescription = 'Fait par Bootstrap 4';
    $loggeduser = $this->request->getSession()->read('Auth.User');
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $cakeDescription ?>:
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>
    <?= $this->Html->css('bootstrap4/bootstrap.min.css') ?>
    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
    
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.6/dist/jquery.fancybox.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css">
    <!-- Bootstrap core CSS -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.7.4/css/mdb.min.css" rel="stylesheet">
    <!-- jQuery UI -->
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <?= $this->Html->css('pgwSlider/pgwslider.min.css') ?>
    
</head>
<body>
    
    <?= $this->Flash->render() ?>
    <div class="container">
        <?php if($loggeduser){ ?>
            <div class="row">
                <div class="col-sm-12">
                    <a href="<?php echo $this->Url->build(array('controller' => 'users', 'action' => 'deconnexion')); ?>" class="btn btn-unique btn-sm btn-tw btnLogout" type="button" role="button" title="Déconnecter">
                        <i class="far fa-user pr-2" aria-hidden="true"></i>Déconnexion
                    </a>
                </div>
            </div>
        <?php
        }
        ?>
        <?= $this->fetch('content') ?>
    </div>
    <footer>
    </footer>

    <?= $this->Html->script('jquery3.3.1/jquery') ?>
    <?= $this->Html->script('bootstrap4/bootstrap.min') ?>
    <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
	<script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.6/dist/jquery.fancybox.min.js"></script>
    <?= $this->Html->script('slick/mySlick') ?>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <?= $this->Html->script('sweetalert') ?>
    <!-- Datatable -->
    <script type="text/javascript" src="https:////cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>

    <?= $this->Html->script('myDatatable') ?>

    <!-- Bootstrap tooltips -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.4/umd/popper.min.js"></script>
    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <!-- MDB core JavaScript -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.7.4/js/mdb.min.js"></script>
    <!-- jQuery UI -->
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <!-- Resizable -->
    <?= $this->Html->script('jquery-ui/resizable') ?>
    <!-- Autocompletion -->
    <?= $this->Html->script('jquery-ui/autocomplete') ?>
    <!-- Slider -->
    <?= $this->Html->script('jquery-ui/slider') ?>
    <!-- Progressbar -->
    <?= $this->Html->script('jquery-ui/progressbar') ?>

    <!-- Notification : Documentation : https://notifyjs.jpillora.com/ -->
    <?= $this->Html->script('notification/noty.min') ?>
    <?= $this->Html->script('notification/demo') ?>
    
    <script type="text/javascript" src="https://cdn.ckeditor.com/4.5.7/standard/ckeditor.js"></script>
    <?= $this->Html->script('ckeditor/demo') ?>
    <?= $this->Html->script('pgwSlider/pgwslider.min') ?>

    <script>
        $(function(){
            $("#test").on('click', function() {

                $.fancybox.open({
                    src  : 'https://fancyapps.com/fancybox/3/docs/#usage',
                    type : 'iframe',
                    opts : {
                        afterShow : function( instance, current ) {
                            console.info( 'done!' );
                        }
                    }
                });

            });
        
        });
        
    </script>

    <script>
        $(document).ready(function() {
            $('.pgwSlider').pgwSlider({
                displayControls: true
            });
        });
    </script>


</body>
</html>
