<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Economie Sociale Solidaire</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="front/img/favicon.png" rel="icon">
  <link href="front/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Roboto:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
 
  <?= $this->Html->css('front/vendor/bootstrap/css/bootstrap.min.css'); ?>
  <?= $this->Html->css('front/vendor/icofont/icofont.min.css'); ?>
  <?= $this->Html->css('front/vendor/boxicons/css/boxicons.min.css'); ?>
  <?= $this->Html->css('front/vendor/owl.carousel/assets/owl.carousel.min.css'); ?>
  <?= $this->Html->css('front/vendor/venobox/venobox.css'); ?>
  <?= $this->Html->css('front/vendor/aos/aos.css'); ?>

  <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />

  <!-- Template Main CSS File -->
  
  <?= $this->Html->css('front/css/style.css'); ?> 
  <?= $this->Html->css('front/css/ess.css'); ?> 
  <?= $this->Html->css('css_vitrine/custom1'); ?>
    <?= $this->Html->css('back/custom_back.css') ?>
  <?= $this->Html->css('vitrine/css/main.css') ?>
  <?= $this->Html->css('front/css/custom.css'); ?>
   
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/css/bootstrap-multiselect.css">

  <!-- =======================================================
  * Template Name: BizLand - v1.1.0
  * Template URL: https://bootstrapmade.com/bizland-bootstrap-business-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body class="is-preload">

    <?= $this->Element('Nav/vitrine-nav-horizontal') ?>

    <?= $this->Flash->render() ?>
    <?= $this->fetch('content') ?>

    <!-- ======= Footer ======= -->
  <footer id="footer">

    <div class="footer-newsletter">
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-lg-6">
            <h4>Newsletter</h4>
            <p>Economie Sociale Solidaire Newsletter</p>
            <form action="" method="post">
              <input type="email" name="email"><input type="submit" value="Soumettre">
            </form>
          </div>
        </div>
      </div>
    </div>

    <div class="footer-top">
      <div class="container">
        <div class="row">

          <div class="col-lg-3 col-md-6 footer-contact">
            <h3>MMESS<span>.</span></h3>
            <p>
              Sphère ministérielle <br>
              Ousmane Tanor DIENG<br>
              1er Arrondissement Pôle urbain de Diamniadio <br><br>
              <strong>Téléphone:</strong> 33 860 26 52 / 33 860 59 71<br>
              <strong>Email:</strong> info@example.com<br>
            </p>
          </div>

          <div class="col-lg-3 col-md-6 footer-links">
            <h4>Equipe</h4>
            <ul>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Ministre : Zahra Iyane THIAM</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">SG: Cheikh NDIAYE</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">DAGE: El hadji Amadou NDIAYE</a></li>
           
            </ul>
          </div>

          <div class="col-lg-3 col-md-6 footer-links">
            <h4>Nos Bénéficiaires</h4>
            <ul>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Les coopératives</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Les finances solidaires</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Les groupements</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Les associations</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Les entreprises sociales </a></li>
            </ul>
          </div>

          <div class="col-lg-3 col-md-6 footer-links">
            <h4>Nos Réseaux Sociaux</h4>
            <p>Nous sommes aussi présents sur l'ensemble des plateformes suivantes:</p>
            <div class="social-links mt-3">
              <a href="#" class="twitter"><i class="bx bxl-twitter"></i></a>
              <a href="https://web.facebook.com/Ministère-de-la-Microfinance-et-de-lEconomie-Sociale-et-Solidaire-114475760104661/" class="facebook"><i class="bx bxl-facebook"></i></a>
              <a href="#" class="instagram"><i class="bx bxl-instagram"></i></a>
              <a href="#" class="google-plus"><i class="bx bxl-skype"></i></a>
              <a href="#" class="linkedin"><i class="bx bxl-linkedin"></i></a>
            </div>
          </div>

        </div>
      </div>
    </div>

    <div class="container py-4">
      <div class="copyright">
        &copy; Copyright <strong><span>MMESS - <?php echo date("Y"); ?> </span></strong>. Tous droits réservés
      </div>
      <div class="credits">
        <!-- All the links in the footer should remain intact. -->
        <!-- You can delete the links only if you purchased the pro version. -->
        <!-- Licensing information: https://bootstrapmade.com/license/ -->
        <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/bizland-bootstrap-business-template/ 
        Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>-->
      </div>
    </div>
  </footer><!-- End Footer -->

  <div id="preloader"></div>
  <a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a>

  <!-- Vendor JS Files -->
  <?= $this->Html->script('vitrine/js/jquery.min') ?>
  <?= $this->Html->script('front/vendor/bootstrap/js/bootstrap.min') ?>
  <?= $this->Html->script('front/vendor/bootstrap/js/bootstrap.bundle.min') ?>
  <?= $this->Html->script('front/vendor/jquery.easing/jquery.easing.min') ?>
  <?= $this->Html->script('front/vendor/php-email-form/validate') ?>
  <?= $this->Html->script('front/vendor/waypoints/jquery.waypoints.min') ?>
  <?= $this->Html->script('front/vendor/counterup/counterup.min') ?>
  <?= $this->Html->script('front/vendor/owl.carousel/owl.carousel.min') ?>
  <?= $this->Html->script('front/vendor/isotope-layout/isotope.pkgd.min') ?>
  <?= $this->Html->script('front/vendor/venobox/venobox.min') ?>
  <?= $this->Html->script('front/vendor/aos/aos') ?>
  <?= $this->Html->script('front/js/main') ?>


  <!-- Template Main JS File -->
  <?= $this->Html->script('vitrine/js/jquery.scrolly.min') ?>
  <?= $this->Html->script('vitrine/js/jquery.scrollex.min') ?>
  <?= $this->Html->script('vitrine/js/browser.min') ?>
  <?= $this->Html->script('vitrine/js/breakpoints.min') ?>
  <?= $this->Html->script('vitrine/js/util') ?>
  <?= $this->Html->script('vitrine/js/main') ?>

  <script type="text/javascript" src="https://cdn.ckeditor.com/4.5.7/standard/ckeditor.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/js/bootstrap-multiselect.js"></script>

  <script> 
    $(function(){
      $('select[multiple]').multiselect({
        selectAllText: 'Tout sélectionner'
        ,nonSelectedText: 'Aucune sélection'
        ,nSelectedText: 'selectionnés'
        ,allSelectedText: 'Tous selectionnés'
      });
    });
  </script>
</body>

</html>
