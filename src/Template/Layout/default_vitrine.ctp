<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Démarrage</title>

  <!-- Font Awesome Icons -->
  <?= $this->Html->css('vendor_vitrine/fontawesome-free/css/all.min.css'); ?>

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Merriweather+Sans:400,700" rel="stylesheet">
  <link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic' rel='stylesheet' type='text/css'>

  <!-- Plugin CSS -->
  <?= $this->Html->css('vendor_vitrine/magnific-popup/magnific-popup.css'); ?>

  <!-- Theme CSS - Includes Bootstrap -->
  <?= $this->Html->css('css_vitrine/creative.min.css'); ?>
  <?= $this->Html->css('css_vitrine/custom1'); ?>

</head>

<body class="is-preload">
    <?= $this->Element('Nav/vitrine-nav-horizontal') ?>

    <?= $this->fetch('content') ?>

    <!-- Footer -->
    <footer class="bg-light py-5">
        <div class="container">
            <div class="small text-center text-muted">Copyright &copy; <?= date('Y') ?> - Démarrage</div>
        </div>
    </footer>

    <!-- Bootstrap core JavaScript -->
    <?php echo $this->Html->script('vendor_vitrine/jquery/jquery.min'); ?>
    <?php echo $this->Html->script('vendor_vitrine/bootstrap/js/bootstrap.bundle.min'); ?>

    <!-- Plugin JavaScript -->
    <?php echo $this->Html->script('vendor_vitrine/jquery-easing/jquery.easing.min'); ?>
    <?php echo $this->Html->script('vendor_vitrine/magnific-popup/jquery.magnific-popup.min'); ?>

    <!-- Custom scripts for this template -->
    <?php echo $this->Html->script('js_vitrine/creative.min'); ?>

    <script type="text/javascript" src="https://cdn.ckeditor.com/4.5.7/standard/ckeditor.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>


</body> 