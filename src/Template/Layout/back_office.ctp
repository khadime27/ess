<?php
    $cakeDescription = 'ESS';
    $loggeduser = $this->request->getSession()->read('Auth.User');
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $cakeDescription ?>:
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>
    <?= $this->Html->css('back/main.css') ?>
    <?= $this->Html->css('back/custom_back.css') ?>
    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/css/bootstrap-multiselect.css">
    <?= $this->Html->css('print.min.css');?><link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.2.5/jquery.fancybox.min.css" integrity="sha256-ygkqlh3CYSUri3LhQxzdcm0n1EQvH2Y+U5S2idbLtxs=" crossorigin="anonymous" />
    <?= $this->Html->css('datatables/dataTables.bootstrap4.min.css'); ?>

    <!-- <link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css"> -->

    
</head>
<body>
    <div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header">
        <?= $this->Element('Nav/top-backoffice') ?>            
        <div class="app-main">
            
            <?php 
                if($userConnect->profiles_id == 1) echo $this->Element('Nav/porteur'); 
                else if($userConnect->profiles_id == 2) echo $this->Element('Nav/admin'); 
                else if($userConnect->profiles_id == 3) echo $this->Element('Nav/operateur'); 
                else if($userConnect->profiles_id == 4) echo $this->Element('Nav/validateur'); 
                else if($userConnect->profiles_id == 5) echo $this->Element('Nav/comite');
                else if($userConnect->profiles_id == 6) echo $this->Element('Nav/superviseur'); 
            ?>  

            <div class="app-main__outer">
                <div class="app-main__inner">
                    <?= $this->Flash->render() ?>
                    <?= $this->fetch('content') ?>
                </div>
            </div>
            

        </div>


    </div>
    
    <!-- JQuery -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <?= $this->Html->script('back/main') ?>

    <script type="text/javascript" src="https://cdn.ckeditor.com/4.5.7/standard/ckeditor.js"></script>

    <!-- <div class="message success" onclick="this.classList.add('hidden')"><?= $message ?></div> -->

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/js/bootstrap-multiselect.js"></script>


    <?php echo $this->Html->script('datatables/jquery.dataTables.min'); ?>
    <?php echo $this->Html->script('datatables/dataTables.bootstrap4.min'); ?>

    <?= $this->Html->script('table2excel.js');?>  
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.2.5/jquery.fancybox.min.js"></script>

    <!-- <script type="text/javascript" src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script> -->

    <script>
        $(function(){
            $('select[multiple]').multiselect({
                selectAllText: 'Tout sélectionner'
                ,nonSelectedText: 'Aucune sélection'
                ,nSelectedText: 'selectionnés'
                ,allSelectedText: 'Tous selectionnés'
            });
        });
    </script>


    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
    <script>
        $(function(){
            setTimeout(function() {
                $('.message').fadeOut('fast');
            }, 5000);

            $('.select2').select2({
                "language": {
                    "noResults": function(){
                        return "Aucun résultat";
                    }
                }
            });

        });
    </script>

    <script>

        $(document).ready(function() {
            $('table').DataTable({
                responsive: true,
                scrollX: 300,
                "destroy": true,
                
                "language": {
                    "lengthMenu": "Afficher _MENU_ par page",
                    "zeroRecords": "Pas d'enregistrement trouvé",
                    "info": "Page _PAGE_ sur _PAGES_",
                    "infoEmpty": "Pas d'enregistrement disponible",
                    "infoFiltered": "(filtrés sur _MAX_ enregistrements)",
                    "search":         "Recherche",
                    "scrollX": true,
                    "paginate": {
                        "first":      "<<",
                        "last":       ">>",
                        "next":       ">",
                        "previous":   "<"
                    }
                }
            });

        });

    </script>
</body>
</html>
