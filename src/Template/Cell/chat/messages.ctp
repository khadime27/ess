<?php $this->setLayout('back_office'); ?>


<?= $this->Html->css('back/chat.css'); ?>

<input type="hidden" id="idProfile" value="<?= $userConnect->profiles_id ?>">
<input type="hidden" id="idPersonne" value="<?= $userConnect->porteurs ? $userConnect->porteurs[0]->id : $userConnect->agents[0]->id ?>">


<div class="container">
    <?php if($userConnect->profiles_id == 1): ?>
    <div class="card">
        <div class="card-header bg-color text-white">Formulaire de filtre</div>
        <div class="card-body">
            <?php echo $this->Form->create('Chat',array('url'=>array('controller'=>'Chats', 'action'=>'index'), 'id' => 'formSelectFolder')); ?>
            <div class="row">
                <div class="col-sm-10">
                    <div class="row">
                        <div class="col-sm-3">
                            <select name="filiere_id" class="form-control">
                                <option value="">Secteurs</option>
                                <?php foreach($filieres as $filiere): ?>
                                    <option value="<?= $filiere->id ?>" <?php if(isset($idFiliere)): if($idFiliere == $filiere->id) echo 'selected'; endif; ?>><?= $filiere->nom ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="col-sm-3">
                            <select id="region_id" name="region_id" class="form-control">
                                <option value="">Régions</option>
                                <?php foreach($regions as $region): ?>
                                    <option value="<?= $region->id ?>" <?php if(isset($idRegion)): if($idRegion == $region->id) echo 'selected'; endif; ?>><?= $region->nom ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="col-sm-3">
                            <select id="departements" name="departement_id" class="form-control">
                                <option value="">Départements</option>
                                <?php if(isset($departementSelect)): ?>
                                    <option value="<?= $departementSelect->id ?>" selected><?= $departementSelect->departementscol ?></option>
                                <?php endif; ?>
                            </select>
                        </div>
                        <div class="col-sm-3">
                            <select name="commune_id" id="communes" class="form-control">
                                <option value="">Communes</option>
                                <?php if(isset($communeSelect)): ?>
                                    <option value="<?= $communeSelect->id ?>" selected><?= $communeSelect->nom ?></option>
                                <?php endif; ?>
                            </select>
                        </div>
                    </div>
                    
                </div>
                <div class="col-sm-2">
                    <button type="submit" class="btn btn-success"><i class="fa fa-check-circle"></i> Valider</button>
                </div>
            </div>
            <?= $this->Form->end() ?>
        </div>
    </div>
    <?php endif; ?>
    <div class="messaging">
      <div class="inbox_msg">
        <div class="inbox_people">
          <div class="headind_srch">
            <div class="recent_heading">
                <ul class="nav nav-tabs md-tabs" id="myTabEx" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active show" id="tabu1-tab-ex" data-toggle="tab" href="#tabu1-ex" role="tab" aria-controls="tabu1-ex"
                        aria-selected="true"><?= $this->fetch('titre1') ?></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="tabu2-tab-ex" data-toggle="tab" href="#tabu2-ex" role="tab" aria-controls="tabu2-ex"
                        aria-selected="false"><?= $this->fetch('titre2') ?></a>
                    </li>
                </ul>
            </div>
            <div class="srch_bar">
                <div class="stylish-input-group">
                    <input type="text" id="dynamic-search" class="search-bar input-gray" placeholder="Rechercher" >
                </div>
            </div>  
          </div>
          <div class="inbox_chat">

                <div class="tab-content pt-1" id="myTabContentEx">
                    <div class="tab-pane fade active show" id="tabu1-ex" role="tabpanel" aria-labelledby="tabu1-tab-ex">
                        <?php foreach($dossiersSameActivities as $dossier): 
                            $interlocuteur = $dossier->porteur;
                            $chemin = "";
                            if($interlocuteur->photo == "") $chemin = "userprofile.png";
                            else $chemin = 'porteurs/'.$interlocuteur->photo; 
                        ?>
                        <a href="#" class="userClick" attr_dossier="<?= $dossier->id ?>" id="<?= $interlocuteur->id ?>" profile="<?= $interlocuteur->user->profiles_id ?>" userinf="<?= $interlocuteur->prenom.' '.$interlocuteur->nom ?>" imguserinf="<?= $this->Url->Build("/").'img/'.$chemin ?>">
                            <div class="chat_list">
                                
                                <div class="chat_people d-flex">
                                    <div class="chat_img"> 
                                        <?= $this->Html->image($chemin, ['alt' => '', 'class' => 'round-avatar']) ?> 
                                    </div>
                                    <div class="chat_ib">
                                        <?php $arrayMessages = []; $arryDates = [];
                                        foreach($chats as $chat):
                                            if($interlocuteur->id == $chat->porteur_id):
                                                $arrayMessages[] = $chat->resume;
                                                $arryDates[] = $chat->date;
                                            endif;
                                        endforeach; ?>
                                        <h5><?= cutString($interlocuteur->prenom." ".$interlocuteur->nom, 20) ?> <span class="chat_date"><?= end($arryDates) ?></span></h5>
                                        <p><?php $lastMessage = cutString(end($arrayMessages), 25); echo $lastMessage; ?></p>
                                        <small><?= $dossier->numero ?></small>

                                    </div>
                                </div>
                            </div>
                        </a>
                        <?php endforeach; ?>
                    </div>

                    <?php if($userConnect->profiles_id == 1): ?>
                    <div class="tab-pane fade" id="tabu2-ex" role="tabpanel" aria-labelledby="tabu2-tab-ex">
                        <?php foreach($agents as $agent):
                            $chemin = "profil-ess.png" ?>
                            <a href="#" class="userClick" id="<?= $agent->id ?>" profile="<?= $agent->user->profiles_id ?>" userinf="<?= $agent->prenom.' '.$agent->nom ?>" imguserinf="<?= $this->Url->Build("/").'img/'.$chemin ?>">
                                <div class="chat_list">
                                    
                                    <div class="chat_people d-flex">
                                        <div class="chat_img"> 
                                            <?= $this->Html->image($chemin, ['alt' => '', 'class' => 'round-avatar']) ?> 
                                        </div>
                                        <div class="chat_ib">
                                            <?php $arrayMessages = []; $arryDates = [];
                                            foreach($chats as $chat):
                                                if($agent->id == $chat->agent_id):
                                                    $arrayMessages[] = $chat->resume;
                                                    $arryDates[] = $chat->date;
                                                endif;
                                            endforeach; ?>
                                            <h5><?= cutString($agent->prenom." ".$agent->nom, 20) ?> <span class="chat_date"><?= end($arryDates) ?></span></h5>
                                            
                                            <p><?php $lastMessage = cutString(end($arrayMessages), 25); echo $lastMessage; ?></p>
                                            <small><?= $agent->agence ? $agent->agence->nom : 'Superadmin' ?></small>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        <?php endforeach; ?>
                    </div>
                    <?php endif; ?>
                </div>

          </div>
        </div>
        <div class="d-flex p-3 bg-white hide" id="user_info">
            <div class="col-sm-10">
                <i id="btn-back-mobile" class="fa fa-arrow-left mt-auto mb-auto mr-2 d-block d-sm-none"></i>
                <?= $this->Html->image("userprofile.png", ['alt' => '', 'class' => 'round-avatar', 'id' => 'img_user_click']) ?>
                <span class="ml-2 mt-3" id="user_click"></span>
            </div>

            <div class="col-sm-2 marg-auto text-right">
                <a class="btn bg-color js-scroll-trigger btn-sm" href="#" id="toBottom" title="En bas de page"><i class="fa fa-chevron-down text-white"></i></i></a>
            </div>
        </div>
        <div class="mesgs d-none d-sm-block">
            <?php echo $this->Form->create('Chat',array('enctype'=>'multipart/form-data', 'url'=>array('controller'=>'Messages', 'action'=>'sendmsg'), 'id' => 'saveForm')); ?>
            
                <div class="msg_history" id="theMessages">
                    <?= $this->Html->image("loading.gif", ['alt' => '', 'class' => 'w-100', 'id' => 'loading-img']) ?>
                </div>
                <input type="hidden" name="interlocuteur" value="" id="interlocuteur">
                <input type="hidden" name="dossier_id" id="idDossier">
                <input type="hidden" name="profile_interlocuteur" id="profile-interlocuteur">
                <input type="hidden" name="created">
                
                <div class="type_msg mt-3">
                    <div class="input_msg_write">
                        <textarea type="text" id="send" name="message" rows="2" class="write_msg input-gray" placeholder="Message..."></textarea>
                        <input type="file" name="url" id="url" class="hide">
                        <label for="url" class="label-icon-file ml-3"><i class="fa fa-clipboard icon-file"></i></label>
                        <button class="msg_send_btn btn bg-color pr-2" type="submit" disabled>Envoyer</button>
                    </div>
                </div>
            <?= $this->Form->end() ?>
        </div>
      </div>
      
      
    </div>
</div>

<?php 

    // Méthode permettant de limiter le nombre de caractère d'une chaine à afficher
    function cutString($chaine, $taille){
        $chaineTransform = substr($chaine, 0, $taille);  // Nombre de caractère souhaité
        if(strlen($chaineTransform) >= $taille){
            echo $chaineTransform.'...';
        }else{
            echo $chaine;
        }
    }

?>


<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<script type="text/javascript">
    $(document).ready(function () {
        $('#toBottom').click(function(){
            //$('#theMessages').scrollTop($('#theMessages')[0].scrollHeight); 
            $('#theMessages').animate({
                scrollTop: $('#theMessages')[0].scrollHeight}, 1000
            );
        });

        $('#send').on("input", function(){
            if($(this).val() != "") {
                $('.msg_send_btn').removeAttr('disabled');
            }else {
                $('.msg_send_btn').attr('disabled', true);
            }
        });

        $('#url').change(function(){
            if($(this).get(0).files.length > 0) {
                $('.msg_send_btn').removeAttr('disabled');
                $('.icon-file').css({
                    'color' : 'green'
                });
            }else{
                $('.msg_send_btn').attr('disabled', true);
                $('.icon-file').css({
                    'color' : 'black'
                });
            }
        });
        

        //$('#loading-img').hide();
        $('#theMessages').scrollTop($('#theMessages').height()); 

        $('#saveForm').submit(function(){

            var formData = new FormData($(this)[0]);    
            //serialize form data
            //var formData = $(this).serialize();

            // Envoi d'un fichier
            var file_data = $('#url').prop('files')[0];   
            formData.append('file', file_data);

            //get form action
            var formUrl = $(this).attr('action');
            
            $.ajax({
                type: 'POST',
                url: formUrl,
                dataType: 'text',
                data: formData,
                contentType:false,
                cache:false,
                processData:false,
                success: function(data,textStatus,xhr){
                        // alert(data)
                        $('#send').val('');
                },
                error: function(xhr,textStatus,error){
                        alert('Error : ' + textStatus);
                }
            }); 

            showMsg();
            $('#theMessages').scrollTop($('#theMessages').height()); 
                
            return false;
        });

        showMsg();

        $('.userClick').click(function(){
            $('#interlocuteur').val($(this).attr('id'));
            $('#idDossier').val($(this).attr('attr_dossier'));
            $('#profile-interlocuteur').val($(this).attr('profile'));
            $('.mesgs').removeClass('d-none');
            $('.mesgs').removeClass('d-sm-block');
            if($(window).width() <= 725){
                $('.inbox_people').hide();
            }
            $('#user_info').removeClass('hide');
            $('#user_click').text($(this).attr('userinf'));
            $('#img_user_click').attr('src', $(this).attr('imguserinf'));
            $('.mesgs').show();

            $('#theMessages').animate({
                scrollTop: $('#theMessages')[0].scrollHeight}, 1000
            );
        });


        $("#region_id").change(function () {
            $('#communes').html('');

            var region_id = $('#region_id option:selected').val();
            
            <?php $url = $this->Url->build(['controller' => 'Regions', 'action' => 'view']); ?>
            $.ajax(
            {type: 'GET',
                url: "<?= $url; ?>/" + region_id,
                async: false,
                success: function (json) {
                    var data = JSON.parse(json);
                    //alert(data);
                    var infos;
                    var listdept = [];
                    infos += '<option selected="selected" value=""> Départements</option>';
                    for (var i in data) {

                    listdept['nom'] = data[i]['departementscol'];
                    infos += '<option required value="' + data[i]['id'] + '">' + listdept['nom'] + '</option>';
                    }
                    $("#departements").html(infos);
                }
            });

        });


        $("#departements").change(function () {
            var departement = $('#departements option:selected').val();
            // alert(departement);

            <?php $url1 = $this->Url->build(['controller' => 'Departements', 'action' => 'view']); ?>
                $.ajax(
                {type: 'GET',
                    url: "<?= $url1; ?>/" + departement,
                    async: false,
                    success: function (json) {
                    var data = JSON.parse(json);
                    
                    var infos;
                    var listcommun = [];
                    infos += '<option selected="selected" value=""> Communes</option>';
                    for (var i in data) {

                        listcommun['nom'] = data[i]['nom'];
                        infos += '<option required value="' + data[i]['id'] + '">' + listcommun['nom'] + '</option>';
                    }
                    $("#communes").html(infos);
                    }
                });
            });


        $("#departement").change(function () {
            $(".commune").show(1000);
        });

        
    });

    var timeout = 1000;
	var showMsg = function() {
        
        //$('#theMessages').scrollTop($('#theMessages').height()); 
        
        var getUrl = window.location;
        var baseUrl = getUrl.protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];
        
        var urlProject = '';

        var urlShow = '';
        var projectFolder = '';
        if(baseUrl.indexOf("localhost") >= 0) {
            urlShow = baseUrl+'/ess/messages/showmsg/'+$('#interlocuteur').val()+'/'+$('#profile-interlocuteur').val()+'/'+$('#idDossier').val();
            projectFolder = baseUrl+'/ess';
            urlProject = projectFolder;
        }else {
            urlShow = baseUrl+'/messages/showmsg/'+$('#interlocuteur').val()+'/'+$('#profile-interlocuteur').val()+'/'+$('#idDossier').val();
            projectFolder = baseUrl;
            urlProject = getUrl.protocol + "//" + getUrl.host;
        }

        $.ajax({
            async: true,
            url : urlShow,
            type : 'GET',
            dataType : 'text',
            success : function(data){
                donnees = JSON.parse(data);
                // console.log(data)
                $('#theMessages').html('');
                if($('#interlocuteur').val() == "") {
                    $('#theMessages').append('<img src="https://img.freepik.com/free-vector/group-chat-concept-illustration_114360-1495.jpg?size=626&ext=jpg" class="w-100" style="margin-top: 50px">');
                    $('.type_msg').hide();
                }else{
                    $('.type_msg').show();
                    $('#theMessages').addClass('h-messages');
                    for(var i = 0; i < donnees.length; i++){
                        // console.log(donnees[i]['message_client']):
                        var dateEnvoi = new Date(donnees[i]["date"]);
                        var dateNow = new Date();

                        var seconds = Math.floor((dateNow - (dateEnvoi))/1000);
                        var minutes = Math.floor(seconds/60);
                        var hours = Math.floor(minutes/60);
                        var days = Math.floor(hours/24);

                        hours = hours-(days*24);
                        minutes = minutes-(days*24*60)-(hours*60);
                        seconds = seconds-(days*24*60*60)-(hours*60*60)-(minutes*60);

                        var timeSent;
                        if(days > 0) {
                            if(days == 1) {
                                timeSent = "Il y'a "+days+" jour";
                            }else{
                                timeSent = "Il y'a "+days+" jours";
                            }
                        }else {
                            if(hours == 0) {
                                if(minutes == 0) {
                                    timeSent = dateEnvoi.getHours()+"h : "+dateEnvoi.getMinutes()+" min";
                                }else {
                                    timeSent = "Il y'a "+minutes+"min";
                                }
                                
                            }
                            if(hours > 0) {
                                if(minutes == 0) {
                                    timeSent = "Il y'a "+hours+"h ";
                                }else{
                                    timeSent = "Il y'a "+hours+"h "+minutes+"min";
                                }
                            }
                            
                        }

                        htmlFile = ''; showFile = '';
                        if(donnees[i]["url"] != null) {
                            if(donnees[i]["url"].indexOf("docx") >= 0 || donnees[i]["url"].indexOf("doc") >= 0) {
                                nameFile = "logoword.png";
                                showFile = '<img src="'+urlProject+'/img/'+nameFile+'" alt=""/>';
                            }else if(donnees[i]["url"].indexOf("xls") >= 0 || donnees[i]["url"].indexOf("xlsx") >= 0) {
                                nameFile = "logoexcel.png";
                                showFile = '<img src="'+urlProject+'/img/'+nameFile+'" alt=""/>';
                            }else if(donnees[i]["url"].indexOf("pdf") >= 0) {
                                nameFile = "logopdf.png";
                                showFile = '<img src="'+urlProject+'/img/'+nameFile+'" alt=""/>';
                            }else {
                                showFile = '<img src="'+urlProject+'/img/messages/'+donnees[i]["url"]+'" alt=""/>';
                            }
                            
                            htmlFile = '<a class="fancy_image" target="_blank" href="'+urlProject+'/img/messages/'+donnees[i]["url"]+'">'+showFile+'</a>';
                        }
                        message = '';
                        if(donnees[i]["resume"] != "") {
                            message = '<p class="test">'+ donnees[i]["resume"] +'</p>';
                        }

                        if($('#idProfile').val() == 1){
                            if($('#idPersonne').val() == donnees[i]['porteur_id']) {
                                if(donnees[i]['dossier_id'] != null) {
                                    $('#theMessages').append(
                                        '<div class="incoming_msg">'+
                                            '<div class="incoming_msg_img"> <img src='+$("#img_user_click").attr("src")+' class="border-round ml-1" alt="sunil"> </div>'+
                                            '<div class="received_msg">'+
                                                '<div class="received_withd_msg">'+
                                                    message+
                                                    htmlFile+
                                                    '<span class="time_date">'+ timeSent +'</span>'+
                                                '</div>'+
                                            '</div>'+
                                        '</div>'
                                    );
                                }else{
                                    $('#theMessages').append('<div class="outgoing_msg">'+
                                        '<div class="sent_msg">'+
                                            message+
                                            htmlFile+
                                            '<span class="time_date">'+ timeSent +'</span> </div>'+
                                        '</div>'
                                    );
                                }
                                
                            } else {
                                $('#theMessages').append('<div class="outgoing_msg">'+
                                    '<div class="sent_msg">'+
                                        message+
                                        htmlFile+
                                        '<span class="time_date">'+ timeSent +'</span> </div>'+
                                    '</div>'
                                );
                            }
                        }
                        else{
                            if(donnees[i]['dossier_id'] == null) {
                                $('#theMessages').append('<div class="outgoing_msg">'+
                                    '<div class="sent_msg">'+
                                        message+
                                        htmlFile+
                                        '<span class="time_date">'+ timeSent +'</span> </div>'+
                                    '</div>'
                                );
                            }else {
                                $('#theMessages').append(
                                    '<div class="incoming_msg">'+
                                        '<div class="incoming_msg_img"> <img src='+$("#img_user_click").attr("src")+' class="border-round ml-1" alt="sunil"> </div>'+
                                        '<div class="received_msg">'+
                                            '<div class="received_withd_msg">'+
                                                message+
                                                htmlFile+
                                                '<span class="time_date">'+ timeSent +'</span>'+
                                            '</div>'+
                                        '</div>'+
                                    '</div>'
                                );
                            }
                        }
                    }
                }
                
                
                
            },
            error: function(data){
                console.log(data);
            }

        });
        setTimeout(showMsg, timeout);
        
    }

</script>

<script>
    $(document).ready(function() {

        $("#dynamic-search").keyup(function() {
            var val = $.trim(this.value);
            if (val === "")
                $('.userClick').show();
            else {
                $('.userClick').hide();
                $('.userClick').has("h5:contains(" + val + ")").show();
            }
        });

        $('#btn-back-mobile').click(function() {
            $('.mesgs').hide();
            $('.inbox_people').show();
        });
    });
</script>