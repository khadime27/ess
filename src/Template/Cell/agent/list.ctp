<?php $this->setLayout('back_office'); ?>

<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            <div class="page-title-icon">
                <i class="pe-7s-users icon-gradient bg-mean-fruit"></i>
            </div>
            <div>
                <?php if($this->fetch('superviseur')) echo 'Gestion des superviseurs';
                      else echo 'Gestion des agents'; 
                ?> : 
                <span class="font-weight-bold text-success text-uppercase">
                    <?php if($userConnect->agents){
                        if($userConnect->agents[0]->comites_id == 1) echo "Espace qualification"; 
                        if($userConnect->agents[0]->comites_id == 2) echo "Espace analyse"; 
                        if($userConnect->profiles_id == 2) echo "Espace superadmin";
                        if($userConnect->profiles_id == 5) echo "Espace comité";
                        if($userConnect->profiles_id == 6) echo "Espace superviseur";
                    } elseif($userConnect->profiles_id == 1) echo "Espace porteur";
                    ?>
                </span>
            </div>
        </div>
        
        <div class="page-title-actions">
            <div class="d-inline-block">
                <button onclick="window.history.go(-1)" class="btn btn-primary">
                    <i class="fa fa-arrow-left fa-w-20"></i> Retour
                </button>
                <button type="button" class="btn-shadow btn btn-info" data-toggle="modal" data-target="#addModal">
                    <span class="btn-icon-wrapper pr-2 opacity-7">
                        <i class="fa fa-plus fa-w-20"></i>
                    </span>
                    Ajouter
                </button>
            </div>
        </div>  
    </div>
</div>  
                        
<div class="row">
    <div class="col-lg-12">
        <div class="main-card mb-6 card">
            <div class="card-body">
                <h5 class="card-title">
                    <?php if(!$this->fetch('superviseur')): ?>
                        Liste des agents <?php if($userConnect->profiles_id == 2) echo "(".$agence->nom.")"; ?>
                    <?php else: echo 'Liste des superviseurs'; endif; ?>
                </h5>
                <table class="mb-0 table">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Nom</th>
                            <th>Email</th>
                            <th>Téléphone</th>
                            <th>Adresse</th>
                            <?php if(!$this->fetch('superviseur')): ?>
                            <th>Service</th>
                            <?php endif; ?>
                            <th class="text-center">Actions</th>
                        </tr>
                    </thead>
                    <tbody id="bodyTable">
                        <?php foreach($agents as $i => $agent): ?>
                            <tr>
                                <td scope="row"><?= $i+1 ?></td>
                                <td><?= $agent->prenom." ".$agent->nom ?></td>
                                <td><?= $agent->user->email ?></td>
                                <td><?= $agent->telephone ?></td>
                                <td><?= $agent->adresse ?></td>
                                <?php if(!$this->fetch('superviseur')): ?>
                                <td><?= $agent->agence->nom ?></td>
                                <?php endif; ?>
                                <td class="actions text-center">
                                    <?php $inf_departement = null; if($agent->departements_id): $inf_departement = $agent->departements_id.'+'.$agent->departement->departementscol.'+'.$agent->departement->region_id; endif; ?>
                                    <button type="button" value="<?= $agent->id.'+'.$agent->prenom.'+'.$agent->nom.'+'.$agent->telephone.'+'.$agent->adresse.'+'.$agent->user->email.'+'.$agent->admin.'+'.$agent->agence_id.'+'.$agent->comites_id.'+'.$inf_departement ?>" class="btn btn-sm btn-info edit_click" title="Modifier" data-toggle="modal" data-target="#modalEdit"><i class="fa fa-edit"></i></button>

                                    <?php if($agent->etat): ?>
                                        <?= $this->Form->postLink(__('<i class="fa fa-times"></i>'), ['action' => 'changeStat', $agent->id, 0], ['confirm' => __('Voulez-vous vraiment désactiver cet agent ?'),'class'=>'btn btn-danger btn-sm','title'=>'Désactiver','escape'=>false]) ?>
                                    <?php else: ?>
                                        <?= $this->Form->postLink(__('<i class="fa fa-check"></i>'), ['action' => 'changeStat', $agent->id, 1], ['confirm' => __('Voulez-vous vraiment activer cet agent ?'),'class'=>'btn btn-success btn-circle btn-sm','title'=>'Activer','escape'=>false]) ?>
                                    <?php endif; ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>


<div class="modal fade" id="addModal" tabindex="1" role="dialog" aria-labelledby="addModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <?php 
            if(!$this->fetch('superviseur'))
                echo $this->Form->create('Agents', array('enctype'=>'multipart/form-data', 'url'=>array('controller' => 'Agents','action'=> 'add')));
            else 
                echo $this->Form->create('Superviseurs', array('enctype'=>'multipart/form-data', 'url'=>array('controller' => 'Superviseurs', 'action'=> 'add')));
            ?>
                <div class="modal-header">
                    <h5 class="modal-title" id="addModalLabel">Ajout <?php if($userConnect->profiles_id == 2) echo "d'un admin service"; else echo "d'un agent"; ?></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                
                <div class="modal-body">
                
                    <div class="form-row mb-3">
                        <div class="col">
                            <label for="prenom">Prénom</label>
                            <input name="prenom" id="prenom" placeholder="Saisir le prénom de l'agent" type="text" class="form-control" required>
                        </div>
                        <div class="col">
                            <label>Nom</label>
                            <input name="nom" id="nom" placeholder="Saisir le nom de l'agent" type="text" class="form-control" required>
                        </div>
                    </div>
                    <div class="form-row mb-3">
                        <div class="col">
                            <label for="telephone">Téléphone</label
                            ><input name="telephone" id="telephone" placeholder="Numéro de téléphone" type="number" class="form-control" required>
                        </div>
                        <div class="col">
                            <label for="adresse">Adresse</label>
                            <input name="adresse" id="adresse" placeholder="Saisir son adresse" type="text" class="form-control" required>
                        </div>
                    </div>
                    <div class="position-relative form-group">
                        <label for="email">Email</label
                        ><input name="email" id="email" placeholder="Saisir son adresse email" type="text" class="form-control" required>
                    </div>
                    <div class="position-relative form-group">
                        <label for="password">Mot de passe</label>
                        <input name="password" id="password" placeholder="Saisir un mot de passe" type="text" class="form-control" required>
                    </div>

                    <?php if(!$this->fetch('superviseur')): ?>
                    <div class="form-row">
                        <?php if($userConnect->profiles_id == 3): ?>
                        <div class="col">
                            <div class="position-relative form-group">
                                <label for="idAgence">Structures</label>
                                <select name="agence_id" id="idAgence" class="form-control">
                                    <option value="">Choisissez une structure</option>
                                    <?php foreach($agences as $service):
                                        if($service->types_agences_id == $agence->types_agences_id): ?>
                                            <option value="<?= $service->id ?>"><?= $service->nom ?></option>
                                    <?php endif; endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <input type="hidden" name="comites_id" value="<?= $userConnect->agents[0]->comites_id ?>">
                        <?php else: ?>
                            <input type="hidden" name="agence_id" value="<?= $agence->id ?>">
                            <?php if($agence->types_agences_id != 2): ?>
                                <div class="col">
                                    <div class="position-relative form-group">
                                        <label for="idComite">Comités</label>
                                        <select name="comites_id" id="idComite" class="form-control">
                                            <option value="">Choisissez un comité</option>
                                            <?php foreach($comites as $comite): ?>
                                                <option value="<?= $comite->id ?>"><?= $comite->nom ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                            <?php else: ?>
                                <input type="hidden" name="profiles_id" value="5">
                            <?php endif; ?>
                        <?php endif; ?>
                        
                    </div>
                    <?php endif; ?>
                    
                    <?php 
                    if(!$this->fetch('superviseur')):
                        if($userConnect->profiles_id == 2 && $agence->types_agences_id == 1): ?>
                        <div class="form-row mb-4">
                            <div class="col-sm-12">
                                <label>L'activité sera exécuté dans quelle région ?</label>
                                <select class="form-control region_id" required>
                                    <option value="">Veuillez choisir la région</option>
                                    <?php foreach($regions as $region): ?>
                                        <option value="<?= $region->id ?>"><?= $region->nom ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-row mb-4">
                            <div class="col-sm-12">
                                <label for="name">Veuillez choisir le département de la région</label>
                                <select type="text" name="departements_id" class="form-control departement js-example-basic" required>
                                    <option selected="selected" disabled value="">Liste des départements</option>
                                </select>
                            </div>
                        </div>
                    <?php endif; endif; ?>


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Annuler</button>
                    <button type="submit" class="btn btn-success">Ajouter</button>
                </div>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>

<div class="modal fade" id="modalEdit" tabindex="1" role="dialog" aria-labelledby="editModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <?php
            if(!$this->fetch('superviseur'))
                echo $this->Form->create('Agents', array('enctype'=>'multipart/form-data', 'url'=>array('controller' => 'Agents','action'=> 'edit')));
            else 
                echo $this->Form->create('Superviseurs', array('enctype'=>'multipart/form-data', 'url'=>array('controller' => 'Superviseurs', 'action'=> 'edit')));
            ?>
                <div class="modal-header">
                    <h5 class="modal-title" id="addModalLabel">Modification de l'agent</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                
                <div class="modal-body">
                
                    <div class="form-row mb-3">
                        <div class="col">
                            <label for="editPrnom">Prénom</label>
                            <input name="prenom" id="editPrenom" placeholder="Saisir le prénom de l'agent" type="text" class="form-control" required>
                        </div>
                        <div class="col">
                            <label>Nom</label>
                            <input name="nom" id="editNom" placeholder="Saisir le nom de l'agent" type="text" class="form-control" required>
                        </div>
                    </div>
                    <div class="form-row mb-3">
                        <div class="col">
                            <label for="editTel">Téléphone</label>
                            <input name="telephone" id="editTel" placeholder="Numéro de téléphone" type="number" class="form-control" required>
                        </div>
                        <div class="col">
                            <label for="editAdresse">Adresse</label>
                            <input name="adresse" id="editAdresse" placeholder="Saisir son adresse" type="text" class="form-control" required>
                        </div>
                    </div>
                    <div class="position-relative form-group">
                        <label for="editEmail">Email</label>
                        <input name="email" id="editEmail" placeholder="Saisir son adresse email" type="text" class="form-control" required>
                    </div>

                    <?php if(!$this->fetch('superviseur')): ?>
                        <div class="position-relative form-group">
                            <label for="editAgence">Structures</label>
                            <select name="agence_id" id="editAgence" class="form-control">
                                <option value="">Choisissez la structure de l'agent</option>
                                <?php foreach($agences as $service): 
                                    if($service->types_agences_id == $agence->types_agences_id): ?>
                                        <option value="<?= $service->id ?>"><?= $service->nom ?></option>
                                <?php endif; endforeach; ?>
                            </select>
                        </div>
                        <?php if($agence->types_agences_id != 2): ?>
                            <div class="position-relative form-group">
                                <label for="editComite">Comités</label>
                                <select name="comites_id" id="editComite" class="form-control">
                                    <option value="">Choisissez un comité</option>
                                    <?php foreach($comites as $comite): ?>
                                        <option value="<?= $comite->id ?>"><?= $comite->nom ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        <?php endif; ?>
                        <?php if($userConnect->profiles_id == 2): ?>
                            <div class="position-relative form-group">
                                <label for="editAdmin">Rôle</label>
                                <select name="admin" id="editAdmin" class="form-control">
                                    <option value="0"></option>
                                    <option value="1">Admin</option>
                                </select>
                            </div>
                        <?php endif; ?>
                    <?php endif; ?>

                    <?php if($userConnect->profiles_id == 2 && $agence->types_agences_id == 1): ?>
                    <div class="form-row mb-4">
                        <div class="col-sm-12">
                            <label>L'activité sera exécuté dans quelle région ?</label>
                            <select class="form-control region_id" required>
                                <option value="">Veuillez choisir la région</option>
                                <?php foreach($regions as $region): ?>
                                    <option value="<?= $region->id ?>"><?= $region->nom ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-row mb-4">
                        <div class="col-sm-12">
                            <label for="name">Veuillez choisir le département de la région</label>
                            <select type="text" name="departements_id" class="form-control departement js-example-basic" required>
                                <option selected="selected" disabled value="">Liste des départements</option>
                            </select>
                        </div>
                    </div>
                    <?php endif; ?>
                    
                    <input type="hidden" name="idAgent" id="idAgent">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Annuler</button>
                    <button type="submit" class="btn btn-success">Enregistrer</button>
                </div>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div> 


<!-- JQuery -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>


<script>
    $(function () {
        $("#bodyTable").on("click", ".edit_click", function() {
            value = $(this).attr('value');
            data = value.split('+');
            $("#idAgent").val(data[0]);
            $('#editPrenom').val(data[1]);
            $('#editNom').val(data[2]);
            $('#editTel').val(data[3]);
            $('#editAdresse').val(data[4]);
            $('#editEmail').val(data[5]);
            valAdmin = data[6];
            idAgence = data[7];
            idComite = data[8];

            $('#editAdmin option').removeAttr('selected');
            $('#editAdmin').find('option[value="'+ valAdmin +'"]').attr("selected",true);
            $('#editAdmin').change();

            $('#editAgence option').removeAttr('selected');
            $('#editAgence').find('option[value="'+ idAgence +'"]').attr("selected",true);
            $('#editAgence').change();

            $('#editComite option').removeAttr('selected');
            $('#editComite').find('option[value="'+ idComite +'"]').attr("selected",true);
            $('#editComite').change();

            idDepartement = data[9];
            nomDepartement = data[10];
            idRegionUpdate = data[11];

            $('.region_id option').removeAttr('selected');
            $('.region_id').find('option[value="'+ idRegionUpdate +'"]').attr("selected",true);
            $('.region_id').change();

            $('.departement option').removeAttr('selected');
            $('.departement').find('option[value="'+ idDepartement +'"]').attr("selected",true);
            $('.departement').change();


        });

        $(".region_id").change(function () {

            // var region_id = $('.region_id option:selected').val();
            var region_id = $(this).val();
        
            if(region_id != "") {
                <?php $url = $this->Url->build(['controller' => 'Regions', 'action' => 'view']); ?>
                $.ajax(
                    {
                    type: 'GET',
                    url: "<?= $url; ?>/" + region_id,
                    async: false,
                    success: function (json) {
                        var data = JSON.parse(json);
                        var infos;
                        var listdept = [];
                        infos += '<option selected="selected" value=""> Veuillez choisir le département</option>';
                        for (var i in data) {

                            listdept['nom'] = data[i]['departementscol'];
                            infos += '<option required value="' + data[i]['id'] + '">' + listdept['nom'] + '</option>';
                        }
                        $(".departement").html(infos);
                    }
                });
            }
        });

    
    });
</script>
