<?php $this->setLayout('back_office'); ?>

<div class="container">
    <div class="row mb-2 pull-right">
        <div class="col-sm-12">
            <button onclick="window.history.go(-1)" class="btn btn-primary">
                <i class="fa fa-arrow-left fa-w-20"></i> Retour
            </button>
        </div>
    </div><br><br>
    <div class="card mt-page">
        <div class="card-body">
            <h5 class="color font-weight-bold mb-3">
                Détails du dossier 
                <?php if($dossier->soumis == "non" || $dossier->statut_dossiers_id == 5): ?>
                    <?= $this->Form->postLink(__('<i class="fa fa-hand-point-right"></i> Soumettre'), ['action' => 'soumettre', $dossier->id], ['confirm' => __('Voulez-vous vraiment soumettre ce dossier ?'),'class'=>'btn btn-success btn-sm pull-right font-weight-bold','title'=>'Soumettre le dossier','escape'=>false]) ?>
                    <?= $this->Html->link(__('<i class="fa fa-edit"></i> Modifier'), ['controller' => 'Dossiers', 'action' => 'edit', $dossier->id],['class'=>'btn btn-info btn-sm pull-right font-weight-bold mr-3','escape'=>false]) ?>
                <?php endif; ?>
                <?php 
                if($userConnect->profiles_id == 1 && $dossier->emission && $dossier->statut_financements_id == null): ?>
                    <?= $this->Html->link(__('<i class="fa fa-check-circle"></i> Demander un financement'), ['controller' => 'Financements', 'action' => 'add', $dossier->id],['class'=>'btn btn-success btn-sm pull-right font-weight-bold mr-3','escape'=>false]) ?>
                <?php endif; ?>
                <?php if($dossier->statut_financements_id != null): ?>
                    <?= $this->Html->link(__('<i class="fa fa-check-circle"></i> Détails financement'), ['controller' => 'Financements', 'action' => 'details', $dossier->id],['class'=>'btn btn-warning btn-sm pull-right font-weight-bold','escape'=>false]) ?>
                <?php endif; ?>
            </h5>
            <div class="row">
                <div class="col-md-6 mb-5">
                    <div class="card card-grey">
                        <div class="card-body">
                            <?php if($checkEntreprise): ?>
                                <h5 class="font-weight-bold">
                                    <?php if(!$checkAssociation) echo "Informations de l'entreprise"; else echo "Informations de l'association"; ?>
                                </h5>
                                <div class="row">
                                    <div class="col-md-4 col-6">Nom</div>
                                    <div class="col-md-8 col-6">: <?= $dossier->porteur->entreprise->nom ?> </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4 col-6">Ninea</div>
                                    <div class="col-md-8 col-6">: <?= $dossier->porteur->entreprise->ninea ?> </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4 col-6">Immatriculation</div>
                                    <div class="col-md-8 col-6">: <?= $dossier->porteur->entreprise->immatriculation ?> </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4 col-6"><?php if(!$checkAssociation) echo "Type d'entreprise"; else echo "Type d'association"; ?></div>
                                    <div class="col-md-8 col-6">: <?= $dossier->porteur->entreprise->type_entreprise->nom ?> </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4 col-6">Statut juridique</div>
                                    <div class="col-md-8 col-6">
                                        : <?php if($dossier->porteur->entreprise->forme)
                                                    echo "<span class='badge badge-success fa fa-clipboard-check no-text-transform'> Formel</span>";
                                                else echo "<span class='badge badge-warning fa fa-calendar-times no-text-transform'> Informel</span>";
                                        ?> 
                                    </div>
                                </div>
                                <div class="max-hr mb-5"></div>

                                <h5 class="font-weight-bold">Adresse <?php if(!$checkAssociation) echo "de l'entreprise"; else echo "de l'association"; ?></h5>
                                <div class="row">
                                    <div class="col-md-4 col-6">Région</div>
                                    <div class="col-md-8 col-6">: <?= $dossier->porteur->entreprise->commune->departement->region->nom ?> </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4 col-6">Département</div>
                                    <div class="col-md-8 col-6">: <?= $dossier->porteur->entreprise->commune->departement->departementscol ?> </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4 col-6">Commune</div>
                                    <div class="col-md-8 col-6">: <?= $dossier->porteur->entreprise->commune->nom ?> </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4 col-6">Téléphone</div>
                                    <div class="col-md-8 col-6">: <?= $dossier->porteur->entreprise->tel_entite ?> </div>
                                </div>
                            <?php else: ?>
                                <h5 class="font-weight-bold">
                                    Informations personnelles
                                </h5>
                                <div class="row">
                                    <div class="col-md-4 col-6">Nom</div>
                                    <div class="col-md-8 col-6">: <?= $dossier->porteur->nom ?> </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4 col-6">Prénom</div>
                                    <div class="col-md-8 col-6">: <?= $dossier->porteur->prenom ?> </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4 col-6">Genre</div>
                                    <div class="col-md-8 col-6">: <?= $dossier->porteur->genre ?> </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4 col-6">Naissance</div>
                                    <div class="col-md-8 col-6">: <?= $dossier->porteur->date_naissance->format('d/m/Y') ?> <i class="fa fa-calendar"></i></div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4 col-6">Lieu naissance</div>
                                    <div class="col-md-8 col-6">: <?= $dossier->porteur->lieu_naissance ?></div>
                                </div>
                                <div class="max-hr mb-5"></div>

                                <h5 class="font-weight-bold">Autres</h5>
                                <div class="row">
                                    <div class="col-md-4 col-6">Résidence</div>
                                    <div class="col-md-8 col-6">: <?= $dossier->porteur->lieu_residence ?> </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4 col-6">Identification</div>
                                    <div class="col-md-8 col-6">: <?= $dossier->porteur->type_identification ?> </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4 col-6">Numéro</div>
                                    <div class="col-md-8 col-6">: <?= $dossier->porteur->num_identification ?> <i class="fa fa-id-card-alt"></i></div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4 col-6">Téléphone</div>
                                    <div class="col-md-8 col-6">: <?= $dossier->porteur->telephone ?> </div>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>

                <div class="col-md-6 mb-5">
                    <div class="card card-grey">
                        <div class="card-body">
                            <h5 class="font-weight-bold">Informations sur le dossier</h5>
                            <div class="row">
                                <div class="col-md-4 col-6">Code</div>
                                <div class="col-md-8">: <?= $dossier->numero ?> </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4 col-6">Intitulé</div>
                                <div class="col-md-8 col-6">: <?= $dossier->intitule ?> </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4 col-6">Création</div>
                                <div class="col-md-8 col-6">: <?= h($dossier->created->format('d/m/Y à H:i')) ?> </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4 col-6">Secteur</div>
                                <div class="col-md-8 col-6">: <?= $dossier->filiere->nom ?> </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4 col-6">Statut</div>
                                <div class="col-md-8 col-6">
                                    <?php $badge = ""; $statut = ""; $icone = "";
                                        if($dossier->soumis == "non"): $badge = "badge-warning"; $statut = "Non soumis"; $icone = "fa fa-bong";
                                        else:
                                            if($dossier->statut_financements_id == null){
                                                if(($dossier->statut_dossiers_id == 1) || ($dossier->soumis == "oui" && $dossier->statut_dossiers_id == null)): $statut = "En cours"; $badge = "badge-info"; $icone = "fa fa-business-time";
                                                elseif($dossier->statut_dossiers_id == 2): $statut = "qualifié"; $badge = "badge-primary"; $icone = "fa fa-user-check";
                                                elseif($dossier->statut_dossiers_id == 3): $statut = "En cours d'analyse"; $badge = "badge-green-light"; $icone ="fa fa-chalkboard-teacher";
                                                elseif($dossier->statut_dossiers_id == 4): $statut = "Attente de confirmation"; $badge = "badge-green-light"; $icone = "fa fa-check-circle";
                                                elseif($dossier->statut_dossiers_id == 5): $statut = "refusé"; $badge = "badge-danger"; $icone = "fa fa-calendar-times";
                                                elseif($dossier->statut_dossiers_id == 6): $statut = "Labélisation confirmée"; $badge = "badge-success"; $icone = "fa fa-calendar-times";
                                                endif;
                                            }else  {
                                                if($dossier->statut_financements_id == 1): $statut = "Financement soumis"; $badge = "badge-green-light"; $icone ="fa fa-chalkboard-teacher";
                                                elseif($dossier->statut_financements_id == 2): $statut = "Financement en cours"; $badge = "badge-green-light"; $icone ="fa fa-chalkboard-teacher";
                                                elseif($dossier->statut_financements_id == 3): $statut = "Financement qualifié"; $badge = "badge-green-light"; $icone ="fa fa-chalkboard-teacher";
                                                elseif($dossier->statut_financements_id == 4): $statut = "Financement en cours d'analyse"; $badge = "badge-green-light"; $icone ="fa fa-chalkboard-teacher";
                                                elseif($dossier->statut_financements_id == 5): $statut = "Financement accepté"; $badge = "badge-green-light"; $icone ="fa fa-chalkboard-teacher";
                                                elseif($dossier->statut_financements_id == 6): $statut = "Financement confirmé"; $badge = "badge-success"; $icone ="fa fa-check-circle";
                                                elseif($dossier->statut_financements_id == 7): $statut = "Financement rejeté"; $badge = "badge-danger"; $icone ="fa fa-calendar-times";
                                                endif;
                                            }
                                        endif;
                                    
                                    ?>
                                    : <span class="badge <?= $badge ?> <?= $icone ?>">
                                        <?= $statut ?>
                                      </span>

                                      <?php if($dossier->label): 
                                        $typeBadge = "";
                                        if($dossier->type_label == 'standard') $typeBadge = "badge-info";
                                        if($dossier->type_label == 'argent') $typeBadge = "badge-green-light";
                                        if($dossier->type_label == 'gold') $typeBadge = "badge-warning"; ?>
                                        <span class="badge <?= $typeBadge ?>"><i class="fa fa-medal"></i> <?= $dossier->type_label ?></span>
                                    <?php endif; ?>
                                </div>
                            </div>
                            

                            <div class="max-hr mb-5"></div>

                            <h5 class="font-weight-bold">Lieu d'exécution</h5>
                            <div class="row">
                                <div class="col-md-4 col-6">Région</div>
                                <div class="col-md-8 col-6">: <?= $dossier->commune->departement->region->nom ?> </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4 col-6">Département</div>
                                <div class="col-md-8 col-6">: <?= $dossier->commune->departement->departementscol ?> </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4 col-6">Commune</div>
                                <div class="col-md-8 col-6">: <?= $dossier->commune->nom ?> </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4 col-6">Impactés</div>
                                <div class="col-md-8 col-6">: <?= $dossier->nb_perso_impact ?> <i class="fa fa-user"></i></div>
                            </div>

                        </div>
                    </div>
                </div>

            </div>

            <?php if((isset($qualificationRejet) && $qualificationRejet != null) || (isset($notationRejet) && $notationRejet != null)): ?>
                <div class="row mb-5">
                    <div class="col-md-12">
                        <div class="card card-grey">
                            <div class="card-body">
                                <div class="card">
                                    <div class="card-header text-white bg-danger">
                                        Raison du rejet (Partie <?php if($qualificationRejet != null) echo "qualification"; elseif($notationRejet != null) echo "notation"; ?>)
                                    </div>
                                    <div class="card-body">
                                        <strong>
                                        <?php
                                            if($qualificationRejet != null) echo $qualificationRejet->raison_rejet;
                                            elseif($notationRejet != null) echo $notationRejet->raison_rejet;
                                        ?>
                                        </strong>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endif; ?>

            <?php if(count($pieces) > 0): ?>
            <div class="row">
                <div class="col-md-12">
                    <div class="card card-grey">
                        <div class="card-body">
                            <h5 class="font-weight-bold">Pièces jointes</h5>
                            <div class="row">
                                <div class="col-md-6 col-6 font-weight-bold">Titre</div>
                                <div class="col-md-6 col-6 font-weight-bold">Action</div>
                            </div>
                            <div class="max-hr-fonce"></div>
                            <?php foreach($pieces as $piece): ?>
                                <div class="row">
                                    <div class="col-md-6 col-6"><?= $piece->titre ?></div>
                                    <div class="col-md-6 col-6">
                                        <?= $this->Html->link(__(' Voir'),'/documents/dossier/'.$piece->url,
                                                array('target'=>'_blank','escape'=>false, 'class'=>'fa fa-eye btn btn-success btn-sm')) ?>
                                    </div>
                                </div>
                            <?php endforeach; ?>

                            <?php if(!$dossier->emission) echo "<h5 class='text-danger'>La fiche sera disponible après labélisation.</h5>" ?>
                            <?php if($dossier->emission) echo "<h5 class='text-success'>Vous pouvez désormais télécharger la fiche.</h5>" ?>
                            <div class="row">
                                <div class="col-sm-6 mb-2">
                                    <div class="bloc-file d-flex content-fiche">
                                        <div>
                                            <img src="http://www.catco.eu/wp-content/uploads/2018/09/logo-pdf.jpg">
                                        </div>
                                        <div class="info-file">
                                            <span class="title-document">Fiche de la demande</span><br/>
                                            <span class="quantity-file">1 KO</span><br/>
                                            <span class="view-files same-line">
                                                <a href="<?php if($dossier->emission) echo 'javascript:;'; else echo '#!'; ?>" class="<?php if($dossier->emission) echo 'btn-download-fiche'; ?>">Télécharger</a>
                                                <a href="<?php if($dossier->emission) echo 'javascript:;'; else echo '#!'; ?>" class="<?php if($dossier->emission) echo 'btn-view-fiche'; ?> ml-3">Aperçu</a>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <?php if($dossier->emission): ?>
                                <div class="col-sm-6">
                                    <div class="bloc-file d-flex content-fiche">
                                        <div>
                                            <img src="http://www.catco.eu/wp-content/uploads/2018/09/logo-pdf.jpg">
                                        </div>
                                        <div class="info-file">
                                            <span class="title-document">Certificat E.S.S</span><br/>
                                            <span class="quantity-file">1 KO</span><br/>
                                            <span class="view-files same-line">
                                                <a href="<?php if($dossier->emission) echo 'javascript:;'; else echo '#!'; ?>" class="<?php if($dossier->emission) echo 'btn-download-fiche-certificat'; ?>">Télécharger</a>
                                                <a href="<?php if($dossier->emission) echo 'javascript:;'; else echo '#!'; ?>" class="<?php if($dossier->emission) echo 'btn-view-fiche-certificat'; ?> ml-3">Aperçu</a>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <?php endif; ?>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <?php endif; ?>

            <div class="max-hr mb-5"></div>

            <?php if(count($dossiersFilieres) > 0): ?>
            <div class="row">
                <div class="col-md-12">
                    <div class="card card-grey">
                        <div class="card-body">
                            <h5 class="font-weight-bold">Secteurs impactés</h5>
                            <div class="row">
                                <div class="col-md-6 col-6 font-weight-bold">Filière</div>
                                <div class="col-md-6 col-6 font-weight-bold">Type</div>
                            </div>
                            <div class="max-hr-fonce"></div>
                            <?php foreach($dossiersFilieres as $dossierFiliere): ?>
                                <div class="row">
                                    <div class="col-md-6 col-6"><?= $dossierFiliere->filiere->nom ?></div>
                                    <div class="col-md-6 col-6"><?= $dossierFiliere->link_type ?></div>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
                
            </div>
            <?php endif; ?>
            
            <?php if(count($dossiersImpacts) > 0): ?>
            <div class="max-hr mb-5"></div>

            <div class="row">
                <div class="col-md-12">
                    <div class="card card-grey">
                        <div class="card-body">
                            <h5 class="font-weight-bold">Types d'impacts</h5>
                            <div class="row">
                                <div class="col-md-6 col-6 font-weight-bold">Type d'impact</div>
                                <div class="col-md-6 col-6 font-weight-bold">Impacte</div>
                            </div>
                            <div class="max-hr-fonce"></div>
                            <?php foreach($dossiersImpacts as $dossierImpact): ?>
                                <div class="row">
                                    <div class="col-md-6 col-6"><?= $dossierImpact->types_impact->libelle ?></div>
                                    <div class="col-md-6 col-6"><?= $dossierImpact->types_impact->impacte->libelle ?></div>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="max-hr mb-5"></div>
            
            <?php endif; ?>

        </div>

    </div>

</div>


<?php 
// Méthode permettant de limiter le nombre de caractère d'une chaine à afficher
function cutString($chaine, $taille){
    $chaineTransform = substr($chaine, 0, $taille);  // Nombre de caractère souhaité
    if(strlen($chaineTransform) >= $taille){
        echo $chaineTransform.'...';
    }else{
        echo $chaine;
    }
}
?>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<?= $this->Html->script('pdfmaker/pdfmake.min') ?>
<?= $this->Html->script('pdfmaker/vfs_fonts') ?>

<script>
    $(function(){
        function getVarPdf(urlImage){
            var dd = {
                pageSize : 'A4',
                pageMargins: [ 30, 60, 30, 80 ],
                header : {
                    margin : [20,18,20,0],
                    columns : [
                        {
                            image: urlImage,
                            width: 70,
                            style: 'font_pdf',
                            margin : [ 25, 0, 0, 0 ],
                        }, {
                            text: 'CR-PROC-TSEN-RH-01 M',
                            alignment: 'right',
                            margin : [ 0, 0, 25, 0 ],
                            fontSize : 10,
                            bold : true
                        } 
                    ]
                },
                
                footer: {
                    columns: [
                    { 
                        text: 'Généré par ESS', 
                        bold: true,
                        alignment: 'center',
                        color: 'gray'
                    }
                    ]
                },
                
                content: [
                    
                    {
                        text: 'DEMANDE LABÉLISATION CODE : <?= $dossier->numero ?>\n\n\n',
                        style: 'big_title',
                        alignment: 'center',
                        decoration: 'underline'
                    },
                    <?php if($checkEntreprise): ?>
                    {
                        style: 'medium_title',
                        decoration: 'underline',
                        margin : [20,10,20,0],
                        text: "<?php if(!$checkAssociation) echo 'Informations de l\'entreprise'; else echo 'Informations de l\'association'; ?>",
                    },
                    {
                        margin : [20,10,20,0],
                        columns: [
                            {
                                text: 'Nom : ',
                                style: 'font_pdf'
                            },
                            {
                                text: '<?= h($dossier->porteur->entreprise->nom) ?>\n',
                                style: 'font_pdf'
                            }
                        ]

                    },
                    {
                        margin : [20,10,20,0],
                        columns: [
                            {
                                text: 'Ninea : ',
                                style: 'font_pdf'
                            },
                            {
                                text: '<?= h($dossier->porteur->entreprise->ninea) ?>\n',
                                style: 'font_pdf'
                            }
                        ]

                    },
                    {
                        margin : [20,10,20,0],
                        columns: [
                            {
                                text: 'Immatriculation : ',
                                style: 'font_pdf'
                            },
                            {
                                text: '<?= h($dossier->porteur->entreprise->immatriculation) ?>\n',
                                style: 'font_pdf'
                            }
                        ]

                    },
                    {
                        margin : [20,10,20,0],
                        columns: [
                            {
                                text: "<?php if(!$checkAssociation) echo 'Type d\'entreprise : '; else echo 'Type association : '; ?>\n",
                                style: 'font_pdf'
                            },
                            {
                                text: '<?= h($dossier->porteur->entreprise->type_entreprise->nom) ?>\n',
                                style: 'font_pdf'
                            }
                        ]

                    },
                    {
                        margin : [20,10,20,0],
                        columns: [
                            {
                                text: 'Statut juridique : ',
                                style: 'font_pdf'
                            },
                            {
                                text: '<?php if($dossier->porteur->entreprise->forme) echo "Formel"; else echo "Informel"; ?>\n',
                                style: 'font_pdf'
                            }
                        ]

                    },

                    {
                        style: 'medium_title',
                        decoration: 'underline',
                        margin : [20,20,20,0],
                        text: "Adresse <?php if(!$checkAssociation) echo 'de l\'entreprise'; else echo 'de l\'association'; ?>",
                    },
                    {
                        margin : [20,10,20,0],
                        columns: [
                            {
                                text: "Région : ",
                                style: 'font_pdf'
                            },
                            {
                                text: '<?= h($dossier->porteur->entreprise->commune->departement->region->nom) ?>\n',
                                style: 'font_pdf'
                            }
                        ]

                    },
                    {
                        margin : [20,10,20,0],
                        columns: [
                            {
                                text: "Département : ",
                                style: 'font_pdf'
                            },
                            {
                                text: '<?= h($dossier->porteur->entreprise->commune->departement->departementscol) ?>\n',
                                style: 'font_pdf'
                            }
                        ]

                    },
                    {
                        margin : [20,10,20,0],
                        columns: [
                            {
                                text: "Commune : ",
                                style: 'font_pdf'
                            },
                            {
                                text: '<?= h($dossier->porteur->entreprise->commune->nom) ?>\n',
                                style: 'font_pdf'
                            }
                        ]

                    },
                    {
                        margin : [20,10,20,0],
                        columns: [
                            {
                                text: "Téléphone : ",
                                style: 'font_pdf'
                            },
                            {
                                text: '<?= h($dossier->porteur->entreprise->tel_entite) ?>\n',
                                style: 'font_pdf'
                            }
                        ]

                    },

                    <?php else: ?>

                    {
                        style: 'medium_title',
                        decoration: 'underline',
                        margin : [20,20,20,0],
                        text: "Informations personnelles",
                    },
                    {
                        margin : [20,10,20,0],
                        columns: [
                            {
                                text: "Prénom et nom : ",
                                style: 'font_pdf'
                            },
                            {
                                text: '<?= h($dossier->porteur->prenom)." ".h($dossier->porteur->nom) ?>\n',
                                style: 'font_pdf'
                            }
                        ]

                    },
                    {
                        margin : [20,10,20,0],
                        columns: [
                            {
                                text: "Genre : ",
                                style: 'font_pdf'
                            },
                            {
                                text: '<?= h($dossier->porteur->genre) ?>\n',
                                style: 'font_pdf'
                            }
                        ]

                    },
                    {
                        margin : [20,10,20,0],
                        columns: [
                            {
                                text: "Naissance : ",
                                style: 'font_pdf'
                            },
                            {
                                text: '<?= h($dossier->porteur->date_naissance->format("d/m/Y")) ?>\n',
                                style: 'font_pdf'
                            }
                        ]

                    },
                    {
                        margin : [20,10,20,0],
                        columns: [
                            {
                                text: "Lieu de naissance : ",
                                style: 'font_pdf'
                            },
                            {
                                text: '<?= h($dossier->porteur->lieu_naissance) ?>\n',
                                style: 'font_pdf'
                            }
                        ]

                    },

                    {
                        style: 'medium_title',
                        decoration: 'underline',
                        margin : [20,20,20,0],
                        text: "Autres",
                    },
                    {
                        margin : [20,10,20,0],
                        columns: [
                            {
                                text: "Résidence : ",
                                style: 'font_pdf'
                            },
                            {
                                text: '<?= h($dossier->porteur->lieu_residence) ?>\n',
                                style: 'font_pdf'
                            }
                        ]

                    },
                    {
                        margin : [20,10,20,0],
                        columns: [
                            {
                                text: "Identification : ",
                                style: 'font_pdf'
                            },
                            {
                                text: '<?= h($dossier->porteur->type_identification) ?>\n',
                                style: 'font_pdf'
                            }
                        ]

                    },
                    {
                        margin : [20,10,20,0],
                        columns: [
                            {
                                text: "Numéro : ",
                                style: 'font_pdf'
                            },
                            {
                                text: '<?= h($dossier->porteur->num_identification) ?>\n',
                                style: 'font_pdf'
                            }
                        ]

                    },
                    {
                        margin : [20,10,20,0],
                        columns: [
                            {
                                text: "Téléphone : ",
                                style: 'font_pdf'
                            },
                            {
                                text: '<?= h($dossier->porteur->telephone) ?>\n',
                                style: 'font_pdf'
                            }
                        ]

                    },

                    <?php endif; ?>

                    {
                        style: 'medium_title',
                        decoration: 'underline',
                        margin : [20,20,20,0],
                        text: "Informations sur le dossier",
                    },
                    {
                        margin : [20,10,20,0],
                        columns: [
                            {
                                text: "Intitulé : ",
                                style: 'font_pdf'
                            },
                            {
                                text: '<?= h($dossier->intitule) ?>\n',
                                style: 'font_pdf'
                            }
                        ]

                    },
                    {
                        margin : [20,10,20,0],
                        columns: [
                            {
                                text: "Création : ",
                                style: 'font_pdf'
                            },
                            {
                                text: '<?= h($dossier->created->format("d/m/Y à H:i")) ?>\n',
                                style: 'font_pdf'
                            }
                        ]

                    },
                    {
                        margin : [20,10,20,0],
                        columns: [
                            {
                                text: "Secteur : ",
                                style: 'font_pdf'
                            },
                            {
                                text: '<?= h($dossier->filiere->nom) ?>\n',
                                style: 'font_pdf'
                            }
                        ]

                    },
                    {
                        margin : [20,10,20,0],
                        columns: [
                            {
                                text: "Label : ",
                                style: 'font_pdf'
                            },
                            {
                                text: '<?= h($dossier->type_label) ?>\n',
                                style: 'font_pdf'
                            }
                        ]

                    },

                    {
                        style: 'medium_title',
                        decoration: 'underline',
                        margin : [20,20,20,0],
                        text: "Lieu d'exécution",
                    },
                    {
                        margin : [20,10,20,0],
                        columns: [
                            {
                                text: "Région : ",
                                style: 'font_pdf'
                            },
                            {
                                text: '<?= h($dossier->commune->departement->region->nom) ?>\n',
                                style: 'font_pdf'
                            }
                        ]

                    },
                    {
                        margin : [20,10,20,0],
                        columns: [
                            {
                                text: "Département : ",
                                style: 'font_pdf'
                            },
                            {
                                text: '<?= h($dossier->commune->departement->departementscol) ?>\n',
                                style: 'font_pdf'
                            }
                        ]

                    },
                    {
                        margin : [20,10,20,0],
                        columns: [
                            {
                                text: "Commune : ",
                                style: 'font_pdf'
                            },
                            {
                                text: '<?= h($dossier->commune->nom) ?>\n',
                                style: 'font_pdf'
                            }
                        ]

                    },
                    {
                        margin : [20,10,20,0],
                        columns: [
                            {
                                text: "Personnes impactées : ",
                                style: 'font_pdf'
                            },
                            {
                                text: '<?= h($dossier->nb_perso_impact) ?>\n',
                                style: 'font_pdf'
                            }
                        ]

                    },

                    {
                        text: 'Secteurs impactés',
                        margin : [20,25,20,0],
                        fontSize : 12,
                        bold: true,
                        decoration: 'underline'
                    },
                    {
                        style: 'table',
                        margin : [20,10,20,0],
                        fontSize: 9,
                        table: {
                            widths: [250, 250],
                            headerRows: 1,
                            heights: 15,
                            body: [
                                [{text: "Filière", margin: [ 0, 2, 0, 0 ], bold: true, decoration: 'underline'}, {text: "Type", margin: [ 0, 2, 0, 0 ], bold: true, decoration: 'underline'}],

                                <?php foreach($dossiersFilieres as $dossierFiliere): ?>
                                    ["<?= $dossierFiliere->filiere->nom ?>", {text: "<?= $dossierFiliere->link_type ?>",  margin: [ 0, 2, 0, 0 ]} ],
                                <?php endforeach; ?>

                            ]
                        }
                    },
                    {
                        text: "Types d'impacts",
                        margin : [20,25,20,0],
                        fontSize : 12,
                        bold: true,
                        decoration: 'underline'
                    },
                    {
                        style: 'table',
                        margin : [20,10,20,0],
                        fontSize: 9,
                        table: {
                            widths: [250, 250],
                            headerRows: 1,
                            heights: 15,
                            body: [
                                [{text: "Type d'impact", margin: [ 0, 2, 0, 0 ], bold: true, decoration: 'underline'}, {text: "Impact", margin: [ 0, 2, 0, 0 ], bold: true, decoration: 'underline'}],

                                <?php foreach($dossiersImpacts as $dossierImpact): ?>
                                    ["<?= $dossierImpact->types_impact->libelle ?>", {text: "<?= $dossierImpact->types_impact->impacte->libelle ?>", margin: [ 0, 2, 0, 0 ]} ],
                                <?php endforeach; ?>

                            ]
                        }
                    },

                ],

                styles: {
                    font_pdf: {
                        fontSize: 10,
                        bold: true
                    },
                    big_title: {
                        fontSize: 15,
                        bold: true,
                        alignment: 'justify'
                    },
                    medium_title: {
                        fontSize: 12,
                        bold: true
                    }
                }

            };

            return dd;

        }

        function toDataURL(url, callback, outputFormat) {
            var img = new Image();
            img.crossOrigin = 'Anonymous';
            img.onload = function() {
                var canvas = document.createElement('CANVAS');
                var ctx = canvas.getContext('2d');
                var dataURL;
                canvas.height = this.naturalHeight;
                canvas.width = this.naturalWidth;
                ctx.drawImage(this, 0, 0);
                dataURL = canvas.toDataURL(outputFormat);
                callback(dataURL);
            };
            img.src = url;
            if (img.complete || img.complete === undefined) {
                //img.src = "data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///ywAAAAAAQABAAACAUwAOw==";
                img.src = src;
            }
        }

        var getUrl = window.location;
        var baseUrl = getUrl.protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];

        var urlImage = '';
        if(baseUrl.indexOf("localhost") >= 0) {
            urlImage = baseUrl+'/ess/img/ess.png';
        }else {
            urlImage = getUrl.protocol + "//" + getUrl.host + "/" + "/img/ess.png";
        }
        
        // Pour consulter la fiche.
        $('.btn-view-fiche').click(function () {
            toDataURL(urlImage, function(dataUrl) {
                
                doc = getVarPdf(dataUrl);
                pdfMake.createPdf(doc).open();

            });
        
        });

        
        // Pour télécharger le document.
        $('.btn-download-fiche').click(function () {
            toDataURL(urlImage, function(dataUrl) {
                
                doc = getVarPdf(dataUrl);
                pdfMake.createPdf(doc).download("Fiche labélisation");

            });
        
        });


        function getCertificat(urlImage, urlSignature, urlMinistere){
            var dd = {
                // pageSize : 'SRA4',
                pageSize: {
                    width: 870,
                    height: 400
                },
                pageOrientation: 'landscape',
                pageMargins: [ 30, 50, 30, 50 ],
                header : {
                    margin : [20,20,20,0],
                    columns : [
                        {
                            image: urlMinistere,
                            width: 50,
                            style: 'font_pdf',
                            margin : [ 60, 0, 0, 0 ],
                        },
                        {
                            <?php $indiRegion = null; $indiDept = null; $indiCommune = null; 
                                $indiRegion = substr($dossier->commune->departement->region->nom, 0, 3);
                                $indiDept = substr($dossier->commune->departement->departementscol, 0, 2);
                                $indiCommune = substr($dossier->commune->nom, 0, 3);
                            ?>
                            text: "MMESS/<?= $indiRegion ?>/<?= $indiDept ?>/<?= $indiCommune ?>/<?= date('Y')."".date('m')."".date('d')."_".$dossier->numero ?>",
                            alignment: 'right',
                            margin : [ 0, 10, 60, 0 ],
                            fontSize : 8,
                            bold : true
                        }
                        
                         
                    ]
                },
                
                content: [
                    {
                        text: 'CERTIFICAT E.S.S',
                        style: 'title_certificat',
                        margin : [ 0, 25, 0, 0 ],
                        alignment: 'center'
                    },
                    {
                        image: urlImage,
                        width: 70,
                        style: 'font_pdf',
                        margin : [ 0, 10, 0, 0 ],
                        alignment: 'center',
                    },
                    {
                        text: "LABEL POUR L'ECONOMIE SOCIALE ET SOLIDAIRE",
                        style: 'sub_title_certificat',
                        margin : [0,10,0,0],
                        alignment: 'center'
                    },
                    {
                        text: "Le présent document atteste que <?= $checkEntreprise ? h($dossier->porteur->entreprise->nom) : h($dossier->porteur->prenom.' '.$dossier->porteur->nom) ?> satisfait "+
                            "aux exigences du programme de reconnaissance des acteurs de "+
                            "l’Economie Sociale et Solidaire pour sa contribution au "+
                            "développement durable à travers ses activités '<?= h($dossier->intitule) ?>'.",
                        margin : [137,10,80,0],
                        style: 'text_certificat'
                    },
                    {
                        text: "Fait à Dakar le : <?= date('d/m/Y') ?> (Durée de validité 2 ans)",
                        margin: [138, 10, 0, 0],
                        fontSize: 10
                    },
                    {
                        margin : [0,40,-40,0],
                        style: "text_signature",
                        columns : [
                            {
                                text: "Le coordonateur",
                                margin : [ 138, 0, 0, 0 ],
                            }, 
                            {
                                text: "Le ministre de la microfinance et de\n L'économie sociale et solidaire",
                                alignment: 'center',
                                margin : [ 0, 0, 15, 0 ]
                            }
                        ]
                    },
                    {
                        columns : [
                            {
                                image: urlSignature,
                                width: 80,
                                margin : [ 585, 5, 0, 0 ]
                            }
                        ]
                        
                    }

                ],

                styles: {
                    font_pdf: {
                        fontSize: 10,
                        bold: true
                    },
                    title_certificat: {
                        fontSize: 24,
                        bold: true
                    },
                    sub_title_certificat: {
                        fontSize: 24,
                        bold: true,
                        alignment: 'justify',
                        color: '#566E20'
                    },
                    text_certificat: {
                        fontSize: 12,
                    },
                    text_signature: {
                        fontSize: 10
                    },
                }

            };

            return dd;

        }

        function toDataURLSignature(url, callback, outputFormat) {
            var img = new Image();
            img.crossOrigin = 'Anonymous';
            img.onload = function() {
                var canvas = document.createElement('CANVAS');
                var ctx = canvas.getContext('2d');
                var dataURL;
                canvas.height = this.naturalHeight;
                canvas.width = this.naturalWidth;
                ctx.drawImage(this, 0, 0);
                dataURL = canvas.toDataURL(outputFormat);
                callback(dataURL);
            };
            img.src = url;
            if (img.complete || img.complete === undefined) {
                //img.src = "data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///ywAAAAAAQABAAACAUwAOw==";
                img.src = src;
            }
        }

        function toDataURLMinistere(url, callback, outputFormat) {
            var img = new Image();
            img.crossOrigin = 'Anonymous';
            img.onload = function() {
                var canvas = document.createElement('CANVAS');
                var ctx = canvas.getContext('2d');
                var dataURL;
                canvas.height = this.naturalHeight;
                canvas.width = this.naturalWidth;
                ctx.drawImage(this, 0, 0);
                dataURL = canvas.toDataURL(outputFormat);
                callback(dataURL);
            };
            img.src = url;
            if (img.complete || img.complete === undefined) {
                //img.src = "data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///ywAAAAAAQABAAACAUwAOw==";
                img.src = src;
            }
        }

        var imgSignature = ''; var imgLogoMinistere = '';
        if(baseUrl.indexOf("localhost") >= 0) {
            imgSignature = baseUrl+'/ess/img/signature.jpg';
            imgLogoMinistere = baseUrl+'/ess/img/portfolio/icon.jpg';
        }else {
            imgSignature = getUrl.protocol + "//" + getUrl.host + "/" + "/img/signature.jpg";
            imgLogoMinistere = getUrl.protocol + "//" + getUrl.host + "/" + "/img/portfolio/icon.jpg";
        }
        $('.btn-view-fiche-certificat').click(function () {
            toDataURL(urlImage, function(dataUrl) {
                toDataURLSignature(imgSignature, function(dataUrlSignature){
                    toDataURLMinistere(imgLogoMinistere, function(dataUrlMinistere){
                        doc = getCertificat(dataUrl, dataUrlSignature, dataUrlMinistere);
                        pdfMake.createPdf(doc).open();
                    });
                });
                
            });
        
        });

        
        // Pour télécharger le document.
        $('.btn-download-fiche-certificat').click(function () {

            toDataURL(urlImage, function(dataUrl) {
                toDataURLSignature(imgSignature, function(dataUrlSignature){
                    toDataURLMinistere(imgLogoMinistere, function(dataUrlMinistere){
                        doc = getCertificat(dataUrl, dataUrlSignature, dataUrlMinistere);
                        pdfMake.createPdf(doc).download("Certificat E.S.S");
                    });
                });
                
            });
        
        });

    });
</script>