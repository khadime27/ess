<?php $this->setLayout('back_office'); ?>
<?php if($this->fetch('type')) $type = $this->fetch('type'); ?>
<div class="app-page-title">
    
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            <div class="page-title-icon">
                <i class="pe-7s-drawer icon-gradient bg-happy-itmeo"></i>
            </div>
            <div><?= $this->fetch('titre') ?> :
                <span class="font-weight-bold text-success text-uppercase">
                    <?php if($userConnect->agents){
                        if($userConnect->agents[0]->comites_id == 1) echo "Espace qualification"; 
                        if($userConnect->agents[0]->comites_id == 2) echo "Espace analyse"; 
                        if($userConnect->profiles_id == 2) echo "Espace superadmin";
                        if($userConnect->profiles_id == 5) echo "Espace comité";
                        if($userConnect->profiles_id == 6) echo "Espace superviseur";
                    } elseif($userConnect->profiles_id == 1) echo "Espace porteur";
                    ?>
                </span>
                <div class="page-title-subheading"><?= $this->fetch('phrase') ?></div>
            </div>
        </div>
        
        <div class="page-title-actions">
            <button onclick="window.history.go(-1)" class="btn btn-primary">
                <i class="fa fa-arrow-left fa-w-20"></i> Retour
            </button>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <div class="main-card mb-3 card">
            <?php if($userConnect->profiles_id == 6 && $this->fetch('type-page') == "notraites"): ?>
            <div class="card-header">
                <div class="row w-100">
                    <div class="col-sm-12">
                        <button class="btn btn-warning btn-sm pull-right btn-circle m-3 sendEmission" style="width: 150px"><b>Émettre</b></button>
                    </div>
                </div>
            </div>
            <?php endif; ?>
            <div class="card-body"><h5 class="card-title">Dossiers</h5>
                <?= $this->Flash->render() ?>
                <table class="mb-0 table">
                    <?php if($userConnect->profiles_id == 6 && $this->fetch('type-page') == "notraites"): ?>
                        <input type="checkbox" name="" class="selectAll mr-1" id="selectall" /> <label for="selectall"><strong>Tout sélectionner</strong></label>
                    <?php endif; ?>
                    <thead>
                        <tr>
                            <?php if($userConnect->profiles_id == 6 && $this->fetch('type-page') == "notraites"): ?>
                                <th></th>
                            <?php endif; ?>
                            <th>Code</th>
                            <th>Porteur</th>
                            <th>Intitulé</th>
                            <th>Filière</th>
                            <?php if($this->fetch('comite') || $this->fetch('superviseur')): ?>
                                <th>Note</th>
                            <?php endif; ?>
                            <th class="text-center"><?= __('Actions') ?></th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php foreach($dossiers as $dossier): ?>
                        <tr>
                            <?php if($userConnect->profiles_id == 6 && $this->fetch('type-page') == "notraites"): ?>
                                <td><input type="checkbox" name="" value="<?= $dossier->id ?>" class="checkbox" /></td>
                            <?php endif; ?>
                            <td><?= $dossier->numero ?></td>
                            <td><?= h($dossier->porteur->prenom." ".$dossier->porteur->nom) ?></td>
                            <td><?= h($dossier->intitule) ?></td>
                            <td><?= h($dossier->filiere->nom) ?></td>
                            <?php if($this->fetch('comite') || $this->fetch('superviseur')): ?>
                                <td><?php if($this->fetch('type') == "label") echo h($dossier->note_final)." / 20"; else echo h($dossier->note_financement)." / 20"; ?></td>
                            <?php endif; ?>
                            <td class="actions text-center">
                                <?= $this->Html->link(__('<i class="fa fa-eye"></i> Détails'), ['controller' => 'Dossiers', 'action' => 'view', $dossier->id],['class'=>' btn btn-primary btn-sm','escape'=>false]) ?>
   
                                <?php if($userConnect->profiles_id == 4): // Validateur
                                    if($userConnect->agents[0]->comites_id == 1): ?> 
                                        <?= $this->Html->link(__('<i class="fa fa-folder"></i> Qualification'), ['controller' => 'Qualifications', 'action' => 'edit', $dossier->id, $dossier->code_assignation],['class'=>' btn btn-success btn-sm','escape'=>false]) ?>
                                    <?php 
                                    elseif($userConnect->agents[0]->comites_id == 2) : 
                                        $typeAnalyse = 'label';
                                        if(isset($type)): if($type == "finance") $typeAnalyse = $type; endif;
                                        if(!$this->fetch('detail')): ?>
                                            <?= $this->Html->link(__('<i class="fa fa-folder"></i> Analyse'), ['controller' => 'Analyses', 'action' => 'edit', $dossier->id, $typeAnalyse],['class'=>' btn btn-success btn-sm','escape'=>false]) ?>
                                        <?php else: ?>
                                            <?= $this->Html->link(__('<i class="fa fa-folder"></i> Voir analyse'), ['controller' => 'Analyses', 'action' => 'edit', $dossier->id, $typeAnalyse, 1, $dossier->notation_id],['class'=>' btn btn-success btn-sm','escape'=>false]) ?>
                                        <?php endif; ?>
                                    <?php endif; ?>
                                <?php endif; ?>

                                <?php if($userConnect->profiles_id == 3): // Operateur
                                    if($type == "label"): // Labélisation
                                        if($userConnect->agents[0]->admin && $dossier->statut_dossiers_id == null): ?>
                                            <button type="button" value='<?= $dossier->id ?>' class="btn btn-sm btn-success edit_click" data-toggle="modal" data-target="#modalAssignation"><i class="fa fa fa-arrow-circle-right"></i> Assignation</button>
                                        <?php elseif($userConnect->agents[0]->admin && $dossier->statut_dossiers_id == 2 && $userConnect->agents[0]->comites_id == 2): ?>
                                            <button type="button" value='<?= $dossier->id ?>' class="btn btn-sm btn-success edit_click" data-toggle="modal" data-target="#modalAssignation"><i class="fa fa fa-arrow-circle-right"></i> Assignation</button>
                                        <?php elseif($userConnect->agents[0]->admin && $dossier->statut_dossiers_id >= 2):
                                            $controleur = ""; $action = ""; $title_button = "";
                                            if($userConnect->agents[0]->comites_id == 1): $controleur = "Qualifications"; $action = "details"; $title_button = "Voir qualification"; endif;
                                            if($userConnect->agents[0]->comites_id == 2): $controleur = "Analyses"; $action = "validateurs"; $title_button = "Notation"; endif; ?>
                                            <?= $this->Html->link(__('<i class="fa fa-users"></i>'.$title_button), ['controller' => $controleur, 'action' => $action, $dossier->id, $dossier->code_assignation, $type],['class'=>' btn btn-warning btn-sm','escape'=>false]) ?>
                                        <?php endif;
                                    else: // Financement
                                        if($type == "finance"):
                                            if($userConnect->agents[0]->admin && $dossier->statut_financements_id == 1): ?>
                                                <button type="button" value='<?= $dossier->id ?>' class="btn btn-sm btn-success edit_click" data-toggle="modal" data-target="#modalAssignation"><i class="fa fa fa-arrow-circle-right"></i> Assignation</button>
                                            <?php elseif($userConnect->agents[0]->admin && $dossier->statut_financements_id == 3 && $userConnect->agents[0]->comites_id == 2): ?>
                                                <button type="button" value='<?= $dossier->id ?>' class="btn btn-sm btn-success edit_click" data-toggle="modal" data-target="#modalAssignation"><i class="fa fa fa-arrow-circle-right"></i> Assignation</button>
                                            <?php elseif($userConnect->agents[0]->admin && $dossier->statut_financements_id >= 5):
                                                $controleur = ""; $action = ""; $title_button = "";
                                                if($userConnect->agents[0]->comites_id == 1): $controleur = "Qualifications"; $action = "details"; $title_button = "Qualification"; endif;
                                                if($userConnect->agents[0]->comites_id == 2): $controleur = "Analyses"; $action = "validateurs"; $title_button = "Notation"; endif; ?>
                                                <?= $this->Html->link(__('<i class="fa fa-users"></i>'.$title_button), ['controller' => $controleur, 'action' => $action, $dossier->id, $dossier->code_assignation, $type],['class'=>' btn btn-warning btn-sm','escape'=>false]) ?>
                                            <?php endif; 
                                        endif; ?>
                                <?php endif; endif; ?>

                                <?php if($userConnect->profiles_id == 5 && ($dossier->statut_dossiers_id == 4 || ($dossier->statut_financements_id == 5))): ?>
                                    <?= $this->Form->postLink(__('Accepter'), ['action' => 'confirmerLabel', $dossier->id], ['confirm' => __('Voulez-vous vraiment accepter ce dossier ?'),'class'=>'btn btn-success btn-circle btn-sm','title'=>'Accepter','escape'=>false]) ?>
                                <?php endif; ?>

                                <?php if($userConnect->profiles_id == 6 && $dossier->statut_dossiers_id == 6 && !$dossier->emission): ?>
                                    <?= $this->Form->postLink(__('<i class="fa fa-check-circle"></i> BON POUR EMISSION'), ['action' => 'confirmEmission', $dossier->id], ['confirm' => __('Voulez-vous vraiment émettre ce dossier ?'),'class'=>'btn btn-success btn-circle btn-sm','title'=>'Accepter','escape'=>false]) ?>
                                <?php endif; ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
                
            </div>

        </div>   

    </div>

</div>

<!-- Modal assignation d'un dossier -->
<div class="modal fade" id="modalAssignation" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Assignation du dossier</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <?= $this->Form->create('Dossiers', array('enctype'=>'multipart/form-data', 'url'=>array('controller' => 'Dossiers', 'action' => 'assignation'), 'id' => 'formAssignation')); ?>    
                    <div class="form-row">
                        <label>Agents comités <?php if($userConnect->agents[0]->comites_id==1) echo "de qualification"; else echo "d'analyse (1er validateur)"; ?> </label>
                        <select name="agent_id" class="form-control mb-3">
                            <option value="">Choisissez un agent</option>
                            <?php foreach($agents as $agent): ?>
                                <option value="<?= $agent->id ?>"><?= $agent->prenom." ".$agent->nom ?> (<?= $agent->agence->nom ?>)</option>
                            <?php endforeach; ?>
                        </select>
                        <?php if($userConnect->agents[0]->comites_id==2): ?>
                            <label>Agents comités d'analyse (2ième validateur)</label>
                            <select name="agent_id2" class="form-control mb-3">
                                <option value="">Choisissez un agent</option>
                                <?php foreach($agents as $agent): ?>
                                    <option value="<?= $agent->id ?>"><?= $agent->prenom." ".$agent->nom ?> (<?= $agent->agence->nom ?>)</option>
                                <?php endforeach; ?>
                            </select>

                            <label>Agents comités d'analayse (3ième validateur) </label>
                            <select name="agent_id3" class="form-control mb-3">
                                <option value="">Choisissez un agent</option>
                                <?php foreach($agents as $agent): ?>
                                    <option value="<?= $agent->id ?>"><?= $agent->prenom." ".$agent->nom ?> (<?= $agent->agence->nom ?>)</option>
                                <?php endforeach; ?>
                            </select>
                        <?php endif; ?>
                        <?php echo $this->Form->hidden('dossier_id',['value'=>'','id'=>'dossier_id']); ?>
                    </div>
                <?= $this->Form->end() ?>
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Fermer</button>
                <button type="submit" class="btn btn-success btnValidate">Valider</button>
            </div>
        </div>
    </div>
</div>




<!-- JQuery -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<script>
    $(function () {
        $(".edit_click").click(function() {
        data = this.value.split('+');
            $("#dossier_id").val(data[0]);
        });

        $('.btnValidate').click(function(){
            $('#formAssignation').submit();
        });
    
    });

    $(function(){

        $('.selectAll').click(function() {
            if ($(this).is(':checked')) {
                $('.checkbox').attr('checked', true);
            } else {
                $('.checkbox').attr('checked', false);
            }
        });
        
        $checkbox = $('.checkbox');
      
        $('.sendEmission').click(function(){
            var chkArray = [];
            chkArray = $.map($checkbox, function(el){
                if(el.checked) { return el.value };
            });
          
            if(chkArray.length > 0) {
                sendEmission(chkArray);
            }else {
                alert("Opération échouée, il faut cocher au moins une ligne.");
            }
            
        });
      
        function sendEmission(ids){
            <?php $url = $this->Url->build(['controller' => 'Labelisations', 'action' => 'sendEmission']); ?>

            $.ajax({
                url : '<?= $url ?>',
                type : 'POST',
                data: {data : ids}, 
                cache: false,
                success : function(data){
                    alert('Opération bien effectuée !');
                    document.location.reload();
                    
                    //var donnees = JSON.parse(data);
                    console.log(data)
                },
                error : function(data){
                    console.log(data);
                }

            });
            
        }
    });
</script>
