<?php $this->setLayout('back_office'); ?>
<div class="app-page-title">
    
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            <div class="page-title-icon">
                <i class="pe-7s-drawer icon-gradient bg-happy-itmeo"></i>
            </div>
            <div><?= $this->fetch('titre') ?> :
                <span class="font-weight-bold text-success text-uppercase">
                    <?php if($userConnect->agents){
                        if($userConnect->agents[0]->comites_id == 1) echo "Espace qualification"; 
                        if($userConnect->agents[0]->comites_id == 2) echo "Espace analyse"; 
                        if($userConnect->profiles_id == 2) echo "Espace superadmin";
                        if($userConnect->profiles_id == 5) echo "Espace comité";
                        if($userConnect->profiles_id == 6) echo "Espace superviseur";
                    } elseif($userConnect->profiles_id == 1) echo "Espace porteur";
                    ?>
                </span>
                <div class="page-title-subheading"></div>
            </div>
        </div>
        
        <div class="page-title-actions">
            <button onclick="window.history.go(-1)" class="btn btn-primary">
                <i class="fa fa-arrow-left fa-w-20"></i> Retour
            </button>
            <button type="button" class="btn-shadow btn btn-info" data-toggle="modal" data-target="#addModal">
                <span class="btn-icon-wrapper pr-2 opacity-7">
                    <i class="fa fa-plus fa-w-20"></i>
                </span>
                Ajouter
            </button>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <div class="main-card mb-3 card">
            <div class="card-body"><h5 class="card-title"><?= $this->fetch('titre') ?></h5>
                <?= $this->Flash->render() ?>
                <table class="mb-0 table">
                    <thead>
                        <tr>
                            <th>N°</th>
                            <th>Libellé</th>
                            <th class="text-center"><?= __('Actions') ?></th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php foreach($elements as $i => $element): ?>
                        <tr>
                            <td><?= $i+1 ?></td>
                            <td><?= h($element->nom) ?></td>
                            <td class="actions text-center">
                                <button type="button" value="<?= $element->id.'+'.$element->nom.'+'.$element->photo ?>" class="btn btn-sm btn-info edit_click" title="Modifier" data-toggle="modal" data-target="#modalEdit"><i class="fa fa-edit"></i></button>
                                <?php if(!$element->statut): ?>
                                    <?= $this->Form->postLink(__('<i class="fa fa-check"></i>'), ['action' => 'changeStatutFiliere', $element->id, 1], ['confirm' => __('Voulez-vous vraiment activer ce secteur ?'),'class'=>'btn btn-success btn-sm','title'=>'Activer','escape'=>false]) ?>
                                <?php else: ?>
                                    <?= $this->Form->postLink(__('<i class="fa fa-times"></i>'), ['action' => 'changeStatutFiliere', $element->id, 0], ['confirm' => __('Voulez-vous vraiment désactiver ce secteur ?'),'class'=>'btn btn-danger btn-sm','title'=>'Désactiver','escape'=>false]) ?>
                                <?php endif; ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
                
            </div>

        </div>   

    </div>

</div>

<div class="modal fade" id="addModal" tabindex="1" role="dialog" aria-labelledby="addModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <?= $this->Form->create('Parametres', array('enctype'=>'multipart/form-data', 'url'=>array('action'=> $this->fetch('add_action')))); ?>
                <div class="modal-header">
                    <h5 class="modal-title" id="addModalLabel"><?= $this->fetch('title_add') ?></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                
                <div class="modal-body">
                
                    <div class="position-relative form-group">
                        <label for="nom">Nom</label>
                        <input name="nom" id="nom" placeholder="Saisir le nom" type="text" class="form-control" required>
                    </div>
                    <div class="position-relative form-group">
                        <label for="photo" class="">Image du secteur</label
                        ><input name="photo" type="file" id="photo" class="form-control" required>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Annuler</button>
                    <button type="submit" class="btn btn-success">Ajouter</button>
                </div>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>

<div class="modal fade" id="modalEdit" tabindex="1" role="dialog" aria-labelledby="editModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <?= $this->Form->create('Parametres', array('enctype'=>'multipart/form-data', 'url'=>array('action'=> $this->fetch('edit_action')))); ?>
                <div class="modal-header">
                    <h5 class="modal-title" id="addModalLabel"><?= $this->fetch('title_edit') ?></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                
                <div class="modal-body">
                    <input type="hidden" name="idElement" id="editElement">
                    <div class="position-relative form-group">
                        <label for="editNom">Nom</label>
                        <input name="nom" id="editNom" placeholder="Saisir le nom" type="text" class="form-control" required>
                    </div>
                    <div class="position-relative form-group">
                        <label for="editPhoto">Image du secteur</label>
                        <input name="photo" type="file" class="form-control" required>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <img id="img-secteur" src="" style="width: 100%; height: 190px" alt="Pas d'image" />
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Annuler</button>
                    <button type="submit" class="btn btn-success">Enregistrer</button>
                </div>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>

<!-- JQuery -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>


<script>
    $(function () {
        var getUrl = window.location;
        var baseUrl = getUrl.protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];

        var url = '';
        
        $(".edit_click").click(function() {
            data = this.value.split('+');
            $("#editElement").val(data[0]);
            $('#editNom').val(data[1]);
            if(baseUrl.indexOf("localhost") >= 0) url = baseUrl+"/ess/img/filieres/"+data[2];
            else url = baseUrl+"/img/filieres/"+data[2];
            $('#img-secteur').attr('src', url);

        });
    
    });
</script>