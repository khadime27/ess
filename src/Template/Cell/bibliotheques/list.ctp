<?php $this->setLayout('back_office') ?>

<!-- Test -->
<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            <div class="page-title-icon">
                <i class="pe-7s-home icon-gradient bg-mean-fruit">
                </i>
            </div>
            <div><?= $this->fetch('titre') ?> : 
                <span class="font-weight-bold text-success text-uppercase">
                    <?php if($userConnect->agents){
                        if($userConnect->agents[0]->comites_id == 1) echo "Espace qualification"; 
                        if($userConnect->agents[0]->comites_id == 2) echo "Espace analyse"; 
                        if($userConnect->profiles_id == 2) echo "Espace superadmin";
                        if($userConnect->profiles_id == 5) echo "Espace comité";
                        if($userConnect->profiles_id == 6) echo "Espace superviseur";
                    } elseif($userConnect->profiles_id == 1) echo "Espace porteur";
                    ?>
                </span>
            </div>
        </div>
        <div class="page-title-actions">
            <div class="d-inline-block">
                <button onclick="window.history.go(-1)" class="btn btn-primary">
                    <i class="fa fa-arrow-left fa-w-20"></i> Retour
                </button>
                
            </div>

        </div>    
    </div>
</div>


<div class="row mt-5">
    <div class="col-sm-12">
        <div class="card card-bibliotheque mt3">
            <div class="card-header">
                <h5 class="text-success font-weight-bold w-100 mt-2">
                    Bibliothèque
                    <?php if($userConnect->profiles_id == 2): ?>
                    <button class="btn btn-success pull-right" data-toggle="modal" data-target="#pjModal">
                        <b>Ajouter</b>
                    </button>
                    <?php endif; ?>
                </h5>
            </div>
            <div class="card-body pl0">
                <!--<div class="loader hide" style="margin-bottom: 20px;"></div>-->
                <form action="" style="display: none;"></form>  <!-- On met ceci pour permettre de supprimer le premier fichier qui s'empêchait de supprimer -->
                <?php if($bibliotheques == null): ?>
                    <div class="card" style="min-height: 250px; padding-top: 90px">
                        <div class="card-body"><h2 class="font-weight-bold text-center">AUCUN DOCUMENT !</h2></div>
                    </div>
                <?php else: ?>
                <div class="row">
                    <?php if(count($bibliotheques) > 0): foreach ($bibliotheques as $k => $pj): 
                        $extension = strtolower(pathinfo($pj->document, PATHINFO_EXTENSION)); ?>
                        <div class="col-md-4 mb-3 col-file">
                            <div class="bloc-file-bibli">
                                <div class="text-center">
                                    <?php if($extension == 'pdf'){ ?>
                                        <?= $this->Html->image('icones/logo-pdf.jpg', ['class' => 'img-fluid', 'alt' => 'PDF']) ?>
                                    <?php } elseif($extension == 'png' || $extension == 'jpg' || $extension == 'jpeg'){ ?>
                                        <?= $this->Html->image('icones/logo-img.png', ['class' => 'img-fluid', 'alt' => 'IMG']) ?>
                                    <?php } elseif($extension == 'gif'){ ?>
                                        <?= $this->Html->image('icones/logo-gif.jpg', ['class' => 'img-fluid', 'alt' => 'GIF']) ?>
                                    <?php } elseif($extension == 'doc' || $extension == 'docx'){ ?>
                                        <?= $this->Html->image('icones/logo-doc.png', ['class' => 'img-fluid', 'alt' => 'DOC']) ?>
                                    <?php } elseif($extension == 'xls' || $extension == 'xlsx'){ ?>
                                        <?= $this->Html->image('icones/logo-xls.jpg', ['class' => 'img-fluid', 'alt' => 'XLS']) ?>
                                    <?php } ?>
                                </div>
                                <div class="info-file text-center">
                                    <div style="height: 45px">
                                        <span class="title-document" style="word-wrap: break-word;"><?= coupeStr($pj->libelle) ?></span><br/>
                                    </div>
                                    <span class="view-files same-line">
                                        <div style="margin: auto;">
                                            <?php if(in_array(strtolower($extension), ['pdf','png','jpg','jpeg','gif'])): ?>
                                                <a href="<?=$this->Url->Build('/documents/bibliotheques/'.$pj->document)?>" data-fancybox="<?=$pj->libelle?>" data-caption="<?=$pj->libelle?>" title="Consulter le document"><strong><i class="fa fa-eye"></i> Aperçu</strong></a>
                                            <?php else: ?>
                                                <a href="<?= $pj->document ?>" class="ml3" download><strong><i class="fa fa-download"></i> Télécharger</strong></a>
                                            <?php endif; ?>
                                            
                                            <?php if($userConnect->profiles_id == 2): ?>
                                            <a class="popupUpdateModal ml3" data-toggle="modal" href="#" data-target="#update_modal" value='<?= $pj->id."+".$pj->libelle ?>' title="Modifier le nom">
                                                <strong><i class="fa fa-edit"></i> Modifier</strong>
                                            </a>
                                            <?= $this->Form->postLink(__('<strong><i class="fa fa-trash"></i> Supprimer</strong>'), ['controller'=>'Bibliotheques','action' => 'delete', $pj->id], ['confirm' => __('Voulez-vous vraiment supprimer le document?'), 'class' => 'ml3', 'title'=>'Supprimer le document','escape'=>false]) ?>
                                            <?php endif; ?>
                                        </div>
                                    </span>
                                </div>
                            </div>
                            
                        </div>
                    <?php endforeach; endif; ?>
                </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>


<!-- Modal pj-->
<div class="modal fade-scale" id="pjModal" tabindex="-1" role="dialog" aria-labelledby="pjModalLabel">
    <div class="modal-dialog" role="document">
        <form id="myForm" action="<?= $this->Url->Build(['controller' => 'Bibliotheques', 'action' => 'add']) ?>" method="post" enctype="multipart/form-data">
            
            <div class="modal-content">
                <div class="modal-header bg-bleu l-white">
                    <h4 id="pjModalLabel">Chargement du document</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" class="l-white">&times;</span></button>
                </div>
                <div class="modal-body">
                    
                    <div class="form-group row">
                        <label class="control-label col-sm-4" for="libelle">Titre:</label>
                        <div class="col-sm-8 bloc-libelle">
                            <input type="text" name="libelle" class="form-control mb1" id="libelle" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="document" class="control-label col-sm-4">Document:</label>
                        <div class="col-sm-8">
                            <input type="file" name="document" id="document" class="form-control" required>
                            <small class="pull-right" style="margin-top: 5px;">Taille maximum du fichier 20Mo.</small>
                        </div>
                    </div>
                    <!--<div class="loader hide"></div>-->
                    
                    <!--
                    <div class='progress' id="progress_div">
                        <div class='bar progress-bar progress-bar-striped bg-info' id='bar'></div>
                        <div class='percent' id='percent'>0%</div>
                    </div>
                    -->

                </div>
                <div class="modal-footer">
                    <a class="btn btn-danger" href="<?=$this->Url->Build(["controller" => "Bibliotheques", "action"=>"index"])?>" >Annuler</a>
                    <button type="submit" class="btn btn-primary" id="btnPj" disabled onclick="upload_image();" >Valider</button>
                </div>
            </div>
        </form>

    </div>
</div>

<!-- Modal update Indice -->
<div class="modal" id="update_modal">
    <div class="modal-dialog">
        <div class="modal-content" style="max-width: 500px">
            <div class="modal-header bg-bleu">
                <h5 class="w-100">
                    Modification du nom<span class="extra-title muted"></span>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </h5>
                
            </div>
            <form action="<?= $this->Url->Build(['controller' => 'Bibliotheques', 'action' => 'edit']) ?>" method="post" enctype="multipart/form-data">

                <div class="modal-body form-horizontal">
                    <div class="row mb-2">
                        <label class="col-sm-3 mt-2" for="libelleUpdate">Titre : </label>
                        <div class="col-sm-9">
                            <input type="text" name="libelle" id="libelleUpdate" class="form-control mb3" required>
                        </div>
                        <input type="hidden" name="id_pj" id="idPj">
                    </div>
                    <div class="form-group row">
                        <label for="document1" class="control-label col-sm-3 mt-2">Document :</label>
                        <div class="col-sm-9">
                            <input type="file" name="document" id="document1" class="form-control">
                            <small class="pull-right" style="margin-top: 5px;">Taille maximum du fichier 20Mo.</small>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="col-xs-12 col-md-12 text-right">
                        <button class="btn btn-md btn-primary" type="submit" style="width:150px;">Valider</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>


<script>
    $(function(){
        $('.popupUpdateModal').click(function(){
            data = $(this).attr('value').split('+');
            pjId = data[0];
            pjLibelle = data[1];

            $('#idPj').val(pjId);
            $('#libelleUpdate').val(pjLibelle);
            
        });

        $('input[type="file"]').change(function(e){
            var fileName = e.target.files[0].name;
            //$('#libelle').val(fileName.split('.').shift());
            if(fileName != '' && $('#libelle').val() != ''){
                $('#btnPj').removeAttr('disabled');
            }
            
            //alert('The file "' + fileName.split('.').shift() +  '" has been selected.');
        });

        $('.bloc-libelle').on('change keyup',"#libelle", function(){
            var filename = '';
            $('input[type="file"]').change(function(e){
                filename = e.target.files[0].name;
                
            });
            if($('#libelle').val() == '' && filename == ''){
                $('#btnPj').attr('disabled', 'disabled');
            }else{
                $('#btnPj').removeAttr('disabled');
            }
        });

        
    });

    function upload_image() 
    {
        var bar = $('#bar');
        var percent = $('#percent');
        $('#myForm').ajaxForm({
            beforeSubmit: function() {
                document.getElementById("progress_div").style.display="block";
                var percentVal = '0%';
                bar.width(percentVal)
                percent.html(percentVal);
            },

            uploadProgress: function(event, position, total, percentComplete) {
                var percentVal = percentComplete + '%';
                bar.width(percentVal);
                percent.html(percentVal);
            },
                
            success: function() {
                var percentVal = '100%';
                bar.width(percentVal)
                percent.html(percentVal);
            },

            complete: function(xhr) {
                if(xhr.responseText)
                {
                    var getUrl = window.location;
                    var baseUrl = getUrl.protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];
                    var urlController = baseUrl+'/bibliotheques/';
                    
                    window.location = urlController;
                    //$('.modal').modal('hide');
                    //document.getElementById("output_image").innerHTML=xhr.responseText;
                }
            }
        }); 
    }

    

</script>



<?php 
    // Méthode permettant de limiter le nombre de caractère d'une chaine à afficher
    function coupeStr($chaine){
        $chaineTransform = substr($chaine, 0, 65);  // Nombre de caractère souhaité
        if(strlen($chaineTransform) >= 65){
            echo $chaineTransform.'...';
        }else{
            echo $chaine;
        }
    }
?>