<?php $this->setLayout('back_office') ?>


<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            <div class="page-title-icon">
                <i class="pe-7s-home icon-gradient bg-mean-fruit">
                </i>
            </div>
            <div><?= $this->fetch('titre') ?> : 
                <span class="font-weight-bold text-success text-uppercase">
                    <?php if($userConnect->agents){
                        if($userConnect->agents[0]->comites_id == 1) echo "Espace qualification"; 
                        if($userConnect->agents[0]->comites_id == 2) echo "Espace analyse"; 
                        if($userConnect->profiles_id == 2) echo "Espace superadmin";
                        if($userConnect->profiles_id == 5) echo "Espace comité";
                        if($userConnect->profiles_id == 6) echo "Espace superviseur";
                    } elseif($userConnect->profiles_id == 1) echo "Espace porteur";
                    ?>
                </span>
            </div>
        </div>
        <div class="page-title-actions">
            <div class="d-inline-block">
                <button onclick="window.history.go(-1)" class="btn btn-primary">
                    <i class="fa fa-arrow-left fa-w-20"></i> Retour
                </button>
                <button type="button" class="btn-shadow btn btn-info" data-toggle="modal" data-target="#addModal">
                    <span class="btn-icon-wrapper pr-2 opacity-7">
                        <i class="fa fa-plus fa-w-20"></i>
                    </span>
                    Ajouter
                </button>
                
            </div>

        </div>    
    </div>
</div>  
                        
<div class="row">
    <div class="col-lg-12">
        <div class="main-card mb-6 card">
            <div class="card-body"><h5 class="card-title"><?= $this->fetch('titre2') ?></h5>
                <table class="mb-0 table">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Nom</th>
                            <th>Téléphone</th>
                            <th>Adresse</th>
                            <th class="text-center">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($agences as $i => $agence): ?>
                            <tr>
                                <th scope="row"><?= $i+1 ?></th>
                                <td><?= $agence->nom ?></td>
                                <td><?= $agence->telephone ?></td>
                                <td><?= $agence->adresse ?></td>
                                <td class="actions text-center">
                                    <button type="button" value="<?= $agence->id.'+'.$agence->nom.'+'.$agence->telephone.'+'.$agence->adresse.'+'.$agence->types_agences_id ?>" class="btn btn-sm btn-info edit_click" data-toggle="modal" data-target="#modalEdit">Modifier</button>

                                    <?php if($agence->etat): ?>
                                        <?= $this->Form->postLink(__('Désactiver'), ['action' => 'changeStat', $agence->id, 0], ['confirm' => __('Voulez-vous vraiment désactiver ce service ?'),'class'=>'btn btn-danger btn-sm w-80px','title'=>'Désactiver']) ?>
                                    <?php else: ?>
                                        <?= $this->Form->postLink(__('Activer'), ['action' => 'changeStat', $agence->id, 1], ['confirm' => __('Voulez-vous vraiment activer ce service ?'),'class'=>'btn btn-success btn-circle btn-sm w-80px','title'=>'Activer']) ?>
                                    <?php endif; ?>

                                    <?= $this->Html->link(__('<i class="fa fa-users"></i> Agents'), ['controller' => 'Agents', 'action' => 'view', $agence->id],['class'=>' btn btn-warning btn-sm','escape'=>false]) ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>


<div class="modal fade" id="addModal" tabindex="1" role="dialog" aria-labelledby="addModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <?= $this->Form->create('Agences', array('enctype'=>'multipart/form-data', 'url'=>array('action'=> 'add'))); ?>
                <div class="modal-header">
                    <h5 class="modal-title" id="addModalLabel"><?= $this->fetch('title_add') ?></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                
                <div class="modal-body">
                
                    <div class="position-relative form-group">
                        <label for="nom">Nom</label>
                        <input name="nom" id="nom" placeholder="Saisir le nom" type="text" class="form-control" required>
                    </div>
                    <div class="position-relative form-group">
                        <label for="telephone" class="">Téléphone</label
                        ><input name="telephone" id="telephone" placeholder="Saisir le numéro de téléphone" type="number" class="form-control" required>
                    </div>
                    <div class="position-relative form-group">
                        <label for="adresse">Adresse du service</label>
                        <input name="adresse" id="adresse" placeholder="Saisir l'adresse" type="text" class="form-control" required>
                    </div>
                    <?php if($this->fetch('type') == "comite"): ?>
                        <div class="position-relative form-group">
                            <label for="region">Régions</label>
                            <select name="regions_id" id="region" class="form-control">
                                <?php foreach($regions as $region): ?>
                                    <option value="<?= $region->id ?>"><?= $region->nom ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    <?php endif; ?>
                    <input type="hidden" name="type" value="<?= $this->fetch('type') ?>">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Annuler</button>
                    <button type="submit" class="btn btn-success">Ajouter</button>
                </div>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>

<div class="modal fade" id="modalEdit" tabindex="1" role="dialog" aria-labelledby="editModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <?= $this->Form->create('Agences', array('enctype'=>'multipart/form-data', 'url'=>array('action'=> 'edit'))); ?>
                <div class="modal-header">
                    <h5 class="modal-title" id="editModalLabel"><?= $this->fetch('title_update') ?></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                
                <div class="modal-body">
                
                    <div class="position-relative form-group">
                        <label for="editNom">Nom</label>
                        <input name="nom" id="editNom" placeholder="Saisir le nom" type="text" class="form-control" required>
                    </div>
                    <div class="position-relative form-group">
                        <label for="editTel">Téléphone</label
                        ><input name="telephone" id="editTel" placeholder="Saisir le numéro de téléphone" type="number" class="form-control" required>
                    </div>
                    <div class="position-relative form-group">
                        <label for="editAdresse">Adresse</label>
                        <input name="adresse" id="editAdresse" placeholder="Saisir l'adresse" type="text" class="form-control" required>
                    </div>
                    <input type="hidden" name="idAgence" id="idAgence">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Annuler</button>
                    <button type="submit" class="btn btn-success">Ajouter</button>
                </div>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div> 


<!-- JQuery -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>


<script>
    $(function () {
        $(".edit_click").click(function() {
        data = this.value.split('+');
            $("#idAgence").val(data[0]);
            $('#editNom').val(data[1]);
            $('#editTel').val(data[2]);
            $('#editAdresse').val(data[3]);

        });

        $('.btnValidate').click(function(){
            $('#formAssignation').submit();
        });
    
    });
</script>
