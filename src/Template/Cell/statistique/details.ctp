<?php $this->setLayout('back_office'); ?>
<?php if($this->fetch('type')) $type = $this->fetch('type'); ?>

<style>
    .dataTables_filter input[type="search"] {
        width: 50% !important;
    }
</style>
<div class="app-page-title">
    
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            <div class="page-title-icon">
                <i class="pe-7s-drawer icon-gradient bg-happy-itmeo"></i>
            </div>
            <div><?= $this->fetch('titre') ?> :
                <span class="font-weight-bold text-success text-uppercase">
                    <?php if($userConnect->agents){
                        if($userConnect->agents[0]->comites_id == 1) echo "Espace qualification"; 
                        if($userConnect->agents[0]->comites_id == 2) echo "Espace analyse"; 
                        if($userConnect->profiles_id == 2) echo "Espace superadmin";
                        if($userConnect->profiles_id == 5) echo "Espace comité";
                        if($userConnect->profiles_id == 6) echo "Espace superviseur";
                    } elseif($userConnect->profiles_id == 1) echo "Espace porteur";
                    ?>
                </span>
                <div class="page-title-subheading"><?= $this->fetch('phrase') ?></div>
            </div>
        </div>
        
        <div class="page-title-actions">
            <button onclick="window.history.go(-1)" class="btn btn-primary">
                <i class="fa fa-arrow-left fa-w-20"></i> Retour
            </button>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-6 col-xs-12">
        <div class="main-card mb-3 card">
            <div class="card-body">
                <h5 class="card-title">
                    <?php if($this->fetch('page') == 'detailCible') echo "Cibles impactés par zone"; 
                          elseif($this->fetch('page') == 'detailEmploi') echo "Nombre d'emploi et saisonnier par zone";
                    ?>
                </h5>
                <?= $this->Flash->render() ?>
                <table class="mb-0 table">
                    <thead>
                        <tr>
                            <th>N°</th>
                            <th>Région</th>
                            <?php if($this->fetch('page') == 'detailCible'): ?>
                                <th>Nombre impacté</th>
                            <?php elseif($this->fetch('page') == 'detailEmploi'): ?>
                                <th>Nb emplois</th>
                                <th>Nb saisonnier</th>
                            <?php endif; ?>
                        </tr>
                    </thead>
                    <tbody>
                    <?php foreach($regions as $i => $region):
                        $nbPersoImpact = 0; $nbEmploi = 0; $nbSaisonnier = 0;
                        foreach($dossiersLabelises as $dossier):
                            if($dossier->commune->departement->region_id == $region->id):
                                if($this->fetch('page') == 'detailCible'):
                                    $nbPersoImpact += $dossier->nb_perso_impact;
                                elseif($this->fetch('page') == 'detailEmploi'):
                                    if($dossier->porteur->entreprise):
                                        $nbEmploi += $dossier->porteur->entreprise->nb_employe;
                                        $nbSaisonnier += $dossier->porteur->entreprise->nb_saisonnier;
                                    endif;
                                endif;
                                
                            endif;
                        endforeach; ?>
                        <tr>
                            <td><?= $i+1 ?></td>
                            <td><?= h($region->nom) ?></td>
                            <td>
                                <span>
                                    <?php if($this->fetch('page') == 'detailCible' && $nbPersoImpact > 0): ?>
                                        <span class="badge badge-success">
                                            <?= $nbPersoImpact ?>
                                        </span>
                                    <?php elseif($this->fetch('page') == 'detailEmploi' && $nbEmploi > 0): ?>
                                        <span class="badge badge-success">
                                            <?= $nbEmploi ?>
                                        </span>
                                    <?php endif; ?>
                                </span>
                            </td>
                            <?php if($this->fetch('page') == 'detailEmploi'): ?>
                            <td>
                                <span class="<?php if($nbSaisonnier > 0) echo 'badge badge-success'; ?>">
                                    <?= $nbSaisonnier ? $nbSaisonnier : '' ?>
                                </span>
                            </td>
                            <?php endif; ?>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
                
            </div>

        </div>   

    </div>
    <div class="col-sm-6 col-xs-12">
        <div class="main-card mb-3 card">
            <div class="card-body">
                <h5 class="card-title">
                    <?php if($this->fetch('page') == 'detailCible') echo "Cibles impactés par secteur"; 
                            elseif($this->fetch('page') == 'detailEmploi') echo "Nombre d'emploi et saisonnier par secteur";
                    ?>
                </h5>
                <?= $this->Flash->render() ?>
                <table class="mb-0 table">
                    <thead>
                        <tr>
                            <th>N°</th>
                            <th>Secteur</th>
                            <?php if($this->fetch('page') == 'detailCible'): ?>
                                <th>Nombre impacté</th>
                            <?php elseif($this->fetch('page') == 'detailEmploi'): ?>
                                <th>Nb emplois</th>
                                <th>Nb saisonnier</th>
                            <?php endif; ?>
                        </tr>
                    </thead>
                    <tbody>
                    <?php foreach($filieres as $i => $filiere):
                        $nbPersoImpact = 0; $nbEmploi = 0; $nbSaisonnier = 0;
                        foreach($dossiersLabelises as $dossier):
                            if($dossier->filiere_id == $filiere->id):
                                if($this->fetch('page') == 'detailCible'):
                                    $nbPersoImpact += $dossier->nb_perso_impact;
                                elseif($this->fetch('page') == 'detailEmploi'):
                                    if($dossier->porteur->entreprise):
                                        $nbEmploi += $dossier->porteur->entreprise->nb_employe;
                                        $nbSaisonnier += $dossier->porteur->entreprise->nb_saisonnier;
                                    endif;
                                endif;
                                
                            endif;
                        endforeach; ?>
                        <tr>
                            <td><?= $i+1 ?></td>
                            <td><?= h($filiere->nom) ?></td>
                            <td>
                                <span>
                                    <?php if($this->fetch('page') == 'detailCible' && $nbPersoImpact > 0): ?>
                                        <span class="badge badge-success">
                                            <?= $nbPersoImpact ?>
                                        </span>
                                    <?php elseif($this->fetch('page') == 'detailEmploi' && $nbEmploi > 0): ?>
                                        <span class="badge badge-success">
                                            <?= $nbEmploi ?>
                                        </span>
                                    <?php endif; ?>
                                </span>
                            </td>
                            <?php if($this->fetch('page') == 'detailEmploi'): ?>
                            <td>
                                <span class="<?php if($nbSaisonnier > 0) echo 'badge badge-success'; ?>">
                                    <?= $nbSaisonnier ? $nbSaisonnier : '' ?>
                                </span>
                            </td>
                            <?php endif; ?>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
                
            </div>

        </div>   

    </div>

</div>