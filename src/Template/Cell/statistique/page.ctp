<?php $this->setLayout('back_office'); ?>

<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            <div class="page-title-icon">
                <i class="pe-7s-compass icon-gradient bg-mean-fruit">
                </i>
            </div>
            <div><?= $this->fetch('titre') ?>
                <div class="page-title-subheading">
                    Vue en temps réel des dossiers de l'ESS 
                    <h5 class='text-success font-weight-bold'>
                        <?php if($this->fetch('departement')) echo "Région de ".$region->nom; ?> 
                        <?php if($this->fetch('commune')) echo "Département de ".$departement->departementscol; ?> 
                    </h5>
                </div>
            </div>
        </div>   
    </div>
    <?php if($userConnect->profiles_id == 6): ?>
    <!--
    <button id="btn-capture" class="btn btn-success float-right">
        <strong>Capturer</strong>
    </button>
    -->
    <button id="btn-export-pdf" class="btn btn-primary float-right mr-2" onclick="printContent('exportContent')">
        <strong>Exporter en PDF</strong>
    </button>
    <button id="btn-export-global" class="btn btn-success float-right mr-2">
        <strong>Exporter en xls</strong>
    </button>
    <!--
    <button id="btn-export" onclick="exportHTML()" class="btn btn-warning float-right mr-2">
        <strong>Exporter en .doc</strong>
    </button>
    -->
    <?php endif; ?>
</div>            
<div class="row">
    <div class="col-md-6 col-xl-4">
        <div class="card mb-3 widget-content bg-warning">
            <div class="widget-content-wrapper text-white">
                <div class="widget-content-left">
                    <div class="widget-heading">En cours</div>
                    <div class="widget-subheading">Dossiers en cours</div>
                </div>
                <div class="widget-content-right">
                    <div class="widget-numbers text-white"><span><?= count($dossiersEncours) ?></span></div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-xl-4">
        <div class="card mb-3 widget-content bg-grow-early">
            <div class="widget-content-wrapper text-white">
                <div class="widget-content-left">
                    <div class="widget-heading">Acceptés </div>
                    <div class="widget-subheading">Dossiers acceptés</div>
                </div>
                <div class="widget-content-right">
                    <div class="widget-numbers text-white"><span><?= count($dossiersAcceptes) ?></span></div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-xl-4">
        <div class="card mb-3 widget-content bg-danger">
            <div class="widget-content-wrapper text-white">
                <div class="widget-content-left">
                    <div class="widget-heading">Rejetés</div>
                    <div class="widget-subheading">Dossiers rejetés</div>
                </div>
                <div class="widget-content-right">
                    <div class="widget-numbers text-white"><span><?= count($dossiersRejetes) ?></span></div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php if($userConnect->profiles_id == 6): ?>
<div class="row">
    <div class="col-md-6 col-xl-4">
        <a href="<?php echo $this->Url->build(array('controller' => 'Stats', 'action' => 'detailsCibleImpact')); ?>">
            <div class="card mb-3 widget-content bg-primary">
                <div class="widget-content-wrapper text-white">
                    <div class="widget-content-left">
                        <div class="widget-heading">Cible impacté</div>
                        <div class="widget-subheading">Personne impacté</div>
                    </div>
                    <div class="widget-content-right">
                        <div class="widget-numbers text-white"><span><?= $nbPersoImpact ?></span></div>
                    </div>
                </div>
            </div>
        </a>
    </div>
    <div class="col-md-6 col-xl-4">
        <a href="<?php echo $this->Url->build(array('controller' => 'Stats', 'action' => 'detailsEmplois')); ?>">
            <div class="card mb-3 widget-content bg-info">
                <div class="widget-content-wrapper text-white">
                    <div class="widget-content-left">
                        <div class="widget-heading">Emplois créés</div>
                        <div class="text-white text-center" style="font-weight: bold; font-size: 16px">
                            <span class="badge badge-warning"><?= $nbEmplois ?></span>
                        </div>
                    </div>
                    <div class="widget-content-right">
                        <div class="widget-heading">Saisonniers créés</div>
                        <div class="text-white text-center" style="font-weight: bold; font-size: 16px">
                            <span class="badge badge-warning"><?= $nbSaisonnier ?></span>
                        </div>
                    </div>
                </div>
            </div>
        </a>
    </div>
    <div class="col-md-6 col-xl-4">
        <div class="card mb-3 widget-content bg-success">
            <div class="widget-content-wrapper text-white">
                <div class="widget-content-left">
                    <div class="widget-heading">Financement</div>
                    <div class="widget-subheading">Financements accordés</div>
                </div>
                <div class="widget-content-right">
                    <div class="widget-numbers text-white"><span>100 M</span></div>
                </div>
            </div>
        </div>
    </div>
    
</div>

<div class="row hide">
    <div class="col-sm-12">
        <table id="stats-global">
            <thead>
                <tr>
                    <th>Porteur</th>
                    <th>Téléphone</th>
                    <th>Genre</th>
                    <th>Date de naissance</th>
                    <th>Nom structure</th>
                    <th>Intitulé</th>
                    <th>Filière</th>
                    <th>Note labélisation (sur 20)</th>
                    <th>Commune</th>
                    <th>Département</th>
                    <th>Région</th>
                    <th>Nb. personnes impactés</th>
                    <th>Nb. femmes impactées</th>
                    <th>Nb. hommes impactés</th>
                    <th>Type de label</th>
                </tr>
            </thead>
            <tbody>
            <?php foreach($dossiersEmis as $dossier): ?>
                <tr>
                    <td><?= h($dossier->porteur->prenom." ".$dossier->porteur->nom) ?></td>
                    <td><?= h($dossier->porteur->telephone) ?></td>
                    <td><?= h($dossier->porteur->genre) ?></td>
                    <td><?= $dossier->porteur->date_naissance ? h($dossier->porteur->date_naissance->format('d/m/Y')) : '' ?></td>
                    <td><?= $dossier->porteur->entreprise ? $dossier->porteur->entreprise->nom : '' ?> </td>
                    <td><?= h($dossier->intitule) ?></td>
                    <td><?= h($dossier->filiere->nom) ?></td>
                    <td><?= h($dossier->note_final) ?></td>
                    <td><?= h($dossier->commune->nom) ?></td>
                    <td><?= h($dossier->commune->departement->departementscol) ?></td>
                    <td><?= h($dossier->commune->departement->region->nom) ?></td>
                    <td><?= h($dossier->nb_perso_impact) ?></td>
                    <td><?= h($dossier->nb_femme_impact) ?></td>
                    <td><?= h($dossier->nb_homme_impact) ?></td>
                    <td><?= h($dossier->type_label) ?></td>
                </tr>
            <?php endforeach; ?>
            </tobdy>
        </table>
    </div>
</div>
<?php endif; ?>


<div id="exportContent">
    <div class="row">
        <div class="col-md-12 col-lg-12">
            <div class="mb-3 card">
                <div class="card-header-tab card-header-tab-animation card-header">
                    <div class="card-header-title">
                        <i class="header-icon lnr-apartment icon-gradient bg-love-kiss"> </i>
                        Demandes par type de structure
                    </div>
                </div>
                <div class="card-body">
                    <div class="tab-content">
                        <div class="tab-pane fade show active">
                            <div class="mb-3 widget-chart widget-chart2 text-left w-100">
                                <div class="widget-chat-wrapper-outer">
                                    <div class="widget-chart-wrapper widget-chart-wrapper-lg opacity-10 m-0">
                                        <div id="chartTypeStructure" class="chart"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row" style="page-break-before:always;">
        <div class="col-md-12 col-lg-12">
            <div class="mb-3 card">
                <div class="card-header-tab card-header-tab-animation card-header">
                    <div class="card-header-title">
                        <i class="header-icon lnr-apartment icon-gradient bg-love-kiss"> </i>
                        Demandes par secteurs d'activités
                    </div>
                </div>
                <div class="card-body">
                    <div class="tab-content">
                        <div class="tab-pane fade show active">
                            <div class="mb-3 widget-chart widget-chart2 text-left w-100">
                                <div class="widget-chat-wrapper-outer">
                                    <div class="widget-chart-wrapper widget-chart-wrapper-lg opacity-10 m-0">
                                        <div id="chartFilieres" class="chart"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <?php if($this->fetch('home-stat')): ?>
    <div class="row" style="page-break-before:always;">
        <div class="col-md-12 col-lg-12">
            <div class="mb-3 card">
                <div class="card-header-tab card-header-tab-animation card-header">
                    <div class="card-header-title">
                        <i class="header-icon lnr-apartment icon-gradient bg-love-kiss"> </i>
                        Demandes par région
                    </div>
                </div>
                <div class="card-body">
                    <div class="tab-content">
                        <div class="tab-pane fade show active">
                            <div class="mb-3 widget-chart widget-chart2 text-left w-100">
                                <div class="widget-chat-wrapper-outer">
                                    <div class="widget-chart-wrapper widget-chart-wrapper-lg opacity-10 m-0">
                                        <div id="chartRegion" class="chart"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <?php endif; ?>



    <?php if($this->fetch('departement')): ?>
    <div class="row" style="page-break-before:always;">
        
        <div class="col-md-12 col-lg-12">
            <div class="mb-3 card">
                <div class="card-header-tab card-header-tab-animation card-header">
                    <div class="card-header-title">
                        <i class="header-icon lnr-apartment icon-gradient bg-love-kiss"> </i>
                        Demandes par département
                    </div>
                </div>
                <div class="card-body">
                    <div class="tab-content">
                        <div class="tab-pane fade show active">
                            <div class="mb-3 widget-chart widget-chart2 text-left w-100">
                                <div class="widget-chat-wrapper-outer">
                                    <div class="widget-chart-wrapper widget-chart-wrapper-lg opacity-10 m-0">
                                        <div id="chartDepartement" class="chart"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        

    </div>
    <?php endif; ?>

    <?php if($this->fetch('commune')): ?>
    <div class="row" style="page-break-before:always;">
        <div class="col-md-12 col-lg-12">
            <div class="mb-3 card">
                <div class="card-header-tab card-header-tab-animation card-header">
                    <div class="card-header-title">
                        <i class="header-icon lnr-apartment icon-gradient bg-love-kiss"> </i>
                        Demandes par commune
                    </div>
                </div>
                <div class="card-body">
                    <div class="tab-content">
                        <div class="tab-pane fade show active">
                            <div class="mb-3 widget-chart widget-chart2 text-left w-100">
                                <div class="widget-chat-wrapper-outer">
                                    <div class="widget-chart-wrapper widget-chart-wrapper-lg opacity-10 m-0">
                                        <div id="chartCommune" class="chart"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <?php endif; ?>

    <div class="row" style="page-break-before:always;">
        <div class="col-md-12 col-lg-12">
            <div class="mb-3 card">
                <div class="card-header-tab card-header-tab-animation card-header">
                    <div class="card-header-title">
                        <i class="header-icon lnr-apartment icon-gradient bg-love-kiss"> </i>
                        Demandes par genre
                    </div>
                </div>
                <div class="card-body">
                    <div class="tab-content">
                        <div class="tab-pane fade show active">
                            <div class="mb-3 widget-chart widget-chart2 text-left w-100">
                                <div class="widget-chat-wrapper-outer">
                                    <div class="widget-chart-wrapper widget-chart-wrapper-lg opacity-10 m-0">
                                        <div id="chartGenre" class="chart"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>


</div>

<?php if($this->fetch('departement')): ?>
<div class="row">
    <div class="col-md-12 col-lg-12">
        <div class="mb-3 card">
            <div class="card-header-tab card-header-tab-animation card-header">
                <div class="card-header-title">Liste des départements</div>
            </div>
            <div class="card-body min-card">
                <table class="mb-0 table">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Départements</th>
                            <th class="text-center"><?= __('Actions') ?></th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php foreach($departements as $i => $departement): ?>
                        <tr>
                            <td><?= $i+1 ?></td>
                            <td><?= $departement->departementscol ?></td>
                            <td class="actions text-center">
                                <?= $this->Html->link(__('<i class="fa fa-eye"></i>'), ['action' => 'voirCommunes', $departement->id],['class'=>' btn bg-color text-white btn-circle btn-sm','escape'=>false]) ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<?php endif; ?>

<?php if($this->fetch('home-stat')): ?>
<div class="row">
    <div class="col-md-12 col-lg-12">
        <div class="mb-3 card">
            <div class="card-header-tab card-header-tab-animation card-header">
                <div class="card-header-title">Liste des régions</div>
            </div>
            <div class="card-body min-card">
                <table class="mb-0 table">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Région</th>
                            <th class="text-center"><?= __('Actions') ?></th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php foreach($regions as $i => $region): ?>
                        <tr>
                            <td><?= $i+1 ?></td>
                            <td><?= $region->nom ?></td>
                            <td class="actions text-center">
                                <?= $this->Html->link(__('<i class="fa fa-eye"></i>'), ['action' => 'voirDepartements', $region->id],['class'=>' btn bg-color text-white btn-circle btn-sm','escape'=>false]) ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<?php endif; ?>

<!-- Logiques statistiques -->
<?php 

    // STATS dossiers par type de structure
    $types_entreprises = [];
    foreach($typesEntreprises as $j => $type):
        $types_entreprises[] = $type->nom;
        $nbDossiers = 0;
        // $nombresDossiersParStruc[$type->nom] = 0;
        $nombresDossiersParStruc[$j]["label"] = $type->nom;
        $nombresDossiersParStruc[$j]["donnee"] = 0;
        foreach($allDossiers as $dossier):
            if($dossier->porteur->entreprise && $dossier->porteur->entreprise->type_entreprise_id == $type->id):
                $nbDossiers += 1;
                $nombresDossiersParStruc[$j]["donnee"] = $nbDossiers;
                // $nombresDossiersParStruc[$type->nom] = $nbDossiers;
            endif; 
        endforeach;
    endforeach;

    // STATS dossiers par secteur d'activité
    $tab_filieres = [];
    foreach($filieres as $i => $filiere):
        $tab_filieres[] = $filiere->nom;
        $nbDossiers = 0;
        $nombresDossiersParSecteur[$i]["label"] = $filiere->nom;
        $nombresDossiersParSecteur[$i]["donnee"] = 0;
        foreach($allDossiers as $dossier):
            if($dossier->filiere_id == $filiere->id):
                $nbDossiers += 1;
                $nombresDossiersParSecteur[$i]["donnee"] = $nbDossiers;
                // $nombresDossiersParSecteur[$filiere->nom] = $nbDossiers;
            endif; 
        endforeach;
    endforeach;

    if($this->fetch('departement')):
        // STATS demandes par département
        $tab_departements = [];
        foreach($departements as $i => $departement):
            $tab_departements[] = $departement->departementscol;
            $nbDossiers = 0;
            $nombresDossiersParDepartement[$i]['label'] = $departement->departementscol;
            $nombresDossiersParDepartement[$i]['donnee'] = 0;
            foreach($allDossiers as $dossier):
                if($dossier->commune->departement_id == $departement->id):
                    $nbDossiers += 1;
                    $nombresDossiersParDepartement[$i]['donnee'] = $nbDossiers;
                endif; 
            endforeach;
        endforeach;

    endif;

    if($this->fetch('commune')):
        // STATS demandes par commune
        $tab_communes = [];
        foreach($communes as $j => $commune):
            $tab_communes[] = $commune->nom;
            $nbDossiers = 0;
            $nombresDossiersParCommune[$j]['label'] = $commune->nom;
            $nombresDossiersParCommune[$j]['donnee'] = 0;
            foreach($allDossiers as $dossier):
                if($dossier->commune_id == $commune->id):
                    $nbDossiers += 1;
                    $nombresDossiersParCommune[$j]['donnee'] = $nbDossiers;
                endif; 
            endforeach;
        endforeach;

    endif; 

    

    // STATS demandes par région
    if($this->fetch('home-stat')):
        $tab_regions = [];
        foreach($regions as $i => $region):
            $tab_regions[] = $region->nom;
            $nbDossiers = 0;
            $nombresDossiersParRegion[$i]['label'] = $region->nom;
            $nombresDossiersParRegion[$i]['donnee'] = 0;
            foreach($allDossiers as $dossier):
                if($dossier->commune->departement->region_id == $region->id):
                    $nbDossiers += 1;
                    $nombresDossiersParRegion[$i]['donnee'] = $nbDossiers;
                endif; 
            endforeach;
        endforeach;

    endif;

    
    foreach($genres as $i => $genre):
        $nbDossiers = 0;
        $nombresDossiersParGenre[$i]['label'] = $genre;
        $nombresDossiersParGenre[$i]['donnee'] = 0;
        foreach($dossiersEmis as $dossier):
            if($dossier->porteur->genre == $genre):
                $nbDossiers += 1;
                $nombresDossiersParGenre[$i]['donnee'] = $nbDossiers;
            endif; 
        endforeach;
    endforeach;
            
?>


<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
<?php echo $this->Html->script('plugins/chart-plugin'); ?>


<!-- Styles -->
<style>
.chart {
  width: 100%;
  height: 500px;
}

</style>

<!-- Resources -->
<script src="https://cdn.amcharts.com/lib/4/core.js"></script>
<script src="https://cdn.amcharts.com/lib/4/charts.js"></script>
<script src="https://cdn.amcharts.com/lib/4/themes/animated.js"></script>


<!-- Chart code -->
<script>
    <?php 
        $byTypeEntreprises = $nombresDossiersParStruc; $js_array_type = json_encode($byTypeEntreprises); 
        $bySecteurs = $nombresDossiersParSecteur; $js_array_secteurs = json_encode($bySecteurs);
        $byGenres = $nombresDossiersParGenre; $js_array_genres = json_encode($byGenres); 
    ?>
    var datasTypeEntreprises = <?= $js_array_type ?>;
    var datasSecteurs = <?= $js_array_secteurs ?>;
    var datasGenres = <?= $js_array_genres ?>;

    getStats(chartTypeStructure, datasTypeEntreprises, am4charts.PieChart3D);
    getStats(chartFilieres, datasSecteurs, am4charts.PieChart3D);
    getStats(chartGenre, datasGenres, am4charts.PieChart3D);

    <?php if($this->fetch('home-stat')): 
        $byRegions = $nombresDossiersParRegion; $js_array_regions = json_encode($byRegions); ?>
        var datasRegions = <?= $js_array_regions ?>;
        getStats(chartRegion, datasRegions, am4charts.PieChart3D);
    <?php endif; ?>
    <?php if($this->fetch('departement')):
        $byDepartements = $nombresDossiersParDepartement; $js_array_departements = json_encode($byDepartements); ?>
        var datasDepartements = <?= $js_array_departements ?>;
        getStats(chartDepartement, datasDepartements, am4charts.PieChart3D);
    <?php endif; ?>
    <?php if($this->fetch('commune')):
        $byCommunes = $nombresDossiersParCommune; $js_array_communes = json_encode($byCommunes); ?>
        var datasCommunes = <?= $js_array_communes ?>;
        getStats(chartCommune, datasCommunes, am4charts.PieChart3D);
    <?php endif; ?>


    function getStats(idChart, arrayData, typeChart) {
        am4core.ready(function() {

            // Themes begin
            am4core.useTheme(am4themes_animated);
            // Themes end

            var chart = am4core.create(idChart, typeChart);
            chart.hiddenState.properties.opacity = 0; // this creates initial fade-in

            chart.legend = new am4charts.Legend();

            chart.data = arrayData;

            chart.innerRadius = 100;

            var series = chart.series.push(new am4charts.PieSeries3D());
            series.dataFields.value = "donnee";
            series.dataFields.category = "label";
            /*
            series.dataFields.depthValue = "donnee";
            series.slices.template.cornerRadius = 5;
            series.colors.step = 3;
            */

        }); // end am4core.ready()
    }
</script>

<script>
    function exportHTML(){
       var header = "<html xmlns:o='urn:schemas-microsoft-com:office:office' "+
            "xmlns:w='urn:schemas-microsoft-com:office:word' "+
            "xmlns='http://www.w3.org/TR/REC-html40'>"+
            "<head><meta charset='utf-8'><title>Export HTML to Word Document with JavaScript</title></head><body>";
       var footer = "</body></html>";

       var content = "";
       $.each($('.chart'), function(index, elt) {
            content += $(this).html()+"<br><br>";
       });
       var sourceHTML = header+content+footer;
       
       var source = 'data:application/vnd.ms-word;charset=utf-8,' + encodeURIComponent(sourceHTML);
       var fileDownload = document.createElement("a");
       document.body.appendChild(fileDownload);
       fileDownload.href = source;
       fileDownload.download = 'document.doc';
       fileDownload.click();
       document.body.removeChild(fileDownload);
    }
</script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.min.js" integrity="sha512-s/XK4vYVXTGeUSv4bRPOuxSDmDlTedEpMEcAQk0t/FMd9V6ft8iXdwSBxV0eD60c6w/tjotSlKu9J2AAW1ckTA==" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/FileSaver.js/2.0.0/FileSaver.min.js" integrity="sha512-csNcFYJniKjJxRWRV1R7fvnXrycHP6qDR21mgz1ZP55xY5d+aHLfo9/FcGDQLfn2IfngbAHd8LdfsagcCqgTcQ==" crossorigin="anonymous"></script>

<script>
    $(function(){
        $('#btn-capture').click(function(){
            html2canvas($("#exportContent"), {
                onrendered: function(canvas) {
                    theCanvas = canvas;

                    canvas.toBlob(function(blob) {
                        saveAs(blob, "Dashboard.png"); 
                    });
                }
            });
        });
    });
</script>

<script>
    $('#btn-export-global').click(function () {

        $("#stats-global").table2excel({
            // exclude CSS class
            exclude:".noExl",
            name:"Worksheet Name",
            filename:"Stats global",//do not include extension
            fileext:".xlsx" // file extension
        });

        window.location.reload();
    });
</script>

<script>

    function printContent(el) {
        $('.card').css({
            'box-shadow': "none",
            "background": "none"
        });

        var restorepage = document.body.innerHTML;
        var printcontent = document.getElementById(el).innerHTML;
        document.body.innerHTML = printcontent;

        window.print();
        document.body.innerHTML = restorepage;

        window.location.reload(); // Rechargement de la page
    }

</script>