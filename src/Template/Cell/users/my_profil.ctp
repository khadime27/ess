<?php $this->setLayout('back_office') ?>

<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            <div class="page-title-icon">
                <i class="pe-7s-user icon-gradient bg-mean-fruit"></i>
            </div>
            <div class="page-title-subheading">
                <h3 class="text-success font-weight-bold">Modification du profil</h3>
            </div>
        </div>   
    </div>
</div>            
<div class="container">
    <?= $this->Form->create('Users', array('enctype'=>'multipart/form-data', 'url'=>array('action'=> 'edit', $userConnect->id))) ?>
        <div class="card mb-5">
            <div class="card-header">Informations personnelles</div>
            <div class="card-body">
                <div class="form-row mb-4">
                    <div class="col">
                        <label>Prénom</label>
                        <input type="text" name="prenom" value="<?= $userConnect->profiles_id == 1 ? $userConnect->porteurs[0]->prenom : $userConnect->agents[0]->prenom ?>" class="form-control" placeholder="Prénom" required>
                    </div>
                    <div class="col">
                        <label>Nom</label>
                        <input type="text" name="nom" value="<?= $userConnect->profiles_id == 1 ? $userConnect->porteurs[0]->nom : $userConnect->agents[0]->nom ?>" class="form-control" placeholder="Nom" required>
                    </div>
                </div>
                <div class="form-row mb-4">
                    <div class="col">
                        <label>Email</label>
                        <input type="email" name="email" value="<?= $userConnect->email ?>" class="form-control" placeholder="Adresse email" required>
                    </div>
                    <div class="col">
                        <label>Mot de passe</label>
                        <input type="text" name="motDePasse" class="form-control" placeholder="Mot de passe">
                    </div>
                </div>
                <div class="form-row mb-4">
                    <div class="col">
                        <label>Téléphone</label>
                        <input type="text" name="telephone" value="<?= $userConnect->profiles_id == 1 ? $userConnect->porteurs[0]->telephone : $userConnect->agents[0]->telephone ?>" placeholder="Numéro de téléphone" class="form-control">
                    </div>

                    <div class="col">
                        <label>Photo de profile</label>
                        <input type="file" name="photo" class="form-control mb-4" />
                    </div>
                </div>
                <div class="form-row mb-4">
                    <?php if($userConnect->profiles_id == 1): ?>
                    <div class="col">
                        <label>Date de naissance</label>
                        <input type="date" name="date_naissance" value="<?= $userConnect->porteurs[0]->date_naissance ? $userConnect->porteurs[0]->date_naissance->format('Y-m-d') : '' ?>" class="form-control">
                    </div>
                    <div class="col">
                        <label>Lieu de naissance</label>
                        <input type="text" name="lieu_naissance" value="<?= $userConnect->porteurs[0]->lieu_naissance ?>" placeholder="Votre lieu de naissance" class="form-control">
                    </div>
                    <?php endif; ?>
                    <div class="col">
                        <label>Lieu de résidence</label>
                        <input type="text" name="<?= $userConnect->profiles_id == 1 ? 'lieu_residence' : 'adresse' ?>" value="<?= $userConnect->profiles_id == 1 ? $userConnect->porteurs[0]->lieu_residence : $userConnect->agents[0]->adresse ?>" class="form-control" placeholder="Lieu de résidence">
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <div class="row w-100">
                    <div class="col-sm-12">
                        <button class="btn bg-color text-white float-right" type="submit">
                            Enregistrer                        
                        </button>
                    </div>
                </div>
            </div>
        </div>
        
    <?= $this->Form->end() ?>
</div>