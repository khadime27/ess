<?php $this->setLayout('back_office'); ?>

<div class="container">
    <div class="row mb-2 pull-right">
        <div class="col-sm-12">
            <button onclick="window.history.go(-1)" class="btn btn-primary">
                <i class="fa fa-arrow-left fa-w-20"></i> Retour
            </button>
            <?php if($dossier->statut_financements_id == null): ?>
                <?php if($financesEngagements != null && $financementsActifs != null && $financementsPassifs != null && $situationsComptes != null): ?>
                    <?= $this->Form->postLink(__('<i class="fa fa-hand-point-right"></i> Soumettre'), ['action' => 'demander', $dossier->id], ['confirm' => __('Voulez-vous vraiment soumettre ce dossier pour un financement ?'),'class'=>'btn btn-success pull-right font-weight-bold ml-2','title'=>'Soumettre le dossier','escape'=>false]) ?>
                <?php endif; ?>
            <?php endif; ?>
        </div>
    </div><br><br>
    <div class="card mt-page">
        <div class="card-header bg-color text-white">
            <div class="row w-100">
                <div class="col-sm-10 marg-auto">
                    Engagements actuels de la structure ou du projet
                </div>
                <div class="col-sm-2">
                    <?php if($dossier->statut_financements_id == null): ?>
                        <button id="btn-update-engagement" class="btn btn-warning pull-right">Modifier</button>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <div class="card-body">
            <?php echo $this->Form->create('Chat',array('url'=>array('controller'=>'FinancesEngagements', 'action'=>'add'))); ?>
                <?= $this->Flash->render() ?>
                <table class="mb-0 table">
                    <thead>
                        <tr>
                            <th>Libellé</th>
                            <th>Montant</th>
                            <th>Encours</th>
                            <th>Différé (en mois)</th>
                            <th>Date dernière échéance</th>
                            <th>Durée (en mois)</th>
                            <th>Banque</th>
                            <th>Taux d'intérêt</th>
                            <th class="hide"></th>
                        </tr>
                    </thead>
                    <tbody>
                        
                        <?php foreach($parametresEngagements as $i => $parametre):
                            if($financesEngagements != null):
                                foreach($financesEngagements as $engagement):
                                    if($parametre->libelle == $engagement->libelle): ?>
                                <tr class="engagement">
                                    <td><input type="text" value="<?= $parametre->libelle ?>" name="finanances_engagements[<?= $i ?>][libelle]" readonly class="border-0" title="<?= $parametre->libelle ?>"></td>
                                    <td><input type="number" name="finanances_engagements[<?= $i ?>][montant]" value="<?= $engagement->montant ?>" readonly class="champ-update"></td>
                                    <td><input type="number" name="finanances_engagements[<?= $i ?>][encours]" value="<?= $engagement->encours ?>" readonly class="champ-update"></td>
                                    <td><input type="number" name="finanances_engagements[<?= $i ?>][differe_mois]" value="<?= $engagement->differe_mois ?>" readonly class="champ-update"></td>
                                    <td><input type="date" name="finanances_engagements[<?= $i ?>][date_last_echeance]" value="<?= $engagement->date_last_echeance->format('Y-m-d') ?>" readonly class="champ-update"></td>
                                    <td><input type="number" name="finanances_engagements[<?= $i ?>][duree_mois]" value="<?= $engagement->duree_mois ?>" readonly class="champ-update"></td>
                                    <td><input type="text" name="finanances_engagements[<?= $i ?>][banque]" value="<?= $engagement->banque ?>" readonly class="champ-update"></td>
                                    <td><input type="text" name="finanances_engagements[<?= $i ?>][taux_interet]" value="<?= $engagement->taux_interet ?>" readonly class="champ-update"></td>
                                    <td>
                                        <input type="hidden" name="finanances_engagements[<?= $i ?>][dossiers_id]" value="<?= $dossier->id ?>">
                                        <input type="hidden" name="finanances_engagements[<?= $i ?>][idEngagement]" value="<?= $engagement->id ?>">
                                    </td>
                                
                                </tr>
                        <?php 
                                    endif;
                                endforeach; 
                            else: ?>
                                <tr>
                                    <td><input type="text" value="<?= $parametre->libelle ?>" name="finanances_engagements[<?= $i ?>][libelle]" readonly title="<?= $parametre->libelle ?>"></td>
                                    <td><input type="number" name="finanances_engagements[<?= $i ?>][montant]"></td>
                                    <td><input type="number" name="finanances_engagements[<?= $i ?>][encours]"></td>
                                    <td><input type="number" name="finanances_engagements[<?= $i ?>][differe_mois]"></td>
                                    <td><input type="date" name="finanances_engagements[<?= $i ?>][date_last_echeance]"></td>
                                    <td><input type="number" name="finanances_engagements[<?= $i ?>][duree_mois]"></td>
                                    <td><input type="text" name="finanances_engagements[<?= $i ?>][banque]"></td>
                                    <td><input type="text" name="finanances_engagements[<?= $i ?>][taux_interet]"></td>
                                    <td><input type="hidden" name="finanances_engagements[<?= $i ?>][dossiers_id]" value="<?= $dossier->id ?>"></td>
                                </tr>
                        <?php
                            endif;
                        endforeach; 
                        ?>
                        
                    </tbody>
                </table>
                
                <button type="submit" id="save-engagement" class="<?php if($financesEngagements != null) echo 'hide'; ?> btn btn-success pull-right mt-3">Enregistrer</button>
                
            <?= $this->Form->end() ?>
        </div>
    </div>

    <div class="card mt-page mt-5">
        <div class="card-header bg-color text-white">
            <div class="row w-100">
                <div class="col-sm-10 marg-auto">
                    Situation des comptes
                </div>
                <div class="col-sm-2">
                    <?php if($dossier->statut_financements_id == null): ?>
                        <button id="btn-update-situation" class="btn btn-warning pull-right">Modifier</button>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <div class="card-body">
            <?php echo $this->Form->create('Chat',array('url'=>array('controller'=>'SituationsComptes', 'action'=>'add'))); ?>
                <?= $this->Flash->render() ?>
                <table class="mb-0 table">
                    <thead>
                        <tr>
                            <th>Rubriques</th>
                            <th>Année N-3</th>
                            <th>Année N-2</th>
                            <th>Année N-1</th>
                            <th>Année N</th>
                            <th>Prévision année N</th>
                            <th class="hide"></th>
                        </tr>
                    </thead>
                    <tbody>
                        
                        <?php foreach($parametresSituationsComptes as $i => $parametre):
                            if($situationsComptes != null):
                                foreach($situationsComptes as $situationCompte):
                                    if($situationCompte->libelle == $parametre->libelle): ?>
                                    <tr class="situation">
                                        <td><input type="text" value="<?= $parametre->libelle ?>" name="situations_comptes[<?= $i ?>][libelle]" class="border-0" readonly title="<?= $parametre->libelle ?>"></td>
                                        <td><input type="number" value="<?= $situationCompte->annee_moins_trois ?>" name="situations_comptes[<?= $i ?>][annee_moins_trois]" readonly class="champ-update"></td>
                                        <td><input type="number" value="<?= $situationCompte->annee_moins_deux ?>" name="situations_comptes[<?= $i ?>][annee_moins_deux]" readonly class="champ-update"></td>
                                        <td><input type="number" value="<?= $situationCompte->annee_moins_un ?>" name="situations_comptes[<?= $i ?>][annee_moins_un]" readonly class="champ-update"></td>
                                        <td><input type="double" value="<?= $situationCompte->annee ?>" name="situations_comptes[<?= $i ?>][annee]" readonly class="champ-update"></td>
                                        <td><input type="number" value="<?= $situationCompte->prevision ?>" name="situations_comptes[<?= $i ?>][prevision]" readonly class="champ-update"></td>
                                        <td>
                                            <input type="hidden" name="situations_comptes[<?= $i ?>][dossiers_id]" value="<?= $dossier->id ?>">
                                            <input type="hidden" name="situations_comptes[<?= $i ?>][idSituation]" value="<?= $situationCompte->id ?>">
                                        </td>
                                    </tr>
                            <?php endif; 
                                endforeach;
                            else: ?>
                                <tr>
                                    <td><input type="text" value="<?= $parametre->libelle ?>" name="situations_comptes[<?= $i ?>][libelle]" readonly title="<?= $parametre->libelle ?>"></td>
                                    <td><input type="number" name="situations_comptes[<?= $i ?>][annee_moins_trois]"></td>
                                    <td><input type="number" name="situations_comptes[<?= $i ?>][annee_moins_deux]"></td>
                                    <td><input type="number" name="situations_comptes[<?= $i ?>][annee_moins_un]"></td>
                                    <td><input type="double" name="situations_comptes[<?= $i ?>][annee]"></td>
                                    <td><input type="number" name="situations_comptes[<?= $i ?>][prevision]"></td>
                                    <td><input type="hidden" name="situations_comptes[<?= $i ?>][dossiers_id]" value="<?= $dossier->id ?>"></td>
                                </tr>
                        <?php endif;
                        endforeach; ?>
                        
                    </tbody>
                </table>

                <button type="submit" id="save-situation" class="<?php if($situationsComptes != null) echo 'hide'; ?> btn btn-success pull-right mt-3">Enregistrer</button>
            <?= $this->Form->end() ?>
        </div>
    </div>

    <div class="card mt-page mt-5">
        <div class="card-header bg-color text-white">
            <div class="row w-100">
                <div class="col-sm-10 marg-auto">
                    (*) ACTIFS (FCFA)
                </div>
                <div class="col-sm-2">
                    <?php if($dossier->statut_financements_id == null): ?>
                        <button id="btn-update-actif" class="btn btn-warning pull-right">Modifier</button>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <div class="card-body">
            <?php echo $this->Form->create('Chat',array('url'=>array('controller'=>'FinancementsActifs', 'action'=>'add'))); ?>
                <?= $this->Flash->render() ?>
                <table class="mb-0 table">
                    <thead>
                        <tr>
                            <th>Rubriques</th>
                            <th>Année N-3</th>
                            <th>Année N-2</th>
                            <th>Année N-1</th>
                            <th>Année N</th>
                            <th>Prévision N+1</th>
                            <th>Variation N/N-1</th>
                            <th class="hide"></th>
                        </tr>
                    </thead>
                    <tbody>
                        
                        <?php foreach($parametresActifs as $i => $parametre):
                            if($financementsActifs == null): ?>
                                <tr>
                                    <td><input type="text" value="<?= $parametre->libelle ?>" name="financements_actifs[<?= $i ?>][libelle]" readonly title="<?= $parametre->libelle ?>"></td>
                                    <td><input type="number" name="financements_actifs[<?= $i ?>][annee_moins_trois]"></td>
                                    <td><input type="number" name="financements_actifs[<?= $i ?>][annee_moins_deux]"></td>
                                    <td><input type="number" name="financements_actifs[<?= $i ?>][annee_moins_un]"></td>
                                    <td><input type="double" name="financements_actifs[<?= $i ?>][annee]"></td>
                                    <td><input type="number" name="financements_actifs[<?= $i ?>][prevision]"></td>
                                    <td><input type="number" name="financements_actifs[<?= $i ?>][variation]"></td>
                                    <td><input type="hidden" name="financements_actifs[<?= $i ?>][dossiers_id]" value="<?= $dossier->id ?>"></td>
                                </tr>
                        <?php else:
                                foreach($financementsActifs as $financementActif): 
                                    if($financementActif->libelle == $parametre->libelle): ?>
                                        <tr class="actif">
                                            <td><input type="text" value="<?= $parametre->libelle ?>" name="financements_actifs[<?= $i ?>][libelle]" readonly class="border-0" title="<?= $parametre->libelle ?>"></td>
                                            <td><input type="number" value="<?= $financementActif->annee_moins_trois ?>" name="financements_actifs[<?= $i ?>][annee_moins_trois]" readonly class="champ-update"></td>
                                            <td><input type="number" value="<?= $financementActif->annee_moins_deux ?>" name="financements_actifs[<?= $i ?>][annee_moins_deux]" readonly class="champ-update"></td>
                                            <td><input type="number" value="<?= $financementActif->annee_moins_un ?>" name="financements_actifs[<?= $i ?>][annee_moins_un]" readonly class="champ-update"></td>
                                            <td><input type="double" value="<?= $financementActif->annee ?>" name="financements_actifs[<?= $i ?>][annee]" readonly class="champ-update"></td>
                                            <td><input type="number" value="<?= $financementActif->prevision ?>" name="financements_actifs[<?= $i ?>][prevision]" readonly class="champ-update"></td>
                                            <td><input type="number" value="<?= $financementActif->variation ?>" name="financements_actifs[<?= $i ?>][variation]" readonly class="champ-update"></td>
                                            <td>
                                                <input type="hidden" name="financements_actifs[<?= $i ?>][dossiers_id]" value="<?= $dossier->id ?>">
                                                <input type="hidden" name="financements_actifs[<?= $i ?>][idFinancementActif]" value="<?= $financementActif->id ?>">
                                            </td>
                                        </tr>
                                    <?php endif; 
                                endforeach;
                             endif;
                        endforeach; ?>
                        
                    </tbody>
                </table>
                
                <button type="submit" id="save-actif" class="<?php if($financementsActifs != null) echo 'hide'; ?> btn btn-success pull-right mt-3">Enregistrer</button>
            <?= $this->Form->end() ?>
        </div>
    </div>

    <div class="card mt-page mt-5 mb-5">
        <div class="card-header bg-color text-white">
            <div class="row w-100">
                <div class="col-sm-10 marg-auto">
                    (*) PASSIFS (FCFA)
                </div>
                <div class="col-sm-2">
                    <?php if($dossier->statut_financements_id == null): ?>
                        <button id="btn-update-passif" class="btn btn-warning pull-right">Modifier</button>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <div class="card-body">
            <?php echo $this->Form->create('Chat',array('url'=>array('controller'=>'FinancementsPassifs', 'action'=>'add'))); ?>
                <?= $this->Flash->render() ?>
                <table class="mb-0 table">
                    <thead>
                        <tr>
                            <th>Rubriques</th>
                            <th>Année N-3</th>
                            <th>Année N-2</th>
                            <th>Année N-1</th>
                            <th>Année N</th>
                            <th>Prévision N+1</th>
                            <th>Variation N/N-1</th>
                            <th class="hide"></th>
                        </tr>
                    </thead>
                    <tbody>
                        
                        <?php foreach($parametresPassifs as $i => $parametre): 
                            if($financementsPassifs == null): ?>
                                <tr>
                                    <td><input type="text" value="<?= $parametre->libelle ?>" name="financements_passifs[<?= $i ?>][libelle]" readonly title="<?= $parametre->libelle ?>"></td>
                                    <td><input type="number" name="financements_passifs[<?= $i ?>][annee_moins_trois]"></td>
                                    <td><input type="number" name="financements_passifs[<?= $i ?>][annee_moins_deux]"></td>
                                    <td><input type="number" name="financements_passifs[<?= $i ?>][annee_moins_un]"></td>
                                    <td><input type="double" name="financements_passifs[<?= $i ?>][annee]"></td>
                                    <td><input type="number" name="financements_passifs[<?= $i ?>][prevision]"></td>
                                    <td><input type="number" name="financements_passifs[<?= $i ?>][variation]"></td>
                                    <td><input type="hidden" name="financements_passifs[<?= $i ?>][dossiers_id]" value="<?= $dossier->id ?>"></td>
                                </tr>
                        <?php else:
                                foreach($financementsPassifs as $financementPassif): 
                                    if($financementPassif->libelle == $parametre->libelle): ?>
                                        <tr class="passif">
                                            <td><input type="text" value="<?= $parametre->libelle ?>" name="financements_passifs[<?= $i ?>][libelle]" readonly class="border-0" title="<?= $parametre->libelle ?>"></td>
                                            <td><input type="number" value="<?= $financementPassif->annee_moins_trois ?>" name="financements_passifs[<?= $i ?>][annee_moins_trois]" readonly class="champ-update"></td>
                                            <td><input type="number" value="<?= $financementPassif->annee_moins_deux ?>" name="financements_passifs[<?= $i ?>][annee_moins_deux]" readonly class="champ-update"></td>
                                            <td><input type="number" value="<?= $financementPassif->annee_moins_un ?>" name="financements_passifs[<?= $i ?>][annee_moins_un]" readonly class="champ-update"></td>
                                            <td><input type="double" value="<?= $financementPassif->annee ?>" name="financements_passifs[<?= $i ?>][annee]" readonly class="champ-update"></td>
                                            <td><input type="number" value="<?= $financementPassif->prevision ?>" name="financements_passifs[<?= $i ?>][prevision]" readonly class="champ-update"></td>
                                            <td><input type="number" value="<?= $financementPassif->variation ?>" name="financements_passifs[<?= $i ?>][variation]" readonly class="champ-update"></td>
                                            <td>
                                                <input type="hidden" name="financements_passifs[<?= $i ?>][dossiers_id]" value="<?= $dossier->id ?>">
                                                <input type="hidden" name="financements_passifs[<?= $i ?>][idFinancementPassif]" value="<?= $financementPassif->id ?>">
                                            </td>
                                        </tr>
                                    <?php endif; 
                                endforeach;
                             endif;
                        endforeach; ?>
                        
                    </tbody>
                </table>
                <button type="submit" id="save-passif" class="<?php if($financementsPassifs != null) echo 'hide'; ?> btn btn-success pull-right mt-3">Enregistrer</button>
            <?= $this->Form->end() ?>
        </div>
    </div>

</div>

<?= $this->Html->script('front/vendor/jquery/jquery.min') ?>
<script>
    $(function(){
        $('#btn-update-engagement').click(function(){
            $('.engagement .champ-update').removeAttr('readonly').removeClass('champ-update');
            $('#save-engagement').removeClass('hide');
        });

        $('#btn-update-situation').click(function(){
            $('.situation .champ-update').removeAttr('readonly').removeClass('champ-update');
            $('#save-situation').removeClass('hide');
        });

        $('#btn-update-actif').click(function(){
            $('.actif .champ-update').removeAttr('readonly').removeClass('champ-update');
            $('#save-actif').removeClass('hide');
        });
        
        $('#btn-update-passif').click(function(){
            $('.passif .champ-update').removeAttr('readonly').removeClass('champ-update');
            $('#save-passif').removeClass('hide');
        });

    });
</script>