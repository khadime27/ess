<?php $this->setLayout('back_office'); ?>

<div class="container">
    <div class="row mb-2 pull-right">
        <div class="col-sm-12">
            <button onclick="window.history.go(-1)" class="btn btn-primary">
                <i class="fa fa-arrow-left fa-w-20"></i> Retour
            </button>
        </div>
    </div><br><br>
    <div class="card mt-page">
        <div class="card-header bg-color text-white">
            <h5 class="w-100 marg-auto">Engagements actuels de la structure ou du projet
                <a class="btn pull-right btn-collapse" data-toggle="collapse" href="#multiCollapseExample1" role="button" aria-expanded="false"
                    aria-controls="multiCollapseExample1"><i class="fa fa-eye text-white"></i>
                </a>
            </h5>
        </div>
        <div class="card-body collapse show multi-collapse" id="multiCollapseExample1">
            
            <table class="mb-0 table">
                <thead>
                    <tr>
                        <th>Libellé</th>
                        <th>Montant</th>
                        <th>Encours</th>
                        <th>Différé (en mois)</th>
                        <th>Date dernière échéance</th>
                        <th>Durée (en mois)</th>
                        <th>Banque</th>
                        <th>Taux d'intérêt</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($financesEngagements as $engagement): ?>
                        <tr>
                            <td><?= $engagement->libelle ?></td>
                            <td><?= $engagement->montant ?></td>
                            <td><?= $engagement->encours ?></td>
                            <td><?= $engagement->differe_mois ?></td>
                            <td><?= $engagement->date_last_echeance->format('d/m/Y') ?></td>
                            <td><?= $engagement->duree_mois ?></td>
                            <td><?= $engagement->banque ?></td>
                            <td><?= $engagement->taux_interet ?></td>
                        
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
            
        </div>
    </div>

    <div class="card mt-page mt-5">
        <div class="card-header bg-color text-white">
            
            <h5 class="w-100 marg-auto">Situation des comptes
                <a class="btn pull-right btn-collapse" data-toggle="collapse" href="#multiCollapseExample2" role="button" aria-expanded="false"
                    aria-controls="multiCollapseExample2"><i class="fa fa-eye text-white"></i>
                </a>
            </h5>
        </div>
        <div class="card-body collapse show multi-collapse" id="multiCollapseExample2">
            
            <table class="mb-0 table">
                <thead>
                    <tr>
                        <th>Rubriques</th>
                        <th>Année N-3</th>
                        <th>Année N-2</th>
                        <th>Année N-1</th>
                        <th>Année N</th>
                        <th>Prévision année N</th>
                    </tr>
                </thead>
                <tbody>
                    
                    <?php foreach($situationsComptes as $situationCompte): ?>
                        <tr class="situation">
                            <td><?= $situationCompte->libelle ?></td>
                            <td><?= $situationCompte->annee_moins_trois ?></td>
                            <td><?= $situationCompte->annee_moins_deux ?></td>
                            <td><?= $situationCompte->annee_moins_un ?></td>
                            <td><?= $situationCompte->annee ?></td>
                            <td><?= $situationCompte->prevision ?></td>
                        </tr>
                    <?php endforeach; ?>
                    
                </tbody>
            </table>

        </div>
    </div>

    <div class="card mt-page mt-5">
        <div class="card-header bg-color text-white">
            <h5 class="w-100 marg-auto">(*) ACTIFS (FCFA)
                <a class="btn pull-right btn-collapse" data-toggle="collapse" href="#multiCollapseExample3" role="button" aria-expanded="false"
                    aria-controls="multiCollapseExample3"><i class="fa fa-eye text-white"></i>
                </a>
            </h5>
        </div>
        <div class="card-body collapse show multi-collapse" id="multiCollapseExample3">
            <table class="mb-0 table">
                <thead>
                    <tr>
                        <th>Rubriques</th>
                        <th>Année N-3</th>
                        <th>Année N-2</th>
                        <th>Année N-1</th>
                        <th>Année N</th>
                        <th>Prévision N+1</th>
                        <th>Variation N/N-1</th>
                    </tr>
                </thead>
                <tbody>
                    
                    <?php 
                    foreach($financementsActifs as $financementActif):  ?>
                        <tr>
                            <td><?= $financementActif->libelle ?></td>
                            <td><?= $financementActif->annee_moins_trois ?></td>
                            <td><?= $financementActif->annee_moins_deux ?></td>
                            <td><?= $financementActif->annee_moins_un ?></td>
                            <td><?= $financementActif->annee ?></td>
                            <td><?= $financementActif->prevision ?></td>
                            <td><?= $financementActif->variation ?></td>
                            
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>

        </div>
    </div>

    <div class="card mt-page mt-5 mb-5">
        <div class="card-header bg-color text-white">
            <h5 class="w-100 marg-auto">(*) PASSIFS (FCFA)
                <a class="btn pull-right btn-collapse" data-toggle="collapse" href="#multiCollapseExample4" role="button" aria-expanded="false"
                    aria-controls="multiCollapseExample4"><i class="fa fa-eye text-white"></i>
                </a>
            </h5>
        </div>
        <div class="card-body collapse show multi-collapse" id="multiCollapseExample4">
            <table class="mb-0 table">
                <thead>
                    <tr>
                        <th>Rubriques</th>
                        <th>Année N-3</th>
                        <th>Année N-2</th>
                        <th>Année N-1</th>
                        <th>Année N</th>
                        <th>Prévision N+1</th>
                        <th>Variation N/N-1</th>
                    </tr>
                </thead>
                <tbody>
                    
                    <?php foreach($financementsPassifs as $financementPassif):  ?>
                        <tr class="passif">
                            <td><?= $financementPassif->libelle ?></td>
                            <td><?= $financementPassif->annee_moins_trois ?></td>
                            <td><?= $financementPassif->annee_moins_deux ?></td>
                            <td><?= $financementPassif->annee_moins_un ?></td>
                            <td><?= $financementPassif->annee ?></td>
                            <td><?= $financementPassif->prevision ?></td>
                            <td><?= $financementPassif->variation ?></td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>

        </div>
    </div>

</div>

<?= $this->Html->script('front/vendor/jquery/jquery.min') ?>
<script>
    $(function(){
        $('.btn-collapse').click( function(e) {
            // Hide another collapse when one is opening
            // $('.collapse').collapse('hide');

            // Change direction of icon when is opening or closed
            $('.collapse').on('shown.bs.collapse', function () {
                // console.log("Opened")
                $(this).prev().find('.fa-plus').removeClass('fa-plus').addClass('fa-minus');
            });

            $('.collapse').on('hidden.bs.collapse', function () {
                $(this).prev().find('.fa-minus').removeClass('fa-minus').addClass('fa-plus');
            });
        });
    });
</script>