<?php $this->setLayout('back_office') ?>

<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            <div class="page-title-icon">
                <i class="pe-7s-compass icon-gradient bg-mean-fruit">
                </i>
            </div>
            <div>Tableau de bord : 
                <span class="font-weight-bold text-success text-uppercase">
                    <?php if($userConnect->agents){
                        if($userConnect->agents[0]->comites_id == 1) echo "Espace qualification"; 
                        if($userConnect->agents[0]->comites_id == 2) echo "Espace analyse"; 
                        if($userConnect->profiles_id == 2) echo "Espace superadmin";
                        if($userConnect->profiles_id == 5) echo "Espace comité";
                        if($userConnect->profiles_id == 6) echo "Espace superviseur";
                    } elseif($userConnect->profiles_id == 1) echo "Espace porteur";
                    ?>
                </span>
                <div class="page-title-subheading">Vue temps réel du traitement des dossiers de l'ESS.</div>
            </div>
        </div>
        <div class="page-title-actions">
            <div class="d-inline-block dropdown">
                <button onclick="window.history.go(-1)" class="btn btn-primary">
                    <i class="fa fa-arrow-left fa-w-20"></i> Retour
                </button>
            </div>
        </div>    
    </div>
</div>            
<h3>Labélisation</h3>
<div class="row">
    <div class="col-md-6 col-xl-3">
        <?php if($userConnect->profiles_id == 1): ?>
            <div class="card mb-3 widget-content bg-midnight-bloom">
                <div class="widget-content-wrapper text-white">
                    <div class="widget-content-left">
                        <div class="widget-heading">Non soumis</div>
                        <div class="widget-subheading">Dossiers non soumis</div>
                    </div>
                    <div class="widget-content-right">
                        <div class="widget-numbers text-white"><span><?= $nbNoSoumis ?></span></div>
                    </div>
                </div>
            </div>
        <?php else: ?>
        <div class="card mb-3 widget-content bg-midnight-bloom">
            <div class="widget-content-wrapper text-white">
                <div class="widget-content-left">
                    <div class="widget-heading">Non traités</div>
                    <div class="widget-subheading">Dossiers non traités</div>
                </div>
                <div class="widget-content-right">
                    <div class="widget-numbers text-white"><span><?= $nbNoTraite ?></span></div>
                </div>
            </div>
        </div>
        <?php endif; ?>
    </div>
    <div class="col-md-6 col-xl-3">
        <div class="card mb-3 widget-content bg-warning">
            <div class="widget-content-wrapper text-white">
                <div class="widget-content-left">
                    <div class="widget-heading">En cours</div>
                    <div class="widget-subheading">Dossiers en cours</div>
                </div>
                <div class="widget-content-right">
                    <div class="widget-numbers text-white"><span><?= $nbEncours ?></span></div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-xl-3">
        <div class="card mb-3 widget-content bg-grow-early">
            <div class="widget-content-wrapper text-white">
                <div class="widget-content-left">
                    <div class="widget-heading">Acceptés </div>
                    <div class="widget-subheading">Dossiers acceptés</div>
                </div>
                <div class="widget-content-right">
                    <div class="widget-numbers text-white"><span><?= $nbAccepte ?></span></div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-xl-3">
        <div class="card mb-3 widget-content bg-danger">
            <div class="widget-content-wrapper text-white">
                <div class="widget-content-left">
                    <div class="widget-heading">Rejetés</div>
                    <div class="widget-subheading">Dossiers rejetés</div>
                </div>
                <div class="widget-content-right">
                    <div class="widget-numbers text-warning"><span><?= $nbRefuse ?></span></div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php if($userConnect->profiles_id == 2 || $userConnect->profiles_id == 3 || $userConnect->profiles_id == 5 || $userConnect->profiles_id == 6): ?>
<div class="row">
    <div class="col-md-12 col-lg-6">
        <div class="mb-3 card">
            <div class="card-header-tab card-header-tab-animation card-header">
                <div class="card-header-title">
                    <i class="header-icon lnr-apartment icon-gradient bg-love-kiss"> </i>
                    Demandes de label du mois en cours
                </div>
            </div>
            <div class="card-body">
                <div class="tab-content">
                    <div class="tab-pane fade show active" id="tabs-eg-77">
                        <div class="card mb-3 widget-chart widget-chart2 text-left w-100">
                            <div class="widget-chat-wrapper-outer">
                                <div class="widget-chart-wrapper widget-chart-wrapper-lg opacity-10 m-0">
                                    <canvas id="chartDemandes"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12 col-lg-6">
        <div class="mb-3 card">
            <div class="card-header-tab card-header-tab-animation card-header">
                <div class="card-header-title">
                    <i class="header-icon lnr-apartment icon-gradient bg-love-kiss"> </i>
                    Demandes par mois
                </div>
            </div>
            <div class="card-body">
                <div class="tab-content">
                    <div class="tab-pane fade show active" id="tabs-eg-77">
                        <div class="card mb-3 widget-chart widget-chart2 text-left w-100">
                            <div class="widget-chat-wrapper-outer">
                                <div class="widget-chart-wrapper widget-chart-wrapper-lg opacity-10 m-0">
                                    <canvas id="courbeDemande"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<?php endif; ?>



<?php
    if($userConnect->profiles_id == 2 || $userConnect->profiles_id == 3 || $userConnect->profiles_id == 5 || $userConnect->profiles_id == 6):
        $tab_dossiers = [];
        foreach($statutDossiers as $i => $statut):
            $tab_dossiers[] = $statutDossiers[$i];
            $nbDossiers = 0;
            $nombresDossiersParStatut[$statutDossiers[$i]] = 0;
            foreach($dossiers as $dossier):
                if($dossier->statut_dossiers_id == 6) $dossier->statut_dossier->libelle = "Accepté";
                if($userConnect->profiles_id == 2 || $userConnect->profiles_id == 5 || $userConnect->profiles_id == 6){
                    if($dossier->statut_dossiers_id <= 4) $dossier->statut_dossier->libelle = "En cours";
                }elseif($userConnect->profiles_id == 3) {
                    if($userConnect->agents[0]->comites_id == 1){
                        if($dossier->statut_dossiers_id == 1) $dossier->statut_dossier->libelle = "En cours";
                    }elseif($userConnect->agents[0]->comites_id == 2){
                        if($dossier->statut_dossiers_id == 3) $dossier->statut_dossier->libelle = "En cours";
                    }
                }
                if($dossier->statut_dossier->libelle == $statutDossiers[$i]):
                    $nbDossiers += 1;
                    $nombresDossiersParStatut[$statutDossiers[$i]] = $nbDossiers;
                endif; 
            endforeach;
        endforeach;

        $dossiersParStatut_vjson = json_encode(array_values($nombresDossiersParStatut));  
        $tab_dossiers_json = json_encode(array_values($tab_dossiers));

        $tab_mois = [];
        foreach($mois as $moi):
            $tab_mois[] = $moi['nom'];
            $nbDemandes = 0;
            $nombresDemandes[$moi['nom']] = 0;
            foreach($allDossiers as $dossier):
                if($dossier->created->format('m') == $moi['num']):
                    $nbDemandes += 1;
                    $nombresDemandes[$moi['nom']] = $nbDemandes;
                endif; 
            endforeach; 
        endforeach;

        $demandes_vjson = json_encode(array_values($nombresDemandes));  
        $tab_mois_json = json_encode(array_values($tab_mois));

    endif;
    

?>

<script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
<?php echo $this->Html->script('plugins/chart-plugin'); ?>

<script>
    console.log(<?= $dossiersParStatut_vjson ?>);

    stats('chartDemandes', 'Demandes du mois en cours', <?= $tab_dossiers_json ?>, <?= $dossiersParStatut_vjson ?>, 'pie');
    stats('courbeDemande', 'Courbe des demandes par mois', <?= $tab_mois_json ?>, <?= $demandes_vjson ?>, 'pie');

    // Fonction qui gère les stats
    function stats(id, title, array_abscisse, donnees, typeChart) {
        var ctx = document.getElementById(id).getContext('2d');
        var myChart = new Chart(ctx, {
            type: typeChart,
            data: {
                labels: array_abscisse,
                datasets: [{
                    label: title,
                    data: donnees,
                    backgroundColor: [
                        '#f7b924',
                        '#0ba360',
                        '#d92550',
                        'rgba(153, 102, 255, 1)',
                        'rgba(255, 159, 64, 1)',
                        'rgba(255, 99, 132, 1)',
                        'rgb(86, 238, 71)',
                        'rgb(28, 0, 99)',
                        'rgb(165, 255, 221)',
                        'rgba(201, 89, 143, 1)',
                        'rgba(0, 118, 148, 1)',
                        'rgba(239, 173, 108, 1)'
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                },
                responsive: true,
                plugins: {
                    datalabels: {
                        formatter: (value, ctx) => {
                            if(typeChart == "pie" && value > 0) {
                                let sum = 0;
                                let dataArr = ctx.chart.data.datasets[0].data;
                                dataArr.map(data => {
                                    sum += data;
                                });
                                let percentage = (value * 100 / sum).toFixed(0) + "%";
                                return percentage;
                            }else{
                                value = "";
                                return value;
                            }
                            
                        },
                        color: 'black'
                    }
                }
            }
        });
    }
</script>