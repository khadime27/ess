<?php $this->setLayout('back_office'); ?>
<?php if($this->fetch('type')) $type = $this->fetch('type'); ?>
<div class="app-page-title">
    
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            <div class="page-title-icon">
                <i class="pe-7s-drawer icon-gradient bg-happy-itmeo"></i>
            </div>
            <div><?= $this->fetch('titre') ?> :
                <span class="font-weight-bold text-success text-uppercase">
                    <?php if($userConnect->agents){
                        if($userConnect->agents[0]->comites_id == 1) echo "Espace qualification"; 
                        if($userConnect->agents[0]->comites_id == 2) echo "Espace analyse"; 
                        if($userConnect->profiles_id == 2) echo "Espace superadmin";
                        if($userConnect->profiles_id == 5) echo "Espace comité";
                        if($userConnect->profiles_id == 6) echo "Espace superviseur";
                    } elseif($userConnect->profiles_id == 1) echo "Espace porteur";
                    ?>
                </span>
                <div class="page-title-subheading"><?= $this->fetch('phrase') ?></div>
            </div>
        </div>
        
        <div class="page-title-actions">
            <button onclick="window.history.go(-1)" class="btn btn-primary">
                <i class="fa fa-arrow-left fa-w-20"></i> Retour
            </button>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <div class="main-card mb-3 card">
            <div class="card-body"><h5 class="card-title">Régions</h5>
                <?= $this->Flash->render() ?>
                <table class="mb-0 table">
                    <thead>
                        <tr>
                            <th>N°</th>
                            <th>Région</th>
                            <th>Dossiers non assignés</th>
                            <th class="text-center"><?= __('Actions') ?></th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php foreach($regions as $i => $region): ?>
                        <tr>
                            <td><?= $i+1 ?></td>
                            <td><?= h($region->nom) ?></td>
                            <td>
                                <?php $nbDossiers = 0;
                                foreach($dossiers as $dossier):
                                    if($dossier->commune->departement->region_id == $region->id) $nbDossiers += 1;
                                endforeach; ?>
                                <span class="badge <?php if($nbDossiers > 0) echo 'badge-success'; else echo 'badge badge-danger'; ?>"><?= $nbDossiers ?></span>
                            </td>
                            <td class="actions text-center">
                                <?= $this->Html->link(__('<i class="fa fa-eye"></i> Voir dossiers'), ['action' => 'notraites', $region->id, $type],['class'=>' btn btn-primary btn-sm', 'escape'=>false]) ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
                
            </div>

        </div>   

    </div>

</div>