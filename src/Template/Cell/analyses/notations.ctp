<?php $this->setLayout('back_office'); ?>

<div class="app-page-title">
    
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            <div class="page-title-icon">
                <i class="pe-7s-drawer icon-gradient bg-happy-itmeo"></i>
            </div>
            <div><?= $this->fetch('titre') ?>
                <div class="page-title-subheading"><?= $this->fetch('phrase') ?></div>
            </div>
        </div>
        <div class="page-title-actions">
            <div class="d-inline-block dropdown">
                <button type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn-shadow dropdown-toggle btn btn-info">
                    <span class="btn-icon-wrapper pr-2 opacity-7">
                        <i class="fa fa-business-time fa-w-20"></i>
                    </span>
                    Dossiers
                </button>
                <div tabindex="-1" role="menu" aria-hidden="true" class="dropdown-menu dropdown-menu-right">
                    <ul class="nav flex-column">
                        
                        <li class="nav-item">
                            <a href="<?php echo $this->Url->build(array('controller' => 'Analyses', 'action' => 'encours')); ?>" class="nav-link">
                                <i class="nav-link-icon lnr-inbox"></i>
                                <span>En cours</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?php echo $this->Url->build(array('controller' => 'Analyses', 'action' => 'acceptes')); ?>" class="nav-link">
                                <i class="nav-link-icon lnr-inbox"></i>
                                <span>Acceptés</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?php echo $this->Url->build(array('controller' => 'Analyses', 'action' => 'rejetes')); ?>" class="nav-link">
                                <i class="nav-link-icon lnr-book"></i>
                                <span>Rejetés</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>    
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <div class="main-card mb-3 card">
            <div class="card-body"><h5 class="card-title">Dossiers</h5>
                <?= $this->Flash->render() ?>
                <table class="mb-0 table">
                    <thead>
                        <tr>
                            <th>Agent</th>
                            <th>Structure</th>
                            <th>Comité</th>
                            <th class="text-center"><?= __('Actions') ?></th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php foreach($notations as $notation): ?>
                        <tr>
                            <td><?= h($notation->agent->prenom." ".$notation->agent->nom) ?></td>
                            <td><?= h($notation->agent->agence->nom) ?></td>
                            <td><?= h($notation->agent->comite->nom) ?></td>
                            <td class="actions text-center">
                                <?= $this->Html->link(__('<i class="fa fa-eye"></i> Détails'), ['controller' => 'Dossiers', 'action' => 'view', $notation->dossier->id],['class'=>' btn btn-primary btn-sm','escape'=>false]) ?>
                                
                                <?= $this->Html->link(__('<i class="fa fa-folder"></i> Analyse'), ['controller' => 'Analyses', 'action' => 'view', $notation->id],['class'=>' btn btn-warning btn-sm','escape'=>false]) ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
                
            </div>

        </div>   

    </div>

</div>
