<?php $this->setLayout('back_office'); ?>
<?= $this->Html->css('back/multistep') ?>

<style>
.bg-color {
    background-color: #672211 !important;
}
</style>

<input type="hidden" id="etat-dossier" value="<?= $dossier->statut_dossiers_id ?>">
<input type="hidden" id="etat-financement" value="<?= $dossier->statut_financements_id ?>">

<input type="hidden" id="checkDisabled" value="<?php if($assignation && $assignation->status_validations_id == 6) echo '1'; ?>"

<?php if($dossier->statut_financements_id != null): ?>
<div class="row">
    <div class="col-sm-12">
        <?= $this->Html->link(__('<i class="fa fa-check-circle"></i> Détails financement'), ['controller' => 'Financements', 'action' => 'details', $dossier->id],['class'=>'btn btn-warning btn-sm pull-right font-weight-bold','escape'=>false]) ?>
    </div>
</div>
<?php endif; ?>

<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-11 col-sm-10 col-md-10 col-lg-6 col-xl-12 text-center p-0 mt-3 mb-2">
            <div class="card p-5 mt-3 mb-3">
                <h2 id="heading">Analyse du dossier</h2>
                <p>Remplissez tous les champs du formulaire pour passer à l'étape suivante</p>
                <p>Qualifié par : <span class="badge badge-success"><?= $qualificationDossier->agent->prenom." ".$qualificationDossier->agent->nom ?> (<?= $qualificationDossier->agent->agence->nom ?>)</span></p>
                <?php if($notation && $notation->agent): ?>
                    <p class="mt-1">Analysé par : <span class="badge badge-success"><?= $notation->agent->prenom." ".$notation->agent->nom ?> (<?= $notation->agent->agence->nom ?>)</span></p>
                    <p class="col-sm-6 badge badge-warning mt-1" style="margin: auto; font-size: 18px">Moyenne notation : <?= $moyenne ?></p>
                <?php endif; ?>
                <?= $this->Form->create('Dossiers', array('url'=>array('controller' => 'Notations', 'action'=> 'add'), 'id' => 'msform')) ?>
                    <input type="hidden" name="dossiers_id" value="<?= $dossier->id ?>">
                    <input type="hidden" name="agents_id" value="<?= $userConnect->agents[0]->id ?>">
                    <input type="hidden" name="code" value="<?= $assignation->code ?>">
                    <input type="hidden" name="result" value="accept">
                    <input type="hidden" name="typeDemande" value="<?= $qualificationDossier->type_demande ?>">
                    <!-- progressbar -->
                    <ul id="progressbar">
                        <li class="active" id="account"><strong>Détails dossier</strong></li>
                        <li id="personal"><strong>Qualification</strong></li>
                        <li id="payment"><strong>Notation</strong></li>
                        <li id="confirm"><strong>Attribution</strong></li>
                    </ul>
                    <div class="progress">
                        <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuemin="0" aria-valuemax="100"></div>
                    </div> <br> <!-- fieldsets -->
                    <fieldset>
                        <div class="form-card">
                            <div class="row">
                                <div class="col-7">
                                    <h2 class="fs-title">Etat civil :</h2>
                                </div>
                                <div class="col-5">
                                    <h2 class="steps">Étape 1 - 4</h2>
                                </div>
                            </div> 

                            <div class="card mt-page">
                                <div class="card-body">
                                    <h5 class="color font-weight-bold mb-3">
                                        Administratif
                                    </h5>
                                    
                                    <div class="row">
                                        <div class="col-md-6 mb-5">
                                            <div class="card card-grey">
                                                <div class="card-body">
                                                    <?php if($checkEntreprise): ?>
                                                        <h5 class="font-weight-bold">
                                                            <?php if(!$checkAssociation) echo "Informations de l'entreprise"; else echo "Informations de l'association"; ?>
                                                        </h5>
                                                        <div class="row">
                                                            <div class="col-md-4 col-6">Nom</div>
                                                            <div class="col-md-8 col-6">: <?= $dossier->porteur->entreprise->nom ?> </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-4 col-6">Ninea</div>
                                                            <div class="col-md-8 col-6">: <?= $dossier->porteur->entreprise->ninea ?> </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-4 col-6">Immatriculation</div>
                                                            <div class="col-md-8 col-6">: <?= $dossier->porteur->entreprise->immatriculation ?> </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-4 col-6">Type</div>
                                                            <div class="col-md-8 col-6">: <?= $dossier->porteur->entreprise->type_entreprise->nom ?> </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-4 col-6">Statut juridique</div>
                                                            <div class="col-md-8 col-6">
                                                                : <?php if($dossier->porteur->entreprise->forme)
                                                                            echo "<span class='badge badge-success fa fa-clipboard-check no-text-transform'> Formel</span>";
                                                                        else echo "<span class='badge badge-warning fa fa-calendar-times no-text-transform'> Informel</span>";
                                                                ?> 
                                                            </div>
                                                        </div>
                                                        <div class="max-hr mb-5"></div>

                                                        <h5 class="font-weight-bold">Adresse <?php if(!$checkAssociation) echo "de l'entreprise"; else echo "de l'association"; ?></h5>
                                                        <div class="row">
                                                            <div class="col-md-4 col-6">Région</div>
                                                            <div class="col-md-8 col-6">: <?= $dossier->porteur->entreprise->commune->departement->region->nom ?> </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-4 col-6">Département</div>
                                                            <div class="col-md-8 col-6">: <?= $dossier->porteur->entreprise->commune->departement->departementscol ?> </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-4 col-6">Commune</div>
                                                            <div class="col-md-8 col-6">: <?= $dossier->porteur->entreprise->commune->nom ?> </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-4 col-6">Téléphone</div>
                                                            <div class="col-md-8 col-6">: <?= $dossier->porteur->entreprise->tel_entite ?> </div>
                                                        </div>
                                                    <?php else: ?>
                                                        <h5 class="font-weight-bold">
                                                            Informations personnelles
                                                        </h5>
                                                        <div class="row">
                                                            <div class="col-md-4 col-6">Nom</div>
                                                            <div class="col-md-8 col-6">: <?= $dossier->porteur->nom ?> </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-4 col-6">Prénom</div>
                                                            <div class="col-md-8 col-6">: <?= $dossier->porteur->prenom ?> </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-4 col-6">Genre</div>
                                                            <div class="col-md-8 col-6">: <?= $dossier->porteur->genre ?> </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-4 col-6">Naissance</div>
                                                            <div class="col-md-8 col-6">: <?= $dossier->porteur->date_naissance ?> <i class="fa fa-calendar"></i></div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-4 col-6">Lieu naissance</div>
                                                            <div class="col-md-8 col-6">: <?= $dossier->porteur->lieu_naissance ?></div>
                                                        </div>
                                                        <div class="max-hr mb-5"></div>

                                                        <h5 class="font-weight-bold">Autres</h5>
                                                        <div class="row">
                                                            <div class="col-md-4 col-6">Résidence</div>
                                                            <div class="col-md-8 col-6">: <?= $dossier->porteur->lieu_residence ?> </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-4 col-6">Identification</div>
                                                            <div class="col-md-8 col-6">: <?= $dossier->porteur->type_identification ?> </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-4 col-6">Numéro</div>
                                                            <div class="col-md-8 col-6">: <?= $dossier->porteur->num_identification ?> <i class="fa fa-id-card-alt"></i></div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-4 col-6">Téléphone</div>
                                                            <div class="col-md-8 col-6">: <?= $dossier->porteur->telephone ?> </div>
                                                        </div>
                                                    <?php endif; ?>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-6 mb-5">
                                            <div class="card card-grey">
                                                <div class="card-body">
                                                    <h5 class="font-weight-bold">Informations sur le dossier</h5>
                                                    <div class="row">
                                                        <div class="col-md-4 col-6">Code</div>
                                                        <div class="col-md-8">: <?= $dossier->numero ?> </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-4 col-6">Intitulé</div>
                                                        <div class="col-md-8 col-6">: <?= $dossier->intitule ?> </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-4 col-6">Création</div>
                                                        <div class="col-md-8 col-6">: <?= h($dossier->created->format('d/m/Y à H:i')) ?> </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-4 col-6">Secteur</div>
                                                        <div class="col-md-8 col-6">: <?= $dossier->filiere->nom ?> </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-4 col-6">Statut</div>
                                                        <div class="col-md-8 col-6">
                                                            <?php $badge = ""; $statut = ""; $icone = "";
                                                                if($dossier->soumis == "non"): $badge = "badge-warning"; $statut = "Non soumis"; $icone = "fa fa-bong";
                                                                else:
                                                                    if(($dossier->statut_dossiers_id == 1) || ($dossier->soumis == "oui" && $dossier->statut_dossiers_id == null)): $statut = "En cours"; $badge = "badge-info"; $icone = "fa fa-business-time";
                                                                    elseif($dossier->statut_dossiers_id == 2): $satut = "qualifié"; $badge = "badge-primary"; $icone = "fa fa-user-check";
                                                                    elseif($dossier->statut_dossiers_id == 3): $statut = "En cours d'analyse"; $badge = "badge-green-light"; $icone ="fa fa-chalkboard-teacher";
                                                                    elseif($dossier->statut_dossiers_id == 4): $statut = "labélisé"; $badge = "badge-success"; $icone = "fa fa-check-circle";
                                                                    elseif($dossier->statut_dossiers_id == 5): $statut = "refusé"; $badge = "badge-danger"; $icone = "fa fa-calendar-times";
                                                                endif; endif;
                                                            
                                                            ?>
                                                            : 
                                                            <?php if($statut != null): ?>
                                                                <span class="no-text-transform badge <?= $badge ?> <?= $icone ?>">
                                                                    <?= $statut ?>
                                                                </span>
                                                            <?php endif; ?>
                                                            <?php
                                                                if($dossier->statut_financements_id != null) {
                                                                    $libelleStatut = '';
                                                                    if($dossier->statut_financements_id != 7):
                                                                        if($dossier->statut_financement_id == 1) $libelleStatut = "Financement soumis";
                                                                        elseif($dossier->statut_financements_id > 1 && $dossier->statut_fincements_id <= 5) $libelleStatut = "Financement en cours";
                                                                        elseif($dossier->statut_financements_id == 6) $libelleStatut = "Financement accepté";
                                                                        echo "<span class='badge badge-success'>".$libelleStatut."</span>";
                                                                    else:
                                                                        echo "<span class='badge badge-danger'>Financement rejeté</span>";
                                                                    endif;

                                                                }
                                                            ?>
                                                        </div>
                                                    </div>
                                                    

                                                    <div class="max-hr mb-5"></div>

                                                    <h5 class="font-weight-bold">Lieu d'exécution</h5>
                                                    <div class="row">
                                                        <div class="col-md-4 col-6">Région</div>
                                                        <div class="col-md-8 col-6">: <?= $dossier->commune->departement->region->nom ?> </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-4 col-6">Département</div>
                                                        <div class="col-md-8 col-6">: <?= $dossier->commune->departement->departementscol ?> </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-4 col-6">Commune</div>
                                                        <div class="col-md-8 col-6">: <?= $dossier->commune->nom ?> </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-4 col-6">Impactés</div>
                                                        <div class="col-md-8 col-6">: <?= $dossier->nb_perso_impact ?> <i class="fa fa-user"></i></div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                    <?php if(count($pieces) > 0): ?>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="card card-grey">
                                                <div class="card-body">
                                                    <h5 class="font-weight-bold">Pièces jointes</h5>
                                                    <div class="row">
                                                        <div class="col-md-6 col-6 font-weight-bold">Titre</div>
                                                        <div class="col-md-6 col-6 font-weight-bold">Action</div>
                                                    </div>
                                                    <div class="max-hr-fonce"></div>
                                                    <?php foreach($pieces as $piece): ?>
                                                        <div class="row">
                                                            <div class="col-md-6 col-6"><?= $piece->titre ?></div>
                                                            <div class="col-md-6 col-6">
                                                                <?= $this->Html->link(__(' Voir'),'/documents/dossier/'.$piece->url,
                                                                        array('target'=>'_blank','escape'=>false, 'class'=>'fa fa-eye btn btn-success btn-sm')) ?>
                                                            </div>
                                                        </div>
                                                    <?php endforeach; ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php endif; ?>

                                    <div class="max-hr mb-5"></div>

                                    <?php if(count($dossiersFilieres) > 0): ?>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="card card-grey">
                                                <div class="card-body">
                                                    <h5 class="font-weight-bold">Secteurs impactés</h5>
                                                    <div class="row">
                                                        <div class="col-md-6 col-6 font-weight-bold">Filière</div>
                                                        <div class="col-md-6 col-6 font-weight-bold">Type</div>
                                                    </div>
                                                    <div class="max-hr-fonce"></div>
                                                    <?php foreach($dossiersFilieres as $dossierFiliere): ?>
                                                        <div class="row">
                                                            <div class="col-md-6 col-6"><?= $dossierFiliere->filiere->nom ?></div>
                                                            <div class="col-md-6 col-6"><?= $dossierFiliere->link_type ?></div>
                                                        </div>
                                                    <?php endforeach; ?>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                    <?php endif; ?>
                                    
                                    <?php if(count($dossiersImpacts) > 0): ?>
                                    <div class="max-hr mb-5"></div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="card card-grey">
                                                <div class="card-body">
                                                    <h5 class="font-weight-bold">Types d'impacts</h5>
                                                    <div class="row">
                                                        <div class="col-md-6 col-6 font-weight-bold">Type d'impact</div>
                                                        <div class="col-md-6 col-6 font-weight-bold">Impacte</div>
                                                    </div>
                                                    <div class="max-hr-fonce"></div>
                                                    <?php foreach($dossiersImpacts as $dossierImpact): ?>
                                                        <div class="row">
                                                            <div class="col-md-6 col-6"><?= $dossierImpact->types_impact->libelle ?></div>
                                                            <div class="col-md-6 col-6"><?= $dossierImpact->types_impact->impacte->libelle ?></div>
                                                        </div>
                                                    <?php endforeach; ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="max-hr mb-5"></div>
                                    
                                    <?php endif; ?>

                                </div>

                                <div class="col-sm-12">
                                    <label class="font-weight-bold">Description du projet</label>
                                    <textarea id="editor1" class="form-control" disabled rows="2" cols="30"><?= $dossier->descrip_projet ?></textarea>
                                </div>

                            </div>

                        </div> 
                        <input type="button" name="next" class="next action-button" value="Suivant" />
                    </fieldset>
                    <fieldset>
                        <div class="form-card">
                            <div class="row">
                                <div class="col-7">
                                    <h2 class="fs-title">Qualification du dossier :</h2>
                                </div>
                                <div class="col-5">
                                    <h2 class="steps">Step 2 - 4</h2>
                                </div>
                            </div>

                            <div class="card">
                                <div class="card-header bg-color text-white">
                                    <h5 class="w-100 m-0">Etats civils
                                        <a class="btn pull-right" data-toggle="collapse" href="#multiCollapseExample1" role="button" aria-expanded="false"
                                            aria-controls="multiCollapseExample1"><i class="fa fa-plus text-white"></i>
                                        </a>
                                    </h5>
                                </div>
                                <div class="card card-body collapse multi-collapse" id="multiCollapseExample1">             
                                    <div class="row">
                                        <div class="col-md-6 mb-5">
                                            <div class="card card-grey">
                                                <div class="card-body">
                                                    <?php if($checkEntreprise): ?>
                                                        <h5 class="font-weight-bold">
                                                            <?php if(!$checkAssociation) echo "Informations de l'entreprise"; else echo "Informations de l'association"; ?>
                                                        </h5>
                                                        <div class="row">
                                                            <div class="col-md-4 col-6">Nom</div>
                                                            <div class="col-md-8 col-6">: <?= $dossier->porteur->entreprise->nom ?> </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-4 col-6">Ninea</div>
                                                            <div class="col-md-8 col-6">: <?= $dossier->porteur->entreprise->ninea ?> </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-4 col-6">Immatriculation</div>
                                                            <div class="col-md-8 col-6">: <?= $dossier->porteur->entreprise->immatriculation ?> </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-4 col-6">Type</div>
                                                            <div class="col-md-8 col-6">: <?= $dossier->porteur->entreprise->type_entreprise->nom ?> </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-4 col-6">Statut juridique</div>
                                                            <div class="col-md-8 col-6">
                                                                : <?php if($dossier->porteur->entreprise->forme)
                                                                            echo "<span class='badge badge-success fa fa-clipboard-check no-text-transform'> Formel</span>";
                                                                        else echo "<span class='badge badge-warning fa fa-calendar-times no-text-transform'> Informel</span>";
                                                                ?> 
                                                            </div>
                                                        </div>
                                                        <div class="max-hr mb-4"></div>
                                                        
                                                    <?php else: ?>
                                                        <h5 class="font-weight-bold">
                                                            Informations personnelles
                                                        </h5>
                                                        <div class="row">
                                                            <div class="col-md-4 col-6">Nom</div>
                                                            <div class="col-md-8 col-6">: <?= $dossier->porteur->nom ?> </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-4 col-6">Prénom</div>
                                                            <div class="col-md-8 col-6">: <?= $dossier->porteur->prenom ?> </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-4 col-6">Genre</div>
                                                            <div class="col-md-8 col-6">: <?= $dossier->porteur->genre ?> </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-4 col-6">Naissance</div>
                                                            <div class="col-md-8 col-6">: <?= $dossier->porteur->date_naissance ?> <i class="fa fa-calendar"></i></div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-4 col-6">Lieu naissance</div>
                                                            <div class="col-md-8 col-6">: <?= $dossier->porteur->lieu_naissance ?></div>
                                                        </div>
                                                        <div class="max-hr mb-4"></div>

                                                    <?php endif; ?>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-6 mb-5">
                                            <div class="card card-grey">
                                                <div class="card-body">
                                                    <?php if($checkEntreprise): ?>
                                                        <h5 class="font-weight-bold">Adresse <?php if(!$checkAssociation) echo "de l'entreprise"; else echo "de l'association"; ?></h5>
                                                        <div class="row">
                                                            <div class="col-md-4 col-6">Région</div>
                                                            <div class="col-md-8 col-6">: <?= $dossier->porteur->entreprise->commune->departement->region->nom ?> </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-4 col-6">Département</div>
                                                            <div class="col-md-8 col-6">: <?= $dossier->porteur->entreprise->commune->departement->departementscol ?> </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-4 col-6">Commune</div>
                                                            <div class="col-md-8 col-6">: <?= $dossier->porteur->entreprise->commune->nom ?> </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-4 col-6">Téléphone</div>
                                                            <div class="col-md-8 col-6">: <?= $dossier->porteur->entreprise->tel_entite ?> </div>
                                                        </div>
                                                        <div class="max-hr mb-5"></div>
                                                    <?php else: ?>
                                                        <h5 class="font-weight-bold">Autres</h5>
                                                        <div class="row">
                                                            <div class="col-md-4 col-6">Résidence</div>
                                                            <div class="col-md-8 col-6">: <?= $dossier->porteur->lieu_residence ?> </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-4 col-6">Identification</div>
                                                            <div class="col-md-8 col-6">: <?= $dossier->porteur->type_identification ?> </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-4 col-6">Numéro</div>
                                                            <div class="col-md-8 col-6">: <?= $dossier->porteur->num_identification ?> <i class="fa fa-id-card-alt"></i></div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-4 col-6">Téléphone</div>
                                                            <div class="col-md-8 col-6">: <?= $dossier->porteur->telephone ?> </div>
                                                        </div>
                                                    <?php endif; ?>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>

                                    <label class="font-weight-bold">Commentaire sur l'état civil</label>
                                    <textarea id="editor_etat_civil" class="form-control" rows="2" cols="30"><?= $qualificationDossier->desc_etat_civil ?></textarea>

                                </div>
                            </div>
                            
                            <div class="card mt-3">
                                <div class="card-header bg-color text-white">
                                    <h5 class="w-100 m-0">Informations sur le dossier
                                        <a class="btn pull-right" data-toggle="collapse" href="#multiCollapseExample2" role="button" aria-expanded="false"
                                            aria-controls="multiCollapseExample2"><i class="fa fa-plus text-white"></i>
                                        </a>
                                    </h5>
                                </div>
                                <div class="card card-body collapse multi-collapse" id="multiCollapseExample2">
                                                                    
                                    <div class="row">
                                        <div class="col-md-6 mb-5">
                                            <div class="card card-grey">
                                                <div class="card-body">
                                                    <h5 class="font-weight-bold">Informations sur le dossier</h5>
                                                    <div class="row">
                                                        <div class="col-md-4 col-6">Code</div>
                                                        <div class="col-md-8">: <?= $dossier->numero ?> </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-4 col-6">Intitulé</div>
                                                        <div class="col-md-8 col-6">: <?= $dossier->intitule ?> </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-4 col-6">Création</div>
                                                        <div class="col-md-8 col-6">: <?= h($dossier->created->format('d/m/Y à H:i')) ?> </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-4 col-6">Secteur</div>
                                                        <div class="col-md-8 col-6">: <?= $dossier->filiere->nom ?> </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-4 col-6">Statut</div>
                                                        <div class="col-md-8 col-6">
                                                            <?php $badge = ""; $statut = ""; $icone = "";
                                                                if($dossier->soumis == "non"): $badge = "badge-warning"; $statut = "Non soumis"; $icone = "fa fa-bong";
                                                                else:
                                                                    if(($dossier->statut_dossiers_id == 1) || ($dossier->soumis == "oui" && $dossier->statut_dossiers_id == null)): $statut = "En cours"; $badge = "badge-info"; $icone = "fa fa-business-time";
                                                                    elseif($dossier->statut_dossiers_id == 2): $satut = "qualifié"; $badge = "badge-primary"; $icone = "fa fa-user-check";
                                                                    elseif($dossier->statut_dossiers_id == 3): $statut = "En cours d'analyse"; $badge = "badge-green-light"; $icone ="fa fa-chalkboard-teacher";
                                                                    elseif($dossier->statut_dossiers_id == 4): $statut = "labélisé"; $badge = "badge-success"; $icone = "fa fa-check-circle";
                                                                    elseif($dossier->statut_dossiers_id == 5): $statut = "refusé"; $badge = "badge-danger"; $icone = "fa fa-calendar-times";
                                                                endif; endif;
                                                            
                                                            ?>
                                                            : <span class="no-text-transform badge <?= $badge ?> <?= $icone ?>">
                                                                <?= $statut ?>
                                                            </span>
                                                        </div>
                                                    </div>

                                                </div>

                                                <div class="max-hr mb-4"></div>
                                            </div>
                                        </div>

                                        <div class="col-md-6 mb-5">
                                            <div class="card card-grey">
                                                <div class="card-body">
                                                    <h5 class="font-weight-bold">Lieu d'exécution</h5>
                                                    <div class="row">
                                                        <div class="col-md-4 col-6">Région</div>
                                                        <div class="col-md-8 col-6">: <?= $dossier->commune->departement->region->nom ?> </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-4 col-6">Département</div>
                                                        <div class="col-md-8 col-6">: <?= $dossier->commune->departement->departementscol ?> </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-4 col-6">Commune</div>
                                                        <div class="col-md-8 col-6">: <?= $dossier->commune->nom ?> </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-4 col-6">Impactés</div>
                                                        <div class="col-md-8 col-6">: <?= $dossier->nb_perso_impact ?> <i class="fa fa-user"></i></div>
                                                    </div>
                                                </div>

                                                <div class="max-hr mb-5"></div>
                                            </div>
                                        </div>

                                    </div>

                                    
                                    <?php if(count($pieces) > 0): ?>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="card card-grey">
                                                <div class="card-body">
                                                    <h5 class="font-weight-bold">Pièces jointes</h5>
                                                    <div class="row">
                                                        <div class="col-md-6 col-6 font-weight-bold">Titre</div>
                                                        <div class="col-md-6 col-6 font-weight-bold">Action</div>
                                                    </div>
                                                    <div class="max-hr-fonce"></div>
                                                    <?php foreach($pieces as $piece): ?>
                                                        <div class="row">
                                                            <div class="col-md-6 col-6"><?= $piece->titre ?></div>
                                                            <div class="col-md-6 col-6">
                                                                <?= $this->Html->link(__(' Voir'),'/documents/dossier/'.$piece->url,
                                                                        array('target'=>'_blank','escape'=>false, 'class'=>'fa fa-eye btn btn-success btn-sm')) ?>
                                                            </div>
                                                        </div>
                                                    <?php endforeach; ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php endif; ?>

                                    <div class="max-hr mb-5"></div>

                                    <label class="font-weight-bold">Description du projet</label>
                                    <textarea class="form-control" id="editor2" name="" rows="2" cols="30" placeholder="Description du projet" disabled><?= $dossier->descrip_projet ?></textarea>

                                    <div class="max-hr mb-5"></div>

                                    <label class="font-weight-bold">Commentaire sur le dossier</label>
                                    <textarea class="form-control mb-5" id="editor_dossier" name="" rows="2" cols="30"><?= $qualificationDossier->desc_dossier ?></textarea>

                                    <div class="max-hr mb-5"></div>
                                            
                                </div>
                            </div>

                            <div class="card mt-3">
                                <div class="card-header bg-color text-white">
                                    <h5 class="w-100 m-0">Impacts de l'activité
                                        <a class="btn pull-right" data-toggle="collapse" href="#multiCollapseExample3" role="button" aria-expanded="false"
                                            aria-controls="multiCollapseExample3"><i class="fa fa-plus text-white"></i>
                                        </a>
                                    </h5>
                                </div>
                                <div class="card card-body collapse multi-collapse" id="multiCollapseExample3">
                                            
                                    <?php if(count($dossiersImpacts) > 0): ?>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="card card-grey">
                                                <div class="card-body">
                                                    <h5 class="font-weight-bold">Types d'impacts</h5>
                                                    <div class="row">
                                                        <div class="col-md-6 col-6 font-weight-bold">Type d'impact</div>
                                                        <div class="col-md-6 col-6 font-weight-bold">Impacte</div>
                                                    </div>
                                                    <div class="max-hr-fonce"></div>
                                                    <?php foreach($dossiersImpacts as $dossierImpact): ?>
                                                        <div class="row">
                                                            <div class="col-md-6 col-6"><?= $dossierImpact->types_impact->libelle ?></div>
                                                            <div class="col-md-6 col-6"><?= $dossierImpact->types_impact->impacte->libelle ?></div>
                                                        </div>
                                                    <?php endforeach; ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="max-hr mb-5"></div>
                                    
                                    <?php endif; ?>

                                    <?php if(count($dossiersFilieres) > 0): ?>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="card card-grey">
                                                <div class="card-body">
                                                    <h5 class="font-weight-bold">Secteurs impactés</h5>
                                                    <div class="row">
                                                        <div class="col-md-6 col-6 font-weight-bold">Filière</div>
                                                        <div class="col-md-6 col-6 font-weight-bold">Type</div>
                                                    </div>
                                                    <div class="max-hr-fonce"></div>
                                                    <?php foreach($dossiersFilieres as $dossierFiliere): ?>
                                                        <div class="row">
                                                            <div class="col-md-6 col-6"><?= $dossierFiliere->filiere->nom ?></div>
                                                            <div class="col-md-6 col-6"><?= $dossierFiliere->link_type ?></div>
                                                        </div>
                                                    <?php endforeach; ?>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>

                                    <div class="max-hr mb-5"></div>

                                    <?php endif; ?>
                                    
                                    <label class="font-weight-bold">Commentaire sur les impacts</label>
                                    <textarea class="form-control mb-5" id="editor_impact" rows="2" cols="30" placeholder="Saisir un commentaire"><?= $qualificationDossier->desc_impact ?></textarea>
                                    
                                    
                                </div>
                            </div>


                            
                        </div> 
                        <input type="button" name="next" class="next action-button" value="Suivant" /> 
                        <input type="button" name="previous" class="previous action-button-previous" value="Précédent" />
                    </fieldset>
                    <fieldset>
                        <div class="form-card">
                            <div class="row">
                                <div class="col-7">
                                    <h2 class="fs-title">Notation de l'activité :</h2>
                                </div>
                                <div class="col-5">
                                    <h2 class="steps">Step 3 - 4</h2>
                                </div>
                            </div> 

                            <div class="card">
                                <div class="card-header bg-color">
                                    <h5 class="w-100 m-0 text-white">
                                        Administrartif et Technique (total <?php if($qualificationDossier->type_demande == "label") echo "30"; else echo "20"; ?> points)
                                        <a class="btn pull-right" data-toggle="collapse" href="#collapseNotation1" role="button" aria-expanded="false"
                                            aria-controls="collapseNotation1"><i class="fa fa-plus text-white"></i>
                                        </a>
                                    </h5>
                                </div>
                                <div class="card-body table-responsive collapse multi-collapse" id="collapseNotation1">
                                    <div class="row">
                                        <div class="col-sm-6">

                                            <div class="col-md-12">
                                                <label class="padding-step">Nature de la Structure juridique et ancienneté</label> 
                                                <input type="text" class="form-control border-radius-25" name="nat_struc_juridique" id="nat_struc_juridique" readonly>
                                            </div>
                                            <div class="col-md-12">
                                                <label class="padding-step">Eligibilité dans le secteur d’activité</label> 
                                                <input type="text" name="eligibilite_filiere" class="form-control border-radius-25" id="eligibilite_filiere" readonly>
                                            </div>  
                                            <div class="col-md-12">
                                                <label class="padding-step">Partenariat et avec relation institution</label> 
                                                <input type="text" name="partenariat_relation" class="form-control border-radius-25" id="partenariat_relation" readonly>
                                            </div>
                                            <div class="col-md-12">
                                                <label class="padding-step">Pertinence de l’activité (points forts)</label> 
                                                <input type="text" name="pertinence_activite" class="form-control border-radius-25" id="pertinence_activite" readonly>
                                            </div>
                                            <div class="col-md-12">
                                                <label class="padding-step">Cohérence de l’activité avec les activités annexes</label> 
                                                <input type="text" name="coherence_activite" class="form-control border-radius-25" id="coherence_activite" readonly>
                                            </div>
                                            <div class="col-md-12">
                                                <label class="padding-step">Positionnement de l’activité de la structure dans sa chaine de valeur</label> 
                                                <input type="text" name="positionnement_activite" class="form-control border-radius-25" id="positionnement_activite" readonly>
                                            </div>

                                        </div>
                                        <div class="col-sm-6 pt-3">
                                            <div class="col-md-12">
                                                <label class="padding-step">Note sur <?php if($qualificationDossier->type_demande == "label") echo "5"; else echo "4"; ?></label> 
                                                <div class="col-md-12 p-0">
                                                    <input class="bar" type="range" id="range_nat_struc_juridique" value="<?= $notation ? $notation->nat_struc_juridique : '0' ?>" min="0" max="<?php if($qualificationDossier->type_demande == 'label') echo '5'; else echo '4'; ?>" />
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <label class="padding-step">Note sur <?php if($qualificationDossier->type_demande == "label") echo "5"; else echo "4"; ?></label> 
                                                <div class="col-md-12 p-0">
                                                    <input class="bar" type="range" id="range_eligibilite_filiere" value="<?= $notation ? $notation->eligibilite_filiere : '0' ?>" min="0" max="<?php if($qualificationDossier->type_demande == 'label') echo '5'; else echo '4'; ?>" />
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <label class="padding-step">Note sur <?php if($qualificationDossier->type_demande == "label") echo "5"; else echo "4"; ?></label> 
                                                <div class="col-md-12 p-0">
                                                    <input class="bar" type="range" id="range_partenariat_relation" value="<?= $notation ? $notation->partenariat_relation : '0' ?>" min="0" max="<?php if($qualificationDossier->type_demande == 'label') echo '5'; else echo '4'; ?>" />
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <label class="padding-step">Note sur <?php if($qualificationDossier->type_demande == "label") echo "5"; else echo "4"; ?></label> 
                                                <div class="col-md-12 p-0">
                                                    <input class="bar" type="range" id="range_pertinence_activite" value="<?= $notation ? $notation->pertinence_activite: '0' ?>" min="0" max="<?php if($qualificationDossier->type_demande == 'label') echo '5'; else echo '4'; ?>" />
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <label class="padding-step">Note sur <?php if($qualificationDossier->type_demande == "label") echo "5"; else echo "2"; ?></label> 
                                                <div class="col-md-12 p-0">
                                                    <input class="bar" type="range" id="range_coherence_activite" value="<?= $notation ? $notation->coherence_activite : '0' ?>" min="0" max="<?php if($qualificationDossier->type_demande == 'label') echo '5'; else echo '2'; ?>" />
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <label class="padding-step">Note sur <?php if($qualificationDossier->type_demande == "label") echo "5"; else echo "2"; ?></label> 
                                                <div class="col-md-12 p-0">
                                                    <input class="bar" type="range" id="range_positionnement_activite" value="<?= $notation ? $notation->positionnement_activite : '0' ?>" min="0" max="<?php if($qualificationDossier->type_demande == 'label') echo '5'; else echo '2'; ?>" />
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <label class="font-weight-bold">Commentaire sur l'aspect technique</label>
                                            <textarea id="editor_technique" name="comment_aspect_tech" class="form-control" rows="2" cols="30"><?= $notation ? $notation->comment_aspect_tech : '' ?></textarea>
                                        </div>

                                    </div>
                                </div>
                            </div>

                            <div class="card mt-5">
                                <div class="card-header bg-color">
                                    <h5 class="w-100 m-0 text-white">
                                        Situation Financière (total <?php if($qualificationDossier->type_demande == "label") echo "30"; else echo "60"; ?> points) 
                                        <a class="btn pull-right" data-toggle="collapse" href="#collapseNotation2" role="button" aria-expanded="false"
                                            aria-controls="collapseNotation2"><i class="fa fa-plus text-white"></i>
                                        </a>
                                    </h5>
                                </div>
                                <div class="card-body table-responsive collapse multi-collapse" id="collapseNotation2">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <?php if($qualificationDossier->type_demande == "label"): ?>
                                            <div class="col-md-12">
                                                <label class="padding-step">Santé financière depuis trois ans</label> 
                                                <input type="text" id="sante_financiere" name="sante_financiere" class="form-control border-radius-25" readonly>
                                            </div>
                                            <div class="col-md-12">
                                                <label class="padding-step">Croissance du résultat durant les trois dernières années</label> 
                                                <input type="text" name="croissance_reultat" id="croissance_reultat" class="form-control border-radius-25" readonly>
                                            </div>
                                            <?php else: ?>
                                            <div class="col-md-12">
                                                <label class="padding-step">Engagements actuels de la structure ou du projet</label> 
                                                <input type="text" id="engagement_actuel" name="engagement_actuel" class="form-control border-radius-25" readonly>
                                            </div>
                                            <div class="col-md-12">
                                                <label class="padding-step">Situation des comptes</label> 
                                                <input type="text" name="situation_compte" id="situation_compte" class="form-control border-radius-25" readonly>
                                            </div>
                                            <?php endif; ?>
                                            <div class="col-md-12">
                                                <label class="padding-step">Situation des Actifs de la structure</label> 
                                                <input type="text" id="situation_actif" name="situation_actif" class="form-control border-radius-25" readonly>
                                            </div>
                                            <div class="col-md-12">
                                                <label class="padding-step">Situation des Passifs de la structure</label> 
                                                <input type="text" name="situation_passif" id="situation_passif" class="form-control border-radius-25" readonly>
                                            </div>  
                                            <?php if($qualificationDossier->type_demande == "label"): ?>
                                            <div class="col-md-12">
                                                <label class="padding-step">Croissance en vue</label> 
                                                <input type="text" name="croissance_en_vue" id="croissance_en_vue" class="form-control border-radius-25" readonly>
                                            </div>
                                            <?php endif; ?>
                                        </div>
                                        <div class="col-sm-6 pt-3">
                                            <?php if($qualificationDossier->type_demande == "label"): ?>
                                            <div class="col-md-12">
                                                <label class="padding-step">Note sur 5</label> 
                                                <div class="col-md-12 p-0">
                                                    <input class="bar" type="range" id="range_sante_financiere" value="<?= $notation ? $notation->sante_financiere : '0' ?>" min="0" max="5" />
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <label class="padding-step">Note sur 10</label> 
                                                <div class="col-md-12 p-0">
                                                    <input class="bar" type="range" id="range_croissance_reultat" value="<?= $notation ? $notation->croissance_reultat : '0' ?>" min="0" max="10" />
                                                </div>
                                            </div>
                                            <?php else: ?>
                                                <div class="col-md-12">
                                                    <label class="padding-step">Note sur 15</label> 
                                                    <div class="col-md-12 p-0">
                                                        <input class="bar" type="range" id="range_engagement_actuel" value="<?= $notation ? $notation->engagement_actuel : '0' ?>" min="0" max="15" />
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <label class="padding-step">Note sur 15</label> 
                                                    <div class="col-md-12 p-0">
                                                        <input class="bar" type="range" id="range_situation_compte" value="<?= $notation ? $notation->situation_compte : '0' ?>" min="0" max="15" />
                                                    </div>
                                                </div>
                                            <?php endif; ?>
                                            <div class="col-md-12">
                                                <label class="padding-step">Note sur <?php if($qualificationDossier->type_demande == "label") echo "5"; else echo "15"; ?></label> 
                                                <div class="col-md-12 p-0">
                                                    <input class="bar" type="range" id="range_situation_actif" value="<?= $notation ? $notation->situation_actif : '0' ?>" min="0" max="<?php if($qualificationDossier->type_demande == 'label') echo '5'; else echo '15'; ?>" />
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <label class="padding-step">Note sur <?php if($qualificationDossier->type_demande == 'label') echo '5'; else echo '15'; ?></label> 
                                                <div class="col-md-12 p-0">
                                                    <input class="bar" type="range" id="range_situation_passif" value="<?= $notation ? $notation->situation_passif : '0' ?>" min="0" max="<?php if($qualificationDossier->type_demande == 'label') echo '5'; else echo '15'; ?>" />
                                                </div>
                                            </div>
                                            <?php if($qualificationDossier->type_demande == "label"): ?>
                                            <div class="col-md-12">
                                                <label class="padding-step">Note sur 5</label> 
                                                <div class="col-md-12 p-0">
                                                    <input class="bar" type="range" id="range_croissance_en_vue" value="<?= $notation ? $notation->croissance_en_vue : '0' ?>" min="0" max="5" />
                                                </div>
                                            </div>
                                            <?php endif; ?>

                                        </div>

                                        <div class="col-md-12">
                                            <label class="font-weight-bold">Commentaire sur l'aspect financier</label>
                                            <textarea id="editor_financier" name="comment_aspect_financ" class="form-control" rows="2" cols="30"><?= $notation ? $notation->comment_aspect_financ : '' ?></textarea>
                                        </div>

                                    </div>
                                </div>
                            </div>


                            <div class="card mt-5">
                                <div class="card-header bg-color">
                                    <h5 class="w-100 m-0 text-white">
                                        Autres critère Environnement/ Social/Culturel (total <?php if($qualificationDossier->type_demande == 'label') echo '40'; else echo '20'; ?> points)  
                                        <a class="btn pull-right" data-toggle="collapse" href="#collapseNotation3" role="button" aria-expanded="false"
                                            aria-controls="collapseNotation3"><i class="fa fa-plus text-white"></i>
                                        </a>
                                    </h5>
                                </div>
                                <div class="card-body table-responsive collapse multi-collapse" id="collapseNotation3">
                                    <div class="row">
                                        <div class="col-sm-6">

                                            <div class="col-md-12">
                                                <label class="padding-step">Critère d’éligibilité ESS </label> 
                                                <input type="text" id="eligibilite_ess" name="eligibilite_ess" class="form-control border-radius-25" readonly>
                                            </div>
                                            <div class="col-md-12">
                                                <label class="padding-step">Impact social/culturel et Impact environnemental de l’activité</label> 
                                                <input type="text" name="impact_social_env" id="impact_social_env" class="form-control border-radius-25" readonly>
                                            </div>
                                            <div class="col-md-12">
                                                <label class="padding-step">Influence territoriale/cible : effet sur l’emploi</label> 
                                                <input type="text" id="influence_territoriale" name="influence_territoriale" class="form-control border-radius-25" readonly>
                                            </div>
                                            <div class="col-md-12">
                                                <label class="padding-step">Autre Critère ESS 1 (gouvernance)</label> 
                                                <input type="text" name="autre_critere_ess1" id="autre_critere_ess1" class="form-control border-radius-25" readonly>
                                            </div>  
                                            <div class="col-md-12">
                                                <label class="padding-step">Autre Critère ESS 2</label> 
                                                <input type="text" name="autre_critere_ess2" id="autre_critere_ess2" class="form-control border-radius-25" readonly>
                                            </div>
                                            <div class="col-md-12">
                                                <label class="padding-step">Pourcentage des revenus des dirigeants par rapports aux employés</label> 
                                                <input type="text" name="pourcentage_revenu_dirigeants" id="pourcentage_revenu_dirigeants" class="form-control border-radius-25" readonly>
                                            </div>  
                                        </div>
                                        <div class="col-sm-6 pt-3">
                                            <div class="col-md-12">
                                                <label class="padding-step">Note sur <?php if($qualificationDossier->type_demande == 'label') echo '30'; else echo '10'; ?></label> 
                                                <div class="col-md-12 p-0">
                                                    <input class="bar" type="range" id="range_eligibilite_ess" value="<?= $notation ? $notation->eligibilite_ess : '0' ?>" min="0" max="<?php if($qualificationDossier->type_demande == 'label') echo '30'; else echo '10'; ?>" />
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <label class="padding-step">Note sur 3</label> 
                                                <div class="col-md-12 p-0">
                                                    <input class="bar" type="range" id="range_impact_social_env" value="<?= $notation ? $notation->impact_social_env : '0' ?>" min="0" max="3" />
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <label class="padding-step">Note sur 2</label> 
                                                <div class="col-md-12 p-0">
                                                    <input class="bar" type="range" id="range_influence_territoriale" value="<?= $notation ? $notation->influence_territoriale : '0' ?>" min="0" max="2" />
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <label class="padding-step">Note sur 1</label> 
                                                <div class="col-md-12 p-0">
                                                    <input class="bar" type="range" id="range_autre_critere_ess1" value="<?= $notation ? $notation->autre_critere_ess1 : '0' ?>" min="0" max="1" />
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <label class="padding-step">Note sur 2</label> 
                                                <div class="col-md-12 p-0">
                                                    <input class="bar" type="range" id="range_autre_critere_ess2" value="<?= $notation ? $notation->autre_critere_ess2 : '0' ?>" min="0" max="2" />
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <label class="padding-step">Note sur 2</label> 
                                                <div class="col-md-12 p-0">
                                                    <input class="bar" type="range" id="range_pourcentage_revenu_dirigeants" value="<?= $notation ? $notation->pourcentage_revenu_dirigeants : '0' ?>" min="0" max="2" />
                                                </div>
                                            </div>

                                        </div>

                                        <div class="col-md-12">
                                            <label class="font-weight-bold">Commentaire sur l'aspect environnemental</label>
                                            <textarea id="editor_environ" name="comment_aspect_env" class="form-control" rows="2" cols="30"><?= $notation ? $notation->comment_aspect_env : '' ?></textarea>
                                        </div>

                                    </div>
                                </div>
                            </div>

                        </div> 
                        <input type="button" name="next" class="next action-button" value="Suivant" /> 
                        <input type="button" name="previous" class="previous action-button-previous" value="Précédent" />
                    </fieldset>
                    <fieldset>
                        <div class="form-card">
                            <div class="row">
                                <div class="col-7">
                                    <h2 class="fs-title">Finalisation :</h2>
                                </div>
                                <div class="col-5">
                                    <h2 class="steps">Step 4 - 4</h2>
                                </div>
                            </div> <br><br>
                            <?php if(!$notation): ?>
                                <h2 class="purple-text text-center"><strong>Cliquez sur valider pour terminer le traiement</strong></h2> <br>
                                <div class="row">
                                    <div class="col-12 text-center"> 
                                        <button type="button" id="btn-modal-rejet" class="btn btn-danger btn-lg font-weight-bold" data-toggle="modal" data-target="#modalRejet">
                                            <i class="fa fa-times"></i> Rejeter
                                        </button>
                                        <button type="button" id="btn-valide" class="btn btn-success btn-lg font-weight-bold">
                                            <i class="fa fa-check"></i> Valider
                                        </button>
                                    </div>
                                </div> <br><br>
                            <?php else: ?>
                                    <h2 class="purple-text text-center">
                                        <strong>
                                            Statut du dossier : 
                                            <?php if($notation->raison_rejet == null): ?> <i class="badge badge-success">Avis favorable</i><?php endif; ?>
                                            <?php if($notation->raison_rejet != null): ?> <i class="badge badge-danger">Avis non favorable</i><?php endif; ?>
                                        </strong>
                                    </h2>
                            <?php endif; ?>
                            <div class="row justify-content-center">
                                <div class="col-7 text-center"></div>
                            </div>
                        </div>
                    </fieldset>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalRejet" tabindex="1" role="dialog" aria-labelledby="rejetModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <?= $this->Form->create('Agents', array('enctype'=>'multipart/form-data', 'url'=>array('controller' => 'Notations', 'action'=> 'add'))); ?>
                <div class="modal-header">
                    <h5 class="modal-title" id="addModalLabel">Rejeter le dossier</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                
                <div class="modal-body">
                    <input type="hidden" name="dossiers_id" value="<?= $dossier->id ?>">
                    <input type="hidden" name="result" value="rejet">
                    <input type="hidden" name="agents_id" value="<?= $userConnect->agents[0]->id ?>">
                    <input type="hidden" name="code" value="<?= $assignation->code ?>">
                    <input type="hidden" name="typeDemande" value="<?= $qualificationDossier->type_demande ?>">
                    <label class="font-weight-bold">Saisir le motif du rejet</label>
                    <textarea name="raison_rejet" class="form-control mb-5" id="editor_rejet" rows="2" cols="30" placeholder="Saisir un commentaire"></textarea>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-info" data-dismiss="modal">Annuler</button>
                    <button type="submit" class="btn btn-danger">Confirmer</button>
                </div>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div> 

<!-- JQuery -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<script>
    $(document).ready(function(){

        CKEDITOR.replace('editor1');
        CKEDITOR.replace('editor2');
        CKEDITOR.replace('editor_etat_civil');
        CKEDITOR.replace('editor_dossier');
        CKEDITOR.replace('editor_impact');
        CKEDITOR.replace('editor_rejet'); 
        CKEDITOR.replace('editor_technique');
        CKEDITOR.replace('editor_financier');
        CKEDITOR.replace('editor_environ');


        if(($('#etat-dossier').val() >= 4 && $('#etat-financement').val() == "") || $('#etat-financement').val() > 4 || $('#checkDisabled').val() == 1) {
            $('input[type="range"]').attr('disabled', true);
            $('textarea').attr('disabled', true);
        }

        notationRangeTech();
        notationRangeFinance();
        notationRangeEnv();

        $('#btn-modal-rejet').click(function(){

            var value2 = CKEDITOR.instances['editor_dossier'].getData();
            $('#desc_dossier').val(value2);

            var value3 = CKEDITOR.instances['editor_impact'].getData();
            $('#desc_impact').val(value3);
        });

        $('#btn-valide').click(function(){
            if (confirm("Voulez-vous vraiment valider ce dossier ?")) {
                $('#msform').submit();
            } else {
                return false;
            }
        });
            

        var current_fs, next_fs, previous_fs; //fieldsets
        var opacity;
        var current = 1;
        var steps = $("fieldset").length;

        setProgressBar(current);

        $(".next").click(function(){

        current_fs = $(this).parent();
        next_fs = $(this).parent().next();

        //Add Class Active
        $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

        //show the next fieldset
        next_fs.show();
        //hide the current fieldset with style
        current_fs.animate({opacity: 0}, {
        step: function(now) {
        // for making fielset appear animation
        opacity = 1 - now;

        current_fs.css({
        'display': 'none',
        'position': 'relative'
        });
        next_fs.css({'opacity': opacity});
        },
        duration: 500
        });
        setProgressBar(++current);
        });

        $(".previous").click(function(){

        current_fs = $(this).parent();
        previous_fs = $(this).parent().prev();

        //Remove class active
        $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");

        //show the previous fieldset
        previous_fs.show();

        //hide the current fieldset with style
        current_fs.animate({opacity: 0}, {
        step: function(now) {
        // for making fielset appear animation
        opacity = 1 - now;

        current_fs.css({
        'display': 'none',
        'position': 'relative'
        });
        previous_fs.css({'opacity': opacity});
        },
        duration: 500
        });
        setProgressBar(--current);
        });

        function setProgressBar(curStep){
            var percent = parseFloat(100 / steps) * curStep;
            percent = percent.toFixed();
            $(".progress-bar")
            .css("width",percent+"%")
        }

        $(".submit").click(function(){
            return false;
        })
        

    });

    function notationRangeTech() {
        $('#nat_struc_juridique').val($('#range_nat_struc_juridique').val());
        $('#eligibilite_filiere').val($('#range_eligibilite_filiere').val());
        $('#partenariat_relation').val($('#range_partenariat_relation').val());
        $('#pertinence_activite').val($('#range_pertinence_activite').val());
        $('#coherence_activite').val($('#range_coherence_activite').val());
        $('#positionnement_activite').val($('#range_positionnement_activite').val());

        $('#range_nat_struc_juridique').change(function(){
            $('#nat_struc_juridique').val($(this).val());
        });
        $('#range_eligibilite_filiere').change(function(){
            $('#eligibilite_filiere').val($(this).val());
        });
        $('#range_partenariat_relation').change(function(){
            $('#partenariat_relation').val($(this).val());
        });
        $('#range_pertinence_activite').change(function(){
            $('#pertinence_activite').val($(this).val());
        });
        $('#range_coherence_activite').change(function(){
            $('#coherence_activite').val($(this).val());
        });
        $('#range_positionnement_activite').change(function(){
            $('#positionnement_activite').val($(this).val());
        });

    }

    function notationRangeFinance() {
        <?php if($qualificationDossier->type_demande == "label"): ?>
            $('#sante_financiere').val($('#range_sante_financiere').val());
            $('#croissance_reultat').val($('#range_croissance_reultat').val());
            $('#croissance_en_vue').val($('#range_croissance_en_vue').val());

            $('#range_sante_financiere').change(function(){
                $('#sante_financiere').val($(this).val());
            });
            $('#range_croissance_reultat').change(function(){
                $('#croissance_reultat').val($(this).val());
            });
            $('#range_croissance_en_vue').change(function(){
                $('#croissance_en_vue').val($(this).val());
            });
        <?php else: ?>
            $('#engagement_actuel').val($('#range_engagement_actuel').val());
            $('#situation_compte').val($('#range_situation_compte').val());

            $('#range_engagement_actuel').change(function(){
                $('#engagement_actuel').val($(this).val());
            });
            $('#range_situation_compte').change(function(){
                $('#situation_compte').val($(this).val());
            });
        <?php endif; ?>
        $('#situation_actif').val($('#range_situation_actif').val());
        $('#situation_passif').val($('#range_situation_passif').val());

        
        
        $('#range_situation_actif').change(function(){
            $('#situation_actif').val($(this).val());
        });
        $('#range_situation_passif').change(function(){
            $('#situation_passif').val($(this).val());
        });
        

    }

    function notationRangeEnv() {
        $('#eligibilite_ess').val($('#range_eligibilite_ess').val());
        $('#impact_social_env').val($('#range_impact_social_env').val());
        $('#influence_territoriale').val($('#range_influence_territoriale').val());
        $('#autre_critere_ess1').val($('#range_autre_critere_ess1').val());
        $('#autre_critere_ess2').val($('#range_autre_critere_ess2').val());
        $('#pourcentage_revenu_dirigeants').val($('#range_pourcentage_revenu_dirigeants').val());

        $('#range_eligibilite_ess').change(function(){
            $('#eligibilite_ess').val($(this).val());
        });
        $('#range_impact_social_env').change(function(){
            $('#impact_social_env').val($(this).val());
        });
        $('#range_influence_territoriale').change(function(){
            $('#influence_territoriale').val($(this).val());
        });
        $('#range_autre_critere_ess1').change(function(){
            $('#autre_critere_ess1').val($(this).val());
        });
        $('#range_autre_critere_ess2').change(function(){
            $('#autre_critere_ess2').val($(this).val());
        });
        $('#range_pourcentage_revenu_dirigeants').change(function(){
            $('#pourcentage_revenu_dirigeants').val($(this).val());
        });

    }
    
</script>