<?php $this->setLayout('back_office'); ?>
<?= $this->Html->css('back/multistep') ?>

<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-11 col-sm-10 col-md-10 col-lg-6 col-xl-12 text-center p-0 mt-3 mb-2">
            <div class="card p-5 mt-3 mb-3">
                <h2 id="heading">Qualification du dossier</h2>
                <?php if(isset($qualificationDossier) && $qualificationDossier != null): ?>
                    <p>Qualifié par : <span class="badge badge-success"><?= $qualificationDossier->agent->prenom." ".$qualificationDossier->agent->nom ?> (<?= $qualificationDossier->agent->agence->nom ?>)</span></p>
                <?php else: ?>
                    <p>Remplissez tous les champs du formulaire pour passer à l'étape suivante</p>
                <?php endif; ?>

                <?= $this->Form->create('Dossiers', array('url'=>array('controller' => 'QualificationDossiers', 'action'=> 'add'), 'id' => 'msform')) ?>
                    <input type="hidden" name="dossiers_id" value="<?= $dossier->id ?>">
                    <input type="hidden" name="result" value="accept">
                    <input type="hidden" name="agents_id" value="<?= $userConnect->agents[0]->id ?>">
                    <input type="hidden" name="codeAssignation" value="<?= $codeAssignation ?>">
                    <!-- progressbar -->
                    <ul id="progressbar">
                        <li class="active" id="account"><strong>Etat civil</strong></li>
                        <li id="personal"><strong>Dossier activité</strong></li>
                        <li id="payment"><strong>Impact</strong></li>
                        <li id="confirm"><strong>Attribution</strong></li>
                    </ul>
                    <div class="progress">
                        <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuemin="0" aria-valuemax="100"></div>
                    </div> <br> <!-- fieldsets -->
                    <fieldset>
                        <div class="form-card">
                            <div class="row">
                                <div class="col-7">
                                    <h2 class="fs-title">Etat civil :</h2>
                                </div>
                                <div class="col-5">
                                    <h2 class="steps">Étape 1 - 4</h2>
                                </div>
                            </div> 

                            <div class="card mt-page">
                                <div class="card-body">
                                    <h5 class="color font-weight-bold mb-3">
                                        Administratif
                                        <?php if($dossier->statut_financements_id != null): ?>
                                            <?= $this->Html->link(__('<i class="fa fa-check-circle"></i> Détails financement'), ['controller' => 'Financements', 'action' => 'details', $dossier->id],['class'=>'btn btn-warning btn-sm pull-right font-weight-bold','escape'=>false]) ?>
                                        <?php endif; ?>
                                    </h5>
                                    
                                    <div class="row">
                                        <div class="col-md-6 mb-5">
                                            <div class="card card-grey">
                                                <div class="card-body">
                                                    <?php if($checkEntreprise): ?>
                                                        <h5 class="font-weight-bold">
                                                            <?php if(!$checkAssociation) echo "Informations de l'entreprise"; else echo "Informations de l'association"; ?>
                                                        </h5>
                                                        <div class="row">
                                                            <div class="col-md-4 col-6">Nom</div>
                                                            <div class="col-md-8 col-6">: <?= $dossier->porteur->entreprise->nom ?> </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-4 col-6">Ninea</div>
                                                            <div class="col-md-8 col-6">: <?= $dossier->porteur->entreprise->ninea ?> </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-4 col-6">Immatriculation</div>
                                                            <div class="col-md-8 col-6">: <?= $dossier->porteur->entreprise->immatriculation ?> </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-4 col-6">Type</div>
                                                            <div class="col-md-8 col-6">: <?= $dossier->porteur->entreprise->type_entreprise->nom ?> </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-4 col-6">Statut juridique</div>
                                                            <div class="col-md-8 col-6">
                                                                : <?php if($dossier->porteur->entreprise->forme)
                                                                            echo "<span class='badge badge-success fa fa-clipboard-check no-text-transform'> Formel</span>";
                                                                        else echo "<span class='badge badge-warning fa fa-calendar-times no-text-transform'> Informel</span>";
                                                                ?> 
                                                            </div>
                                                        </div>
                                                        <div class="max-hr mb-4"></div>
                                                        
                                                    <?php else: ?>
                                                        <h5 class="font-weight-bold">
                                                            Informations personnelles
                                                        </h5>
                                                        <div class="row">
                                                            <div class="col-md-4 col-6">Nom</div>
                                                            <div class="col-md-8 col-6">: <?= $dossier->porteur->nom ?> </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-4 col-6">Prénom</div>
                                                            <div class="col-md-8 col-6">: <?= $dossier->porteur->prenom ?> </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-4 col-6">Genre</div>
                                                            <div class="col-md-8 col-6">: <?= $dossier->porteur->genre ?> </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-4 col-6">Naissance</div>
                                                            <div class="col-md-8 col-6">: <?= $dossier->porteur->date_naissance ?> <i class="fa fa-calendar"></i></div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-4 col-6">Lieu naissance</div>
                                                            <div class="col-md-8 col-6">: <?= $dossier->porteur->lieu_naissance ?></div>
                                                        </div>
                                                        <div class="max-hr mb-4"></div>

                                                    <?php endif; ?>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-6 mb-5">
                                            <div class="card card-grey">
                                                <div class="card-body">
                                                    <?php if($checkEntreprise): ?>
                                                        <h5 class="font-weight-bold">Adresse <?php if(!$checkAssociation) echo "de l'entreprise"; else echo "de l'association"; ?></h5>
                                                        <div class="row">
                                                            <div class="col-md-4 col-6">Région</div>
                                                            <div class="col-md-8 col-6">: <?= $dossier->porteur->entreprise->commune->departement->region->nom ?> </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-4 col-6">Département</div>
                                                            <div class="col-md-8 col-6">: <?= $dossier->porteur->entreprise->commune->departement->departementscol ?> </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-4 col-6">Commune</div>
                                                            <div class="col-md-8 col-6">: <?= $dossier->porteur->entreprise->commune->nom ?> </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-4 col-6">Téléphone</div>
                                                            <div class="col-md-8 col-6">: <?= $dossier->porteur->entreprise->tel_entite ?> </div>
                                                        </div>
                                                        <div class="max-hr mb-5"></div>
                                                    <?php else: ?>
                                                        <h5 class="font-weight-bold">Autres</h5>
                                                        <div class="row">
                                                            <div class="col-md-4 col-6">Résidence</div>
                                                            <div class="col-md-8 col-6">: <?= $dossier->porteur->lieu_residence ?> </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-4 col-6">Identification</div>
                                                            <div class="col-md-8 col-6">: <?= $dossier->porteur->type_identification ?> </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-4 col-6">Numéro</div>
                                                            <div class="col-md-8 col-6">: <?= $dossier->porteur->num_identification ?> <i class="fa fa-id-card-alt"></i></div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-4 col-6">Téléphone</div>
                                                            <div class="col-md-8 col-6">: <?= $dossier->porteur->telephone ?> </div>
                                                        </div>
                                                    <?php endif; ?>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>

                                    <label class="font-weight-bold">Commentaire</label>
                                    <textarea id="editor_etat_civil" name="desc_etat_civil" class="form-control" rows="2" cols="30" <?php if($qualificationDossier) echo 'disabled'; ?>><?= $qualificationDossier ? $qualificationDossier->desc_etat_civil : '' ?></textarea>

                                </div>

                            </div>

                            <!--
                            <label class="fieldlabels">Email: *</label> 
                            <input type="email" name="email" placeholder="Email Id" /> 
                            
                            <label class="fieldlabels">Username: *</label> 
                            <input type="text" name="uname" placeholder="UserName" />

                            <label class="fieldlabels">Password: *</label> 
                            <input type="password" name="pwd" placeholder="Password" /> 
                            
                            <label class="fieldlabels">Confirm Password: *</label> 
                            <input type="password" name="cpwd" placeholder="Confirm Password" />
                            -->

                        </div> 
                        <input type="button" name="next" class="next action-button" value="Suivant" />
                    </fieldset>
                    <fieldset>
                        <div class="form-card">
                            <div class="row">
                                <div class="col-7">
                                    <h2 class="fs-title">Dossier activité :</h2>
                                </div>
                                <div class="col-5">
                                    <h2 class="steps">Step 2 - 4</h2>
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="col-md-6 mb-5">
                                    <div class="card card-grey">
                                        <div class="card-body">
                                            <h5 class="font-weight-bold">Informations sur le dossier</h5>
                                            <div class="row">
                                                <div class="col-md-4 col-6">Code</div>
                                                <div class="col-md-8">: <?= $dossier->numero ?> </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4 col-6">Intitulé</div>
                                                <div class="col-md-8 col-6">: <?= $dossier->intitule ?> </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4 col-6">Création</div>
                                                <div class="col-md-8 col-6">: <?= h($dossier->created->format('d/m/Y à H:i')) ?> </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4 col-6">Secteur</div>
                                                <div class="col-md-8 col-6">: <?= $dossier->filiere->nom ?> </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4 col-6">Statut</div>
                                                <div class="col-md-8 col-6">
                                                    <?php $badge = ""; $statut = ""; $icone = "";
                                                        if($dossier->soumis == "non"): $badge = "badge-warning"; $statut = "Non soumis"; $icone = "fa fa-bong";
                                                        else:
                                                            if(($dossier->statut_dossiers_id == 1) || ($dossier->soumis == "oui" && $dossier->statut_dossiers_id == null)): $statut = "En cours"; $badge = "badge-info"; $icone = "fa fa-business-time";
                                                            elseif($dossier->statut_dossiers_id == 2): $satut = "qualifié"; $badge = "badge-primary"; $icone = "fa fa-user-check";
                                                            elseif($dossier->statut_dossiers_id == 3): $statut = "En cours d'analyse"; $badge = "badge-green-light"; $icone ="fa fa-chalkboard-teacher";
                                                            elseif($dossier->statut_dossiers_id == 4): $statut = "labélisé"; $badge = "badge-success"; $icone = "fa fa-check-circle";
                                                            elseif($dossier->statut_dossiers_id == 5): $statut = "refusé"; $badge = "badge-danger"; $icone = "fa fa-calendar-times";

                                                        endif; endif;
                                                    
                                                    ?>
                                                    :
                                                    <?php if($statut != null): ?>
                                                        <span class="no-text-transform badge <?= $badge ?> <?= $icone ?>">
                                                            <?= $statut ?>
                                                        </span>
                                                    <?php endif; ?>
                                                    <?php
                                                        if($dossier->statut_financements_id != null) {
                                                            $libelleStatut = '';
                                                            if($dossier->statut_financements_id != 7):
                                                                if($dossier->statut_financement_id == 1) $libelleStatut = "Financement soumis";
                                                                elseif($dossier->statut_financements_id > 1 && $dossier->statut_fincements_id <= 5) $libelleStatut = "Financement en cours";
                                                                elseif($dossier->statut_financements_id == 6) $libelleStatut = "Financement accepté";
                                                                echo "<span class='badge badge-success'>".$libelleStatut."</span>";
                                                            else:
                                                                echo "<span class='badge badge-danger'>Financement rejeté</span>";
                                                            endif;

                                                        }
                                                    ?>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="max-hr mb-4"></div>
                                    </div>
                                </div>

                                <div class="col-md-6 mb-5">
                                    <div class="card card-grey">
                                        <div class="card-body">
                                            <h5 class="font-weight-bold">Lieu d'exécution</h5>
                                            <div class="row">
                                                <div class="col-md-4 col-6">Région</div>
                                                <div class="col-md-8 col-6">: <?= $dossier->commune->departement->region->nom ?> </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4 col-6">Département</div>
                                                <div class="col-md-8 col-6">: <?= $dossier->commune->departement->departementscol ?> </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4 col-6">Commune</div>
                                                <div class="col-md-8 col-6">: <?= $dossier->commune->nom ?> </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4 col-6">Impactés</div>
                                                <div class="col-md-8 col-6">: <?= $dossier->nb_perso_impact ?> <i class="fa fa-user"></i></div>
                                            </div>
                                        </div>

                                        <div class="max-hr mb-5"></div>
                                    </div>
                                </div>

                            </div>

                            
                            <?php if(count($pieces) > 0): ?>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="card card-grey">
                                        <div class="card-body">
                                            <h5 class="font-weight-bold">Pièces jointes</h5>
                                            <div class="row">
                                                <div class="col-md-6 col-6 font-weight-bold">Titre</div>
                                                <div class="col-md-6 col-6 font-weight-bold">Action</div>
                                            </div>
                                            <div class="max-hr-fonce"></div>
                                            <?php foreach($pieces as $piece): ?>
                                                <div class="row">
                                                    <div class="col-md-6 col-6"><?= $piece->titre ?></div>
                                                    <div class="col-md-6 col-6">
                                                        <?= $this->Html->link(__(' Voir'),'/documents/dossier/'.$piece->url,
                                                                array('target'=>'_blank','escape'=>false, 'class'=>'fa fa-eye btn btn-success btn-sm')) ?>
                                                    </div>
                                                </div>
                                            <?php endforeach; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php endif; ?>

                            <div class="max-hr mb-5"></div>

                            <label class="font-weight-bold">Description du projet</label>
                            <textarea class="form-control" id="editor2" name="" rows="2" cols="30" placeholder="Description du projet" disabled><?= $dossier->descrip_projet ?></textarea>

                            <div class="max-hr mb-5"></div>

                            <label class="font-weight-bold">Commentaire</label>
                            <textarea name="desc_dossier" class="form-control mb-5" id="editor_dossier" name="" rows="2" cols="30" placeholder="Saisir un commentaire" <?php if($qualificationDossier) echo 'disabled'; ?>><?= $qualificationDossier ? $qualificationDossier->desc_dossier : '' ?></textarea>

                            <div class="max-hr mb-5"></div>
                            
                        </div> 
                        <input type="button" name="next" class="next action-button" value="Suivant" /> 
                        <input type="button" name="previous" class="previous action-button-previous" value="Précédent" />
                    </fieldset>
                    <fieldset>
                        <div class="form-card">
                            <div class="row">
                                <div class="col-7">
                                    <h2 class="fs-title">Impacts :</h2>
                                </div>
                                <div class="col-5">
                                    <h2 class="steps">Step 3 - 4</h2>
                                </div>
                            </div> 

                            
                            <?php if(count($dossiersImpacts) > 0): ?>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="card card-grey">
                                        <div class="card-body">
                                            <h5 class="font-weight-bold">Types d'impacts</h5>
                                            <div class="row">
                                                <div class="col-md-6 col-6 font-weight-bold">Type d'impact</div>
                                                <div class="col-md-6 col-6 font-weight-bold">Impacte</div>
                                            </div>
                                            <div class="max-hr-fonce"></div>
                                            <?php foreach($dossiersImpacts as $dossierImpact): ?>
                                                <div class="row">
                                                    <div class="col-md-6 col-6"><?= $dossierImpact->types_impact->libelle ?></div>
                                                    <div class="col-md-6 col-6"><?= $dossierImpact->types_impact->impacte->libelle ?></div>
                                                </div>
                                            <?php endforeach; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="max-hr mb-5"></div>
                            
                            <?php endif; ?>

                            <?php if(count($dossiersFilieres) > 0): ?>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="card card-grey">
                                        <div class="card-body">
                                            <h5 class="font-weight-bold">Secteurs impactés</h5>
                                            <div class="row">
                                                <div class="col-md-6 col-6 font-weight-bold">Filière</div>
                                                <div class="col-md-6 col-6 font-weight-bold">Type</div>
                                            </div>
                                            <div class="max-hr-fonce"></div>
                                            <?php foreach($dossiersFilieres as $dossierFiliere): ?>
                                                <div class="row">
                                                    <div class="col-md-6 col-6"><?= $dossierFiliere->filiere->nom ?></div>
                                                    <div class="col-md-6 col-6"><?= $dossierFiliere->link_type ?></div>
                                                </div>
                                            <?php endforeach; ?>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>

                            <div class="max-hr mb-5"></div>

                            <?php endif; ?>
                            
                            <label class="font-weight-bold">Commentaire</label>
                            <textarea name="desc_impact" class="form-control mb-5" id="editor_impact" rows="2" cols="30" placeholder="Saisir un commentaire" <?php if($qualificationDossier) echo 'disabled'; ?>><?= $qualificationDossier ? $qualificationDossier->desc_impact : '' ?></textarea>


                        </div> 
                        <input type="button" name="next" class="next action-button" value="Suivant" /> 
                        <input type="button" name="previous" class="previous action-button-previous" value="Précédent" />
                    </fieldset>
                    <fieldset>
                        <div class="form-card">
                            <div class="row">
                                <div class="col-7">
                                    <h2 class="fs-title">Qualification :</h2>
                                </div>
                                <div class="col-5">
                                    <h2 class="steps">Step 4 - 4</h2>
                                </div>
                            </div> <br><br> 
                            <?php if(!$qualificationDossier): ?>
                                <h2 class="purple-text text-center"><strong>Cliquez sur valider pour qualifier le dossier</strong></h2> <br>
                                <div class="row">
                                    <div class="col-12 text-center"> 
                                        <button type="button" id="btn-modal-rejet" class="btn btn-danger btn-lg font-weight-bold" data-toggle="modal" data-target="#modalRejet">
                                            <i class="fa fa-times"></i> Rejeter
                                        </button>
                                        <button type="button" id="btn-valide" class="btn btn-success btn-lg font-weight-bold">
                                            <i class="fa fa-check"></i> Valider
                                        </button>
                                    </div>
                                </div> <br><br>
                            <?php else: ?>
                                <h2 class="purple-text text-center">
                                    <strong>
                                        Statut du dossier : 
                                        <?php if($qualificationDossier->raison_rejet == ""): ?> <i class="badge badge-success">Avis favorable pour analyse</i><?php endif; ?>
                                        <?php if($qualificationDossier->raison_rejet != ""): ?> <i class="badge badge-danger">Avis non favorable pour analyse</i><?php endif; ?>
                                    </strong>
                                </h2>
                            <?php endif; ?>
                            <div class="row justify-content-center">
                                <div class="col-7 text-center"></div>
                            </div>
                        </div>
                    </fieldset>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalRejet" tabindex="1" role="dialog" aria-labelledby="rejetModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <?= $this->Form->create('Agents', array('enctype'=>'multipart/form-data', 'url'=>array('controller' => 'QualificationDossiers', 'action'=> 'add'))); ?>
                <div class="modal-header">
                    <h5 class="modal-title" id="addModalLabel">Rejeter le dossier</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                
                <div class="modal-body">
                    <input type="hidden" name="dossiers_id" value="<?= $dossier->id ?>">
                    <input type="hidden" name="result" value="rejet">
                    <label class="font-weight-bold">Saisir le motif du rejet</label>
                    <textarea name="raison_rejet" class="form-control mb-5" id="editor_rejet" rows="2" cols="30" placeholder="Saisir un commentaire"></textarea>
                    <textarea name="desc_etat_civil" id="etat_civil" class="hide"></textarea>
                    <textarea name="desc_dossier" id="desc_dossier" class="hide"></textarea>
                    <textarea name="desc_impact" id="desc_impact" class="hide"></textarea>
                    <input type="hidden" name="codeAssignation" value="<?= $codeAssignation ?>">
                    <input type="hidden" name="agents_id" value="<?= $userConnect->agents[0]->id ?>">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-info" data-dismiss="modal">Annuler</button>
                    <button type="submit" class="btn btn-danger">Confirmer</button>
                </div>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div> 

<!-- JQuery -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<script>
    $(document).ready(function(){

        CKEDITOR.replace('editor_etat_civil');
        CKEDITOR.replace('editor2');
        CKEDITOR.replace('editor_dossier');
        CKEDITOR.replace('editor_impact');
        CKEDITOR.replace('editor_rejet');

        $('#btn-modal-rejet').click(function(){
            var value1 = CKEDITOR.instances['editor_etat_civil'].getData();
            $('#etat_civil').val(value1);

            var value2 = CKEDITOR.instances['editor_dossier'].getData();
            $('#desc_dossier').val(value2);

            var value3 = CKEDITOR.instances['editor_impact'].getData();
            $('#desc_impact').val(value3);
        });

        $('#btn-valide').click(function(){
            if (confirm("Voulez-vous vraiment valider ce dossier ?")) {
                $('#msform').submit();
            } else {
                return false;
            }
        });
            

        var current_fs, next_fs, previous_fs; //fieldsets
        var opacity;
        var current = 1;
        var steps = $("fieldset").length;

        setProgressBar(current);

        $(".next").click(function(){

        current_fs = $(this).parent();
        next_fs = $(this).parent().next();

        //Add Class Active
        $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

        //show the next fieldset
        next_fs.show();
        //hide the current fieldset with style
        current_fs.animate({opacity: 0}, {
        step: function(now) {
        // for making fielset appear animation
        opacity = 1 - now;

        current_fs.css({
        'display': 'none',
        'position': 'relative'
        });
        next_fs.css({'opacity': opacity});
        },
        duration: 500
        });
        setProgressBar(++current);
        });

        $(".previous").click(function(){

        current_fs = $(this).parent();
        previous_fs = $(this).parent().prev();

        //Remove class active
        $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");

        //show the previous fieldset
        previous_fs.show();

        //hide the current fieldset with style
        current_fs.animate({opacity: 0}, {
        step: function(now) {
        // for making fielset appear animation
        opacity = 1 - now;

        current_fs.css({
        'display': 'none',
        'position': 'relative'
        });
        previous_fs.css({'opacity': opacity});
        },
        duration: 500
        });
        setProgressBar(--current);
        });

        function setProgressBar(curStep){
        var percent = parseFloat(100 / steps) * curStep;
        percent = percent.toFixed();
        $(".progress-bar")
        .css("width",percent+"%")
        }

        $(".submit").click(function(){
        return false;
        })

    });
</script>