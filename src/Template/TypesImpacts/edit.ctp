<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\TypesImpact $typesImpact
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $typesImpact->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $typesImpact->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Types Impacts'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="typesImpacts form large-9 medium-8 columns content">
    <?= $this->Form->create($typesImpact) ?>
    <fieldset>
        <legend><?= __('Edit Types Impact') ?></legend>
        <?php
            echo $this->Form->control('libelle');
            echo $this->Form->control('impactes_id');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
