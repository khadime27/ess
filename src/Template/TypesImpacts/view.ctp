<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\TypesImpact $typesImpact
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Types Impact'), ['action' => 'edit', $typesImpact->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Types Impact'), ['action' => 'delete', $typesImpact->id], ['confirm' => __('Are you sure you want to delete # {0}?', $typesImpact->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Types Impacts'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Types Impact'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="typesImpacts view large-9 medium-8 columns content">
    <h3><?= h($typesImpact->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Libelle') ?></th>
            <td><?= h($typesImpact->libelle) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($typesImpact->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Impactes Id') ?></th>
            <td><?= $this->Number->format($typesImpact->impactes_id) ?></td>
        </tr>
    </table>
</div>
