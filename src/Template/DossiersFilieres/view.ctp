<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\DossiersFiliere $dossiersFiliere
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Dossiers Filiere'), ['action' => 'edit', $dossiersFiliere->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Dossiers Filiere'), ['action' => 'delete', $dossiersFiliere->id], ['confirm' => __('Are you sure you want to delete # {0}?', $dossiersFiliere->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Dossiers Filieres'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Dossiers Filiere'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Dossiers'), ['controller' => 'Dossiers', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Dossier'), ['controller' => 'Dossiers', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Filieres'), ['controller' => 'Filieres', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Filiere'), ['controller' => 'Filieres', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="dossiersFilieres view large-9 medium-8 columns content">
    <h3><?= h($dossiersFiliere->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Dossier') ?></th>
            <td><?= $dossiersFiliere->has('dossier') ? $this->Html->link($dossiersFiliere->dossier->id, ['controller' => 'Dossiers', 'action' => 'view', $dossiersFiliere->dossier->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Filiere') ?></th>
            <td><?= $dossiersFiliere->has('filiere') ? $this->Html->link($dossiersFiliere->filiere->id, ['controller' => 'Filieres', 'action' => 'view', $dossiersFiliere->filiere->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Type') ?></th>
            <td><?= h($dossiersFiliere->type) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($dossiersFiliere->id) ?></td>
        </tr>
    </table>
</div>
