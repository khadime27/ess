<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\DossiersFiliere $dossiersFiliere
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Dossiers Filieres'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Dossiers'), ['controller' => 'Dossiers', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Dossier'), ['controller' => 'Dossiers', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Filieres'), ['controller' => 'Filieres', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Filiere'), ['controller' => 'Filieres', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="dossiersFilieres form large-9 medium-8 columns content">
    <?= $this->Form->create($dossiersFiliere) ?>
    <fieldset>
        <legend><?= __('Add Dossiers Filiere') ?></legend>
        <?php
            echo $this->Form->control('dossiers_id', ['options' => $dossiers, 'empty' => true]);
            echo $this->Form->control('filieres_id', ['options' => $filieres, 'empty' => true]);
            echo $this->Form->control('type');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
