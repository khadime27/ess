<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\DossiersFiliere[]|\Cake\Collection\CollectionInterface $dossiersFilieres
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Dossiers Filiere'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Dossiers'), ['controller' => 'Dossiers', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Dossier'), ['controller' => 'Dossiers', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Filieres'), ['controller' => 'Filieres', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Filiere'), ['controller' => 'Filieres', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="dossiersFilieres index large-9 medium-8 columns content">
    <h3><?= __('Dossiers Filieres') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('dossiers_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('filieres_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('type') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($dossiersFilieres as $dossiersFiliere): ?>
            <tr>
                <td><?= $this->Number->format($dossiersFiliere->id) ?></td>
                <td><?= $dossiersFiliere->has('dossier') ? $this->Html->link($dossiersFiliere->dossier->id, ['controller' => 'Dossiers', 'action' => 'view', $dossiersFiliere->dossier->id]) : '' ?></td>
                <td><?= $dossiersFiliere->has('filiere') ? $this->Html->link($dossiersFiliere->filiere->id, ['controller' => 'Filieres', 'action' => 'view', $dossiersFiliere->filiere->id]) : '' ?></td>
                <td><?= h($dossiersFiliere->type) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $dossiersFiliere->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $dossiersFiliere->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $dossiersFiliere->id], ['confirm' => __('Are you sure you want to delete # {0}?', $dossiersFiliere->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
