<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Assignation $assignation
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Assignation'), ['action' => 'edit', $assignation->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Assignation'), ['action' => 'delete', $assignation->id], ['confirm' => __('Are you sure you want to delete # {0}?', $assignation->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Assignations'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Assignation'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Ots'), ['controller' => 'Ots', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Ot'), ['controller' => 'Ots', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Agences'), ['controller' => 'Agences', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Agence'), ['controller' => 'Agences', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Structures'), ['controller' => 'Structures', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Structure'), ['controller' => 'Structures', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Agents'), ['controller' => 'Agents', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Agent'), ['controller' => 'Agents', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Dossiers'), ['controller' => 'Dossiers', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Dossier'), ['controller' => 'Dossiers', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="assignations view large-9 medium-8 columns content">
    <h3><?= h($assignation->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Ot') ?></th>
            <td><?= $assignation->has('ot') ? $this->Html->link($assignation->ot->id, ['controller' => 'Ots', 'action' => 'view', $assignation->ot->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Agence') ?></th>
            <td><?= $assignation->has('agence') ? $this->Html->link($assignation->agence->id, ['controller' => 'Agences', 'action' => 'view', $assignation->agence->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Structure') ?></th>
            <td><?= $assignation->has('structure') ? $this->Html->link($assignation->structure->id, ['controller' => 'Structures', 'action' => 'view', $assignation->structure->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Agent') ?></th>
            <td><?= $assignation->has('agent') ? $this->Html->link($assignation->agent->id, ['controller' => 'Agents', 'action' => 'view', $assignation->agent->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Dossier') ?></th>
            <td><?= $assignation->has('dossier') ? $this->Html->link($assignation->dossier->id, ['controller' => 'Dossiers', 'action' => 'view', $assignation->dossier->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($assignation->id) ?></td>
        </tr>
    </table>
</div>
