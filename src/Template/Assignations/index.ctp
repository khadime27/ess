<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Assignation[]|\Cake\Collection\CollectionInterface $assignations
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Assignation'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Ots'), ['controller' => 'Ots', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Ot'), ['controller' => 'Ots', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Agences'), ['controller' => 'Agences', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Agence'), ['controller' => 'Agences', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Structures'), ['controller' => 'Structures', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Structure'), ['controller' => 'Structures', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Agents'), ['controller' => 'Agents', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Agent'), ['controller' => 'Agents', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Dossiers'), ['controller' => 'Dossiers', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Dossier'), ['controller' => 'Dossiers', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="assignations index large-9 medium-8 columns content">
    <h3><?= __('Assignations') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('ot_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('agence_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('structure_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('agent_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('dossier_id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($assignations as $assignation): ?>
            <tr>
                <td><?= $this->Number->format($assignation->id) ?></td>
                <td><?= $assignation->has('ot') ? $this->Html->link($assignation->ot->id, ['controller' => 'Ots', 'action' => 'view', $assignation->ot->id]) : '' ?></td>
                <td><?= $assignation->has('agence') ? $this->Html->link($assignation->agence->id, ['controller' => 'Agences', 'action' => 'view', $assignation->agence->id]) : '' ?></td>
                <td><?= $assignation->has('structure') ? $this->Html->link($assignation->structure->id, ['controller' => 'Structures', 'action' => 'view', $assignation->structure->id]) : '' ?></td>
                <td><?= $assignation->has('agent') ? $this->Html->link($assignation->agent->id, ['controller' => 'Agents', 'action' => 'view', $assignation->agent->id]) : '' ?></td>
                <td><?= $assignation->has('dossier') ? $this->Html->link($assignation->dossier->id, ['controller' => 'Dossiers', 'action' => 'view', $assignation->dossier->id]) : '' ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $assignation->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $assignation->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $assignation->id], ['confirm' => __('Are you sure you want to delete # {0}?', $assignation->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
