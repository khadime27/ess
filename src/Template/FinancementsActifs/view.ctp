<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\FinancementsActif $financementsActif
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Financements Actif'), ['action' => 'edit', $financementsActif->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Financements Actif'), ['action' => 'delete', $financementsActif->id], ['confirm' => __('Are you sure you want to delete # {0}?', $financementsActif->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Financements Actifs'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Financements Actif'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Dossiers'), ['controller' => 'Dossiers', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Dossier'), ['controller' => 'Dossiers', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="financementsActifs view large-9 medium-8 columns content">
    <h3><?= h($financementsActif->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Libelle') ?></th>
            <td><?= h($financementsActif->libelle) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Dossier') ?></th>
            <td><?= $financementsActif->has('dossier') ? $this->Html->link($financementsActif->dossier->id, ['controller' => 'Dossiers', 'action' => 'view', $financementsActif->dossier->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($financementsActif->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Annee Moins Trois') ?></th>
            <td><?= $this->Number->format($financementsActif->annee_moins_trois) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Annee Moins Deux') ?></th>
            <td><?= $this->Number->format($financementsActif->annee_moins_deux) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Annee Moins Un') ?></th>
            <td><?= $this->Number->format($financementsActif->annee_moins_un) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Annee') ?></th>
            <td><?= $this->Number->format($financementsActif->annee) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Prevision') ?></th>
            <td><?= $this->Number->format($financementsActif->prevision) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Variation') ?></th>
            <td><?= $this->Number->format($financementsActif->variation) ?></td>
        </tr>
    </table>
</div>
