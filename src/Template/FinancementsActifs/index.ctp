<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\FinancementsActif[]|\Cake\Collection\CollectionInterface $financementsActifs
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Financements Actif'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Dossiers'), ['controller' => 'Dossiers', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Dossier'), ['controller' => 'Dossiers', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="financementsActifs index large-9 medium-8 columns content">
    <h3><?= __('Financements Actifs') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('libelle') ?></th>
                <th scope="col"><?= $this->Paginator->sort('annee_moins_trois') ?></th>
                <th scope="col"><?= $this->Paginator->sort('annee_moins_deux') ?></th>
                <th scope="col"><?= $this->Paginator->sort('annee_moins_un') ?></th>
                <th scope="col"><?= $this->Paginator->sort('annee') ?></th>
                <th scope="col"><?= $this->Paginator->sort('prevision') ?></th>
                <th scope="col"><?= $this->Paginator->sort('variation') ?></th>
                <th scope="col"><?= $this->Paginator->sort('dossiers_id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($financementsActifs as $financementsActif): ?>
            <tr>
                <td><?= $this->Number->format($financementsActif->id) ?></td>
                <td><?= h($financementsActif->libelle) ?></td>
                <td><?= $this->Number->format($financementsActif->annee_moins_trois) ?></td>
                <td><?= $this->Number->format($financementsActif->annee_moins_deux) ?></td>
                <td><?= $this->Number->format($financementsActif->annee_moins_un) ?></td>
                <td><?= $this->Number->format($financementsActif->annee) ?></td>
                <td><?= $this->Number->format($financementsActif->prevision) ?></td>
                <td><?= $this->Number->format($financementsActif->variation) ?></td>
                <td><?= $financementsActif->has('dossier') ? $this->Html->link($financementsActif->dossier->id, ['controller' => 'Dossiers', 'action' => 'view', $financementsActif->dossier->id]) : '' ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $financementsActif->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $financementsActif->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $financementsActif->id], ['confirm' => __('Are you sure you want to delete # {0}?', $financementsActif->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
