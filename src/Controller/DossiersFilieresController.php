<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * DossiersFilieres Controller
 *
 * @property \App\Model\Table\DossiersFilieresTable $DossiersFilieres
 *
 * @method \App\Model\Entity\DossiersFiliere[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class DossiersFilieresController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Dossiers', 'Filieres']
        ];
        $dossiersFilieres = $this->paginate($this->DossiersFilieres);

        $this->set(compact('dossiersFilieres'));
    }

    /**
     * View method
     *
     * @param string|null $id Dossiers Filiere id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $dossiersFiliere = $this->DossiersFilieres->get($id, [
            'contain' => ['Dossiers', 'Filieres']
        ]);

        $this->set('dossiersFiliere', $dossiersFiliere);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $dossiersFiliere = $this->DossiersFilieres->newEntity();
        if ($this->request->is('post')) {
            $dossiersFiliere = $this->DossiersFilieres->patchEntity($dossiersFiliere, $this->request->getData());
            if ($this->DossiersFilieres->save($dossiersFiliere)) {
                $this->Flash->success(__('The dossiers filiere has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The dossiers filiere could not be saved. Please, try again.'));
        }
        $dossiers = $this->DossiersFilieres->Dossiers->find('list', ['limit' => 200]);
        $filieres = $this->DossiersFilieres->Filieres->find('list', ['limit' => 200]);
        $this->set(compact('dossiersFiliere', 'dossiers', 'filieres'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Dossiers Filiere id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $dossiersFiliere = $this->DossiersFilieres->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $dossiersFiliere = $this->DossiersFilieres->patchEntity($dossiersFiliere, $this->request->getData());
            if ($this->DossiersFilieres->save($dossiersFiliere)) {
                $this->Flash->success(__('The dossiers filiere has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The dossiers filiere could not be saved. Please, try again.'));
        }
        $dossiers = $this->DossiersFilieres->Dossiers->find('list', ['limit' => 200]);
        $filieres = $this->DossiersFilieres->Filieres->find('list', ['limit' => 200]);
        $this->set(compact('dossiersFiliere', 'dossiers', 'filieres'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Dossiers Filiere id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $dossiersFiliere = $this->DossiersFilieres->get($id);
        if ($this->DossiersFilieres->delete($dossiersFiliere)) {
            $this->Flash->success(__('The dossiers filiere has been deleted.'));
        } else {
            $this->Flash->error(__('The dossiers filiere could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
