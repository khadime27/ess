<?php
namespace App\Controller\Superviseur;

use App\Controller\AppController;

/**
 * Dashbord Controller
 *
 *
 * @method \App\Model\Entity\Dashbord[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class DashbordController extends AppController
{

    public function index()
    {
        $this->demandesLabelises();
        $this->labelisationsEncours();
        $this->labelisationsAcceptes();
        $this->labelisationsRefuses();
        
        $this->loadModel('Dossiers');
        $allDossiers = $this->Dossiers->find('all', [
            'contain' => ['StatutDossiers'],
            'conditions' => ['Dossiers.statut_dossiers_id IS NOT' => null]
        ]);
        
        $statutDossiers = ["En cours", "Accepté", "Rejeté"];

        $dossiers = [];
        foreach($allDossiers as $dossier) {
            if($dossier->created->format('m') == date('m')) $dossiers[] = $dossier;
        }

        $this->getMois();

        $this->set(compact('allDossiers', 'dossiers', 'statutDossiers'));
    }

    public function getMois() {
        $mois = array(
            array("num" => "01", "nom" => "JANVIER"),
            array("num" => "02", "nom" => "FEVRIER"),
            array("num" => "03", "nom" => "MARS"),
            array("num" => "04", "nom" => "AVRIL"),
            array("num" => "05", "nom" => "MAI"),
            array("num" => "06", "nom" => "JUIN"),
            array("num" => "07", "nom" => "JUILLET"),
            array("num" => "08", "nom" => "AOUT"),
            array("num" => "09", "nom" => "SEPTEMBRE"),
            array("num" => "10", "nom" => "OCTOBRE"),
            array("num" => "11", "nom" => "NOVEMBRE"),
            array("num" => "12", "nom" => "DECEMBRE"),
        );

        $this->set(compact('mois'));
    }

    public function demandesLabelises() {
        $this->loadModel('Dossiers');
        $dossiers = $this->Dossiers->find('All', [
            'contain' => ['NatureDemandes', 'Porteurs', 'Filieres', 'Communes', 'Agents'],
            'conditions' => [
                'OR' => [
                    ['Dossiers.statut_dossiers_id' => 6, 'Dossiers.demande_label' => true, 'Dossiers.emission IS' => null],
                    ['Dossiers.statut_dossiers_id' => 6, 'Dossiers.demande_label' => true, 'Dossiers.emission' => false]
                ]
            ]
        ])->toArray();

        $nbNoTraite = count($dossiers);

        $this->set(compact('nbNoTraite'));
    }

    public function labelisationsEncours() {
        $this->loadModel('Dossiers');
        $dossiers = $this->Dossiers->find('All', [
            'contain' => ['NatureDemandes', 'Porteurs', 'Filieres', 'Communes', 'Agents'],
            'conditions' => ['Dossiers.statut_dossiers_id <= ' => 4, 'Dossiers.demande_label' => true]
        ])->toArray();

        $nbEncours = count($dossiers);

        $this->set(compact('nbEncours'));
    }

    public function labelisationsAcceptes() {
        $this->loadModel('Dossiers');
        $dossiers = $this->Dossiers->find('All', [
            'contain' => ['NatureDemandes', 'Porteurs', 'Filieres', 'Communes', 'Agents'],
            'conditions' => ['Dossiers.statut_dossiers_id' => 6, 'Dossiers.demande_label' => true, 'Dossiers.emission' => true]
        ])->toArray();

        $nbAccepte = count($dossiers);

        $this->set(compact('nbAccepte'));
    }

    public function labelisationsRefuses() {
        $this->loadModel('Dossiers');
        $dossiers = $this->Dossiers->find('All', [
            'contain' => ['NatureDemandes', 'Porteurs', 'Filieres', 'Communes', 'Agents'],
            'conditions' => ['Dossiers.statut_dossiers_id' => 5, 'Dossiers.demande_label' => true]
        ])->toArray();

        $nbRefuse = count($dossiers);

        $this->set(compact('nbRefuse'));
    }

}
