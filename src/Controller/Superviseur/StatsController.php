<?php
namespace App\Controller\Superviseur;

use App\Controller\AppController;


class StatsController extends AppController
{
    public function labelisation() {

        $this->getAllDossiers();

        $dossiersEncours = $this->getDossiersEncours();
        $dossiersAcceptes = $this->stats(6);
        $dossiersRejetes = $this->stats(5);

        $this->getMois();

        $this->loadModel('Filieres');
        $filieres = $this->Filieres->find('all');

        $this->loadModel('TypeEntreprises');
        $typesEntreprises = $this->TypeEntreprises->find('all');

        $this->loadModel('Regions');
        $regions = $this->Regions->find('all');

        $this->getElementStats();

        $this->loadModel('Dossiers');
        $dossiersEmis = $this->Dossiers->find('all', [
            'contain' => ['Porteurs.Entreprises.TypeEntreprises', 'Communes.Departements.Regions', 'Filieres'],
            'conditions' => ['Dossiers.emission' => true]
        ])->toArray();

        $genres = [0 => 'homme', 1 => 'femme'];

        $this->set(compact('dossiersEncours', 'dossiersEmis', 'genres', 'dossiersAcceptes', 'regions', 'dossiersRejetes', 'filieres', 'typesEntreprises'));
    }

    public function getElementStats() {
        $dossiersLabelises = $this->Dossiers->find('all', [
            'contain' => ['Porteurs.Entreprises'],
            'conditions' => ['Dossiers.emission' => true]
        ])->toArray();
        $nbPersoImpact = 0; $nbEmplois = 0; $nbSaisonnier = 0;
        foreach($dossiersLabelises as $dossier) {
            $nbPersoImpact += $dossier->nb_perso_impact;
            if($dossier->porteur->entreprise) {
                $nbEmplois += $dossier->porteur->entreprise->nb_employe;
                $nbSaisonnier += $dossier->porteur->entreprise->nb_saisonnier;
            }
            
        }

        $this->set(compact('nbPersoImpact', 'nbEmplois', 'nbSaisonnier'));
    }

    public function getAllDossiers() {
        $this->loadModel('Dossiers');
        $allDossiers = $this->Dossiers->find('all', [
            'contain' => ['Porteurs.Entreprises.TypeEntreprises', 'Communes.Departements.Regions'],
            'conditions' => ['statut_dossiers_id >=' => 1]
        ])->toArray();

        $this->set(compact('allDossiers'));
    }


    public function stats($statutDossier = null) {
        $this->loadModel('Dossiers');
        
        $allDossiers = $this->Dossiers->find('all', [
            'conditions' => ['statut_dossiers_id' => $statutDossier]
        ])->toArray();

        // Les demandes de l'année en cours
        $dossiers = [];
        foreach($allDossiers as $dossier) {
            if($dossier->created->format('Y') == date('Y')) {
                $dossiers[] = $dossier;
            }
        }

        return $dossiers;
    }

    public function getDossiersEncours() {
        $this->loadModel('Dossiers');
        
        $allDossiers = $this->Dossiers->find('all', [
            'conditions' => ['statut_dossiers_id <=' => 4]
        ])->toArray();

        // Les demandes de l'année en cours
        $dossiers = [];
        foreach($allDossiers as $dossier) {
            if($dossier->statut_dossiers_id >= 1) {
                if($dossier->created->format('Y') == date('Y')) {
                    $dossiers[] = $dossier;
                }
            }
            
        }

        return $dossiers;
    }

    public function getMois() {
        $mois = array(
            array("num" => "01", "nom" => "JANVIER"),
            array("num" => "02", "nom" => "FEVRIER"),
            array("num" => "03", "nom" => "MARS"),
            array("num" => "04", "nom" => "AVRIL"),
            array("num" => "05", "nom" => "MAI"),
            array("num" => "06", "nom" => "JUIN"),
            array("num" => "07", "nom" => "JUILLET"),
            array("num" => "08", "nom" => "AOUT"),
            array("num" => "09", "nom" => "SEPTEMBRE"),
            array("num" => "10", "nom" => "OCTOBRE"),
            array("num" => "11", "nom" => "NOVEMBRE"),
            array("num" => "12", "nom" => "DECEMBRE"),
        );

        $this->set(compact('mois'));
    }

    public function voirDepartements($idRegion = null) {
        $this->loadModel('Departements');
        $departements = $this->Departements->find('all', [
            'conditions' => ['region_id' => $idRegion]
        ])->toArray();

        $dossiersEncours = $this->getDossiersByRegionEnCours($idRegion);
        $dossiersAcceptes = $this->getDossiersByRegion(6, $idRegion);
        $dossiersRejetes = $this->getDossiersByRegion(5, $idRegion);

        $this->loadModel('Regions');
        $region = $this->Regions->get($idRegion);

        $this->getMois();

        $this->loadModel('Filieres');
        $filieres = $this->Filieres->find('all');

        $this->loadModel('TypeEntreprises');
        $typesEntreprises = $this->TypeEntreprises->find('all');

        $this->loadModel('Regions');
        $regions = $this->Regions->find('all');

        $this->loadModel('Dossiers');
        $allDossiers = $this->Dossiers->find('all', [
            'contain' => ['Porteurs.Entreprises.TypeEntreprises', 'Communes.Departements.Regions'],
            'conditions' => ['Regions.id' => $idRegion, 'statut_dossiers_id >=' => 1]
        ])->toArray();

        $this->getElementStats();

        $dossiersEmis = $this->Dossiers->find('all', [
            'contain' => ['Porteurs.Entreprises.TypeEntreprises', 'Communes.Departements.Regions', 'Filieres'],
            'conditions' => ['Regions.id' => $idRegion, 'Dossiers.emission' => true]
        ])->toArray();

        $genres = [0 => 'homme', 1 => 'femme'];

        $this->set(compact('allDossiers', 'departements', 'dossiersEmis', 'genres', 'filieres',  'typesEntreprises', 'regions', 'dossiersEncours', 'dossiersAcceptes', 'dossiersRejetes', 'region'));
    }

    public function getDossiersByRegion($etat = null, $idRegion = null) {
        $this->loadModel('Dossiers');
        $dossiers = $this->Dossiers->find('all', [
            'contain' => ['Communes.Departements.Regions'],
            'conditions' => ['statut_dossiers_id' => $etat, 'Regions.id' => $idRegion]
        ])->toArray();

        return $dossiers;
    }

    public function getDossiersByRegionEnCours($idRegion = null) {
        $this->loadModel('Dossiers');
        $folders = $this->Dossiers->find('all', [
            'contain' => ['Communes.Departements.Regions'],
            'conditions' => ['Dossiers.statut_dossiers_id <=' => 4, 'Regions.id' => $idRegion]
        ])->toArray();

        $dossiers = [];
        foreach($folders as $folder) {
            if($folder->statut_dossiers_id >= 1) {
                $dossiers[] = $folder;
            }
        }

        return $dossiers;
    }

    public function voirCommunes($idDepartement = null) {
        $this->loadModel('Departements');
        $departements = $this->Departements->find('all', [
            'conditions' => ['id' => $idDepartement]
        ])->toArray();

        $dossiersEncours = $this->getDossiersByDepartement(1, $idDepartement);
        $dossiersAcceptes = $this->getDossiersByDepartement(6, $idDepartement);
        $dossiersRejetes = $this->getDossiersByDepartement(5, $idDepartement);

        $departement = $this->Departements->get($idDepartement);

        $this->getMois();

        $this->loadModel('Filieres');
        $filieres = $this->Filieres->find('all');

        $this->loadModel('TypeEntreprises');
        $typesEntreprises = $this->TypeEntreprises->find('all');

        $this->loadModel('Regions');
        $regions = $this->Regions->find('all');

        $this->loadModel('Dossiers');
        $allDossiers = $this->Dossiers->find('all', [
            'contain' => ['Porteurs.Entreprises.TypeEntreprises', 'Communes.Departements'],
            'conditions' => ['Departements.id' => $idDepartement, 'statut_dossiers_id >=' => 1]
        ])->toArray();

        $this->loadModel('Communes');
        $communes = $this->Communes->find('all', [
            'conditions' => ['departement_id' => $idDepartement]
        ]);

        $this->getElementStats();

        $dossiersEmis = $this->Dossiers->find('all', [
            'contain' => ['Porteurs.Entreprises.TypeEntreprises', 'Communes.Departements.Regions', 'Filieres'],
            'conditions' => ['Departements.id' => $idDepartement, 'Dossiers.emission' => true]
        ])->toArray();

        $genres = [0 => 'homme', 1 => 'femme'];

        $this->set(compact('allDossiers', 'departements', 'communes', 'dossiersEmis', 'genres', 'filieres',  'typesEntreprises', 'dossiersEncours', 'dossiersAcceptes', 'dossiersRejetes', 'departement'));
    }

    public function getDossiersByDepartement($etat = null, $idDepartement = null) {
        $this->loadModel('Dossiers');
        $dossiers = $this->Dossiers->find('all', [
            'contain' => ['Communes.Departements'],
            'conditions' => ['Dossiers.statut_dossiers_id' => $etat, 'Departements.id' => $idDepartement]
        ])->toArray();

        return $dossiers;
    }

    public function getDossiersByDepartementEnCours($idDepartement = null) {
        $this->loadModel('Dossiers');
        $folders = $this->Dossiers->find('all', [
            'contain' => ['Communes.Departements'],
            'conditions' => ['Dossiers.statut_dossiers_id <=' => 4, 'Departements.id' => $idDepartement]
        ])->toArray();

        $dossiers = [];
        foreach($folders as $folder) {
            if($folder->statut_dossiers_id >= 1) {
                $dossiers[] = $folder;
            }
        }
        return $dossiers;
    }

    public function getDetailStats() {
        $this->loadModel('Regions');
        $regions = $this->Regions->find('all');

        $this->loadModel('Filieres');
        $filieres = $this->Filieres->find('all');

        $this->loadModel('Dossiers');
        $dossiersLabelises = $this->Dossiers->find('all', [
            'contain' => ['Communes.Departements', 'Porteurs.Entreprises'],
            'conditions' => ['Dossiers.emission' => true]
        ])->toArray();

        $this->set(compact('regions', 'filieres', 'dossiersLabelises'));
    }

    public function detailsCibleImpact() {
        $this->getDetailStats();
    }

    public function detailsEmplois() {
        $this->getDetailStats();
    }


}