<?php
namespace App\Controller\Superviseur;

use App\Controller\AppController;

/**
 * Dashbord Controller
 *
 *
 * @method \App\Model\Entity\Dashbord[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class LabelisationsController extends AppController
{

    public function getDossiers($emission = null) {
        $dataUser = $this->getUser();

        $this->loadModel('Dossiers');

        if($emission == null) {
            $dossiers = $this->Dossiers->find('all', [
                'contain' => ['Communes.Departements.Regions', 'NatureDemandes', 'Porteurs', 'Filieres', 'Communes', 'Agents'],
                'conditions' => [
                    'OR' => [
                        ['statut_dossiers_id' => 6, 'demande_label' => true, 'emission IS' => $emission],
                        ['statut_dossiers_id' => 6, 'demande_label' => true, 'emission' => false]
                    ]
                ]
            ])->toArray();
        }else{ 
            $dossiers = $this->Dossiers->find('all', [
                'contain' => ['Communes.Departements.Regions', 'NatureDemandes', 'Porteurs', 'Filieres', 'Communes', 'Agents'],
                'conditions' => ['statut_dossiers_id' => 6, 'demande_label' => true, 'emission IS' => $emission]
            ])->toArray();
        }
        

        return $dossiers;
    
    }

    public function notraites($idRegion = null) {
        $this->loadModel('Dossiers');

        $dossiers = $this->Dossiers->find('all', [
            'contain' => ['Communes.Departements.Regions', 'NatureDemandes', 'Porteurs', 'Filieres', 'Communes', 'Agents'],
            'conditions' => [
                'OR' => [
                    ['Dossiers.statut_dossiers_id' => 6, 'Dossiers.emission IS' => null, 'Regions.id' => $idRegion],
                    ['Dossiers.statut_dossiers_id' => 6, 'Dossiers.emission' => false, 'Regions.id' => $idRegion]
                ]
            ]
        ])->toArray();
        $this->set(compact('dossiers'));
    }

    public function confirmes() {
        $dossiers = $this->getDossiers(true);
        $this->set(compact('dossiers'));
    }

    // Le dossier sera prêt pour une émission
    public function confirmEmission($idDossier = null) {
        $this->loadModel('Dossiers');
        $dossier = $this->Dossiers->get($idDossier);
        $dossier->emission = true;
        $this->Dossiers->save($dossier);

        $this->Flash->success(__("Enregistrement bien effectué !"));

        $this->redirect($this->referer());
    }

    // Fonction qui valide des dossiers sélectionnés
    public function sendEmission() {
        $ids = $this->request->getData('data');
        
        foreach($ids as $id){
            $this->loadModel('Dossiers');
            $dossier = $this->Dossiers->get($id);
            $dossier->emission = true;
            $this->Dossiers->save($dossier);
        }
      
        $this->redirect($this->referer());
        echo json_encode($ids); die();
    }

    public function regions($type = null) {
        $this->loadModel('Regions');
        $regions = $this->Regions->find('all');

        $dossiers = $this->getDossiers();

        $this->set(compact('regions', 'dossiers', 'type'));
    }

}
