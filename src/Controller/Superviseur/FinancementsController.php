<?php
namespace App\Controller\Superviseur;

use App\Controller\AppController;

/**
 * Dashbord Controller
 *
 *
 * @method \App\Model\Entity\Dashbord[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class FinancementsController extends AppController
{

    public function getDossiers($emission = null) {
        $dataUser = $this->getUser();

        $this->loadModel('Dossiers');

        $dossiers = $this->Dossiers->find('all', [
            'contain' => ['Communes.Departements.Regions', 'NatureDemandes', 'Porteurs', 'Filieres', 'Communes', 'Agents'],
            'conditions' => ['statut_financements_id' => 6, 'demande_label' => true, 'emission_finance' => $emission]
        ])->toArray();

        return $dossiers;
    
    }

    public function acceptes() {
        $dossiers = $this->getDossiers(false);
        $this->set(compact('dossiers'));
    }

    public function confirmes() {
        $dossiers = $this->getDossiers(true);
        $this->set(compact('dossiers'));
    }

    // Le dossier sera prêt pour une émission
    public function confirmEmission($idDossier = null) {
        $this->loadModel('Dossiers');
        $dossier = $this->Dossiers->get($idDossier);
        $dossier->emission_finance = true;
        $this->Dossiers->save($dossier);

        $this->Flash->success(__("Enregistrement bien effectué !"));

        $this->redirect($this->referer());
    }

    public function getElementsParams($idDossier = null) {
        $this->loadModel('FinancesEngagements');
        $financesEngagements = $this->FinancesEngagements->find('all', [
            'conditions' => ['dossiers_id' => $idDossier],
            'order' => ['id' => 'ASC']
        ])->toArray();

        $this->loadModel('FinancementsActifs');
        $financementsActifs = $this->FinancementsActifs->find('all', [
            'conditions' => ['dossiers_id' => $idDossier],
            'order' => ['id' => 'ASC']
        ])->toArray();

        $this->loadModel('FinancementsPassifs');
        $financementsPassifs = $this->FinancementsPassifs->find('all', [
            'conditions' => ['dossiers_id' => $idDossier],
            'order' => ['id' => 'ASC']
        ])->toArray();

        $this->loadModel('SituationsComptes');
        $situationsComptes = $this->SituationsComptes->find('all', [
            'conditions' => ['dossiers_id' => $idDossier],
            'order' => ['id' => 'ASC']
        ])->toArray();

        $this->set(compact('financesEngagements', 'financementsActifs', 'financementsPassifs', 'situationsComptes'));
    }

    public function details($idDossier = null) {
        $this->loadModel('Dossiers');
        $dossier = $this->Dossiers->get($idDossier);

        $this->getElementsParams($idDossier);

        $this->set(compact('dossier'));

    }

}
