<?php
namespace App\Controller\Superviseur;

use App\Controller\AppController;

/**
 * Dashbord Controller
 *
 *
 * @method \App\Model\Entity\Dashbord[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class DossiersController extends AppController
{
    public function view($id = null) {

        $dossier = $this->Dossiers->get($id, [
            'contain' => ['Porteurs', 'Filieres',
                'Communes.Departements.Regions','Porteurs.Entreprises.TypeEntreprises', 
                'Porteurs.Entreprises.Communes.Departements.Regions', 'StatutDossiers'
            ]
        ]);
        $checkEntreprise = false;
        if($dossier->porteur->entreprise) $checkEntreprise = true;

        $checkAssociation = false;
        if($dossier->porteur->entreprise && $dossier->porteur->entreprise->association) $checkAssociation = true;

        $this->loadModel('DossiersFilieres');
        $dossiersFilieres = $this->DossiersFilieres->find('all', [
            'contain' => ['Filieres'],
            'conditions' => ['dossiers_id' => $dossier->id]
        ])->toArray();

        $this->loadModel('DossiersImpacts');
        $dossiersImpacts = $this->DossiersImpacts->find('all', [
            'contain' => ['TypesImpacts.Impactes'],
            'conditions' => ['dossiers_id' => $dossier->id]
        ])->toArray();

        $this->loadModel('PieceJointes');
        $pieces = $this->PieceJointes->find('all', [
            'conditions' => ['dossier_id' => $dossier->id]
        ])->toArray();
        

        $this->set(compact('dossier', 'checkEntreprise', 'checkAssociation', 'dossiersFilieres', 'dossiersImpacts', 'pieces'));

    }
}