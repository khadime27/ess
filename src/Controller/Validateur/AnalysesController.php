<?php
namespace App\Controller\Validateur;

use App\Controller\AppController;


class AnalysesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {

    }

    public function encours($type = null) {
        $this->loadModel('Assignations');
        $userConnect = $this->getUser();
        $idAgent = $userConnect->agents[0]->id;
        
        $assignations = $this->Assignations->find('All', [
            'contain' => ['Dossiers', 'Dossiers.NatureDemandes', 'Dossiers.Porteurs', 'Dossiers.Filieres', 'Dossiers.Communes'],
            'conditions' => ['Assignations.agent_id' => $idAgent, 'Assignations.status_validations_id' => 1, 'Assignations.type_demande' => $type]
        ])->toArray();
        $dossiers = [];
        foreach($assignations as $assignation) {
            $dossiers[] = $assignation->dossier;
        }

        $this->set(compact('dossiers', 'type'));
    }

    public function edit($idDossier = null, $typeAnalyse = null, $detail = null, $idNotation = null) {
        $this->loadModel('Dossiers');

        $dossier = $this->Dossiers->get($idDossier, [
            'contain' => ['Porteurs', 'Filieres',
                'Communes.Departements.Regions','Porteurs.Entreprises.TypeEntreprises', 
                'Porteurs.Entreprises.Communes.Departements.Regions', 'StatutDossiers'
            ]
        ]);
        
        $checkEntreprise = false;
        if($dossier->porteur->entreprise) $checkEntreprise = true;

        $checkAssociation = false;
        if($dossier->porteur->entreprise && $dossier->porteur->entreprise->association) $checkAssociation = true;

        $this->loadModel('DossiersFilieres');
        $dossiersFilieres = $this->DossiersFilieres->find('all', [
            'contain' => ['Filieres'],
            'conditions' => ['dossiers_id' => $dossier->id]
        ])->toArray();

        $this->loadModel('DossiersImpacts');
        $dossiersImpacts = $this->DossiersImpacts->find('all', [
            'contain' => ['TypesImpacts.Impactes'],
            'conditions' => ['dossiers_id' => $dossier->id]
        ])->toArray();

        $this->loadModel('PieceJointes');
        $pieces = $this->PieceJointes->find('all', [
            'conditions' => ['dossier_id' => $dossier->id]
        ])->toArray();

        $this->loadModel('QualificationDossiers');
        $qualificationDossier = $this->QualificationDossiers->find('all', [
            'contain' => ['Dossiers', 'Agents.Agences'],
            'conditions' => ['QualificationDossiers.dossiers_id' => $dossier->id, 'QualificationDossiers.type_demande' => $typeAnalyse]
        ])->first();

        $dataUser = $this->getUser();
        $this->loadModel('Notations');
        $moyenne = null;
        $notation = null;
        if($detail) {
            $notation = $this->Notations->get($idNotation, [
                'contain' => ['Dossiers.Assignations', 'Agents.Agences']
            ]);
            $sommeNote = $notation->aspect_technique + $notation->aspect_financier + $notation->aspect_env;
            $moyenne = $sommeNote ."/". 100;
        }

        $this->loadModel('Assignations');
        $assignation = $this->Assignations->find('all', [
            'conditions' => ['agent_id' => $dataUser->agents[0]->id, 'dossier_id' => $dossier->id, 'type_demande' => $typeAnalyse]
        ])->first();

        $this->getElementsParams($idDossier);

        $this->set(compact('dossier', 'moyenne', 'notation', 'assignation', 'checkEntreprise', 'checkAssociation', 'qualificationDossier', 'dossiersFilieres', 'dossiersImpacts', 'pieces'));
    }

    

    public function acceptes($type = null) {
        $userConnect = $this->getUser();
        $idAgent = $userConnect->agents[0]->id;
        $this->loadModel('Notations');
        $notations = $this->Notations->find('all', [
            'contain' => ['Dossiers', 'Dossiers.NatureDemandes', 'Dossiers.Porteurs', 'Dossiers.Filieres', 'Dossiers.Communes'],
            'conditions' => ['Notations.agents_id' => $idAgent, 'Notations.raison_rejet IS ' => null, 'type_demande' => $type]
        ])->toArray();

        $dossiers = [];
        foreach($notations as $i => $notation) {
            $dossiers[] = $notation->dossier;
            $dossiers[$i]['notation_id'] = $notation->id;
        }
        

        $this->set(compact('dossiers', 'type'));
    }

    public function rejetes($type = null) {
        $userConnect = $this->getUser();
        $idAgent = $userConnect->agents[0]->id;
        $this->loadModel('Notations');

        $notations = $this->Notations->find('all', [
            'contain' => ['Dossiers', 'Dossiers.Assignations', 'Dossiers.NatureDemandes', 'Dossiers.Porteurs', 'Dossiers.Filieres', 'Dossiers.Communes'],
            'conditions' => ['Notations.agents_id' => $idAgent, 'Notations.raison_rejet IS NOT ' => null, 'type_demande' => $type]
        ])->toArray();
        
        $dossiers = [];
        foreach($notations as $i => $notation) {
            $dossiers[] = $notation->dossier;
            $dossiers[$i]['notation_id'] = $notation->id;
        }

        $this->set(compact('dossiers', 'type'));
    }


    public function getElementsParams($idDossier = null) {
        $this->loadModel('FinancesEngagements');
        $financesEngagements = $this->FinancesEngagements->find('all', [
            'conditions' => ['dossiers_id' => $idDossier],
            'order' => ['id' => 'ASC']
        ])->toArray();

        $this->loadModel('FinancementsActifs');
        $financementsActifs = $this->FinancementsActifs->find('all', [
            'conditions' => ['dossiers_id' => $idDossier],
            'order' => ['id' => 'ASC']
        ])->toArray();

        $this->loadModel('FinancementsPassifs');
        $financementsPassifs = $this->FinancementsPassifs->find('all', [
            'conditions' => ['dossiers_id' => $idDossier],
            'order' => ['id' => 'ASC']
        ])->toArray();

        $this->loadModel('SituationsComptes');
        $situationsComptes = $this->SituationsComptes->find('all', [
            'conditions' => ['dossiers_id' => $idDossier],
            'order' => ['id' => 'ASC']
        ])->toArray();

        $this->set(compact('financesEngagements', 'financementsActifs', 'financementsPassifs', 'situationsComptes'));
    }

}
