<?php
namespace App\Controller\Validateur;

use App\Controller\AppController;

/**
 * Dashbord Controller
 *
 *
 * @method \App\Model\Entity\Dashbord[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class DashbordController extends AppController
{

    public function index()
    {
        $this->demandesLabelises();
        $this->labelisationsEncours();
        $this->labelisationsAcceptes();
        $this->labelisationsRefuses();
    }

    public function demandesLabelises() {
        $this->loadModel('Assignations');
        $userConnect = $this->getUser();
        $idAgent = $userConnect->agents[0]->id;
        
        $assignations = $this->Assignations->find('All', [
            'contain' => ['Dossiers', 'Dossiers.NatureDemandes', 'Dossiers.Porteurs', 'Dossiers.Filieres', 'Dossiers.Communes'],
            'conditions' => ['Assignations.agent_id' => $idAgent, 'Assignations.status_validations_id' => 1, 'Assignations.type_demande' => 'label']
        ])->toArray();
        $dossiers = [];
        foreach($assignations as $assignation) {
            $dossiers[] = $assignation->dossier;
        }
        $nbNoTraite = count($dossiers);

        $this->set(compact('nbNoTraite'));
    }

    public function labelisationsEncours() {
        $this->loadModel('Assignations');
        $userConnect = $this->getUser();
        $idAgent = $userConnect->agents[0]->id;
        $assignations = $this->Assignations->find('All', [
            'contain' => ['Dossiers', 'Dossiers.NatureDemandes', 'Dossiers.Porteurs', 'Dossiers.Filieres', 'Dossiers.Communes'],
            'conditions' => ['Assignations.agent_id' => $idAgent, 'Assignations.status_validations_id' => 1, 'Dossiers.demande_label' => true, 'Assignations.type_demande' => 'label']
        ])->toArray();
        $dossiers = [];
        foreach($assignations as $assignation) {
            $dossiers[] = $assignation->dossier;
        }

        $nbEncours = count($dossiers);

        $this->set(compact('nbEncours'));
    }

    public function labelisationsAcceptes() {
        $this->loadModel('Assignations');
        $userConnect = $this->getUser();
        $idAgent = $userConnect->agents[0]->id;
        $assignations = $this->Assignations->find('All', [
            'contain' => ['Dossiers', 'Dossiers.NatureDemandes', 'Dossiers.Porteurs', 'Dossiers.Filieres', 'Dossiers.Communes'],
            'conditions' => ['Assignations.agent_id' => $idAgent, 'Assignations.status_validations_id' => 6, 'Dossiers.demande_label' => true, 'Assignations.type_demande' => 'label']
        ])->toArray();
        $dossiers = [];
        foreach($assignations as $assignation) {
            $dossiers[] = $assignation->dossier;
        }

        $nbAccepte = count($dossiers);

        $this->set(compact('nbAccepte'));
    }

    public function labelisationsRefuses() {
        $this->loadModel('Assignations');
        $userConnect = $this->getUser();
        $idAgent = $userConnect->agents[0]->id;
        $assignations = $this->Assignations->find('All', [
            'contain' => ['Dossiers', 'Dossiers.NatureDemandes', 'Dossiers.Porteurs', 'Dossiers.Filieres', 'Dossiers.Communes'],
            'conditions' => ['Assignations.agent_id' => $idAgent, 'Assignations.status_validations_id' => 5, 'Dossiers.demande_label' => true, 'Assignations.type_demande' => 'label']
        ])->toArray();
        $dossiers = [];
        foreach($assignations as $assignation) {
            $dossiers[] = $assignation->dossier;
        }

        $nbRefuse = count($dossiers);

        $this->set(compact('nbRefuse'));
    }

}
