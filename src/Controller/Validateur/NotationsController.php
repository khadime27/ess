<?php
namespace App\Controller\Validateur;

use App\Controller\AppController;

/**
 * Notations Controller
 *
 * @property \App\Model\Table\NotationsTable $Notations
 *
 * @method \App\Model\Entity\Notation[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class NotationsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Dossiers', 'Agents']
        ];
        $notations = $this->paginate($this->Notations);

        $this->set(compact('notations'));
    }

    /**
     * View method
     *
     * @param string|null $id Notation id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $notation = $this->Notations->get($id, [
            'contain' => ['Dossiers', 'Agents']
        ]);

        $this->set('notation', $notation);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $notation = $this->Notations->newEntity();
        $dataUser = $this->getUser();
        if ($this->request->is('post')) {
            $temp = $this->request->getData();
            $notation = $this->Notations->patchEntity($notation, $temp);

            $aspect_technique = 0; $aspect_financier = 0; $aspect_env = 0;
            if($temp['result'] == 'accept') {
                $aspect_technique = $temp['nat_struc_juridique'] + $temp['eligibilite_filiere'] + $temp['partenariat_relation'] + $temp['pertinence_activite'] + $temp['coherence_activite'] + $temp['positionnement_activite'];
                if($temp['typeDemande'] == "label")
                    $aspect_financier = $temp['sante_financiere'] + $temp['croissance_reultat'] + $temp['situation_actif'] + $temp['situation_passif'] + $temp['croissance_en_vue'];
                else
                    $aspect_financier = $temp['engagement_actuel'] + $temp['situation_compte'] + $temp['situation_actif'] + $temp['situation_passif'];

                $aspect_env = $temp['eligibilite_ess'] + $temp['impact_social_env'] + $temp['influence_territoriale'] + $temp['autre_critere_ess1'] + $temp['autre_critere_ess2'] + $temp['pourcentage_revenu_dirigeants'];
            }
            $notation->aspect_technique = $aspect_technique;
            $notation->aspect_financier = $aspect_financier;
            $notation->aspect_env = $aspect_env;
            $notation->code = $temp['code'];
            $notation->type_demande = $temp['typeDemande'];

            if ($this->Notations->save($notation)) {

                $this->loadModel('Assignations');
                $assignation = $this->Assignations->find('all', [
                    'conditions' => ['dossier_id' => $temp['dossiers_id'], 'agent_id' => $temp['agents_id'], 'status_validations_id' => 1, 'type' => 'analyse', 'type_demande' => $temp['typeDemande']]
                ])->first();
                $this->loadModel('Dossiers');
                $dossier = $this->Dossiers->get($temp['dossiers_id']);
                if($temp['result'] == 'accept') {
                    
                    $assignation->status_validations_id = 6;
                    $this->Assignations->save($assignation);

                    $allAssignations = $this->Assignations->find('all', [
                        'conditions' => ['dossier_id' => $temp['dossiers_id'], 'code' => $assignation->code, 'type' => 'analyse', 'type_demande' => $temp['typeDemande']] 
                    ])->toArray();
                    
                    if($assignation->rang < count($allAssignations)) { // Si la chaine de validation n'est pas encore terminée.
                        $nextValidateur = $this->Assignations->find('all', [
                            'conditions' => ['dossier_id' => $temp['dossiers_id'], 'rang' => $assignation->rang+1, 'type_demande' => $temp['typeDemande']]
                        ])->first();
                        $nextValidateur->status_validations_id = 1;
                        $this->Assignations->save($nextValidateur);
                    }else { // Si c'est le dernier validateur
                        $notations = $this->Notations->find('all', [
                            'conditions' => ['dossiers_id' => $dossier->id, 'code' => $assignation->code]
                        ]);
                        $sommeNote = 0;
                        $moyenneSurVingt = 0;
                        foreach($notations as $notation) {
                            $sommeNote += $notation->aspect_technique + $notation->aspect_financier + $notation->aspect_env;
                        }
                        $sommeMoyenne = $sommeNote / count($allAssignations);
                        $moyenneSurVingt = $sommeMoyenne * 20 / 100;
                        
                        if($moyenneSurVingt >= 10 && $moyenneSurVingt < 12) $dossier->type_label = "standard";
                        elseif($moyenneSurVingt >= 12 && $moyenneSurVingt < 16) $dossier->type_label = "argent";
                        elseif($moyenneSurVingt >= 16) $dossier->type_label = "gold";

                        if($moyenneSurVingt > 10){ 
                            if($dossier->statut_financements_id == null) { // Si ce n'est pas une demande de financement
                                $dossier->label = true; 
                                $dossier->statut_dossiers_id = 4; 
                            }else {
                                $dossier->statut_financements_id = 5;
                            }
                            
                        }
                        else { 
                            if($dossier->statut_financements_id == null){
                                $dossier->label = false; 
                                $dossier->statut_dossiers_id = 5; 
                            }else {
                                $dossier->statut_financements_id = 7;
                            }
                            
                        }

                        if($dossier->statut_financements_id == null) $dossier->note_final = round($moyenneSurVingt, 2);
                        else $dossier->note_financement = round($moyenneSurVingt, 2);
                        

                        $this->Dossiers->save($dossier);
                    }
                }elseif($temp['result'] == 'rejet') {
                    $assignation->status_validations_id = 5;
                    $this->Assignations->save($assignation);

                    
                    if($dossier->statut_financements_id == null) $dossier->statut_dossiers_id = 5;
                    else $dossier->statut_financements_id = 7;
                    
                    $this->Dossiers->save($dossier);
                }
                
                $this->Flash->success(__("Enregistrement bien effectué !"));

                if($dossier->statut_financements_id == null)
                    return $this->redirect(['controller' => 'Analyses', 'action' => 'encours']);
                else
                    return $this->redirect(['controller' => 'Analyses', 'action' => 'encours', 'finance']);
            }
            $this->Flash->error(__("Impossible de faire l'enregistrement. Réessayez plutard !"));
        }
        $this->set(compact('notation'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Notation id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $notation = $this->Notations->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $notation = $this->Notations->patchEntity($notation, $this->request->getData());
            if ($this->Notations->save($notation)) {
                $this->Flash->success(__('The notation has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The notation could not be saved. Please, try again.'));
        }
        $dossiers = $this->Notations->Dossiers->find('list', ['limit' => 200]);
        $agents = $this->Notations->Agents->find('list', ['limit' => 200]);
        $this->set(compact('notation', 'dossiers', 'agents'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Notation id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $notation = $this->Notations->get($id);
        if ($this->Notations->delete($notation)) {
            $this->Flash->success(__('The notation has been deleted.'));
        } else {
            $this->Flash->error(__('The notation could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
