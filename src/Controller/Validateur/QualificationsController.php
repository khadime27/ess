<?php
namespace App\Controller\Validateur;

use App\Controller\AppController;


class QualificationsController extends AppController
{


    public function edit($id = null, $codeAssignation = null)
    {
        $this->loadModel('Dossiers');

        $dossier = $this->Dossiers->get($id, [
            'contain' => ['Porteurs', 'Filieres',
            'Communes.Departements.Regions','Porteurs.Entreprises.TypeEntreprises', 
            'Porteurs.Entreprises.Communes.Departements.Regions', 'StatutDossiers', 'StatutFinancements'
            ]
        ]);
        $checkEntreprise = false;
        if($dossier->porteur->entreprise) $checkEntreprise = true;

        $checkAssociation = false;
        if($dossier->porteur->entreprise && $dossier->porteur->entreprise->association) $checkAssociation = true;

        $this->loadModel('DossiersFilieres');
        $dossiersFilieres = $this->DossiersFilieres->find('all', [
            'contain' => ['Filieres'],
            'conditions' => ['dossiers_id' => $dossier->id]
        ])->toArray();

        $this->loadModel('DossiersImpacts');
        $dossiersImpacts = $this->DossiersImpacts->find('all', [
            'contain' => ['TypesImpacts.Impactes'],
            'conditions' => ['dossiers_id' => $dossier->id]
        ])->toArray();

        $this->loadModel('PieceJointes');
        $pieces = $this->PieceJointes->find('all', [
            'conditions' => ['dossier_id' => $dossier->id]
        ])->toArray();

        $dataUser = $this->getUser();
        $this->loadModel('QualificationDossiers');
        $qualificationDossier = $this->QualificationDossiers->find('all', [
            'contain' => ['Agents.Agences'],
            'conditions' => ['dossiers_id' => $id, 'agents_id' => $dataUser->agents[0]->id, 'code' => $codeAssignation]
        ])->first();

        $this->set(compact('dossier', 'qualificationDossier', 'codeAssignation', 'checkEntreprise', 'checkAssociation', 'dossiersFilieres', 'dossiersImpacts', 'pieces'));
    }

    public function encours($type = null) {
        $this->loadModel('Assignations');
        $userConnect = $this->getUser();
        $idAgent = $userConnect->agents[0]->id;

        $assignations = null;
        if($type == "label") {
            $assignations = $this->Assignations->find('All', [
                'contain' => ['Dossiers', 'Dossiers.NatureDemandes', 'Dossiers.Porteurs', 'Dossiers.Filieres', 'Dossiers.Communes'],
                'conditions' => ['Assignations.agent_id' => $idAgent, 'Dossiers.statut_financements_id IS' => null, 'Assignations.status_validations_id' => 1, 'Dossiers.demande_label' => true, 'type IS' => null ]
            ])->toArray();
        }else {
            $assignations = $this->Assignations->find('All', [
                'contain' => ['Dossiers', 'Dossiers.NatureDemandes', 'Dossiers.Porteurs', 'Dossiers.Filieres', 'Dossiers.Communes'],
                'conditions' => ['Assignations.agent_id' => $idAgent, 'Dossiers.statut_financements_id' => 2, 'Assignations.status_validations_id' => 1, 'type IS' => null ]
            ])->toArray();
        }
        
        $dossiers = [];
        foreach($assignations as $i => $assignation) {
            $dossiers[] = $assignation->dossier;
            $dossiers[$i]['code_assignation'] = $assignation->code;
        }

        $this->set(compact('dossiers'));
    }


    public function acceptes($type = null) {
        $this->loadModel('Assignations');
        $userConnect = $this->getUser();
        $idAgent = $userConnect->agents[0]->id;

        $assignations = null;
        if($type == "label") {
            $assignations = $this->Assignations->find('All', [
                'contain' => ['Dossiers', 'Dossiers.NatureDemandes', 'Dossiers.Porteurs', 'Dossiers.Filieres', 'Dossiers.Communes'],
                'conditions' => ['Assignations.agent_id' => $idAgent, 'Dossiers.statut_financements_id IS' => null, 'Assignations.status_validations_id' => 6, 'Dossiers.demande_label' => true, 'Assignations.type IS' => null ]
            ])->toArray();
        }elseif($type == "finance") {
            $assignations = $this->Assignations->find('All', [
                'contain' => ['Dossiers', 'Dossiers.NatureDemandes', 'Dossiers.Porteurs', 'Dossiers.Filieres', 'Dossiers.Communes'],
                'conditions' => ['Assignations.agent_id' => $idAgent, 'Dossiers.statut_financements_id IS NOT' => null, 'Assignations.status_validations_id' => 6, 'Assignations.type IS' => null ]
            ])->toArray();
        }
        
        $dossiers = [];
        foreach($assignations as $i => $assignation) {
            $dossiers[] = $assignation->dossier;
            $dossiers[$i]['code_assignation'] = $assignation->code;
        }

        $this->set(compact('dossiers'));
    }

    public function rejetes($type = null) {
        $this->loadModel('Assignations');
        $userConnect = $this->getUser();
        $idAgent = $userConnect->agents[0]->id;

        $assignations = null;
        if($type == "label") {
            $assignations = $this->Assignations->find('All', [
                'contain' => ['Dossiers', 'Dossiers.NatureDemandes', 'Dossiers.Porteurs', 'Dossiers.Filieres', 'Dossiers.Communes'],
                'conditions' => ['Assignations.agent_id' => $idAgent, 'Dossiers.statut_financements_id IS' => null, 'Assignations.status_validations_id' => 5, 'Dossiers.demande_label' => true, 'type IS' => null ]
            ])->toArray();
        }else {
            $assignations = $this->Assignations->find('All', [
                'contain' => ['Dossiers', 'Dossiers.NatureDemandes', 'Dossiers.Porteurs', 'Dossiers.Filieres', 'Dossiers.Communes'],
                'conditions' => ['Assignations.agent_id' => $idAgent, 'Dossiers.statut_financements_id IS NOT' => null, 'Assignations.status_validations_id' => 5, 'type IS' => null ]
            ])->toArray();
        }
        
        $dossiers = [];
        foreach($assignations as $i => $assignation) {
            $dossiers[] = $assignation->dossier;
            $dossiers[$i]['code_assignation'] = $assignation->code;
        }

        $this->set(compact('dossiers'));
    }


}
