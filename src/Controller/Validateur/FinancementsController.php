<?php

namespace App\Controller\Validateur;

use App\Controller\AppController;
use Cake\Mailer\Email;
use Cake\ORM\TableRegistry;
use \Mailjet\Resources;
use Cake\Filesystem\File;

/**
 * Dossiers Controller
 *
 * @property \App\Model\Table\DossiersTable $Dossiers
 *
 * @method \App\Model\Entity\Dossier[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class FinancementsController extends AppController {

    public function getDossiers($statut = null) {
        $this->loadModel('Dossiers');
        $userConnect = $this->getUser();
        $idPorteur = $userConnect->porteurs[0]->id;
        $dossiers = $this->Dossiers->find('All', [
            'contain' => ['NatureDemandes', 'Porteurs', 'Filieres', 'Communes', 'Agents'],
            'conditions' => ['Dossiers.porteur_id' => $idPorteur, 'Dossiers.statut_financements_id' => $statut]
        ])->toArray();

        return $dossiers;
        
    }

    public function details($idDossier = null) {
        $this->loadModel('Dossiers');
        $dossier = $this->Dossiers->get($idDossier);

        $this->getElementsParams($idDossier);

        $this->set(compact('dossier'));

    }

    public function getElementsParams($idDossier = null) {
        $this->loadModel('FinancesEngagements');
        $financesEngagements = $this->FinancesEngagements->find('all', [
            'conditions' => ['dossiers_id' => $idDossier],
            'order' => ['id' => 'ASC']
        ])->toArray();

        $this->loadModel('FinancementsActifs');
        $financementsActifs = $this->FinancementsActifs->find('all', [
            'conditions' => ['dossiers_id' => $idDossier],
            'order' => ['id' => 'ASC']
        ])->toArray();

        $this->loadModel('FinancementsPassifs');
        $financementsPassifs = $this->FinancementsPassifs->find('all', [
            'conditions' => ['dossiers_id' => $idDossier],
            'order' => ['id' => 'ASC']
        ])->toArray();

        $this->loadModel('SituationsComptes');
        $situationsComptes = $this->SituationsComptes->find('all', [
            'conditions' => ['dossiers_id' => $idDossier],
            'order' => ['id' => 'ASC']
        ])->toArray();

        $this->set(compact('financesEngagements', 'financementsActifs', 'financementsPassifs', 'situationsComptes'));
    }

}