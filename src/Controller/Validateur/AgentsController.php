<?php
namespace App\Controller\Validateur;

use App\Controller\AppController;

/**
 * Agents Controller
 *
 * @property \App\Model\Table\AgentsTable $Agents
 *
 * @method \App\Model\Entity\Agent[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class AgentsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Ots', 'Users', 'Agences', 'Structures']
        ];
        $agents = $this->paginate($this->Agents);

        $this->set(compact('agents'));
    }

    /**
     * View method
     *
     * @param string|null $id Agent id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $agent = $this->Agents->get($id, [
            'contain' => ['Ots', 'Users', 'Agences', 'Structures', 'Agents', 'Animations', 'Assignations', 'Dossiers', 'Messages', 'StatutDossiers', 'SuiviEvaluations']
        ]);

        $this->set('agent', $agent);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $agent = $this->Agents->newEntity();
        if ($this->request->is('post')) {
            $agent = $this->Agents->patchEntity($agent, $this->request->getData());
            if ($this->Agents->save($agent)) {
                $this->Flash->success(__('The agent has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The agent could not be saved. Please, try again.'));
        }
        $ots = $this->Agents->Ots->find('list', ['limit' => 200]);
        $users = $this->Agents->Users->find('list', ['limit' => 200]);
        $agences = $this->Agents->Agences->find('list', ['limit' => 200]);
        $structures = $this->Agents->Structures->find('list', ['limit' => 200]);
        $this->set(compact('agent', 'ots', 'users', 'agences', 'structures'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Agent id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $agent = $this->Agents->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $agent = $this->Agents->patchEntity($agent, $this->request->getData());
            if ($this->Agents->save($agent)) {
                $this->Flash->success(__('The agent has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The agent could not be saved. Please, try again.'));
        }
        $ots = $this->Agents->Ots->find('list', ['limit' => 200]);
        $users = $this->Agents->Users->find('list', ['limit' => 200]);
        $agences = $this->Agents->Agences->find('list', ['limit' => 200]);
        $structures = $this->Agents->Structures->find('list', ['limit' => 200]);
        $this->set(compact('agent', 'ots', 'users', 'agences', 'structures'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Agent id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $agent = $this->Agents->get($id);
        if ($this->Agents->delete($agent)) {
            $this->Flash->success(__('The agent has been deleted.'));
        } else {
            $this->Flash->error(__('The agent could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
