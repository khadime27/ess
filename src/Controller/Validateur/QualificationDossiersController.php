<?php
namespace App\Controller\Validateur;

use App\Controller\AppController;

/**
 * QualificationDossiers Controller
 *
 * @property \App\Model\Table\QualificationDossiersTable $QualificationDossiers
 *
 * @method \App\Model\Entity\QualificationDossier[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class QualificationDossiersController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Dossiers']
        ];
        $qualificationDossiers = $this->paginate($this->QualificationDossiers);

        $this->set(compact('qualificationDossiers'));
    }

    /**
     * View method
     *
     * @param string|null $id Qualification Dossier id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $qualificationDossier = $this->QualificationDossiers->get($id, [
            'contain' => ['Dossiers']
        ]);

        $this->set('qualificationDossier', $qualificationDossier);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $qualificationDossier = $this->QualificationDossiers->newEntity();
        if ($this->request->is('post')) {
            $temp = $this->request->getData();
            $qualificationDossier = $this->QualificationDossiers->patchEntity($qualificationDossier, $temp);
            
            $this->loadModel('Dossiers');
            $dossier = $this->Dossiers->get($temp['dossiers_id']);

            $this->loadModel('Assignations');
            $assignation = $this->Assignations->find('all', [
                'conditions' => ['dossier_id' => $dossier->id, 'code' => $temp['codeAssignation']]
            ])->first();
            if($temp['result'] == "accept") {
                if($dossier->statut_financements_id == null) $dossier->statut_dossiers_id = 2;
                else $dossier->statut_financements_id = 3;

                $assignation->status_validations_id = 6;
                
            } else {
                $dossier->statut_dossiers_id = 5;
                $assignation->status_validations_id = 5;
            } 
            $this->Assignations->save($assignation);

            $qualificationDossier->code = $temp['codeAssignation'];
            if($dossier->statut_financements_id == null) $qualificationDossier->type_demande = "label";
            else $qualificationDossier->type_demande = "finance";
            if ($this->QualificationDossiers->save($qualificationDossier)) {
                $this->Dossiers->save($dossier);
                
                $this->Flash->success(__("Enregistrement bien effectué !"));

                return $this->redirect(['controller' => 'Labelisations', 'action' => 'encours']);
            }
            $this->Flash->error(__("Impossible de faire l'enregistrement. Merci de réessayer !"));
        }
        $dossiers = $this->QualificationDossiers->Dossiers->find('list', ['limit' => 200]);
        $this->set(compact('qualificationDossier', 'dossiers'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Qualification Dossier id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $qualificationDossier = $this->QualificationDossiers->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $qualificationDossier = $this->QualificationDossiers->patchEntity($qualificationDossier, $this->request->getData());
            if ($this->QualificationDossiers->save($qualificationDossier)) {
                $this->Flash->success(__('The qualification dossier has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The qualification dossier could not be saved. Please, try again.'));
        }
        $dossiers = $this->QualificationDossiers->Dossiers->find('list', ['limit' => 200]);
        $this->set(compact('qualificationDossier', 'dossiers'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Qualification Dossier id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $qualificationDossier = $this->QualificationDossiers->get($id);
        if ($this->QualificationDossiers->delete($qualificationDossier)) {
            $this->Flash->success(__('The qualification dossier has been deleted.'));
        } else {
            $this->Flash->error(__('The qualification dossier could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
