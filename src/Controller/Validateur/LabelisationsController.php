<?php
namespace App\Controller\Validateur;

use App\Controller\AppController;


class LabelisationsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {

    }

    public function encours() {
        $this->loadModel('Assignations');
        $userConnect = $this->getUser();
        $idAgent = $userConnect->agents[0]->id;
        $assignations = $this->Assignations->find('All', [
            'contain' => ['Dossiers', 'Dossiers.NatureDemandes', 'Dossiers.Porteurs', 'Dossiers.Filieres', 'Dossiers.Communes'],
            'conditions' => ['Assignations.agent_id' => $idAgent, 'Dossiers.statut_dossiers_id' => 1, 'Dossiers.demande_label' => true]
        ])->toArray();
        $dossiers = [];
        foreach($assignations as $assignation) {
            $dossiers[] = $assignation->dossier;
        }


        $this->set(compact('dossiers'));
    }

    public function acceptes() {
        $this->loadModel('Assignations');
        $userConnect = $this->getUser();
        $idAgent = $userConnect->agents[0]->id;
        
        $assignations = $this->Assignations->find('All', [
            'contain' => ['Dossiers', 'Dossiers.NatureDemandes', 'Dossiers.Porteurs', 'Dossiers.Filieres', 'Dossiers.Communes'],
            'conditions' => ['Assignations.agent_id' => $idAgent, 'Assignations.status_validations_id' => 6, 'Dossiers.demande_label' => true]
        ])->toArray();

        $dossiers = [];
        foreach($assignations as $assignation) {
            $dossiers[] = $assignation->dossier;
        }
        

        $this->set(compact('dossiers'));
    }

    public function rejetes() {
        $this->loadModel('Assignations');
        $userConnect = $this->getUser();
        $idAgent = $userConnect->agents[0]->id;
        $assignations = $this->Assignations->find('All', [
            'contain' => ['Dossiers', 'Dossiers.NatureDemandes', 'Dossiers.Porteurs', 'Dossiers.Filieres', 'Dossiers.Communes'],
            'conditions' => ['Assignations.agent_id' => $idAgent, 'Assignations.status_validations_id' => 5, 'Dossiers.demande_label' => true]
        ])->toArray();
        $dossiers = $assignations;

        $dossiers = [];
        foreach($assignations as $assignation) {
            $dossiers[] = $assignation->dossier;
        }

        $this->set(compact('dossiers'));
    }

    

}
