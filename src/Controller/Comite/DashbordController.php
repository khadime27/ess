<?php
namespace App\Controller\Comite;

use App\Controller\AppController;

/**
 * Dashbord Controller
 *
 *
 * @method \App\Model\Entity\Dashbord[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class DashbordController extends AppController
{

    public function index() {
        $dataUser = $this->getUser();
        $agence = $dataUser->agents[0]->agence;

        $this->loadModel('Dossiers');

        $labelsAcceptes = $this->Dossiers->find('all', [
            'contain' => ['Communes.Departements.Regions'],
            'conditions' => ['statut_dossiers_id' => 4, 'demande_label' => true, 'Regions.id' => $agence->regions_id]
        ])->toArray();

        $labelsConfirmes = $this->Dossiers->find('all', [
            'contain' => ['Communes.Departements.Regions'],
            'conditions' => ['statut_dossiers_id' => 6, 'demande_label' => true, 'Regions.id' => $agence->regions_id]
        ])->toArray();

        $this->loadModel('Agents');
        $agent = $this->Agents->get($dataUser->agents[0]->id, [
            'contain' => ['Agences']
        ]);

        $this->getStats();

        $this->set(compact('labelsAcceptes', 'labelsConfirmes', 'agent'));
    }

    public function getStats() {
        $dataUser = $this->getUser();
        $agence = $dataUser->agents[0]->agence;

        $this->loadModel('Dossiers');
        $theFolders = $this->Dossiers->find('all', [
            'contain' => ['StatutDossiers', 'Communes.Departements'],
            'conditions' => ['Dossiers.statut_dossiers_id IS NOT' => null]
        ])->toArray();

        $allDossiers = [];
        foreach($theFolders as $folder) {
            if($folder->commune->departement->region_id == $agence->regions_id) $allDossiers[] = $folder;
        }
        
        $statutDossiers = ["En cours", "Accepté", "Rejeté"];

        $dossiers = [];
        foreach($allDossiers as $dossier) {
            if($dossier->created->format('m') == date('m')) $dossiers[] = $dossier;
        }

        $this->getMois();

        $this->set(compact('allDossiers', 'dossiers', 'statutDossiers'));
    }

    public function getMois() {
        $mois = array(
            array("num" => "01", "nom" => "JANVIER"),
            array("num" => "02", "nom" => "FEVRIER"),
            array("num" => "03", "nom" => "MARS"),
            array("num" => "04", "nom" => "AVRIL"),
            array("num" => "05", "nom" => "MAI"),
            array("num" => "06", "nom" => "JUIN"),
            array("num" => "07", "nom" => "JUILLET"),
            array("num" => "08", "nom" => "AOUT"),
            array("num" => "09", "nom" => "SEPTEMBRE"),
            array("num" => "10", "nom" => "OCTOBRE"),
            array("num" => "11", "nom" => "NOVEMBRE"),
            array("num" => "12", "nom" => "DECEMBRE"),
        );

        $this->set(compact('mois'));
    }

}