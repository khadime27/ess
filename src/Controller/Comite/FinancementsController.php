<?php
namespace App\Controller\Comite;

use App\Controller\AppController;

class FinancementsController extends AppController
{

    public function getDossiers($statut = null) {
        $dataUser = $this->getUser();
        $agence = $dataUser->agents[0]->agence;

        $this->loadModel('Dossiers');

        $dossiers = $this->Dossiers->find('all', [
            'contain' => ['Communes.Departements.Regions', 'NatureDemandes', 'Porteurs', 'Filieres', 'Communes', 'Agents'],
            'conditions' => ['statut_financements_id' => $statut, 'Regions.id' => $agence->regions_id]
        ])->toArray();

        return $dossiers;
    
    }

    public function acceptes() {
        $dossiers = $this->getDossiers(5);
        $this->set(compact('dossiers'));
    }

    public function confirmes() {
        $dossiers = $this->getDossiers(6);
        $this->set(compact('dossiers'));
    }

    public function confirmerLabel($idDossier = null) {
        $this->loadModel('Dossiers');
        $dossier = $this->Dossiers->get($idDossier);
        $dossier->statut_financements_id = 6;
        $dossier->emission_finance = false;

        if($this->Dossiers->save($dossier)) $this->Flash->success(__("Le dossier a été validé avec succès."));
        else $this->Flash->error(__("Impossible de valider ce dossier. Réessayez plutard !"));
        $this->redirect($this->referer());
    }

    public function getElementsParams($idDossier = null) {
        $this->loadModel('FinancesEngagements');
        $financesEngagements = $this->FinancesEngagements->find('all', [
            'conditions' => ['dossiers_id' => $idDossier],
            'order' => ['id' => 'ASC']
        ])->toArray();

        $this->loadModel('FinancementsActifs');
        $financementsActifs = $this->FinancementsActifs->find('all', [
            'conditions' => ['dossiers_id' => $idDossier],
            'order' => ['id' => 'ASC']
        ])->toArray();

        $this->loadModel('FinancementsPassifs');
        $financementsPassifs = $this->FinancementsPassifs->find('all', [
            'conditions' => ['dossiers_id' => $idDossier],
            'order' => ['id' => 'ASC']
        ])->toArray();

        $this->loadModel('SituationsComptes');
        $situationsComptes = $this->SituationsComptes->find('all', [
            'conditions' => ['dossiers_id' => $idDossier],
            'order' => ['id' => 'ASC']
        ])->toArray();

        $this->set(compact('financesEngagements', 'financementsActifs', 'financementsPassifs', 'situationsComptes'));
    }

    public function details($idDossier = null) {
        $this->loadModel('Dossiers');
        $dossier = $this->Dossiers->get($idDossier);

        $this->getElementsParams($idDossier);

        $this->set(compact('dossier'));

    }

}
