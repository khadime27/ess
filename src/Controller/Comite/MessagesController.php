<?php
namespace App\Controller\Comite;

use App\Controller\AppController;

/**
 * Messages Controller
 *
 * @property \App\Model\Table\MessagesTable $Messages
 *
 * @method \App\Model\Entity\Message[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class MessagesController extends AppController
{

    public function sendmsg() {
        $userConnect = $this->getUser();
        
        if( $this->request->is('ajax') ) {

            $temp = $this->request->getData();
            $chat = $this->Messages->newEntity();

            $chat->agent_id = $userConnect->agents[0]->id;
            $chat->porteur_id = $temp['interlocuteur'];
            $chat->dossier_id = $temp['dossier_id'];
            $chat->resume = $temp['message'];

            $filepath = null; $fileName = null;
            if($temp['url']['size'] != 0){

                $image = $temp['url'];
            
                $fileName = $image['name'];
                $fileName = $this->sanitize($fileName);  // Pour éliminer les mauvais caractères 
                $fileName = time().$fileName;        // On concaténe le nom avec une chaine générée
                $filepath = WWW_ROOT.DS.'img'.DS.'messages'.DS.$fileName;

                // Le nom de l'image dans la base
                $chat->url = $fileName;
                
            }else $user->photo = null;
            
            if($this->Messages->save($chat)) {
                if($temp['url']['size'] != 0) {
                    $image = $temp['url'];
                    $file_tmp = $image['tmp_name'];
                    move_uploaded_file($file_tmp, $filepath);    // Déplacement de l'image dans le dossier voulu
                }
                echo json_encode('success');
            }else {
                echo json_encode('error');
            }

            die();
        }
    
    }

    public function showmsg($idInterlocuteur = null, $profileInterlocuteur = null, $idDossier = null) {
        $this->autoRender = false;

        $userConnect = $this->getUser();
        $messages = [];
        $this->loadModel('Dossiers');
        $dossierUser = $this->Dossiers->find('all', [
            'conditions' => ['porteur_id' => $userConnect->porteurs[0]->id]
        ])->first();

        if($userConnect->profiles_id == 1) {
            if($profileInterlocuteur == 1) { // Se fait entre 2 porteurs
                // Les messages qu'il a envoyé et qu'il a reçu à ce dossier
                $messages = $this->Messages->find('all', [
                    'conditions' => [
                        'OR' => [
                            ['porteur_id' => $userConnect->porteurs[0]->id, 'dossier_id' => $idDossier],
                            ['porteur_id' => $idInterlocuteur, 'dossier_id' => $dossierUser->id]
                        ]
                    ],
                    'order' => ['Messages.date' => 'ASC']
                ]);
            }elseif($profileInterlocuteur > 1) { // Se fait avec un agent
                // Les messages qu'il a envoyé et qu'il a reçu à ce dossier
                $messages = $this->Messages->find('all', [
                    'contain' => ['Dossiers'],
                    'conditions' => [
                        'OR' => [
                            ['Messages.porteur_id' => $userConnect->porteurs[0]->id, 'Messages.agent_id' => $idInterlocuteur],
                            ['Messages.agent_id' => $idInterlocuteur, 'Messages.porteur_id' => $userConnect->porteurs[0]->id]
                        ]
                    ],
                    'order' => ['Messages.date' => 'ASC']
                ]);
            }
            
        } else {
            $messages = $this->Messages->find('all', [
                'contain' => ['Dossiers'],
                'conditions' => ['Messages.agent_id' => $userConnect->agents[0]->id, 'Messages.porteur_id' => $idInterlocuteur],
                'order' => ['Messages.date' => 'ASC']
            ]);
        }

        echo json_encode($messages);

    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Agents', 'Dossiers', 'Porteurs']
        ];
        $messages = $this->paginate($this->Messages);

        $this->set(compact('messages'));
    }

    /**
     * View method
     *
     * @param string|null $id Message id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $message = $this->Messages->get($id, [
            'contain' => ['Agents', 'Dossiers', 'Porteurs', 'PieceJointes']
        ]);

        $this->set('message', $message);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $message = $this->Messages->newEntity();
        if ($this->request->is('post')) {
            $message = $this->Messages->patchEntity($message, $this->request->getData());
            if ($this->Messages->save($message)) {
                $this->Flash->success(__('The message has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The message could not be saved. Please, try again.'));
        }
        $agents = $this->Messages->Agents->find('list', ['limit' => 200]);
        $dossiers = $this->Messages->Dossiers->find('list', ['limit' => 200]);
        $porteurs = $this->Messages->Porteurs->find('list', ['limit' => 200]);
        $this->set(compact('message', 'agents', 'dossiers', 'porteurs'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Message id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $message = $this->Messages->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $message = $this->Messages->patchEntity($message, $this->request->getData());
            if ($this->Messages->save($message)) {
                $this->Flash->success(__('The message has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The message could not be saved. Please, try again.'));
        }
        $agents = $this->Messages->Agents->find('list', ['limit' => 200]);
        $dossiers = $this->Messages->Dossiers->find('list', ['limit' => 200]);
        $porteurs = $this->Messages->Porteurs->find('list', ['limit' => 200]);
        $this->set(compact('message', 'agents', 'dossiers', 'porteurs'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Message id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $message = $this->Messages->get($id);
        if ($this->Messages->delete($message)) {
            $this->Flash->success(__('The message has been deleted.'));
        } else {
            $this->Flash->error(__('The message could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
    
}
