<?php
namespace App\Controller\Comite;

use App\Controller\AppController;

/**
 * Agents Controller
 *
 * @property \App\Model\Table\AgentsTable $Agents
 *
 * @method \App\Model\Entity\Agent[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class AgentsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $userConnect = $this->getUser();
        $agence = $userConnect->agents[0]->agence;
        $agents = $this->Agents->find('all', [
            'contain' => ['Agences', 'Users'],
            'conditions' => ['Agents.admin' => 0, 'agence_id' => $agence->id]
        ])->toArray();

        $this->loadModel('Agences');
        $agences = $this->Agences->find('all');

        $this->loadModel('Comites');
        $comites = $this->Comites->find('all');

        $this->set(compact('agents', 'agence', 'agences', 'comites'));
    }

    /**
     * View method
     *
     * @param string|null $id Agent id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $agent = $this->Agents->get($id, [
            'contain' => ['Ots', 'Users', 'Agences', 'Structures', 'Agents', 'Animations', 'Assignations', 'Dossiers', 'Messages', 'StatutDossiers', 'SuiviEvaluations']
        ]);

        $this->set('agent', $agent);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $agent = $this->Agents->newEntity();
        $this->loadModel('Users');
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $temp = $this->request->getData();
            $agent = $this->Agents->patchEntity($agent, $temp);

            $user->email = $temp['email'];
            $user->password = $temp['password'];
            $user->profiles_id = 5;  // Profile comité
            $user->actif = true;

            if($this->Users->save($user)) {
                $agent->user_id = $user->id;
                $agent->prenom = $temp['prenom'];
                $agent->nom = $temp['nom'];
                $agent->telephone = $temp['telephone'];
                $agent->adresse = $temp['adresse'];
                $agent->agence_id = $temp['agence_id'];
                $agent->structure_id = 1;
                $agent->admin = 0;
                $agent->etat = true;
                if ($this->Agents->save($agent)) {
                    $this->Flash->success(__("Enregistrement bien effectué !"));

                    return $this->redirect(['action' => 'index']);
                }
                $this->Flash->error(__("Impossible de faire l'enregistrement. Réessayez plutard !"));
            }

            
        }
        $this->set(compact('agent'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Agent id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit()
    {
        
        if ($this->request->is(['patch', 'post', 'put'])) {
            $temp = $this->request->getData();
            $id = $temp['idAgent'];
            $agent = $this->Agents->get($id);
            $this->loadModel('Users');
            $user = $this->Users->get($agent->user_id);
            
            $agent = $this->Agents->patchEntity($agent, $temp);

            $user->email = $temp['email'];

            if($this->Users->save($user)) {
                $agent->prenom = $temp['prenom'];
                $agent->nom = $temp['nom'];
                $agent->telephone = $temp['telephone'];
                $agent->adresse = $temp['adresse'];
                $agent->agence_id = $temp['agence_id'];
                if ($this->Agents->save($agent)) {
                    $this->Flash->success(__("Enregistrement bien effectué !"));

                    return $this->redirect(['action' => 'index']);
                }
                $this->Flash->error(__("Impossible de faire l'enregistrement. Réessayez plutard !"));
            }
        }
        $this->set(compact('agent'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Agent id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $agent = $this->Agents->get($id);
        if ($this->Agents->delete($agent)) {
            $this->Flash->success(__('The agent has been deleted.'));
        } else {
            $this->Flash->error(__('The agent could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function changeStat($idAgent = null, $stat = null) {
        $agent = $this->Agents->get($idAgent, [
            'contain' => []
        ]);
        if($stat == 1) $agent->etat = 1;
        else $agent->etat = 0;

        if($this->Agents->save($agent)) {
            $this->Flash->success(__('Enregistrement bien effectué.'));
        }else {
            $this->Flash->error(__("Impossible d'enregistrer. Réessayez plutard !"));
        }
        return $this->redirect(['action' => 'index']);
        
    }

}
