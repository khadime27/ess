<?php
namespace App\Controller\Comite;

use App\Controller\AppController;

/**
 * Dashbord Controller
 *
 *
 * @method \App\Model\Entity\Dashbord[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class LabelisationsController extends AppController
{

    public function getDossiers($statut = null) {
        $dataUser = $this->getUser();
        $agence = $dataUser->agents[0]->agence;

        $this->loadModel('Dossiers');

        $dossiers = $this->Dossiers->find('all', [
            'contain' => ['Communes.Departements.Regions', 'NatureDemandes', 'Porteurs', 'Filieres', 'Communes', 'Agents'],
            'conditions' => ['statut_dossiers_id' => $statut, 'demande_label' => true, 'label' => true, 'Regions.id' => $agence->regions_id]
        ])->toArray();

        return $dossiers;
    
    }

    public function acceptes() {
        $dossiers = $this->getDossiers(4);
        $this->set(compact('dossiers'));
    }

    public function confirmes() {
        $dossiers = $this->getDossiers(6);
        $this->set(compact('dossiers'));
    }

    public function confirmerLabel($idDossier = null) {
        $this->loadModel('Dossiers');
        $dossier = $this->Dossiers->get($idDossier);
        $dossier->statut_dossiers_id = 6;

        if($this->Dossiers->save($dossier)) $this->Flash->success(__("Le dossier a été validé avec succès."));
        else $this->Flash->error(__("Impossible de valider ce dossier. Réessayez plutard !"));
        $this->redirect($this->referer());
    }

}
