<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Bibliotheques Controller
 *
 * @property \App\Model\Table\BibliothequesTable $Bibliotheques
 *
 * @method \App\Model\Entity\Bibliotheque[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class BibliothequesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $bibliotheques = $this->Bibliotheques->find('all');

        $this->set(compact('bibliotheques'));
    }

    /**
     * View method
     *
     * @param string|null $id Bibliotheque id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $bibliotheque = $this->Bibliotheques->get($id, [
            'contain' => []
        ]);

        $this->set('bibliotheque', $bibliotheque);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $bibliotheque = $this->Bibliotheques->newEntity();
        if ($this->request->is('post')) {
            $bibliotheque = $this->Bibliotheques->patchEntity($bibliotheque, $this->request->getData());
            if ($this->Bibliotheques->save($bibliotheque)) {
                $this->Flash->success(__('The bibliotheque has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The bibliotheque could not be saved. Please, try again.'));
        }
        $this->set(compact('bibliotheque'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Bibliotheque id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $bibliotheque = $this->Bibliotheques->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $bibliotheque = $this->Bibliotheques->patchEntity($bibliotheque, $this->request->getData());
            if ($this->Bibliotheques->save($bibliotheque)) {
                $this->Flash->success(__('The bibliotheque has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The bibliotheque could not be saved. Please, try again.'));
        }
        $this->set(compact('bibliotheque'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Bibliotheque id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $bibliotheque = $this->Bibliotheques->get($id);
        if ($this->Bibliotheques->delete($bibliotheque)) {
            $this->Flash->success(__('The bibliotheque has been deleted.'));
        } else {
            $this->Flash->error(__('The bibliotheque could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
