<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Dossiers Controller
 *
 * @property \App\Model\Table\DossiersTable $Dossiers
 *
 * @method \App\Model\Entity\Dossier[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class DossiersController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['NatureDemandes', 'Porteurs', 'Filieres', 'Communes', 'Agents']
        ];
        $dossiers = $this->paginate($this->Dossiers);

        $this->set(compact('dossiers'));
    }

    /**
     * View method
     *
     * @param string|null $id Dossier id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $dossier = $this->Dossiers->get($id, [
            'contain' => ['NatureDemandes', 'Porteurs', 'Filieres', 'Communes', 'Agents', 'Assignations', 'FinancementCredits', 'Imputations', 'Messages', 'Notations', 'PieceJointes', 'StatutDossiers', 'Suretes']
        ]);

        $this->set('dossier', $dossier);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $dossier = $this->Dossiers->newEntity();
        if ($this->request->is('post')) {
            $dossier = $this->Dossiers->patchEntity($dossier, $this->request->getData());
            if ($this->Dossiers->save($dossier)) {
                $this->Flash->success(__('The dossier has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The dossier could not be saved. Please, try again.'));
        }
        $natureDemandes = $this->Dossiers->NatureDemandes->find('list', ['limit' => 200]);
        $porteurs = $this->Dossiers->Porteurs->find('list', ['limit' => 200]);
        $filieres = $this->Dossiers->Filieres->find('list', ['limit' => 200]);
        $communes = $this->Dossiers->Communes->find('list', ['limit' => 200]);
        $agents = $this->Dossiers->Agents->find('list', ['limit' => 200]);
        $this->set(compact('dossier', 'natureDemandes', 'porteurs', 'filieres', 'communes', 'agents'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Dossier id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $dossier = $this->Dossiers->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $dossier = $this->Dossiers->patchEntity($dossier, $this->request->getData());
            if ($this->Dossiers->save($dossier)) {
                $this->Flash->success(__('The dossier has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The dossier could not be saved. Please, try again.'));
        }
        $natureDemandes = $this->Dossiers->NatureDemandes->find('list', ['limit' => 200]);
        $porteurs = $this->Dossiers->Porteurs->find('list', ['limit' => 200]);
        $filieres = $this->Dossiers->Filieres->find('list', ['limit' => 200]);
        $communes = $this->Dossiers->Communes->find('list', ['limit' => 200]);
        $agents = $this->Dossiers->Agents->find('list', ['limit' => 200]);
        $this->set(compact('dossier', 'natureDemandes', 'porteurs', 'filieres', 'communes', 'agents'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Dossier id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $dossier = $this->Dossiers->get($id);
        if ($this->Dossiers->delete($dossier)) {
            $this->Flash->success(__('The dossier has been deleted.'));
        } else {
            $this->Flash->error(__('The dossier could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function projectBySecteur($idFiliere = null) {
        $this->loadModel('Filieres');
        $filiere = $this->Filieres->get($idFiliere);
        $this->loadModel('Dossiers');
        $dossiers = $this->Dossiers->find('all', [
            'conditions' => ['filiere_id' => $idFiliere]
        ])->toArray();

        $this->set(compact('dossiers', 'filiere'));
    }

    public function detailproject($id = null) {
        $dossier = $this->Dossiers->get($id);

        $this->set(compact('dossier'));
    }
}
