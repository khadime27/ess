<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * PieceJointes Controller
 *
 * @property \App\Model\Table\PieceJointesTable $PieceJointes
 *
 * @method \App\Model\Entity\PieceJointe[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class PieceJointesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Messages', 'Dossiers', 'Mediations', 'Animations', 'SuiviEvaluations']
        ];
        $pieceJointes = $this->paginate($this->PieceJointes);

        $this->set(compact('pieceJointes'));
    }

    /**
     * View method
     *
     * @param string|null $id Piece Jointe id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $pieceJointe = $this->PieceJointes->get($id, [
            'contain' => ['Messages', 'Dossiers', 'Mediations', 'Animations', 'SuiviEvaluations']
        ]);

        $this->set('pieceJointe', $pieceJointe);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $pieceJointe = $this->PieceJointes->newEntity();
        if ($this->request->is('post')) {
            $pieceJointe = $this->PieceJointes->patchEntity($pieceJointe, $this->request->getData());
            if ($this->PieceJointes->save($pieceJointe)) {
                $this->Flash->success(__('The piece jointe has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The piece jointe could not be saved. Please, try again.'));
        }
        $messages = $this->PieceJointes->Messages->find('list', ['limit' => 200]);
        $dossiers = $this->PieceJointes->Dossiers->find('list', ['limit' => 200]);
        $mediations = $this->PieceJointes->Mediations->find('list', ['limit' => 200]);
        $animations = $this->PieceJointes->Animations->find('list', ['limit' => 200]);
        $suiviEvaluations = $this->PieceJointes->SuiviEvaluations->find('list', ['limit' => 200]);
        $this->set(compact('pieceJointe', 'messages', 'dossiers', 'mediations', 'animations', 'suiviEvaluations'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Piece Jointe id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $pieceJointe = $this->PieceJointes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $pieceJointe = $this->PieceJointes->patchEntity($pieceJointe, $this->request->getData());
            if ($this->PieceJointes->save($pieceJointe)) {
                $this->Flash->success(__('The piece jointe has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The piece jointe could not be saved. Please, try again.'));
        }
        $messages = $this->PieceJointes->Messages->find('list', ['limit' => 200]);
        $dossiers = $this->PieceJointes->Dossiers->find('list', ['limit' => 200]);
        $mediations = $this->PieceJointes->Mediations->find('list', ['limit' => 200]);
        $animations = $this->PieceJointes->Animations->find('list', ['limit' => 200]);
        $suiviEvaluations = $this->PieceJointes->SuiviEvaluations->find('list', ['limit' => 200]);
        $this->set(compact('pieceJointe', 'messages', 'dossiers', 'mediations', 'animations', 'suiviEvaluations'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Piece Jointe id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $pieceJointe = $this->PieceJointes->get($id);
        if ($this->PieceJointes->delete($pieceJointe)) {
            $this->Flash->success(__('The piece jointe has been deleted.'));
        } else {
            $this->Flash->error(__('The piece jointe could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
