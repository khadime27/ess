<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 *
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UsersController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Profiles']
        ];
        $users = $this->paginate($this->Users);

        $this->set(compact('users'));
    }

    /**
     * View method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => ['Profiles']
        ]);

        $this->set('user', $user);
    } 

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $user = $this->Users->newEntity();
        $this->loadModel('Profiles');
        $profiles = $this->Profiles->find('all');
        if ($this->request->is('post')) {
            $temp = $this->request->getData();
            $user = $this->Users->patchEntity($user, $temp);
            
            $user->email = $temp['email'];
            $user->password = $temp['password'];
            $user->profiles_id = 1;  // Profile Porteur
            $codeGenere = null;
            if($temp['type-entite'] != null) {
                $codeGenere = str_replace(' ', '', $temp['nomEntreprise'])."-".$this->genererCode(4);
            }else {
                $codeGenere = str_replace(' ', '', $temp['prenom'])."-".$this->genererCode(4);
            }
            $user->codeIdentification = $codeGenere;
            $user->actif = true;

            $filepath = null; $fileName = null;
            if($temp['photo']['size'] != 0){

                $image = $temp['photo'];
            
                $fileName = $image['name'];
                $fileName = $this->sanitize($fileName);  // Pour éliminer les mauvais caractères 
                $fileName = time().$fileName;        // On concaténe le nom avec une chaine générée
                $filepath = WWW_ROOT.DS.'img'.DS.'porteurs'.DS.$fileName;

                // Le nom de l'image dans la base
                $user->photo = $fileName;
                
            }else $user->photo = null;

            if ($this->Users->save($user)) {
                if($temp['photo']['size'] != 0) {
                    $image = $temp['photo'];
                    $file_tmp = $image['tmp_name'];
                    move_uploaded_file($file_tmp, $filepath);    // Déplacement de l'image dans le dossier voulu
                }

                $this->loadModel('Porteurs');
                $porteur = $this->Porteurs->newEntity();
                $porteur->user_id = $user->id;
                $porteur->prenom = $temp['prenom'];
                $porteur->nom = $temp['nom'];
                
                if($temp['type-entite'] == null) {
                    $porteur->telephone = $temp['telephone'];
                    $porteur->genre = $temp['genre'];
                    $porteur->date_naissance = $temp['lieu_naissance'];
                    $porteur->lieu_residence = $temp['lieu_residence'];
                    $porteur->type_identification = $temp['type_identification'];
                    $porteur->num_identification = $temp['num_identification'];
                }

                $this->Porteurs->save($porteur);

                if($temp['type-entite'] != null){
                    $this->loadModel('Entreprises');
                    
                    $entreprise = $this->Entreprises->find('all', [
                        'conditions' => ['ninea' => $temp['ninea']]
                    ])->first();
                    if($entreprise == null) {
                        $entreprise = $this->Entreprises->newEntity();
                        if($temp['type-entite'] == "entreprise") $entreprise->association = false;
                        else $entreprise->association = true;

                        if($temp['statut-juridique'] == "Formel") $entreprise->formel = true;
                        else $entreprise->formel = false;
                        $entreprise->ninea = $temp['ninea'];
                        $entreprise->nom = $temp['nomEntreprise'];
                        $entreprise->type_entreprise_id = $temp['type_entreprise_id'];
                        $entreprise->nb_employe = $temp['nb_employe'];
                        $entreprise->date_creation = $temp['date_creation'];
                        $entreprise->immatriculation = $temp['immatriculation'];
                        $entreprise->type_social = $temp['type_social'];
                        $entreprise->social_link = $temp['social_link'];
                        $entreprise->tel_entite = $temp['telephone'];
                        $entreprise->communes_id = $temp['communes_id'];
                        $entreprise->nb_salarie = $temp['nb_salarie'];
                        $entreprise->nb_stageaire = $temp['nb_stageaire'];
                        $entreprise->nb_saisonnier = $temp['nb_saisonnier'];
                    }

                    
                    if($this->Entreprises->save($entreprise)) {
                        $porteur->entreprise_id = $entreprise->id;
                        $this->Porteurs->save($porteur);
                    }
                }

                $this->Flash->success(__('Inscription bien effectuée !'));

                return $this->redirect(['controller' => 'Users', 'action' => 'connecter']);
            }
            $this->Flash->error(__('Impossible de s\'inscrire. Réessayez !'));
        }
        $this->loadModel('Departements');
        $this->loadModel('Regions');
        $this->loadModel('Filieres');
        $this->loadModel('TypeEntreprises');
        
        $departements = $this->Departements->find('all');
        $regions = $this->Regions->find('all');
        $filieres = $this->Filieres->find('all');
        $typesEntreprises = $this->TypeEntreprises->find('all');
        $this->set(compact('user', 'profiles', 'regions', 'departements', 'filieres', 'typesEntreprises'));
    }

    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $profiles = $this->Users->Profiles->find('list', ['limit' => 200]);
        $this->set(compact('user', 'profiles'));
    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);
        if ($this->Users->delete($user)) {
            $this->Flash->success(__('The user has been deleted.'));
        } else {
            $this->Flash->error(__('The user could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function initialize()
    {
        parent::initialize();
        // Ajoute l'action 'add' à la liste des actions autorisées (pour que quiconque puisse créer un compte)
        $this->Auth->allow(['add']);
    }

    public function isAuthorized($user)
    {
        $action = $this->request->getParam('action');
        // Les actions  'edit' et 'delete' sont toujours autorisés pour les utilisateurs
        // authentifiés sur l'application
        if (in_array($action, ['edit', 'delete', 'test'])) {
            
            return true;
        }
        
    }

    public function connecter()
    {
        if ($this->request->is('post')) {
            $user = $this->Auth->identify();
            if ($user) {
                if($user['actif']==1){
                    $this->Auth->setUser($user);
                    return $this->loginRedirect($user['profiles_id']);
                }else $this->Flash->error('Vos accès ont été désactivés. Merci de contacter votre administrateur.');
            } else $this->Flash->error('Votre email ou mot de passe est incorrect. Merci de réessayer.');
        }

    }

    public function loginRedirect($profil){
        if($profil==1) return $this->redirect(['prefix'=>'porteur','controller' => 'Dashbord', 'action' => 'index']); 
        else if($profil==2) return $this->redirect(['prefix'=>'structure','controller' => 'Dashbord', 'action' => 'index']);
        else if($profil==3) return $this->redirect(['prefix'=>'operateur','controller' => 'Dashbord', 'action' => 'index']);
        else if($profil==4) return $this->redirect(['prefix'=>'validateur','controller' => 'Dashbord', 'action' => 'index']);
        else if($profil==5) return $this->redirect(['prefix'=>'comite','controller' => 'Dashbord', 'action' => 'index']);
        else if($profil==6) return $this->redirect(['prefix'=>'superviseur','controller' => 'Dashbord', 'action' => 'index']);
    }

    public function deconnexion()
    {
        return $this->redirect($this->Auth->logout());
    }

    public function test() {
        
    }

    public function resetPassword($email){
        $this->autoRender = false;

        $user = $this->Users->find('all',[
            'conditions' => ['Users.email' => $email]
        ])->first();
        $rep = null;
        if($user) {
            
            $code = $this->genererCode(4);
            $pwd = $code;
            $user->password = $pwd;

            //Envoi de la notification
            $objet = "Réinitialisation du mot de passe";

            $msg = "Bonjour, <br>"
                    ."Vous avez oublé votre mot de passe ?"
                    ."<div class='panel' style='background-color: #ccc'>"
                    ."Un nouveau mot de passe vous a été envoyé : <b>".$pwd."</b><br>"
                    ."PS : Pensez à modifier votre mot de passe à la première connexion.<br><br>";

            //$this->sendEmail($objet,[$email],$msg);
            $this->Users->save($user);
            
            $rep='ok';
        }else $rep='ko';
        echo json_encode($rep);
    }

    //make sure to sanitize the uploaded file name because sometimes it causes major problems.
    //Never trust on the user input. This sanitize() function will remove the extra characters 
    //(you can say bad characters) and replace them with dash(-)
    function sanitize($string, $force_lowercase = true, $anal = false) {
        $strip = array("~", "`", "!", "@", "#", "$", "%", "^", "&", "*", "(", ")", "_", "=", "+", "[", "{", "]","}", "\\", "|", ";", ":", "\"", "'", "&#8216;", "&#8217;", "&#8220;", "&#8221;", "&#8211;", "&#8212;","â€”", "â€“", ",", "<",">", "/", "?");
        $clean = trim(str_replace($strip, "", strip_tags($string)));
        $clean = preg_replace('/\s+/', "-", $clean);
        $clean = ($anal) ? preg_replace("/[^a-zA-Z0-9]/", "", $clean) : $clean ;
        return ($force_lowercase) ?
            (function_exists('mb_strtolower')) ?
                mb_strtolower($clean, 'UTF-8') :
                strtolower($clean) :
            $clean;
    }


}
