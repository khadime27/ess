<?php
namespace App\Controller\Porteur;

use App\Controller\AppController;


class LabelisationsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {

    }

    public function nonsoumis() {
        $this->loadModel('Dossiers');
        $userConnect = $this->getUser();
        $idPorteur = $userConnect->porteurs[0]->id;
        $dossiers = $this->Dossiers->find('All', [
            'contain' => ['NatureDemandes', 'Porteurs', 'Filieres', 'Communes', 'Agents'],
            'conditions' => [
                'OR' => [
                    ['Dossiers.porteur_id' => $idPorteur, 'Dossiers.soumis' => 'non', 'Dossiers.statut_dossiers_id IS' => null, 'Dossiers.demande_label' => true],
                    ['Dossiers.porteur_id' => $idPorteur, 'Dossiers.statut_dossiers_id' => 5, 'Dossiers.demande_label' => true]
                ]
            ]
        ])->toArray();

        $this->set(compact('dossiers'));
    }

    public function notraites() {
        $this->loadModel('Dossiers');
        $userConnect = $this->getUser();
        $idPorteur = $userConnect->porteurs[0]->id;
        $dossiers = $this->Dossiers->find('All', [
            'contain' => ['NatureDemandes', 'Porteurs', 'Filieres', 'Communes', 'Agents'],
            'conditions' => ['Dossiers.porteur_id' => $idPorteur, 'Dossiers.soumis' => 'oui', 'Dossiers.statut_dossiers_id IS' => null, 'Dossiers.demande_label' => true]
        ])->toArray();

        $this->set(compact('dossiers'));
    }

    public function encours() {
        $this->loadModel('Dossiers');
        $userConnect = $this->getUser();
        $idPorteur = $userConnect->porteurs[0]->id;
        $dossierSoumis = $this->Dossiers->find('All', [
            'contain' => ['NatureDemandes', 'Porteurs', 'Filieres', 'Communes', 'Agents'],
            'conditions' => ['Dossiers.porteur_id' => $idPorteur, 'Dossiers.statut_dossiers_id >=' => 1, 'Dossiers.demande_label' => true]
        ])->toArray();

        $dossiers = [];
        foreach($dossierSoumis as $dossier) {
            if($dossier->statut_dossiers_id <= 4) $dossiers[] = $dossier;
        }

        $this->set(compact('dossiers'));
    }

    public function acceptes() {
        $this->loadModel('Dossiers');
        $userConnect = $this->getUser();
        $idPorteur = $userConnect->porteurs[0]->id;
        $dossiers = $this->Dossiers->find('All', [
            'contain' => ['NatureDemandes', 'Porteurs', 'Filieres', 'Communes', 'Agents'],
            'conditions' => ['Dossiers.porteur_id' => $idPorteur, 'Dossiers.statut_dossiers_id' => 6, 'Dossiers.demande_label' => true]
        ])->toArray();

        $this->set(compact('dossiers'));
    }

    public function rejetes() {
        $this->loadModel('Dossiers');
        $userConnect = $this->getUser();
        $idPorteur = $userConnect->porteurs[0]->id;
        $dossiers = $this->Dossiers->find('All', [
            'contain' => ['NatureDemandes', 'Porteurs', 'Filieres', 'Communes', 'Agents'],
            'conditions' => ['Dossiers.porteur_id' => $idPorteur, 'Dossiers.statut_dossiers_id' => 5, 'Dossiers.demande_label' => true]
        ])->toArray();

        $this->set(compact('dossiers'));
    }

    

}
