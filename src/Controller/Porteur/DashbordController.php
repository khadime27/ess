<?php
namespace App\Controller\Porteur;

use App\Controller\AppController;

/**
 * Dashbord Controller
 *
 *
 * @method \App\Model\Entity\Dashbord[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class DashbordController extends AppController
{

    public function index()
    {
        $this->demandesLabelises();
        $this->labelisationsEncours();
        $this->labelisationsAcceptes();
        $this->labelisationsRefuses();
        $this->labelisationsNonSoumis();
    }

    public function demandesLabelises() {
        $this->loadModel('Dossiers');
        $userConnect = $this->getUser();
        $idPorteur = $userConnect->porteurs[0]->id;
        $dossiers = $this->Dossiers->find('All', [
            'contain' => ['NatureDemandes', 'Porteurs', 'Filieres', 'Communes', 'Agents'],
            'conditions' => ['Dossiers.porteur_id' => $idPorteur, 'Dossiers.soumis' => 'oui', 'Dossiers.statut_dossiers_id IS' => null, 'Dossiers.demande_label' => true]
        ])->toArray();

        $nbNoTraite = count($dossiers);

        $this->set(compact('nbNoTraite'));
    }

    public function labelisationsNonSoumis() {
        $this->loadModel('Dossiers');
        $userConnect = $this->getUser();
        $idPorteur = $userConnect->porteurs[0]->id;
        $dossiers = $this->Dossiers->find('All', [
            'contain' => ['NatureDemandes', 'Porteurs', 'Filieres', 'Communes', 'Agents'],
            'conditions' => [
                'OR' => [
                    ['Dossiers.porteur_id' => $idPorteur, 'Dossiers.soumis' => 'non', 'Dossiers.statut_dossiers_id IS' => null, 'Dossiers.demande_label' => true],
                    ['Dossiers.porteur_id' => $idPorteur, 'Dossiers.statut_dossiers_id' => 5, 'Dossiers.demande_label' => true]
                ]
            ]
        ])->toArray();

        $nbNoSoumis = count($dossiers);

        $this->set(compact('nbNoSoumis'));
    }

    public function labelisationsEncours() {
        $this->loadModel('Dossiers');
        $userConnect = $this->getUser();
        $idPorteur = $userConnect->porteurs[0]->id;
        $dossiers = $this->Dossiers->find('All', [
            'contain' => ['NatureDemandes', 'Porteurs', 'Filieres', 'Communes', 'Agents'],
            'conditions' => ['Dossiers.porteur_id' => $idPorteur, 'Dossiers.statut_dossiers_id <= ' => 4, 'Dossiers.demande_label' => true]
        ])->toArray();

        $nbEncours = count($dossiers);

        $this->set(compact('nbEncours'));
    }

    public function labelisationsAcceptes() {
        $this->loadModel('Dossiers');
        $userConnect = $this->getUser();
        $idPorteur = $userConnect->porteurs[0]->id;
        $dossiers = $this->Dossiers->find('All', [
            'contain' => ['NatureDemandes', 'Porteurs', 'Filieres', 'Communes', 'Agents'],
            'conditions' => ['Dossiers.porteur_id' => $idPorteur, 'Dossiers.statut_dossiers_id' => 6, 'Dossiers.demande_label' => true]
        ])->toArray();

        $nbAccepte = count($dossiers);

        $this->set(compact('nbAccepte'));
    }

    public function labelisationsRefuses() {
        $this->loadModel('Dossiers');
        $userConnect = $this->getUser();
        $idPorteur = $userConnect->porteurs[0]->id;
        $dossiers = $this->Dossiers->find('All', [
            'contain' => ['NatureDemandes', 'Porteurs', 'Filieres', 'Communes', 'Agents'],
            'conditions' => ['Dossiers.porteur_id' => $idPorteur, 'Dossiers.statut_dossiers_id' => 5, 'Dossiers.demande_label' => true]
        ])->toArray();

        $nbRefuse = count($dossiers);

        $this->set(compact('nbRefuse'));
    }

}
