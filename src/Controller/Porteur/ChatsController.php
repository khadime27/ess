<?php
namespace App\Controller\Porteur;

use App\Controller\AppController;

/**
 * Chats Controller
 *
 * @property \App\Model\Table\ChatsTable $Chats
 *
 * @method \App\Model\Entity\Chat[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ChatsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->loadModel('Messages');

        $userConnect = $this->getUser();
        $this->loadModel('Dossiers');

        $dossiersSameActivities = $this->Dossiers->find('all', [
            'contain' => ['Porteurs.Users'],
            'conditions' => ['Porteurs.user_id != ' => $userConnect->id],
            'group' => ['Porteurs.id']
        ]);
        
        $this->loadModel('Messages');
        $chats = $this->Messages->find('all', [
            'contain' => ['Dossiers'],
            'conditions' => ['Dossiers.porteur_id' => $userConnect->porteurs[0]->id]
        ]);
        $this->loadModel('Agents');
        $agents = $this->Agents->find('all', [
            'contain' => ['Users', 'Agences'],
            'conditions' => ['Agents.admin' => true]
        ]);

        $this->getElementsFiltre();

        if ($this->request->is('post')) {
            $this->trieDossier();
        }else {
            $this->set(compact('dossiersSameActivities', 'chats', 'agents'));

        }

    }

    public function getElementsFiltre() {
        $this->loadModel('Filieres');
        $filieres = $this->Filieres->find('all');

        $this->loadModel('Regions');
        $regions = $this->Regions->find('all');

        $this->set(compact('filieres', 'regions'));
    }

    public function trieDossier() {
        $userConnect = $this->getUser();
        $chats = []; $agents = [];
        $dossiersSameActivities = [];

        if ($this->request->is('post')) {
            $temp = $this->request->getData();
            $idFiliere = $temp['filiere_id'];
            $idCommune = null;
            if(array_key_exists('commune_id', $temp)) $idCommune = $temp['commune_id'];
            
            $idDepartement = $temp['departement_id'];
            $idRegion = $temp['region_id'];
            $this->loadModel('Dossiers');

            $tabConditions = '';
            if($idFiliere != null && $idRegion == null) 
                $tabConditions = ['Porteurs.user_id != ' => $userConnect->id, 'Dossiers.filiere_id' => $idFiliere];
            elseif($idFiliere != null && $idRegion != null && $idDepartement == null) 
                $tabConditions = ['Porteurs.user_id != ' => $userConnect->id, 'Dossiers.filiere_id' => $idFiliere, 'Departements.region_id' => $idRegion];
            elseif($idFiliere != null && $idDepartement != null && $idCommune == null) 
                $tabConditions = ['Porteurs.user_id != ' => $userConnect->id, 'Dossiers.filiere_id' => $idFiliere, 'Communes.departement_id' => $idDepartement];
            elseif($idFiliere != null && $idCommune != null) 
                $tabConditions = ['Porteurs.user_id != ' => $userConnect->id, 'Dossiers.filiere_id' => $idFiliere, 'Dossiers.commune_id' => $idCommune];
            elseif($idFiliere == null && $idRegion != null && $idDepartement == null) 
                $tabConditions = ['Porteurs.user_id != ' => $userConnect->id, 'Departements.region_id' => $idRegion];
            elseif($idFiliere == null && $idDepartement != null && $idCommune == null) 
                $tabConditions = ['Porteurs.user_id != ' => $userConnect->id, 'Communes.departement_id' => $idDepartement];
            elseif($idFiliere == null && $idCommune != null) 
                $tabConditions = ['Porteurs.user_id != ' => $userConnect->id, 'Dossiers.commune_id' => $idCommune];
            
            $dossiersSameActivities = $this->Dossiers->find('all', [
                'contain' => ['Porteurs.Users', 'Communes.Departements'],
                'conditions' => $tabConditions,
                'group' => ['Porteurs.id']
            ]);


            $this->loadModel('Messages');
            $chats = $this->Messages->find('all', [
                'contain' => ['Dossiers'],
                'conditions' => ['Dossiers.porteur_id' => $userConnect->id]
            ]);

            $this->loadModel('Agents');
            $agents = $this->Agents->find('all', [
                'contain' => ['Users', 'Agences'],
                'conditions' => [
                    'OR' => [
                        ['Agences.regions_id' => $idRegion, 'Agents.admin' => true],
                        ['Users.profiles_id' => 2]
                    ]
                ]
            ]);

            $departementSelect = null; $communeSelect = null;
            if($idDepartement) {
                $this->loadModel('Departements');
                $departementSelect = $this->Departements->get($idDepartement);
            }
            
            if($idCommune) {
                $this->loadModel('Communes');
                $communeSelect = $this->Communes->get($idCommune);
            }

            
        }

        $this->set(compact('dossiersSameActivities', 'agents', 'chats', 'idFiliere', 'idRegion', 'departementSelect', 'communeSelect'));
    }

}