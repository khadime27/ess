<?php
namespace App\Controller\Porteur;

use App\Controller\AppController;

/**
 * SituationsComptes Controller
 *
 * @property \App\Model\Table\SituationsComptesTable $SituationsComptes
 *
 * @method \App\Model\Entity\SituationsCompte[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class SituationsComptesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Dossiers']
        ];
        $situationsComptes = $this->paginate($this->SituationsComptes);

        $this->set(compact('situationsComptes'));
    }

    /**
     * View method
     *
     * @param string|null $id Situations Compte id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $situationsCompte = $this->SituationsComptes->get($id, [
            'contain' => ['Dossiers']
        ]);

        $this->set('situationsCompte', $situationsCompte);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $situationsCompte = $this->SituationsComptes->newEntity();
        if ($this->request->is('post')) {
            $datas = $this->request->getData('situations_comptes');
            $lignesErrors = [];
            foreach($datas as $i => $temp) {
                if(!array_key_exists('idSituation', $temp))
                    $situationsCompte = $this->SituationsComptes->newEntity();
                else
                    $situationsCompte = $this->SituationsComptes->get($temp['idSituation']);
                    
                $situationsCompte = $this->SituationsComptes->patchEntity($situationsCompte, $temp);
                if (!$this->SituationsComptes->save($situationsCompte)) {
                    $lignesErrors[] = $i+1;
                }
            }
            if($lignesErrors == []) $this->Flash->success(__("Enregistrement bien effectué !"));
            else $this->Flash->error(__("Erreur d'enregistrement de l'engagement pour les lignes : ".implode(", ", $lignesErrors)));
        
            return $this->redirect($this->referer());
        }
        $this->set(compact('situationsCompte'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Situations Compte id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $situationsCompte = $this->SituationsComptes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $situationsCompte = $this->SituationsComptes->patchEntity($situationsCompte, $this->request->getData());
            if ($this->SituationsComptes->save($situationsCompte)) {
                $this->Flash->success(__('The situations compte has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The situations compte could not be saved. Please, try again.'));
        }
        $dossiers = $this->SituationsComptes->Dossiers->find('list', ['limit' => 200]);
        $this->set(compact('situationsCompte', 'dossiers'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Situations Compte id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $situationsCompte = $this->SituationsComptes->get($id);
        if ($this->SituationsComptes->delete($situationsCompte)) {
            $this->Flash->success(__('The situations compte has been deleted.'));
        } else {
            $this->Flash->error(__('The situations compte could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
