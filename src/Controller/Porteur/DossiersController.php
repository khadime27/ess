<?php

namespace App\Controller\Porteur;

use App\Controller\AppController;
use Cake\Mailer\Email;
use Cake\ORM\TableRegistry;
use \Mailjet\Resources;
use Cake\Filesystem\File;

/**
 * Dossiers Controller
 *
 * @property \App\Model\Table\DossiersTable $Dossiers
 *
 * @method \App\Model\Entity\Dossier[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class DossiersController extends AppController {

  public function index() {
    $dataUser = $this->getUser();
    $id = $dataUser->porteurs[0]->id;
    $this->paginate = [
      'contain' => ['Porteurs', 'Filieres', 'Communes', 'StatutDossiers'],
      'conditions' => ['porteur_id' => $id]
    ];

    $dossiers = $this->paginate($this->Dossiers);
    $porteurs = $this->Dossiers->Porteurs->find('list', ['limit' => 200]);
    $filieres = $this->Dossiers->Filieres->find('list', ['limit' => 200]);
    $communes = $this->Dossiers->Communes->find('list', ['limit' => 200]);
    $this->loadModel('Departements');
    $this->loadModel('Regions');
    $departements = $this->Departements->find('list', ['limit' => 200]);
    $regions = $this->Regions->find('list', ['limit' => 200]);
   // dd($dossiers);
    $this->set(compact('dossiers', 'porteurs', 'regions', 'departements', 'filieres', 'communes'));
  }

  /**
   * View method
   *
   * @param string|null $id Dossier id.
   * @return \Cake\Http\Response|void
   * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
   */
  public function view($id = null) {

      $dossier = $this->Dossiers->get($id, [
          'contain' => ['Porteurs', 'Filieres',
            'Communes.Departements.Regions','Porteurs.Entreprises.TypeEntreprises', 
            'Porteurs.Entreprises.Communes.Departements.Regions', 'StatutDossiers'
          ]
      ]);
      $checkEntreprise = false;
      if($dossier->porteur->entreprise) $checkEntreprise = true;

      $checkAssociation = false;
      if($dossier->porteur->entreprise && $dossier->porteur->entreprise->association) $checkAssociation = true;

      $this->loadModel('DossiersFilieres');
      $dossiersFilieres = $this->DossiersFilieres->find('all', [
        'contain' => ['Filieres'],
        'conditions' => ['dossiers_id' => $dossier->id]
      ])->toArray();

      $this->loadModel('DossiersImpacts');
      $dossiersImpacts = $this->DossiersImpacts->find('all', [
        'contain' => ['TypesImpacts.Impactes'],
        'conditions' => ['dossiers_id' => $dossier->id]
      ])->toArray();

      $this->loadModel('PieceJointes');
      $pieces = $this->PieceJointes->find('all', [
        'conditions' => ['dossier_id' => $dossier->id]
      ])->toArray();

      $this->loadModel('QualificationDossiers');
      $qualificationRejet = $this->QualificationDossiers->find('all', [
        'conditions' => ['dossiers_id' => $dossier->id, 'raison_rejet IS NOT' => null]
      ])->first();

      $this->loadModel('Notations');
      $notationRejet = $this->Notations->find('all', [
        'conditions' => ['dossiers_id' => $dossier->id, 'raison_rejet IS NOT' => null]
      ])->first();
      

      $this->set(compact('dossier', 'checkEntreprise', 'checkAssociation', 'qualificationRejet', 'notationRejet', 'dossiersFilieres', 'dossiersImpacts', 'pieces'));

  }

  public function mediation($id = null) {
    $dataUser = $this->getUser();
    $_SESSION['finance_id'] = $id;
    $this->loadModel('Mediations');
    $mediations = $this->Mediations->find('All', [
        'conditions' => ['Mediations.financement_credit_id' => $id],
        'contain' => ['PieceJointes', 'Status'],
        'order' => ['Mediations.created' => 'DESC']
    ]);
    $this->loadModel('Status');
    $status = $this->Status->find('list', ['limit' => 200]);
    $this->set(compact('mediations', 'status', 'dataUser'));
  }

  public function addMediation() {
    $id_financement = $_SESSION['finance_id'];
    $this->loadModel('Mediations');
    $this->loadModel('Piece_jointes');
    $mediation = $this->Mediations->newEntity();
    if ($this->request->is('post')) {
      $document = $this->request->getData('url');
      $id = $id_financement;
      $table = 'Piece_jointes';
      $mediation->financement_credit_id = $id_financement;
      $mediation->description = $this->request->getData('description');
      $mediation->titre = $this->request->getData('titre');
      $mediation->statu_id = $this->request->getData('statu_id');
      if ($this->Mediations->save($mediation)) {
        $this->DocMediation($document, $table, $mediation->id);
        return $this->redirect($this->referer());
      }
    }
    $this->loadModel('Status');
    $status = $this->Status->find('list', ['limit' => 200]);
    $this->set(compact('status'));
  }

  public function DocMediation($document, $table, $id_mediation) {
    $result = false;
    $errors = array();
    $extensions = array("jpeg", "jpg", "gif", "png", "pdf", "docx");
    $bytes = 1024;
    $allowedKB = 10000;
    $totalBytes = $allowedKB * $bytes;
    $uploadThisFile = true;
    $file_name = $document['name'];
    $file_tmp = $document['tmp_name'];

    $ext = pathinfo($file_name, PATHINFO_EXTENSION);
    if (!in_array(strtolower($ext), $extensions)) {
      array_push($errors, "Le nom du fichier est invalide " . $file_name);
      $uploadThisFile = false;
    }

    if ($document['size'] > $totalBytes) {
      array_push($errors, "la taille du fichier ne doit pas depasser 100KB. Name:- " . $file_name);
      $uploadThisFile = false;
    }

    $target_dir = WWW_ROOT . DS . "documents" . DS . "dossier" . DS;
    if (!file_exists($target_dir)) {
      mkdir($target_dir, 0777, true);
    }
    if ($uploadThisFile) {
      $newFileName = $this->genererCode(5) . '.' . $ext;
      $data = $this->$table->newEntity();
      $data->url = $newFileName;
      $data->mediation_id = $id_mediation;
      $desti_file = $target_dir . $newFileName;
      move_uploaded_file($file_tmp, $desti_file);

      $test = $this->$table->save($data);

      $result = true;
    }
    return $result;
  }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add(){
        $nbr_dossier = $this->countdossier();
        $checkResult = 0;

        $dataUser = $this->getUser();
        $checkEntreprise = false;
        if($dataUser->porteurs[0]->entreprise != null) $checkEntreprise = true;

        $checkAssociation = false;
        if($dataUser->porteurs[0]->entreprise != null) {
          if($dataUser->porteurs[0]->entreprise->association) $checkAssociation = true;
        }

        $dataUser = $this->getUser();
        $dossier = $this->Dossiers->newEntity();
        $entreprise = null;
        //dd($dataUser->porteurs[0]->prenom);
        $dossiersImpacts = null; $secteursInteresses = null; $secteursEnRelations = null;
        if ($this->request->is('post')) {
            if($this->request->getData('code-dossier') != null) {
              $dossier = $this->getDossierByCode($this->request->getData('code-dossier'));


              if($dossier != null) {
                $this->loadModel('DossiersImpacts');
                $dossiersImpacts = $this->DossiersImpacts->find('all', [
                  'conditions' => ['dossiers_id' => $dossier->id]
                ]);
                $this->loadModel('DossiersFilieres');
                $secteursInteresses = $this->DossiersFilieres->find('all', [
                  'conditions' => ['dossiers_id' => $dossier->id, 'link_type' => 'interesse']
                ]);
                $secteursEnRelations = $this->DossiersFilieres->find('all', [
                  'conditions' => ['dossiers_id' => $dossier->id, 'link_type' => 'relation']
                ]);
              } else {
                $this->Flash->error(__("Veuillez entrer un code correct !"));
                return $this->redirect(['action' => 'add']);
              }

            } 
            else {
              if ($nbr_dossier <= 5) {
                  $soumission = 'ok';
                  $idDossier = $this->request->getData('idDossier');
                  if($this->request->getData('renew') == '1') $dossier = $this->Dossiers->get($idDossier);
                  else $dossier = $this->Dossiers->newEntity();

                  
                  $dossier->intitule = $this->request->getData('intitule');
                  $dossier->filiere_id = (int)$this->request->getData('filiere_id');
                  $dossier->descrip_projet = $this->request->getData('descrip_projet');
                  $nom = $dataUser->porteurs[0]->prenom ." ".$dataUser->porteurs[0]->nom;
                  $dossier->demande_label = true;
                  $dossier->nb_perso_impact = (int)$this->request->getData('nb_perso_impact');
                  $dossier->emisson = false;
                  
                  $num_identification = $this->request->getData('num_identification');
                  $this->updatePorteur($num_identification, $entreprise, $dataUser->porteurs[0]->id);
                  
                  
                  $commun = (int)$this->request->getData('nomcom');
                  $depart_id = $this->request->getData('departement_id');
                  
                  $docs = $this->request->getData('fields');
                  $url = $this->request->getData('url');
                  
                  
                  if($checkEntreprise){
                    $this->loadModel('Entreprises');
                    
                    $entreprise = $this->Entreprises->find('all', [
                      'conditions' => ['id' => $dataUser->porteurs[0]->entreprise->id]
                    ])->first();
                    $entreprise->ninea = $this->request->getData('ninea');
                    $entreprise->nom = $this->request->getData('nomEntreprise');
                    $entreprise->type_entreprise_id = $this->request->getData('type_entreprise_id');
                    $entreprise->nb_employe = $this->request->getData('nb_employe');
                    $entreprise->immatriculation = $this->request->getData('immatriculation');
                    if($this->request->getData('statut-juridique') == "Formel") $entreprise->formel = true;
                    else $entreprise->formel = false;
                    
                    if($this->Entreprises->save($entreprise)) {
                        $this->updatePorteur($num_identification, $entreprise->id, $dataUser->porteurs[0]->id);
                    }
                  }


                  $code = null;
                  if($this->request->getData('renew') == '0') {
                    if($checkEntreprise) {
                        $code = str_replace(' ', '', $this->request->getData('nomEntreprise'))."-".$this->genererCode(4);
                    }else {
                        $code = str_replace(' ', '', $dataUser->porteurs[0]->prenom)."-".$this->genererCode(4);
                    }
                    $dossier->numero = $code;
                  }
                  $dossier->porteur_id = $dataUser->porteurs[0]->id;
                  
                  $dossier->soumis = 'non';
                  $dossier->commune_id = $commun;
                  
                  $dossier->label = false;
                  $dossier->statut_dossiers_id = null;


                  if($this->Dossiers->save($dossier)) {
                      $impacteSociales = $this->request->getData('impacts_sociales');
                      $impacteEnvirones = $this->request->getData('impacts_environes');
                      $this->saveImpactes($dossier->id, $impacteSociales, $impacteEnvirones);

                      $secteursInteresses = $this->request->getData('secteurs_interesses');
                      $secteursEnRelations = $this->request->getData('secteurs_relations');
                      $this->saveLinksSecteurs($dossier->id, $secteursInteresses, $secteursEnRelations);
                      
                      $resul = true;
                      $table = 'Dossiers';
                      $result = $this->addDoc($docs, $dossier->id, $url);
                      if ($result or $resul) {
                          $this->Flash->success(__('Votre dossier a été créé avec succés. Votre numéro de dossier vous a été envoyé sur votre boite mail.'));
                          $checkResult = 1;
                          // $this->sendEmail($dataUser->email, $code, $nom);
                          // return $this->redirect(['controller' => 'Dossiers', 'action' => 'dashboard']);
                          return $this->redirect(['controller' => 'Dossiers', 'action' => 'view', $dossier->id]);
                      } else {
                          $this->Flash->error(__('Le dossier n\'a pu être ajouté . Merci de réessayer'));
                      }
                  } else {
                      $this->Flash->error(__("Erreur d'enregistrement du dossier."));
                  }
                  
              } else {
                  $soumission = 'ko';
                  $this->Flash->error(__('Désolé vous ne pouvez pas soumettre plus de deux projets'));
              }
            }
        }

        $porteurs = $this->Dossiers->Porteurs->find('all');
        $filieres = $this->Dossiers->Filieres->find('all');
        $communes = $this->Dossiers->Communes->find('all');
        $this->loadModel('Departements');
        $this->loadModel('Regions');
        $departements = $this->Departements->find('all');
        $regions = $this->Regions->find('all');
        $this->loadModel('TypeEntreprises');
        $typeEntreprises = $this->TypeEntreprises->find('all');
        

        $this->loadModel('TypesPieces');
        $typesPieces = $this->TypesPieces->find('all');


        $this->loadModel('TypesImpacts');
        $typesImpacts = $this->TypesImpacts->find('all');

        $this->loadModel('Filieres');
        $filieres = $this->Filieres->find('all');

        $this->set(compact('dossier', 'dossiersImpacts', 'checkResult', 'soumission', 'secteursEnRelations', 'secteursInteresses', 'typesPieces', 'checkEntreprise', 'porteurs', 'checkAssociation', 'typesImpacts', 'filieres', 'regions', 'departements', 'filieres', 'communes', 'num_identification','dataUser', 'typeEntreprises'));
    }

    public function countdossier() {
      $dataUser = $this->getUser();
      $id = $dataUser->porteurs[0]->id;
      $nb_dos_porteur = $this->Dossiers->find('All', [
                  'contain' => ['Porteurs', 'Filieres', 'Communes', 'StatutDossiers'],
                  'conditions' => ['Dossiers.porteur_id' => $id]
              ])->count();
      return $nb_dos_porteur;
    }

    // Fonction permettant d'enregistrer les types d'impacts en relation avec le dossier
    public function saveImpactes($idDossier = null, $impacteSociales = [], $impacteEnvirones = []) {
        $this->loadModel('DossiersImpacts');
        $this->deleteImpactIfExist($idDossier);
        
        foreach($impacteSociales as $impacteId) {
            $dossierImpact = $this->DossiersImpacts->newEntity();
            $dossierImpact->dossiers_id = $idDossier;
            $dossierImpact->types_impacts_id = $impacteId;
            $this->DossiersImpacts->save($dossierImpact);
        }
        foreach($impacteEnvirones as $impacteId) {
            $dossierImpact = $this->DossiersImpacts->newEntity();
            $dossierImpact->dossiers_id = $idDossier;
            $dossierImpact->types_impacts_id = $impacteId;
            $this->DossiersImpacts->save($dossierImpact);
        }
    }

    public function deleteImpactIfExist($idDossier = null) {
        $this->loadModel('DossiersImpacts');

        $dossiersImpacts = $this->DossiersImpacts->find('all', [
            'conditons' => ['dossiers_id' => $idDossier]
        ]);
        foreach($dossiersImpacts as $dossierImpact) {
            if($dossierImpact->dossiers_id == $idDossier){
                $this->DossiersImpacts->delete($dossierImpact);
            } 
        }
    }

    // Fonction permettant d'enregistrer les secteurs en relation (ou intéressé) avec le projet
    public function saveLinksSecteurs($idDossier = null, $secteursInteresses = [], $secteursEnRelations = []) {
        $this->loadModel('DossiersFilieres');
        $this->deleteFiliereIfExist($idDossier);
        
        foreach($secteursInteresses as $idSecteur) {
            $dossierFiliere = $this->DossiersFilieres->newEntity();
            $dossierFiliere->dossiers_id = $idDossier;
            $dossierFiliere->filieres_id = $idSecteur;
            $dossierFiliere->link_type = "interesse";
            $this->DossiersFilieres->save($dossierFiliere);
        }
        foreach($secteursEnRelations as $idSecteur) {
            $dossierFiliere = $this->DossiersFilieres->newEntity();
            $dossierFiliere->dossiers_id = $idDossier;
            $dossierFiliere->filieres_id = $idSecteur;
            $dossierFiliere->link_type = "relation";
            $this->DossiersFilieres->save($dossierFiliere);
        }
    }

    public function deleteFiliereIfExist($idDossier = null) {
        $this->loadModel('DossiersFilieres');

        $dossiersFilieres = $this->DossiersFilieres->find('all', [
            'conditons' => ['dossiers_id' => $idDossier]
        ]);
        foreach($dossiersFilieres as $dossierFiliere) {
            if($dossierFiliere->dossiers_id == $idDossier){
                $this->DossiersFilieres->delete($dossierFiliere);
            } 
        }
    }

  public function sendEmail($email_user, $code, $nom) {
    try{
        $mj = new \Mailjet\Client('3b795f369165a7a108363397cb73a61c', '00abdc94e2f7abdee5ff8ef6f65abcf8');
        $body = [
          'FromEmail' => "noreply@ess.com",
            'FromName' => "Plateforme ESS",
            'Subject' => "Numéro de Dossier",
            'Html-part' => "Bonjour ". $nom .", <br>
            Votre dossier a été soumis à notre équipe technique avec succès.<br>
            Votre numero de dossier est : " . $code. ".<br><br>
            Revenez régulièrement pour suivre votre dossier.<br>
            Ce message vous a été envoyé automatiquement.<br>
            Nous vous remercions de ne pas y répondre.<br>
            Pour toute demande, merci de contacter votre ingénieur de projet.<br><br>
            Equipe ESS.",
            'Recipients' => [
                [
                    'Email' => $email_user
                ]
            ]
        ];
        $response = $mj->post(Resources::$Email, ['body' => $body]);
     }catch(\Exception $e){
        // dd($e);
      }
  }

  public function SendNotif($email_user, $nom) {
     try{
        $mj = new \Mailjet\Client('3b795f369165a7a108363397cb73a61c', '00abdc94e2f7abdee5ff8ef6f65abcf8');
        $body = [
          'FromEmail' => "noreply@ess.com",
          'FromName' => "Plateforme ESS",
          'Subject' => "Nouveau projet",
          'Html-part' => " Bonjour Mr  " . $nom .", <br>
            Un nouveau dossier a été soumis. Merci de bien vouloir l’assigner à une agence ou à un OTS.<br>
            Ce message vous a été envoyé automatiquement.<br>
            Nous vous remercions de ne pas y répondre.<br><br>
            Equipe ESS.",
            'Recipients' => [
               ['Email' => $email_user]
            ]
           ];
            $response = $mj->post(Resources::$Email, ['body' => $body]);
          }catch(\Exception $e){
             // dd($e);
          }
  }

  /**
   * Edit method
   *
   * @param string|null $id Dossier id.
   * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
   * @throws \Cake\Network\Exception\NotFoundException When record not found.
   */
  public function supprimerImage($nom) {
    $path = WWW_ROOT . DS . "documents" . DS . "dossier" . DS;
    $file = new File($path . $nom);
    $file->delete();
  }

  public function piecejointes($id = null) {
    $this->loadModel('Piece_jointes');
    $pj = $this->Piece_jointes->find('All', [
        'conditions' => ['dossier_id' => $id]
    ]);
    //$this->set('pj', $pj);
    return $pj;
  }

  public function Activdossier($id = null) {
    $this->loadModel('Users');
    $date_soumis = date("Y-m-d H:i:s");
   
    $list_admins = $this->Users->find('All',[
      'conditions'=>['Users.profil'=>'admin','Users.type'=>'structure'],
      'contain'=> ["Agents"]
    ])->toArray();
   
    foreach($list_admins as $list_admin){
     $nom =$list_admin->agents[0]->prenom . " " .$list_admin->agents[0]->nom;
     $email_admin = "moussafall.dev@gmail.com";
     // $this->SendNotif($email_admin, $nom);
    }
   
    $this->Dossiers->updateAll(['soumis' => "oui", 'date_soumission' => $date_soumis], ['id' => $id]);
    return $this->redirect($this->referer());
  }
  
  public function updatePorteur($num_identification, $entreprise, $id_porteur){
      $this->loadModel('Porteurs');
      $this->Porteurs->updateAll(['num_identification' => $num_identification, 'entreprise_id' => $entreprise], ['id' => $id_porteur]);
  }

  public function modifpj() {
    $this->loadModel('Piece_jointes');
    $image = $this->request->getData('url');
    $id = $this->request->getData('id');
    $table = 'Piece_jointes';
    $val = $this->request->getData('titre');
    $pj = $this->Piece_jointes->get($id);
    $nom = $pj->url;
    $return = $this->ajoutDoc($image, $id, $table, $val, $nom);
    if ($return) {
      return $this->redirect($this->referer());
    }
  }

  public function addpj() {
    $this->loadModel('Piece_jointes');
    $document = $this->request->getData('url');
    $id = $this->request->getData('id');
    $table = 'Piece_jointes';
    $val = $this->request->getData('titre');
    $return = $this->ajoutDoc($document, $id, $table, $val);
    if ($return) {
      return $this->redirect($this->referer());
    }
  }

  public function ajoutDoc($document, $pj_id, $table, $val, $nom = null) {
    $result = false;
    $errors = array();
    $extensions = array("jpeg", "jpg", "gif", "png", "pdf", "docx");
    $bytes = 1024;
    $allowedKB = 10000;
    $totalBytes = $allowedKB * $bytes;
    $uploadThisFile = true;
    $file_name = $document['name'];
    $file_tmp = $document['tmp_name'];

    $ext = pathinfo($file_name, PATHINFO_EXTENSION);
    if (!in_array(strtolower($ext), $extensions)) {
      array_push($errors, "Le nom du fichier est invalide " . $file_name);
      $uploadThisFile = false;
    }

    if ($document['size'] > $totalBytes) {
      array_push($errors, "la taille du fichier ne doit pas depasser 100KB. Name:- " . $file_name);
      $uploadThisFile = false;
    }

    $target_dir = WWW_ROOT . DS . "documents" . DS . "dossier" . DS;
    if (!file_exists($target_dir)) {
      mkdir($target_dir, 0777, true);
    }
    if ($uploadThisFile) {
      $newFileName = $this->genererCode(5) . '-' . $pj_id . '.' . $ext;
      // dd($newFileName);
      if ($nom) {
        $data = $this->$table->get($pj_id);
        $this->supprimerImage($nom);
      } else {
        $data = $this->$table->newEntity();
        $data->dossier_id = $pj_id;
      }

      $data->url = $newFileName;
      $data->titre = $val;
      $desti_file = $target_dir . $newFileName;
      move_uploaded_file($file_tmp, $desti_file);

      $this->$table->save($data);
      $result = true;
    }
    return $result;
  }

  /**
   * Edit method
   *
   * @param string|null $id Promotion id.
   * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
   * @throws \Cake\Network\Exception\NotFoundException When record not found.
   */
  public function edit($id = null) {

      $dataUser = $this->getUser();
      $checkEntreprise = false;
      if($dataUser->porteurs[0]->entreprise != null) $checkEntreprise = true;

      $checkAssociation = false;
      if($dataUser->porteurs[0]->entreprise != null) {
        if($dataUser->porteurs[0]->entreprise->association) $checkAssociation = true;
      }

      $dataUser = $this->getUser();
      $dossier = $this->Dossiers->get($id, [
        'contain' => ['Porteurs', 'Porteurs.Entreprises', 'Filieres', 'Communes.Departements.Regions', 'PieceJointes']
      ]);

      $this->loadModel('TypesImpacts');
      $typesImpacts = $this->TypesImpacts->find('all');

      $this->loadModel('DossiersImpacts');
      $dossiersImpacts = $this->DossiersImpacts->find('all', [
        'conditions' => ['dossiers_id' => $dossier->id]
      ]);

      $this->loadModel('DossiersFilieres');
      $secteursInteresses = $this->DossiersFilieres->find('all', [
        'conditions' => ['dossiers_id' => $dossier->id, 'link_type' => 'interesse']
      ]);
      $secteursEnRelations = $this->DossiersFilieres->find('all', [
        'conditions' => ['dossiers_id' => $dossier->id, 'link_type' => 'relation']
      ]); 
    
      $entreprise = null;
      if ($this->request->is('post')) {
        
          
          $dossier->intitule = $this->request->getData('intitule');
          $dossier->filiere_id = (int)$this->request->getData('filiere_id');
          $dossier->descrip_projet = $this->request->getData('descrip_projet');
          $nom = $dataUser->porteurs[0]->prenom ." ".$dataUser->porteurs[0]->nom;
          $dossier->nb_perso_impact = (int)$this->request->getData('nb_perso_impact');
          
          $num_identification = $this->request->getData('num_identification');
          $this->updatePorteur($num_identification, $entreprise, $dataUser->porteurs[0]->id);
          
          
          $commun = (int)$this->request->getData('nomcom');
          $depart_id = $this->request->getData('departement_id');
          
          $docs = $this->request->getData('fields');
          $url = $this->request->getData('url');
          
          
          if($checkEntreprise){
            $this->loadModel('Entreprises');
            
            $entreprise = $this->Entreprises->find('all', [
              'conditions' => ['id' => $dataUser->porteurs[0]->entreprise->id]
            ])->first();
            $entreprise->ninea = $this->request->getData('ninea');
            $entreprise->nom = $this->request->getData('nomEntreprise');
            $entreprise->type_entreprise_id = $this->request->getData('type_entreprise_id');
            $entreprise->nb_employe = $this->request->getData('nb_employe');
            $entreprise->immatriculation = $this->request->getData('immatriculation');
            if($this->request->getData('statut-juridique') == "Formel") $entreprise->formel = true;
            else $entreprise->formel = false;
            
            if($this->Entreprises->save($entreprise)) {
                $this->updatePorteur($num_identification, $entreprise->id, $dataUser->porteurs[0]->id);
            }
          }

          $dossier->commune_id = $commun;


          if($this->Dossiers->save($dossier)) {
              $impacteSociales = $this->request->getData('impacts_sociales');
              $impacteEnvirones = $this->request->getData('impacts_environes');
              $this->saveImpactes($dossier->id, $impacteSociales, $impacteEnvirones);

              $secteursInteresses = $this->request->getData('secteurs_interesses');
              $secteursEnRelations = $this->request->getData('secteurs_relations');
              $this->saveLinksSecteurs($dossier->id, $secteursInteresses, $secteursEnRelations);
              
              $resul = true;
              $table = 'Dossiers';
              $result = $this->addDoc($docs, $dossier->id, $url);
              if ($result or $resul) {
                  $this->Flash->success(__("Modification bien effectuée !"));
                  return $this->redirect($this->referer());
              } else {
                  $this->Flash->error(__('Le dossier n\'a pu être enregistré . Merci de réessayer'));
              }
          } else {
              $this->Flash->error(__("Erreur d'enregistrement du dossier."));
          }

      }

      $porteurs = $this->Dossiers->Porteurs->find('all');
      $filieres = $this->Dossiers->Filieres->find('all');
      $communes = $this->Dossiers->Communes->find('all');
      $this->loadModel('Departements');
      $this->loadModel('Regions');
      $departements = $this->Departements->find('all');
      $regions = $this->Regions->find('all');
      $this->loadModel('TypeEntreprises');
      $typeEntreprises = $this->TypeEntreprises->find('all');

      $this->loadModel('TypesPieces');
      $typesPieces = $this->TypesPieces->find('all');


      $this->loadModel('Filieres');
      $filieres = $this->Filieres->find('all');

      $this->set(compact('dossier', 'id', 'dossiersImpacts', 'soumission', 'secteursEnRelations', 'secteursInteresses', 'typesPieces', 'checkEntreprise', 'porteurs', 'checkAssociation', 'typesImpacts', 'filieres', 'regions', 'departements', 'filieres', 'communes', 'num_identification','dataUser', 'typeEntreprises'));
  }

    /**
     * Delete method
     *
     * @param string|null $id Dossier id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $dossier = $this->Dossiers->get($id);
        if ($this->Dossiers->delete($dossier)) {
        $this->Flash->success(__('Le doosier a été supprimé.'));
        } else {
        $this->Flash->error(__('The dossier could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function addDoc($doc, $id, $url) {
        $file_count = count($doc);
        $result = false;
        $errors = array();
        $extensions = array("jpeg", "jpg", "gif", "png", "pdf", "docx");
        $bytes = 1024;
        $allowedKB = 100000;
        $totalBytes = $allowedKB * $bytes;
        $uploadThisFile = true;

        for ($i = 0; $i < $file_count; $i++) {
            $uploadThisFile = true;
            $file_name = $doc[$i]['name'];
            $file_tmp = $doc[$i]['tmp_name'];
            $titre_img = $url[$i];

            $ext = pathinfo($file_name, PATHINFO_EXTENSION);

            if (!in_array(strtolower($ext), $extensions)) {
                array_push($errors, "Le nom du fichier est invalide " . $file_name);
                $uploadThisFile = false;
            }

            if ($this->request->data['fields'][$i]['size'] > $totalBytes) {
                array_push($errors, "File size must be less than 100KB. Name:- " . $file_name);
                $uploadThisFile = false;
            }

            $target_dir = WWW_ROOT . DS . "documents" . DS . "dossier" . DS;
            if (!file_exists($target_dir)) {
                mkdir($target_dir, 0777, true);
            }
            if ($uploadThisFile) {
                $filename = basename($file_name, $ext);
                $newFileName = $filename . $ext;
                $desti_file = $target_dir . '/' . $newFileName;

                move_uploaded_file($file_tmp, $desti_file);
                $pjTable = TableRegistry::get('piece_jointes');
 
                $piece_jointe = $pjTable->newEntity();
                $piece_jointe->url = $newFileName;
                $piece_jointe->titre = $titre_img;
                $piece_jointe->dossier_id = $id;
                // dd($piece_jointe);
                $pjTable->save($piece_jointe);
            }
        }
        $count = count($errors);

        return $result;
    }

  public function dashboard() {
    $dataUser = $this->getUser();
    $id = $dataUser->porteurs[0]->id;
    $this->paginate = [
        'contain' => ['Porteurs', 'Filieres', 'Communes', 'StatutDossiers','Imputations'],
        'conditions' => ['Dossiers.porteur_id' => $id]
    ];
    $dossiers = $this->paginate($this->Dossiers);
    $porteurs = $this->Dossiers->Porteurs->find('list', ['limit' => 200]);
    $filieres = $this->Dossiers->Filieres->find('list', ['limit' => 200]);
    $communes = $this->Dossiers->Communes->find('list', ['limit' => 200]);
    $this->loadModel('Departements');
    $this->loadModel('Regions');
    $departements = $this->Dossiers->Communes->Departements->find('list', ['limit' => 200]);
    $regions = $this->Dossiers->Communes->Departements->Regions->find('list', ['limit' => 200]);
    // Les dossiers en brouillons
    $dossiersBrouillons = $this->paginate($this->Dossiers->find('all')
      ->where(['Dossiers.soumis' => 'non']));
    // Nombre de dossiers en brouillons
    $nbDossiersBrouillons = $dossiersBrouillons->count();
    // Nombre de sossiers
    $nbDossiers = $dossiers->count();
    // Les dossiers en Traitement
    $this->dossierTraitements();
   // dd($dossiersBrouillons);
    $this->set(compact('dossiers', 'porteurs', 'regions', 'departements', 'filieres', 'communes', 'dossiersClotures', 'dossiersBrouillons', 'nbDossiersClotures', 'nbDossiersBrouillons', 'nbDossiers'));
  }

  // Méthode pour récupérer les dossiers qui sont en traitement...
  public function dossierTraitements() {
    $dataUser = $this->getUser();
    $dossier_id = $dataUser->dossiers['0']->dossier_id;
    $id_porteur = $dataUser->porteurs[0]->id;
    $dossier_porteurs = $this->Dossiers->find('All', [
        'contain' => ['Assignations', 'Filieres','Imputations'],
        'conditions' => [
          'Dossiers.porteur_id' => $id_porteur
        ]
    ])->toArray();
    $nbDossier = [];
    
    foreach ($dossier_porteurs as $dossiers) {
      if (!empty($dossier->imputations) and $dossier->soumis == 'valide') {
        $nbDossier[] = $dossiers->imputations[0]->id;
      }
      if(empty($dossiers->imputations) and $dossiers->soumis == 'oui'){
        $nbDossierSmis[] = $dossier_porteurs;
      }
    }

    $nbDossierTraitements = count($nbDossier);
    $nbDossierSoumis = count($nbDossierSmis);
    $dossierStructures = $this->Dossiers->find('All', [
        'contain' => ['Assignations', 'Filieres'],
        'conditions' => ['Dossiers.porteur_id' => $id_porteur, 'Dossiers.soumis' => 'structuré']
    ]);

    $nbDossierStructures = $dossierStructures->count();
    $this->set(compact('dossier_porteurs', 'nbDossierTraitements','nbDossierSoumis', 'nbDossierStructures', 'dossierStructures'));
  }

  public function brouillons() {
    $this->dashboard();
  }

  public function traitements() {
    $this->dashboard();
  }

  public function soumis(){
    $this->dashboard();
  }

  public function structures() {
    $this->dashboard();
  }

  public function getDossierByCode($codeDossier = null) {
    $userConnect = $this->getUser();
    $idPorteur = $userConnect->porteurs[0]->id;
    $folder = $this->Dossiers->find('all', [
      'contain' => ['Porteurs', 'Porteurs.Entreprises', 'Filieres', 'Communes.Departements.Regions', 'PieceJointes'],
      'conditions' => ['Dossiers.porteur_id' => $idPorteur, 'Dossiers.numero' => $codeDossier, 'Dossiers.statut_dossiers_id' => 6]
    ])->first();

    
    return $folder;
  }

    public function soumettre($idDossier = nulll) {
        $dossier = $this->Dossiers->get($idDossier);
        $dossier->soumis = "oui";
        $dossier->statut_dossiers_id = null;

        if($this->Dossiers->save($dossier)) {
            $this->deleteElements($dossier);

            $this->Flash->success(__("La soumission a été bien effectuée. Votre dossier est en cours de traitement !"));
        }else {
            $this->Flash->error(__("Impossible de soumettre le dossier. Réessayez plutard !"));
        }
        return $this->redirect($this->referer());
        
    }
    
    public function deleteElements($dossier = null) {
        $this->loadModel('QualificationDossiers');
        $qualifications = $this->QualificationDossiers->find('all', [
          'conditions' => ['dossiers_id' => $dossier->id]
        ]);
        if($qualifications != null) {
          foreach($qualifications as $qualification) {
            $this->QualificationDossiers->delete($qualification);
          }
        }  

        $this->loadModel('Notations');
        $notations = $this->Notations->find('all', [
          'conditions' => ['dossiers_id' => $dossier->id]
        ]);
        if($notations != null) {
          foreach($notations as $notation){
            $this->Notations->delete($notation);
          }
        } 

        $this->loadModel('Assignations');
        $assignations = $this->Assignations->find('all', [
          'conditions' => ['dossier_id' => $dossier->id]
        ]);
        if($assignations != null) {
          foreach($assignations as $assignation) {
            $this->Assignations->delete($assignation);
          }
        }
    }

}
