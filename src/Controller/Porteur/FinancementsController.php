<?php

namespace App\Controller\Porteur;

use App\Controller\AppController;
use Cake\Mailer\Email;
use Cake\ORM\TableRegistry;
use \Mailjet\Resources;
use Cake\Filesystem\File;

/**
 * Dossiers Controller
 *
 * @property \App\Model\Table\DossiersTable $Dossiers
 *
 * @method \App\Model\Entity\Dossier[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class FinancementsController extends AppController {

    public function demander($idDossier = null) {
        $this->loadModel('Dossiers');
        $dossier = $this->Dossiers->get($idDossier);
        $dossier->statut_financements_id = 1; // Financement soumis

        if($this->Dossiers->save($dossier)) {
            $this->Flash->success(__("La soumission a été bien effectuée. Votre dossier est en cours de traitement !"));
        }else {
            $this->Flash->error(__("Impossible de soumettre le dossier. Réessayez plutard !"));
        }
        return $this->redirect($this->referer());
    }

    public function getDossiers($statut = null) {
        $this->loadModel('Dossiers');
        $userConnect = $this->getUser();
        $idPorteur = $userConnect->porteurs[0]->id;
        $dossiers = $this->Dossiers->find('All', [
            'contain' => ['NatureDemandes', 'Porteurs', 'Filieres', 'Communes', 'Agents'],
            'conditions' => ['Dossiers.porteur_id' => $idPorteur, 'Dossiers.statut_financements_id' => $statut]
        ])->toArray();

        return $dossiers;
        
    }

    public function nontraites() {
        $dossiers = $this->getDossiers(1); 

        $this->set(compact('dossiers'));
    }

    public function encours() {
        $this->loadModel('Dossiers');
        $userConnect = $this->getUser();
        $idPorteur = $userConnect->porteurs[0]->id;
        $dossiersEnCours = $this->Dossiers->find('All', [
            'contain' => ['NatureDemandes', 'Porteurs', 'Filieres', 'Communes', 'Agents'],
            'conditions' => ['Dossiers.porteur_id' => $idPorteur, 'Dossiers.statut_financements_id >= ' => 2]
        ])->toArray();

        $dossiers = [];
        foreach($dossiersEnCours as $dossier) {
            if($dossier->statut_financements_id <= 4) $dossiers[] = $dossier; // Les dossiers en cours sont les statuts 2, 3 et 4
        }

        $this->set(compact('dossiers'));
    }
    
    public function acceptes() {
        $dossiers = $this->getDossiers(6);
        $this->set(compact('dossiers'));
    }

    public function add($idDossier = null) {
        $this->loadModel('Dossiers');
        $dossier = $this->Dossiers->get($idDossier);

        $parametresEngagements = $this->getListParametres('engagement');
        $parametresSituationsComptes = $this->getListParametres('situation compte');
        $parametresActifs = $this->getListParametres('actif');
        $parametresPassifs = $this->getListParametres('passif');

        $this->getElementsParams($idDossier);

        $this->set(compact('parametresEngagements', 'dossier', 'financesEngagements', 'parametresSituationsComptes', 'parametresActifs', 'parametresPassifs'));

    }

    public function getListParametres($type = null) {
        $this->loadModel('FinancesParametres');
        $parametres = $this->FinancesParametres->find('all', [
            'conditions' => ['type' => $type]
        ]);

        return $parametres;
    }

    public function getElementsParams($idDossier = null) {
        $this->loadModel('FinancesEngagements');
        $financesEngagements = $this->FinancesEngagements->find('all', [
            'conditions' => ['dossiers_id' => $idDossier],
            'order' => ['id' => 'ASC']
        ])->toArray();

        $this->loadModel('FinancementsActifs');
        $financementsActifs = $this->FinancementsActifs->find('all', [
            'conditions' => ['dossiers_id' => $idDossier],
            'order' => ['id' => 'ASC']
        ])->toArray();

        $this->loadModel('FinancementsPassifs');
        $financementsPassifs = $this->FinancementsPassifs->find('all', [
            'conditions' => ['dossiers_id' => $idDossier],
            'order' => ['id' => 'ASC']
        ])->toArray();

        $this->loadModel('SituationsComptes');
        $situationsComptes = $this->SituationsComptes->find('all', [
            'conditions' => ['dossiers_id' => $idDossier],
            'order' => ['id' => 'ASC']
        ])->toArray();

        $this->set(compact('financesEngagements', 'financementsActifs', 'financementsPassifs', 'situationsComptes'));
    }

    public function details($idDossier = null) {
        $this->loadModel('Dossiers');
        $dossier = $this->Dossiers->get($idDossier);

        $this->getElementsParams($idDossier);

        $this->set(compact('dossier'));

    }


}