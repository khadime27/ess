<?php
namespace App\Controller\Porteur;

use App\Controller\AppController;

/**
 * FinancementsActifs Controller
 *
 * @property \App\Model\Table\FinancementsActifsTable $FinancementsActifs
 *
 * @method \App\Model\Entity\FinancementsActif[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class FinancementsActifsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Dossiers']
        ];
        $financementsActifs = $this->paginate($this->FinancementsActifs);

        $this->set(compact('financementsActifs'));
    }

    /**
     * View method
     *
     * @param string|null $id Financements Actif id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $financementsActif = $this->FinancementsActifs->get($id, [
            'contain' => ['Dossiers']
        ]);

        $this->set('financementsActif', $financementsActif);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $financementsActif = $this->FinancementsActifs->newEntity();
        if ($this->request->is('post')) {
            $datas = $this->request->getData('financements_actifs');
            $lignesErrors = [];
            foreach($datas as $i => $temp) {
                if(!array_key_exists('idFinancementActif', $temp))
                    $financementsActif = $this->FinancementsActifs->newEntity();
                else
                    $financementsActif = $this->FinancementsActifs->get($temp['idFinancementActif']);
                    
                $financementsActif = $this->FinancementsActifs->patchEntity($financementsActif, $temp);
                if (!$this->FinancementsActifs->save($financementsActif)) {
                    $lignesErrors[] = $i+1;
                }
            }
            if($lignesErrors == []) $this->Flash->success(__("Enregistrement bien effectué !"));
            else $this->Flash->error(__("Erreur d'enregistrement de l'engagement pour les lignes : ".implode(", ", $lignesErrors)));
            
            return $this->redirect($this->referer());

        }
        $this->set(compact('financementsActif'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Financements Actif id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $financementsActif = $this->FinancementsActifs->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $financementsActif = $this->FinancementsActifs->patchEntity($financementsActif, $this->request->getData());
            if ($this->FinancementsActifs->save($financementsActif)) {
                $this->Flash->success(__('The financements actif has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The financements actif could not be saved. Please, try again.'));
        }
        $dossiers = $this->FinancementsActifs->Dossiers->find('list', ['limit' => 200]);
        $this->set(compact('financementsActif', 'dossiers'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Financements Actif id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $financementsActif = $this->FinancementsActifs->get($id);
        if ($this->FinancementsActifs->delete($financementsActif)) {
            $this->Flash->success(__('The financements actif has been deleted.'));
        } else {
            $this->Flash->error(__('The financements actif could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
