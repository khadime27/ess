<?php
namespace App\Controller\Porteur;

use App\Controller\AppController;

/**
 * FinancesEngagements Controller
 *
 * @property \App\Model\Table\FinancesEngagementsTable $FinancesEngagements
 *
 * @method \App\Model\Entity\FinancesEngagement[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class FinancesEngagementsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Dossiers']
        ];
        $financesEngagements = $this->paginate($this->FinancesEngagements);

        $this->set(compact('financesEngagements'));
    }

    /**
     * View method
     *
     * @param string|null $id Finances Engagement id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $financesEngagement = $this->FinancesEngagements->get($id, [
            'contain' => ['Dossiers']
        ]);

        $this->set('financesEngagement', $financesEngagement);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $financesEngagement = $this->FinancesEngagements->newEntity();
        if ($this->request->is('post')) {
            $datas = $this->request->getData('finanances_engagements');
            $lignesErrors = [];
            foreach($datas as $i => $temp) {
                if(!array_key_exists('idEngagement', $temp))
                    $financesEngagement = $this->FinancesEngagements->newEntity();
                else
                    $financesEngagement = $this->FinancesEngagements->get($temp['idEngagement']);

                $financesEngagement = $this->FinancesEngagements->patchEntity($financesEngagement, $temp);
                if (!$this->FinancesEngagements->save($financesEngagement)) {
                    $lignesErrors[] = $i+1;
                }
                
            }
            if($lignesErrors == []) $this->Flash->success(__("Enregistrement bien effectué !"));
            else $this->Flash->error(__("Erreur d'enregistrement de l'engagement pour les lignes : ".implode(", ", $lignesErrors)));
            
            return $this->redirect($this->referer());
        }
        $this->set(compact('financesEngagement'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Finances Engagement id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $financesEngagement = $this->FinancesEngagements->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $financesEngagement = $this->FinancesEngagements->patchEntity($financesEngagement, $this->request->getData());
            if ($this->FinancesEngagements->save($financesEngagement)) {
                $this->Flash->success(__('The finances engagement has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The finances engagement could not be saved. Please, try again.'));
        }
        $dossiers = $this->FinancesEngagements->Dossiers->find('list', ['limit' => 200]);
        $this->set(compact('financesEngagement', 'dossiers'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Finances Engagement id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $financesEngagement = $this->FinancesEngagements->get($id);
        if ($this->FinancesEngagements->delete($financesEngagement)) {
            $this->Flash->success(__('The finances engagement has been deleted.'));
        } else {
            $this->Flash->error(__('The finances engagement could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
