<?php
namespace App\Controller\Porteur;

use App\Controller\AppController;

/**
 * FinancementsPassifs Controller
 *
 * @property \App\Model\Table\FinancementsPassifsTable $FinancementsPassifs
 *
 * @method \App\Model\Entity\FinancementsPassif[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class FinancementsPassifsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Dossiers']
        ];
        $financementsPassifs = $this->paginate($this->FinancementsPassifs);

        $this->set(compact('financementsPassifs'));
    }

    /**
     * View method
     *
     * @param string|null $id Financements Passif id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $financementsPassif = $this->FinancementsPassifs->get($id, [
            'contain' => ['Dossiers']
        ]);

        $this->set('financementsPassif', $financementsPassif);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $financementsPassif = $this->FinancementsPassifs->newEntity();
        if ($this->request->is('post')) {
            $datas = $this->request->getData('financements_passifs');
            $lignesErrors = [];
            foreach($datas as $i => $temp) {
                if(!array_key_exists('idFinancementPassif', $temp))
                    $financementsPassif = $this->FinancementsPassifs->newEntity();
                else
                    $financementsPassif = $this->FinancementsPassifs->get($temp['idFinancementPassif']);
                    
                $financementsPassif = $this->FinancementsPassifs->patchEntity($financementsPassif, $temp);
                if (!$this->FinancementsPassifs->save($financementsPassif)) {
                    $lignesErrors[] = $i+1;
                }
            }
            if($lignesErrors == []) $this->Flash->success(__("Enregistrement bien effectué !"));
            else $this->Flash->error(__("Erreur d'enregistrement de l'engagement pour les lignes : ".implode(", ", $lignesErrors)));
            
            return $this->redirect($this->referer());
        }
        $this->set(compact('financementsPassif'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Financements Passif id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $financementsPassif = $this->FinancementsPassifs->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $financementsPassif = $this->FinancementsPassifs->patchEntity($financementsPassif, $this->request->getData());
            if ($this->FinancementsPassifs->save($financementsPassif)) {
                $this->Flash->success(__('The financements passif has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The financements passif could not be saved. Please, try again.'));
        }
        $dossiers = $this->FinancementsPassifs->Dossiers->find('list', ['limit' => 200]);
        $this->set(compact('financementsPassif', 'dossiers'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Financements Passif id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $financementsPassif = $this->FinancementsPassifs->get($id);
        if ($this->FinancementsPassifs->delete($financementsPassif)) {
            $this->Flash->success(__('The financements passif has been deleted.'));
        } else {
            $this->Flash->error(__('The financements passif could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
