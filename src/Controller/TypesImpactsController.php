<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * TypesImpacts Controller
 *
 * @property \App\Model\Table\TypesImpactsTable $TypesImpacts
 *
 * @method \App\Model\Entity\TypesImpact[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class TypesImpactsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $typesImpacts = $this->paginate($this->TypesImpacts);

        $this->set(compact('typesImpacts'));
    }

    /**
     * View method
     *
     * @param string|null $id Types Impact id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $typesImpact = $this->TypesImpacts->get($id, [
            'contain' => []
        ]);

        $this->set('typesImpact', $typesImpact);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $typesImpact = $this->TypesImpacts->newEntity();
        if ($this->request->is('post')) {
            $typesImpact = $this->TypesImpacts->patchEntity($typesImpact, $this->request->getData());
            if ($this->TypesImpacts->save($typesImpact)) {
                $this->Flash->success(__('The types impact has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The types impact could not be saved. Please, try again.'));
        }
        $this->set(compact('typesImpact'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Types Impact id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $typesImpact = $this->TypesImpacts->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $typesImpact = $this->TypesImpacts->patchEntity($typesImpact, $this->request->getData());
            if ($this->TypesImpacts->save($typesImpact)) {
                $this->Flash->success(__('The types impact has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The types impact could not be saved. Please, try again.'));
        }
        $this->set(compact('typesImpact'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Types Impact id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $typesImpact = $this->TypesImpacts->get($id);
        if ($this->TypesImpacts->delete($typesImpact)) {
            $this->Flash->success(__('The types impact has been deleted.'));
        } else {
            $this->Flash->error(__('The types impact could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
