<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * DossiersImpacts Controller
 *
 * @property \App\Model\Table\DossiersImpactsTable $DossiersImpacts
 *
 * @method \App\Model\Entity\DossiersImpact[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class DossiersImpactsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Dossiers', 'TypesImpacts']
        ];
        $dossiersImpacts = $this->paginate($this->DossiersImpacts);

        $this->set(compact('dossiersImpacts'));
    }

    /**
     * View method
     *
     * @param string|null $id Dossiers Impact id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $dossiersImpact = $this->DossiersImpacts->get($id, [
            'contain' => ['Dossiers', 'TypesImpacts']
        ]);

        $this->set('dossiersImpact', $dossiersImpact);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $dossiersImpact = $this->DossiersImpacts->newEntity();
        if ($this->request->is('post')) {
            $dossiersImpact = $this->DossiersImpacts->patchEntity($dossiersImpact, $this->request->getData());
            if ($this->DossiersImpacts->save($dossiersImpact)) {
                $this->Flash->success(__('The dossiers impact has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The dossiers impact could not be saved. Please, try again.'));
        }
        $dossiers = $this->DossiersImpacts->Dossiers->find('list', ['limit' => 200]);
        $typesImpacts = $this->DossiersImpacts->TypesImpacts->find('list', ['limit' => 200]);
        $this->set(compact('dossiersImpact', 'dossiers', 'typesImpacts'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Dossiers Impact id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $dossiersImpact = $this->DossiersImpacts->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $dossiersImpact = $this->DossiersImpacts->patchEntity($dossiersImpact, $this->request->getData());
            if ($this->DossiersImpacts->save($dossiersImpact)) {
                $this->Flash->success(__('The dossiers impact has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The dossiers impact could not be saved. Please, try again.'));
        }
        $dossiers = $this->DossiersImpacts->Dossiers->find('list', ['limit' => 200]);
        $typesImpacts = $this->DossiersImpacts->TypesImpacts->find('list', ['limit' => 200]);
        $this->set(compact('dossiersImpact', 'dossiers', 'typesImpacts'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Dossiers Impact id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $dossiersImpact = $this->DossiersImpacts->get($id);
        if ($this->DossiersImpacts->delete($dossiersImpact)) {
            $this->Flash->success(__('The dossiers impact has been deleted.'));
        } else {
            $this->Flash->error(__('The dossiers impact could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
