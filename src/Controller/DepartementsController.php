<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Departements Controller
 *
 * @property \App\Model\Table\DepartementsTable $Departements
 *
 * @method \App\Model\Entity\Departement[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class DepartementsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Regions']
        ];
        $departements = $this->paginate($this->Departements);

        $this->set(compact('departements'));
    }

    /**
     * View method
     *
     * @param string|null $id Departement id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $departement = $this->Departements->get($id, [
            'contain' => ['Regions', 'Communes']
        ])->toArray();
        $this->set('departement', $departement);
        echo json_encode($departement['communes']); die();
    }

    public function initialize()
    {
        parent::initialize();
        // Actions autorisées sans connexion
        $this->Auth->allow(['view']);
    }


    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $departement = $this->Departements->newEntity();
        if ($this->request->is('post')) {
            $departement = $this->Departements->patchEntity($departement, $this->request->getData());
            if ($this->Departements->save($departement)) {
                $this->Flash->success(__('The departement has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The departement could not be saved. Please, try again.'));
        }
        $regions = $this->Departements->Regions->find('list', ['limit' => 200]);
        $this->set(compact('departement', 'regions'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Departement id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $departement = $this->Departements->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $departement = $this->Departements->patchEntity($departement, $this->request->getData());
            if ($this->Departements->save($departement)) {
                $this->Flash->success(__('The departement has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The departement could not be saved. Please, try again.'));
        }
        $regions = $this->Departements->Regions->find('list', ['limit' => 200]);
        $this->set(compact('departement', 'regions'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Departement id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $departement = $this->Departements->get($id);
        if ($this->Departements->delete($departement)) {
            $this->Flash->success(__('The departement has been deleted.'));
        } else {
            $this->Flash->error(__('The departement could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
