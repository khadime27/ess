<?php
namespace App\Controller\Operateur;

use App\Controller\AppController;


class AnalysesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {

    }

    public function notraites($idRegion = null, $type = null) {
        $this->loadModel('Dossiers');
        $userConnect = $this->getUser();

        if($type == "label") {
            $dossiers = $this->Dossiers->find('All', [
                'contain' => ['NatureDemandes', 'Porteurs', 'Filieres', 'Communes.Departements', 'Agents'],
                'conditions' => ['Departements.region_id' => $idRegion, 'Dossiers.statut_dossiers_id' => 2, 'Dossiers.demande_label' => true]
            ])->toArray();
        }elseif($type == "finance") {
            $dossiers = $this->Dossiers->find('All', [
                'contain' => ['NatureDemandes', 'Porteurs', 'Filieres', 'Communes.Departements', 'Agents'],
                'conditions' => ['Departements.region_id' => $idRegion, 'Dossiers.statut_financements_id' => 3]
            ])->toArray();
        }

        
        $this->loadModel('Agents');
        $agents = $this->Agents->find('all', [
            'contain' => ['Agences'],
            'conditions' => ['Agences.regions_id' => $idRegion, 'Agents.id IS NOT' => $userConnect->agents[0]->id, 'Agents.comites_id' => 2]
        ]);

        $this->set(compact('dossiers', 'agents', 'type'));
    }

    public function encours($typeDemande = null) {
        $this->loadModel('Dossiers');
        $userConnect = $this->getUser();
        $this->loadModel('Assignations');

        $assignations = null;
        if($typeDemande == null) {
            $assignations = $this->Assignations->find('All', [
                'contain' => ['Dossiers', 'Dossiers.NatureDemandes', 'Dossiers.Porteurs', 'Dossiers.Filieres', 'Dossiers.Communes'],
                'conditions' => ['Assignations.type' => 'analyse', 'Assignations.type_demande' => 'label', 'Dossiers.statut_dossiers_id' => 3, 'Dossiers.demande_label' => true],
                'group' => 'Assignations.code'
            ])->toArray();
        }else {

            $assignations = $this->Assignations->find('All', [
                'contain' => ['Dossiers', 'Dossiers.NatureDemandes', 'Dossiers.Porteurs', 'Dossiers.Filieres', 'Dossiers.Communes'],
                'conditions' => [
                    'OR' => [
                        ['Assignations.type' => 'analyse', 'Assignations.type_demande' => 'finance', 'Dossiers.statut_financements_id' => 4],
                        ['Assignations.type' => 'analyse', 'Assignations.type_demande' => 'finance', 'Dossiers.statut_financements_id' => 5]
                    ]
                ],
                'group' => 'Assignations.code'
            ])->toArray();
        }

        $dossiers = [];
        foreach($assignations as $i => $assignation) {
            $dossiers[] = $assignation->dossier;
            $dossiers[$i]['code_assignation'] = $assignation->code;
        }

        if($typeDemande == null) $type = "label"; else $type = "finance";

        $this->set(compact('dossiers', 'type'));
    }

    public function acceptes($type = null) {
        $this->loadModel('Assignations');

        if($type == "label") {
            $assignations = $this->Assignations->find('All', [
                'contain' => ['Dossiers', 'Dossiers.NatureDemandes', 'Dossiers.Porteurs', 'Dossiers.Filieres', 'Dossiers.Communes'],
                'conditions' => ['Assignations.type' => 'analyse', 'Assignations.type_demande' => 'label', 'Dossiers.statut_dossiers_id' => 4, 'Dossiers.demande_label' => true],
                'group' => 'Assignations.code'
            ])->toArray();
        }else {
            $assignations = $this->Assignations->find('All', [
                'contain' => ['Dossiers', 'Dossiers.NatureDemandes', 'Dossiers.Porteurs', 'Dossiers.Filieres', 'Dossiers.Communes'],
                'conditions' => ['Assignations.type' => 'analyse', 'Assignations.type_demande' => 'finance', 'Dossiers.statut_financements_id' => 6],
                'group' => 'Assignations.code'
            ])->toArray();
        }
        
        $dossiers = [];
        foreach($assignations as $i => $assignation) {
            $dossiers[] = $assignation->dossier;
            $dossiers[$i]['code_assignation'] = $assignation->code;
        }


        $this->set(compact('dossiers', 'type'));
    }

    public function rejetes($type = null) {
        $this->loadModel('Dossiers');
        $userConnect = $this->getUser();

        if($type == null) {
            $dossiers = $this->Dossiers->find('All', [
                'contain' => ['NatureDemandes', 'Porteurs', 'Filieres', 'Communes'],
                'conditions' => ['Dossiers.statut_dossiers_id' => 5, 'Dossiers.demande_label' => true]
            ])->toArray();
        }else{
            $dossiers = $this->Dossiers->find('All', [
                'contain' => ['NatureDemandes', 'Porteurs', 'Filieres', 'Communes'],
                'conditions' => ['Dossiers.statut_financements_id' => 7]
            ])->toArray();
        }
        

        $this->set(compact('dossiers'));
    }

    public function view($idNotation = null) {

        $this->loadModel('Notations');
        $notation = $this->Notations->get($idNotation, [
            'contain' => ['Dossiers.Assignations', 'Agents.Agences']
        ]);
        $sommeNote = $notation->aspect_technique + $notation->aspect_financier + $notation->aspect_env;
        $moyenne = $sommeNote ."/". 100;

        $this->loadModel('Dossiers');

        $dossier = $this->Dossiers->get($notation->dossiers_id, [
            'contain' => ['Porteurs', 'Filieres',
                'Communes.Departements.Regions','Porteurs.Entreprises.TypeEntreprises', 
                'Porteurs.Entreprises.Communes.Departements.Regions', 'StatutDossiers'
            ]
        ]);
        
        $checkEntreprise = false;
        if($dossier->porteur->entreprise) $checkEntreprise = true;

        $checkAssociation = false;
        if($dossier->porteur->entreprise && $dossier->porteur->entreprise->association) $checkAssociation = true;

        $this->loadModel('DossiersFilieres');
        $dossiersFilieres = $this->DossiersFilieres->find('all', [
            'contain' => ['Filieres'],
            'conditions' => ['dossiers_id' => $dossier->id]
        ])->toArray();

        $this->loadModel('DossiersImpacts');
        $dossiersImpacts = $this->DossiersImpacts->find('all', [
            'contain' => ['TypesImpacts.Impactes'],
            'conditions' => ['dossiers_id' => $dossier->id]
        ])->toArray();

        $this->loadModel('PieceJointes');
        $pieces = $this->PieceJointes->find('all', [
            'conditions' => ['dossier_id' => $dossier->id]
        ])->toArray();

        $this->loadModel('QualificationDossiers');
        $qualificationDossier = $this->QualificationDossiers->find('all', [
            'contain' => ['Dossiers', 'Agents.Agences'],
            'conditions' => ['QualificationDossiers.dossiers_id' => $dossier->id]
        ])->first();


        $this->loadModel('Assignations');
        $assignation = $this->Assignations->find('all', [
            'conditions' => ['agent_id' => $notation->agents_id, 'dossier_id' => $notation->dossiers_id]
        ])->first();

        $this->set(compact('dossier', 'moyenne', 'notation', 'assignation', 'checkEntreprise', 'checkAssociation', 'qualificationDossier', 'dossiersFilieres', 'dossiersImpacts', 'pieces'));
    }

    public function validateurs($idDossier = null, $code = null, $type = null) {
        $this->loadModel('Notations');
        $notations = $this->Notations->find('all', [
            'contain' => ['Dossiers.Assignations', 'Dossiers.Porteurs', 'Agents.Agences', 'Agents.Comites'],
            'conditions' => ['Notations.dossiers_id' => $idDossier, 'Notations.code' => $code, 'Notations.type_demande' => $type]
        ])->toArray();
        
        $this->set(compact('notations'));

    }

    public function regions($type = null) {
        $this->loadModel('Regions');
        $regions = $this->Regions->find('all');

        $this->loadModel('Dossiers');

        if($type == "label") {
            $dossiers = $this->Dossiers->find('All', [
                'contain' => ['Communes.Departements'],
                'conditions' => ['Dossiers.statut_dossiers_id' => 2, 'Dossiers.demande_label' => true]
            ])->toArray();
        }elseif($type == "finance") {
            $dossiers = $this->Dossiers->find('All', [
                'contain' => ['Communes.Departements'],
                'conditions' => ['Dossiers.statut_financements_id' => 3]
            ])->toArray();
        }
        

        $this->set(compact('regions', 'dossiers', 'type'));
    }

    

}
