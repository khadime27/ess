<?php
namespace App\Controller\Operateur;

use App\Controller\AppController;


class LabelisationsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {

    }

    public function notraites($idRegion = null, $type = null) {
        $this->loadModel('Dossiers');

        if($type == "label") {
            $dossiers = $this->Dossiers->find('All', [
                'contain' => ['NatureDemandes', 'Porteurs', 'Filieres', 'Communes.Departements', 'Agents'],
                'conditions' => ['Departements.region_id' => $idRegion, 'Dossiers.soumis' => 'oui', 'Dossiers.statut_dossiers_id IS' => null, 'Dossiers.demande_label' => true]
            ])->toArray();
        }elseif($type == "finance") {
            $dossiers = $this->Dossiers->find('All', [
                'contain' => ['NatureDemandes', 'Porteurs', 'Filieres', 'Communes.Departements', 'Agents'],
                'conditions' => ['Departements.region_id' => $idRegion, 'Dossiers.statut_financements_id' => 1]
            ])->toArray();
        }
        
        
        $this->loadModel('Agents');
        $agents = $this->Agents->find('all', [
            'contain' => ['Agences', 'Comites'],
            'conditions' => ['Comites.id' => 1, 'Agences.regions_id' => $idRegion]
        ]);

        $this->set(compact('dossiers', 'agents'));
    }

    public function encours() {
        $this->loadModel('Dossiers');
        $dossiers = $this->Dossiers->find('All', [
            'contain' => ['NatureDemandes', 'Porteurs', 'Filieres', 'Communes', 'Agents'],
            'conditions' => ['Dossiers.statut_dossiers_id' => 1, 'Dossiers.demande_label' => true]
        ])->toArray();

        $this->set(compact('dossiers'));
    }

    public function acceptes() {

        $this->loadModel('Assignations');

        $assignations = $this->Assignations->find('All', [
            'contain' => ['Dossiers', 'Dossiers.NatureDemandes', 'Dossiers.Porteurs', 'Dossiers.Filieres', 'Dossiers.Communes'],
            'conditions' => ['Assignations.type IS' => null, 'Assignations.status_validations_id' => 6, 'Dossiers.demande_label' => true],
            'group' => 'Assignations.code'
        ])->toArray();
        $dossiers = [];
        foreach($assignations as $i => $assignation) {
            $dossiers[] = $assignation->dossier;
            $dossiers[$i]['code_assignation'] = $assignation->code;
        }

        $this->set(compact('dossiers'));
    }

    public function rejetes() {
        $this->loadModel('Dossiers');
        $dossiers = $this->Dossiers->find('All', [
            'contain' => ['NatureDemandes', 'Porteurs', 'Filieres', 'Communes', 'Agents'],
            'conditions' => ['Dossiers.statut_dossiers_id' => 5, 'Dossiers.demande_label' => true]
        ])->toArray();

        $this->set(compact('dossiers'));
    }

    public function regions() {
        $this->loadModel('Regions');
        $regions = $this->Regions->find('all');

        $this->loadModel('Dossiers');
        $dossiers = $this->Dossiers->find('All', [
            'contain' => ['Communes.Departements'],
            'conditions' => ['Dossiers.soumis' => 'oui', 'Dossiers.statut_dossiers_id IS' => null, 'Dossiers.demande_label' => true]
        ])->toArray();

        $this->set(compact('regions', 'dossiers'));
    }

    

}
