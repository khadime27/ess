<?php
namespace App\Controller\Operateur;

use App\Controller\AppController;

/**
 * Chats Controller
 *
 * @property \App\Model\Table\ChatsTable $Chats
 *
 * @method \App\Model\Entity\Chat[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ChatsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->loadModel('Messages');

        $userConnect = $this->getUser();
        $this->loadModel('Dossiers');
        
        $dossiersSameActivities = $this->Dossiers->find('all', [
            'contain' => ['Porteurs.Users'],
            'group' => ['Porteurs.id']
        ]);
        
        $this->loadModel('Messages');
        $chats = $this->Messages->find('all', [
            'contain' => ['Dossiers'],
            'conditions' => ['Messages.agent_id' => $userConnect->agents[0]->id]
        ]);

        $this->set(compact('dossiersSameActivities', 'chats'));
    }

}