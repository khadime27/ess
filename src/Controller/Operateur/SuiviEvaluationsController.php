<?php
namespace App\Controller\Operateur;

use App\Controller\AppController;

/**
 * SuiviEvaluations Controller
 *
 * @property \App\Model\Table\SuiviEvaluationsTable $SuiviEvaluations
 *
 * @method \App\Model\Entity\SuiviEvaluation[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class SuiviEvaluationsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['TypeEvaluations', 'Agents']
        ];
        $suiviEvaluations = $this->paginate($this->SuiviEvaluations);

        $this->set(compact('suiviEvaluations'));
    }

    /**
     * View method
     *
     * @param string|null $id Suivi Evaluation id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $suiviEvaluation = $this->SuiviEvaluations->get($id, [
            'contain' => ['TypeEvaluations', 'Agents', 'PieceJointes']
        ]);

        $this->set('suiviEvaluation', $suiviEvaluation);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $suiviEvaluation = $this->SuiviEvaluations->newEntity();
        if ($this->request->is('post')) {
            $suiviEvaluation = $this->SuiviEvaluations->patchEntity($suiviEvaluation, $this->request->getData());
            if ($this->SuiviEvaluations->save($suiviEvaluation)) {
                $this->Flash->success(__('The suivi evaluation has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The suivi evaluation could not be saved. Please, try again.'));
        }
        $typeEvaluations = $this->SuiviEvaluations->TypeEvaluations->find('list', ['limit' => 200]);
        $agents = $this->SuiviEvaluations->Agents->find('list', ['limit' => 200]);
        $this->set(compact('suiviEvaluation', 'typeEvaluations', 'agents'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Suivi Evaluation id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $suiviEvaluation = $this->SuiviEvaluations->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $suiviEvaluation = $this->SuiviEvaluations->patchEntity($suiviEvaluation, $this->request->getData());
            if ($this->SuiviEvaluations->save($suiviEvaluation)) {
                $this->Flash->success(__('The suivi evaluation has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The suivi evaluation could not be saved. Please, try again.'));
        }
        $typeEvaluations = $this->SuiviEvaluations->TypeEvaluations->find('list', ['limit' => 200]);
        $agents = $this->SuiviEvaluations->Agents->find('list', ['limit' => 200]);
        $this->set(compact('suiviEvaluation', 'typeEvaluations', 'agents'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Suivi Evaluation id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $suiviEvaluation = $this->SuiviEvaluations->get($id);
        if ($this->SuiviEvaluations->delete($suiviEvaluation)) {
            $this->Flash->success(__('The suivi evaluation has been deleted.'));
        } else {
            $this->Flash->error(__('The suivi evaluation could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
