<?php
namespace App\Controller\Operateur;

use App\Controller\AppController;

/**
 * Dashbord Controller
 *
 *
 * @method \App\Model\Entity\Dashbord[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class DashbordController extends AppController
{

    public function index()
    {
        $this->demandesNonTraites();
        $this->labelisationsEncours();
        $this->labelisationsAcceptes();
        $this->labelisationsRefuses();

        $this->loadModel('Dossiers');
        $allDossiers = $this->Dossiers->find('all', [
            'contain' => ['StatutDossiers'],
            'conditions' => ['Dossiers.statut_dossiers_id IS NOT' => null]
        ]);
        
        $statutDossiers = ["En cours", "Accepté", "Rejeté"];

        $dossiers = [];
        foreach($allDossiers as $dossier) {
            if($dossier->created->format('m') == date('m')) $dossiers[] = $dossier;
        }

        $this->getMois();

        $this->set(compact('allDossiers', 'dossiers', 'statutDossiers'));
    }

    public function getMois() {
        $mois = array(
            array("num" => "01", "nom" => "JANVIER"),
            array("num" => "02", "nom" => "FEVRIER"),
            array("num" => "03", "nom" => "MARS"),
            array("num" => "04", "nom" => "AVRIL"),
            array("num" => "05", "nom" => "MAI"),
            array("num" => "06", "nom" => "JUIN"),
            array("num" => "07", "nom" => "JUILLET"),
            array("num" => "08", "nom" => "AOUT"),
            array("num" => "09", "nom" => "SEPTEMBRE"),
            array("num" => "10", "nom" => "OCTOBRE"),
            array("num" => "11", "nom" => "NOVEMBRE"),
            array("num" => "12", "nom" => "DECEMBRE"),
        );

        $this->set(compact('mois'));
    }

    public function demandesNonTraites() {
        $this->loadModel('Dossiers');

        $dataUser = $this->getUser();
        $agent = $dataUser->agents[0];
        
        $statut = null;
        if($agent->comites_id == 1) $statut = null; elseif($agent->comites_id == 2) $statut = 2;
        $dossiers = $this->Dossiers->find('All', [
            'contain' => ['NatureDemandes', 'Porteurs', 'Filieres', 'Communes', 'Agents'],
            'conditions' => ['Dossiers.soumis' => 'oui', 'Dossiers.statut_dossiers_id IS' => $statut, 'Dossiers.demande_label' => true]
        ])->toArray();

        $nbNoTraite = count($dossiers);

        $this->set(compact('nbNoTraite'));
    }

    public function labelisationsEncours() {
        $dataUser = $this->getUser();
        $agent = $dataUser->agents[0];
        $this->loadModel('Dossiers');
        
        $statut = null;
        if($agent->comites_id == 1) $statut = 1; elseif($agent->comites_id == 2) $statut = 3;
        $dossiers = $this->Dossiers->find('All', [
            'contain' => ['NatureDemandes', 'Porteurs', 'Filieres', 'Communes', 'Agents'],
            'conditions' => ['Dossiers.statut_dossiers_id' => $statut, 'Dossiers.demande_label' => true]
        ])->toArray();

        $nbEncours = count($dossiers);

        $this->set(compact('nbEncours'));
    }

    public function labelisationsAcceptes() {
        $dataUser = $this->getUser();
        $agent = $dataUser->agents[0];
        $this->loadModel('Dossiers');
        
        $statut = null;
        if($agent->comites_id == 1) $statut = 2; elseif($agent->comites_id == 2) $statut = 4;
        $dossiers = $this->Dossiers->find('All', [
            'contain' => ['NatureDemandes', 'Porteurs', 'Filieres', 'Communes', 'Agents'],
            'conditions' => ['Dossiers.statut_dossiers_id' => $statut, 'Dossiers.demande_label' => true]
        ])->toArray();

        $nbAccepte = count($dossiers);

        $this->set(compact('nbAccepte'));
    }

    public function labelisationsRefuses() {
        $this->loadModel('Dossiers');
        $dossiers = $this->Dossiers->find('All', [
            'contain' => ['NatureDemandes', 'Porteurs', 'Filieres', 'Communes', 'Agents'],
            'conditions' => ['Dossiers.statut_dossiers_id' => 5, 'Dossiers.demande_label' => true]
        ])->toArray();

        $nbRefuse = count($dossiers);

        $this->set(compact('nbRefuse'));
    }

}
