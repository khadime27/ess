<?php
namespace App\Controller\Operateur;

use App\Controller\AppController;

/**
 * Bibliotheques Controller
 *
 * @property \App\Model\Table\BibliothequesTable $Bibliotheques
 *
 * @method \App\Model\Entity\Bibliotheque[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class BibliothequesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $bibliotheques = $this->Bibliotheques->find('all')->toArray();

        $this->set(compact('bibliotheques'));
    }

    
}
