<?php
namespace App\Controller\Operateur;

use App\Controller\AppController;

/**
 * Dossiers Controller
 *
 * @property \App\Model\Table\DossiersTable $Dossiers
 *
 * @method \App\Model\Entity\Dossier[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class DossiersController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['NatureDemandes', 'Porteurs', 'Filieres', 'Communes', 'Agents']
        ];
        $dossiers = $this->paginate($this->Dossiers);

        $this->set(compact('dossiers'));
    }

    public function demandesLabelises() {
        $dossiers = $this->Dossiers->find('All', [
            'contain' => ['NatureDemandes', 'Porteurs', 'Filieres', 'Communes', 'Agents'],
            'conditions' => ['Dossiers.soumis' => 'oui', 'Dossiers.statut_dossiers_id IS' => null, 'Dossiers.demande_label' => true]
        ])->toArray();
        
        $this->loadModel('Agents');
        $agents = $this->Agents->find('all', [
            'contain' => ['Agences'],
            'conditions' => ['Agences.types_agences_id' => 1]
        ]);

        $this->set(compact('dossiers', 'agents'));
    }

    public function labelisationsEncours() {
        $dossiers = $this->Dossiers->find('All', [
            'contain' => ['NatureDemandes', 'Porteurs', 'Filieres', 'Communes', 'Agents'],
            'conditions' => ['Dossiers.statut_dossiers_id' => 1, 'Dossiers.demande_label' => true]
        ])->toArray();

        $this->set(compact('dossiers'));
    }

    public function labelisationsAcceptes() {
        $dossiers = $this->Dossiers->find('All', [
            'contain' => ['NatureDemandes', 'Porteurs', 'Filieres', 'Communes', 'Agents'],
            'conditions' => ['Dossiers.statut_dossiers_id' => 4, 'Dossiers.demande_label' => true]
        ])->toArray();

        $this->set(compact('dossiers'));
    }

    public function labelisationsRefuses() {
        $dossiers = $this->Dossiers->find('All', [
            'contain' => ['NatureDemandes', 'Porteurs', 'Filieres', 'Communes', 'Agents'],
            'conditions' => ['Dossiers.statut_dossiers_id' => 5, 'Dossiers.demande_label' => true]
        ])->toArray();

        $this->set(compact('dossiers'));
    }

    /**
     * View method
     *
     * @param string|null $id Dossier id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null) {

        $dossier = $this->Dossiers->get($id, [
            'contain' => ['Porteurs', 'Filieres',
                'Communes.Departements.Regions','Porteurs.Entreprises.TypeEntreprises', 
                'Porteurs.Entreprises.Communes.Departements.Regions', 'StatutDossiers'
            ]
        ]);
        $checkEntreprise = false;
        if($dossier->porteur->entreprise) $checkEntreprise = true;

        $checkAssociation = false;
        if($dossier->porteur->entreprise && $dossier->porteur->entreprise->association) $checkAssociation = true;

        $this->loadModel('DossiersFilieres');
        $dossiersFilieres = $this->DossiersFilieres->find('all', [
            'contain' => ['Filieres'],
            'conditions' => ['dossiers_id' => $dossier->id]
        ])->toArray();

        $this->loadModel('DossiersImpacts');
        $dossiersImpacts = $this->DossiersImpacts->find('all', [
            'contain' => ['TypesImpacts.Impactes'],
            'conditions' => ['dossiers_id' => $dossier->id]
        ])->toArray();

        $this->loadModel('PieceJointes');
        $pieces = $this->PieceJointes->find('all', [
            'conditions' => ['dossier_id' => $dossier->id]
        ])->toArray();
        

        $this->set(compact('dossier', 'checkEntreprise', 'checkAssociation', 'dossiersFilieres', 'dossiersImpacts', 'pieces'));

    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $dossier = $this->Dossiers->newEntity();
        if ($this->request->is('post')) {
            $dossier = $this->Dossiers->patchEntity($dossier, $this->request->getData());
            if ($this->Dossiers->save($dossier)) {
                $this->Flash->success(__('The dossier has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The dossier could not be saved. Please, try again.'));
        }
        $natureDemandes = $this->Dossiers->NatureDemandes->find('list', ['limit' => 200]);
        $porteurs = $this->Dossiers->Porteurs->find('list', ['limit' => 200]);
        $filieres = $this->Dossiers->Filieres->find('list', ['limit' => 200]);
        $communes = $this->Dossiers->Communes->find('list', ['limit' => 200]);
        $agents = $this->Dossiers->Agents->find('list', ['limit' => 200]);
        $this->set(compact('dossier', 'natureDemandes', 'porteurs', 'filieres', 'communes', 'agents'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Dossier id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $dossier = $this->Dossiers->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $dossier = $this->Dossiers->patchEntity($dossier, $this->request->getData());
            if ($this->Dossiers->save($dossier)) {
                $this->Flash->success(__('The dossier has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The dossier could not be saved. Please, try again.'));
        }
        $natureDemandes = $this->Dossiers->NatureDemandes->find('list', ['limit' => 200]);
        $porteurs = $this->Dossiers->Porteurs->find('list', ['limit' => 200]);
        $filieres = $this->Dossiers->Filieres->find('list', ['limit' => 200]);
        $communes = $this->Dossiers->Communes->find('list', ['limit' => 200]);
        $agents = $this->Dossiers->Agents->find('list', ['limit' => 200]);
        $this->set(compact('dossier', 'natureDemandes', 'porteurs', 'filieres', 'communes', 'agents'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Dossier id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $dossier = $this->Dossiers->get($id);
        if ($this->Dossiers->delete($dossier)) {
            $this->Flash->success(__('The dossier has been deleted.'));
        } else {
            $this->Flash->error(__('The dossier could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function assignation() {
        $this->loadModel('Assignations');
        $userConnect = $this->getUser();
        $assignation = $this->Assignations->newEntity();
        if ($this->request->is('post')) {
            $temp = $this->request->getData();
            $assignation = $this->Assignations->patchEntity($assignation, $temp);
            $dossier = $this->Dossiers->get($temp['dossier_id']);

            if($userConnect->agents[0]->comites_id == 1) { // Si c'est le comité de qualification => Ici on a un seul validateur
                $assignation->rang = 1;
                $assignation->status_validations_id = 1;
                $assignation->code = $this->genererCode(4);
                if($dossier->statut_financements_id == null) $assignation->type_demande = "label";
                else $assignation->type_demande = "finance";
                if ($this->Assignations->save($assignation)) {
                    if($dossier->statut_financements_id == null) $dossier->statut_dossiers_id = 1; // Si c'est pas une demande de financement, on met le dossier en cours de labélisation
                    else $dossier->statut_financements_id = 2; // Si c'est une demande de financement, on le met en cours de financement

                    $this->Dossiers->save($dossier);
                    $this->Flash->success(__("Assignation bien effectuée. Ce dossier est en cours de traitement."));

                    return $this->redirect($this->referer());
                }
                $this->Flash->error(__("Impossible d'assigner ce dossier. Veuillez réessayez !"));

                
            } else { // Comité d'analyse => Ici on a plusieurs validateurs, pour l'instant maximum 3 validateurs
                $validateurs = array($temp['agent_id']);
                if($temp['agent_id2'] != null) array_push($validateurs, $temp['agent_id2']);
                if($temp['agent_id3'] != null) array_push($validateurs, $temp['agent_id3']);
                $code = $this->genererCode(4);
                foreach($validateurs as $i => $validateur) {
                    $assignation = $this->Assignations->newEntity();

                    $assignation->agent_id = $validateur;
                    $assignation->dossier_id = $dossier->id;
                    $assignation->rang = $i+1;
                    $assignation->type = "analyse";
                    $assignation->code = $code;
                    if($i == 0) $assignation->status_validations_id = 1; // C'est en cours pour le 1er validateur

                    if($dossier->statut_financements_id == null) $assignation->type_demande = "label";
                    else $assignation->type_demande = "finance";
                    $this->Assignations->save($assignation);
                }
                if($dossier->statut_financements_id == null) $dossier->statut_dossiers_id = 3;// Si c'est pas une demande de financement, on met le dossier en cours d'analyse pour labélisation
                else $dossier->statut_financements_id = 4; // Si c'est une demande de financement, on le met en cours d'analyse pour financement

                $this->Dossiers->save($dossier);

                $this->Flash->success(__("Assignation bien effectuée. Ce dossier est en cours d'analyse."));

                return $this->redirect($this->referer());
            }
            
        }
    }
}
