<?php

namespace App\Controller\Operateur;

use App\Controller\AppController;
use Cake\Mailer\Email;
use Cake\ORM\TableRegistry;
use \Mailjet\Resources;
use Cake\Filesystem\File;

/**
 * Dossiers Controller
 *
 * @property \App\Model\Table\DossiersTable $Dossiers
 *
 * @method \App\Model\Entity\Dossier[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class FinancesQualificationsController extends AppController {

    public function getDossiers($statut = null) {
        $this->loadModel('Dossiers');
        $dossiers = $this->Dossiers->find('All', [
            'contain' => ['NatureDemandes', 'Porteurs', 'Filieres', 'Communes', 'Agents'],
            'conditions' => ['Dossiers.statut_financements_id' => $statut]
        ])->toArray();

        return $dossiers;
        
    }

    public function notraites() {
        $dossiers = $this->getDossiers(1); 

        $this->loadModel('Agents');
        $agents = $this->Agents->find('all', [
            'contain' => ['Agences', 'Comites'],
            'conditions' => ['Comites.id' => 1]
        ]);

        $this->set(compact('dossiers', 'agents'));
    }

    public function encours() {
        $this->loadModel('Dossiers');
        $dossiersEnCours = $this->Dossiers->find('All', [
            'contain' => ['NatureDemandes', 'Porteurs', 'Filieres', 'Communes', 'Agents'],
            'conditions' => ['Dossiers.statut_financements_id >= ' => 2]
        ])->toArray();

        $dossiers = [];
        foreach($dossiersEnCours as $dossier) {
            if($dossier->statut_financements_id <= 4) $dossiers[] = $dossier; // Les dossiers en cours sont les statuts 2, 3 et 4
        }

        $this->set(compact('dossiers'));
    }
    
    public function acceptes() {
        $dossiers = $this->getDossiers(6);
        $this->set(compact('dossiers'));
    }

    public function rejeter($idDossier = null) {
        $this->loadModel('Dossiers');
        $dossier = $this->Dossiers->get($idDossier);
        $dossier->statut_dossiers_id = 7; // Financement rejeté

        if($this->Dossiers->save($dossier)) {
            $this->Flash->success(__("L'annulation a été bien effectué."));
        }else {
            $this->Flash->error(__("Impossible de faire l'action. Réessayez plutard !"));
        }
        return $this->redirect($this->referer());
    }

}