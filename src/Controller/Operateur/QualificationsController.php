<?php
namespace App\Controller\Operateur;

use App\Controller\AppController;


class QualificationsController extends AppController
{   
    public function details($id = null, $codeAssignation = null)
    {
        $this->loadModel('Dossiers');

        $dossier = $this->Dossiers->get($id, [
            'contain' => ['Porteurs', 'Filieres',
            'Communes.Departements.Regions','Porteurs.Entreprises.TypeEntreprises', 
            'Porteurs.Entreprises.Communes.Departements.Regions', 'StatutDossiers'
            ]
        ]);
        $checkEntreprise = false;
        if($dossier->porteur->entreprise) $checkEntreprise = true;

        $checkAssociation = false;
        if($dossier->porteur->entreprise && $dossier->porteur->entreprise->association) $checkAssociation = true;

        $this->loadModel('DossiersFilieres');
        $dossiersFilieres = $this->DossiersFilieres->find('all', [
            'contain' => ['Filieres'],
            'conditions' => ['dossiers_id' => $dossier->id]
        ])->toArray();

        $this->loadModel('DossiersImpacts');
        $dossiersImpacts = $this->DossiersImpacts->find('all', [
            'contain' => ['TypesImpacts.Impactes'],
            'conditions' => ['dossiers_id' => $dossier->id]
        ])->toArray();

        $this->loadModel('PieceJointes');
        $pieces = $this->PieceJointes->find('all', [
            'conditions' => ['dossier_id' => $dossier->id]
        ])->toArray();

        $this->loadModel('QualificationDossiers');
        $qualificationDossier = $this->QualificationDossiers->find('all', [
            'contain' => ['Agents.Agences'],
            'conditions' => ['dossiers_id' => $id, 'code' => $codeAssignation]
        ])->first();

        $this->set(compact('dossier', 'qualificationDossier', 'codeAssignation', 'checkEntreprise', 'checkAssociation', 'dossiersFilieres', 'dossiersImpacts', 'pieces'));
    }

}
