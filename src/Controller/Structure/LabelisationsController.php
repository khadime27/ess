<?php
namespace App\Controller\Structure;

use App\Controller\AppController;


class LabelisationsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {

    }

    public function notraites() {
        $this->loadModel('Dossiers');
        $dossiers = $this->Dossiers->find('All', [
            'contain' => ['NatureDemandes', 'Porteurs', 'Filieres', 'Communes', 'Agents'],
            'conditions' => ['Dossiers.soumis' => 'oui', 'Dossiers.statut_dossiers_id IS' => null, 'Dossiers.demande_label' => true]
        ])->toArray();

        $this->set(compact('dossiers'));
    }

    public function encours() {
        $this->loadModel('Dossiers');
        $dossiers = $this->Dossiers->find('All', [
            'contain' => ['NatureDemandes', 'Porteurs', 'Filieres', 'Communes', 'Agents'],
            'conditions' => ['Dossiers.statut_dossiers_id' => 1, 'Dossiers.demande_label' => true]
        ])->toArray();

        $this->set(compact('dossiers'));
    }

    public function acceptes() {
        $this->loadModel('Dossiers');
        $dossiers = $this->Dossiers->find('All', [
            'contain' => ['NatureDemandes', 'Porteurs', 'Filieres', 'Communes', 'Agents'],
            'conditions' => ['Dossiers.statut_dossiers_id' => 6, 'Dossiers.demande_label' => true, 'Dossiers.emission' => true]
        ])->toArray();

        $this->set(compact('dossiers'));
    }

    public function rejetes() {
        $this->loadModel('Dossiers');
        $dossiers = $this->Dossiers->find('All', [
            'contain' => ['NatureDemandes', 'Porteurs', 'Filieres', 'Communes', 'Agents'],
            'conditions' => ['Dossiers.statut_dossiers_id' => 5, 'Dossiers.demande_label' => true]
        ])->toArray();

        $this->set(compact('dossiers'));
    }

    

}
