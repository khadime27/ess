<?php
namespace App\Controller\Structure;

use App\Controller\AppController;

/**
 * Agences Controller
 *
 * @property \App\Model\Table\AgencesTable $Agences
 *
 * @method \App\Model\Entity\Agence[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class AgencesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $agences = $this->Agences->find('all', [
            'contain' => ['TypeAgences'],
            'conditions' => ['Agences.types_agences_id' => 1]
        ])->toArray();

        $this->set(compact('agences'));
    }

    public function comites() {
        $agences = $this->Agences->find('all', [
            'contain' => ['TypeAgences'],
            'conditions' => ['Agences.types_agences_id' => 2]
        ])->toArray();

        $this->loadModel('Regions');
        $regions = $this->Regions->find('all');

        $this->set(compact('agences', 'regions'));
    }

    /**
     * View method
     *
     * @param string|null $id Agence id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $agence = $this->Agences->get($id, [
            'contain' => ['Structures', 'Agents', 'Assignations']
        ]);

        $this->set('agence', $agence);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $agence = $this->Agences->newEntity();
        if ($this->request->is('post')) {
            $agence = $this->Agences->patchEntity($agence, $this->request->getData());
            $agence->structure_id = 1;
            $agence->etat = 1;
            if($this->request->getData('type') == "service") $agence->types_agences_id = 1;
            elseif($this->request->getData('type') == "comite") $agence->types_agences_id = 2;
            if ($this->Agences->save($agence)) {
                $this->Flash->success(__('Enregistrement bien effectué !'));

                return $this->redirect($this->referer());
            }
            $this->Flash->error(__("Impossible de faire l'enregistrement. Réessayez plutard !"));
        }
                
                
    }

    /**
     * Edit method
     *
     * @param string|null $id Agence id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit()
    {
        
        if ($this->request->is(['patch', 'post', 'put'])) {
            $temp = $this->request->getData();
            $agence = $this->Agences->get($temp['idAgence'], [
                'contain' => []
            ]);
            $agence = $this->Agences->patchEntity($agence, $this->request->getData());
            if ($this->Agences->save($agence)) {
                $this->Flash->success(__('Enregistrement bien effectué !'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__("Impossible de faire l'enregistrement. Réessayez plutard !"));
        }
        $this->set(compact('agence'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Agence id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $agence = $this->Agences->get($id);
        if ($this->Agences->delete($agence)) {
            $this->Flash->success(__('The agence has been deleted.'));
        } else {
            $this->Flash->error(__('The agence could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function changeStat($idAgence = null, $stat = null) {
        $agence = $this->Agences->get($idAgence, [
            'contain' => []
        ]);
        if($stat == 1) $agence->etat = 1;
        else $agence->etat = 0;

        if($this->Agences->save($agence)) {
            $this->Flash->success(__('Enregistrement bien effectué.'));
        }else {
            $this->Flash->error(__("Impossible d'enregistrer. Réessayez plutard !"));
        }
        return $this->redirect(['action' => 'index']);
        
    }
}
