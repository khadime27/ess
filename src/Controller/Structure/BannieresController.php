<?php
namespace App\Controller\Structure;

use App\Controller\AppController;

/**
 * Bannieres Controller
 *
 * @property \App\Model\Table\BannieresTable $Bannieres
 *
 * @method \App\Model\Entity\Banniere[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class BannieresController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $bannieres = $this->paginate($this->Bannieres);

        $this->set(compact('bannieres'));
    }

    /**
     * View method
     *
     * @param string|null $id Banniere id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $banniere = $this->Bannieres->get($id, [
            'contain' => []
        ]);

        $this->set('banniere', $banniere);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $banniere = $this->Bannieres->newEntity();
        if ($this->request->is('post')) {
            $temp = $this->request->getData();
            $banniere = $this->Bannieres->patchEntity($banniere, $temp);

            $filepath = null; $fileName = null;
            if($temp['photo']['size'] != 0){

                $image = $temp['photo'];
            
                $fileName = $image['name'];
                $fileName = $this->sanitize($fileName);  // Pour éliminer les mauvais caractères 
                $fileName = time().$fileName;        // On concaténe le nom avec une chaine générée
                $filepath = WWW_ROOT.DS.'img'.DS.'bannieres'.DS.$fileName;

                // Le nom de l'image dans la base
                $banniere->photo = $fileName;
                
            }else $banniere->photo = null;

            if ($this->Bannieres->save($banniere)) {
                if($temp['photo']['size'] != 0) {
                    $image = $temp['photo'];
                    $file_tmp = $image['tmp_name'];
                    move_uploaded_file($file_tmp, $filepath);    // Déplacement du document dans le dossier voulu
                }
                $this->Flash->success(__("L'image a été bien enregistrée."));

                return $this->redirect($this->referer());
            }
            $this->Flash->error(__("Impossible d'enregistrer l'image. Merci de réessayer !"));
        }
        $this->set(compact('banniere'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Banniere id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        if ($this->request->is('post')) {
            $temp = $this->request->getData();
            $banniere = $this->Bannieres->get($temp['id_pj']);
            $banniere = $this->Bannieres->patchEntity($banniere, $temp);

            $oldDocument = $banniere->photo;

            $filepath = null; $fileName = null;
            if($temp['photo']['size'] != 0){
                
                @unlink(WWW_ROOT.DS.'img'.DS.'bannieres'.DS.$banniere->photo); // Suppression de l'ancien document
                
                $image = $temp['photo'];
                $fileName = $image['name'];
                $fileName = $this->sanitize($fileName);  // Pour éliminer les mauvais caractères 
                $fileName = time().$fileName;        // On concaténe le nom avec une chaine générée    
                $filepath = WWW_ROOT.DS.'img'.DS.'bannieres'.DS.$fileName;  // Le chemin où on veut stocker le document

                // Le nom du document dans la base
                $banniere->photo = $fileName;
                
            }
            if($temp['photo']['size'] == 0) $banniere->photo = $oldDocument;

            if ($this->Bannieres->save($banniere)) {
                if($temp['photo']['size'] != 0) {
                    $image = $temp['photo'];
                    $file_tmp = $image['tmp_name'];
                    move_uploaded_file($file_tmp, $filepath);    // Déplacement du document dans le dossier voulu
                }

                $this->Flash->success(__('Enregistrement bien effectué !'));

                return $this->redirect($this->referer());
            }else $this->Flash->error(__("Impossible de faire l'enregistrement."));


        }
    }

    /**
     * Delete method
     *
     * @param string|null $id Banniere id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $banniere = $this->Bannieres->get($id);
        if ($this->Bannieres->delete($banniere)) {
            $this->Flash->success(__('The banniere has been deleted.'));
        } else {
            $this->Flash->error(__('The banniere could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
