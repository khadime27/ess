<?php
namespace App\Controller\Structure;

use App\Controller\AppController;

/**
 * Agents Controller
 *
 * @property \App\Model\Table\AgentsTable $Agents
 *
 * @method \App\Model\Entity\Agent[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class SuperviseursController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->loadModel('Agents');
        $superviseurs = $this->Agents->find('all', [
            'contain' => ['Users'],
            'conditions' => ['Users.profiles_id' => 6]
        ])->toArray();

        $agents = $superviseurs;

        $this->set(compact('agents'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $this->loadModel('Agents');
        $agent = $this->Agents->newEntity();
        $this->loadModel('Users');
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $temp = $this->request->getData();
            $agent = $this->Agents->patchEntity($agent, $temp);

            $user->email = $temp['email'];
            $user->password = $temp['password'];
            $user->profiles_id = 6;
            $user->actif = true;

            if($this->Users->save($user)) {
                $agent->user_id = $user->id;
                $agent->prenom = $temp['prenom'];
                $agent->nom = $temp['nom'];
                $agent->telephone = $temp['telephone'];
                $agent->adresse = $temp['adresse'];
                $agent->structure_id = 1;
                $agent->etat = true;
                $agent->admin = false;
                $agent->superviseur = true;
                if ($this->Agents->save($agent)) {
                    $this->Flash->success(__("Enregistrement bien effectué !"));

                    return $this->redirect(['action' => 'index']);
                }
                $this->Flash->error(__("Impossible de faire l'enregistrement. Réessayez plutard !"));
            }

            
        }
        $this->set(compact('agent'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Agent id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit()
    {
        $this->loadModel('Agents');
        if ($this->request->is(['patch', 'post', 'put'])) {
            $temp = $this->request->getData();
            $id = $temp['idAgent'];
            $agent = $this->Agents->get($id);
            $this->loadModel('Users');
            $user = $this->Users->get($agent->user_id);
            
            $agent = $this->Agents->patchEntity($agent, $temp);

            $user->email = $temp['email'];

            if($this->Users->save($user)) {
                $agent->prenom = $temp['prenom'];
                $agent->nom = $temp['nom'];
                $agent->telephone = $temp['telephone'];
                $agent->adresse = $temp['adresse'];
                if ($this->Agents->save($agent)) {
                    $this->Flash->success(__("Enregistrement bien effectué !"));

                    return $this->redirect(['action' => 'index']);
                }
                $this->Flash->error(__("Impossible de faire l'enregistrement. Réessayez plutard !"));
            }
        }
        $this->set(compact('agent'));
    }


    public function changeStat($idAgent = null, $stat = null) {
        $this->loadModel('Agents');
        $this->loadModel('Users');

        $agent = $this->Agents->get($idAgent, [
            'contain' => ['Users']
        ]);
        if($stat == 1) { $agent->etat = 1; $agent->user->actif = 1; }
        else { $agent->etat = 0; $agent->user->actif = 0; }

        $this->Users->save($agent->user);

        if($this->Agents->save($agent)) {
            $this->Flash->success(__('Enregistrement bien effectué.'));
        }else {
            $this->Flash->error(__("Impossible d'enregistrer. Réessayez plutard !"));
        }
        return $this->redirect($this->referer());
        
    }
    
}
