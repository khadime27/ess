<?php
namespace App\Controller\Structure;

use App\Controller\AppController;

/**
 * Agents Controller
 *
 * @property \App\Model\Table\AgentsTable $Agents
 *
 * @method \App\Model\Entity\Agent[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class AgentsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Ots', 'Users', 'Agences', 'Structures']
        ];
        $agents = $this->paginate($this->Agents);

        $this->loadModel('Comites');
        $comites = $this->Comites->find('all');

        $this->set(compact('agents', 'comites'));
    }

    /**
     * View method
     *
     * @param string|null $id Agent id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($idAgence = null)
    {
        // $agents1 affichhe seulement les agents avec un département
        $agents1 = $this->Agents->find('all', [
            'contain' => ['Agences', 'Users', 'Comites', 'Departements.Regions'],
            'conditions' => ['agence_id' => $idAgence]
        ])->toArray();

        $agents2 = $this->Agents->find('all', [
            'contain' => ['Agences', 'Users', 'Comites'],
            'conditions' => ['agence_id' => $idAgence, 'departements_id IS' => null]
        ])->toArray();

        $agents = array_merge($agents1, $agents2);


        $this->loadModel('Agences');
        $agence = $this->Agences->get($idAgence);

        $this->loadModel('Comites');
        $comites = $this->Comites->find('all');

        $agences = $this->Agences->find('all');

        $this->loadModel('Regions');
        $regions = $this->Regions->find('all');

        $this->set(compact('agents', 'agence', 'comites', 'agences', 'regions'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $agent = $this->Agents->newEntity();
        $this->loadModel('Users');
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $temp = $this->request->getData();
            $agent = $this->Agents->patchEntity($agent, $temp);
            $agent->superviseur = false;

            $user->email = $temp['email'];
            $user->password = $temp['password'];
            if(array_key_exists('profiles_id', $temp)) $user->profiles_id = $temp['profiles_id'];
            else $user->profiles_id = 3;  // Profile Validateur
            $user->actif = true;

            if($this->Users->save($user)) {
                $agent->user_id = $user->id;
                $agent->prenom = $temp['prenom'];
                $agent->nom = $temp['nom'];
                $agent->telephone = $temp['telephone'];
                $agent->adresse = $temp['adresse'];
                $agent->agence_id = $temp['agence_id'];
                if(array_key_exists('comites_id', $temp)) $agent->comites_id = $temp['comites_id'];
                $agent->structure_id = 1;
                $agent->etat = true;
                $agent->admin = 1;
                if ($this->Agents->save($agent)) {
                    $this->Flash->success(__("Enregistrement bien effectué !"));

                    return $this->redirect($this->referer());
                }
                $this->Flash->error(__("Impossible de faire l'enregistrement. Réessayez plutard !"));
            }

            
        }
        $this->set(compact('agent'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Agent id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit()
    {
        $userConnect = $this->getUser();
        if ($this->request->is(['patch', 'post', 'put'])) {
            $temp = $this->request->getData();
            $id = $temp['idAgent'];
            $agent = $this->Agents->get($id);
            $this->loadModel('Users');
            $user = $this->Users->get($agent->user_id);
            
            $agent = $this->Agents->patchEntity($agent, $temp);

            $user->email = $temp['email'];
            if($userConnect->profiles_id == 2 && $temp['admin'] == "1") $user->profiles_id = 3; // Opérateur
            elseif($userConnect->profiles_id == 2 && $temp['admin'] == "0") $user->profiles_id = 4; // Validateur

            if($this->Users->save($user)) {
                $agent->prenom = $temp['prenom'];
                $agent->nom = $temp['nom'];
                $agent->telephone = $temp['telephone'];
                $agent->adresse = $temp['adresse'];
                if($userConnect->profiles_id == 2) $agent->admin = $temp['admin'];
                if ($this->Agents->save($agent)) {
                    $this->Flash->success(__("Enregistrement bien effectué !"));

                    return $this->redirect($this->referer());
                }
                $this->Flash->error(__("Impossible de faire l'enregistrement. Réessayez plutard !"));
            }
        }
        $this->set(compact('agent'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Agent id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $agent = $this->Agents->get($id);
        if ($this->Agents->delete($agent)) {
            $this->Flash->success(__('The agent has been deleted.'));
        } else {
            $this->Flash->error(__('The agent could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function changeStat($idAgent = null, $stat = null) {
        $this->loadModel('Agents');
        $this->loadModel('Users');

        $agent = $this->Agents->get($idAgent, [
            'contain' => ['Users']
        ]);
        if($stat == 1) { $agent->etat = 1; $agent->user->actif = 1; }
        else { $agent->etat = 0; $agent->user->actif = 0; }

        $this->Users->save($agent->user);

        if($this->Agents->save($agent)) {
            $this->Flash->success(__('Enregistrement bien effectué.'));
        }else {
            $this->Flash->error(__("Impossible d'enregistrer. Réessayez plutard !"));
        }
        return $this->redirect($this->referer());
        
    }
    
}
