<?php
namespace App\Controller\Structure;

use App\Controller\AppController;

/**
 * Bibliotheques Controller
 *
 * @property \App\Model\Table\BibliothequesTable $Bibliotheques
 *
 * @method \App\Model\Entity\Bibliotheque[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class BibliothequesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $bibliotheques = $this->Bibliotheques->find('all')->toArray();

        $this->set(compact('bibliotheques'));
    }

    /**
     * View method
     *
     * @param string|null $id Bibliotheque id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $bibliotheque = $this->Bibliotheques->get($id, [
            'contain' => []
        ]);

        $this->set('bibliotheque', $bibliotheque);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $bibliotheque = $this->Bibliotheques->newEntity();
        if ($this->request->is('post')) {
            $temp = $this->request->getData();
            $bibliotheque = $this->Bibliotheques->patchEntity($bibliotheque, $temp);

            $filepath = null; $fileName = null;
            if($temp['document']['size'] != 0){

                $image = $temp['document'];
            
                $fileName = $image['name'];
                $fileName = $this->sanitize($fileName);  // Pour éliminer les mauvais caractères 
                $fileName = time().$fileName;        // On concaténe le nom avec une chaine générée
                $filepath = WWW_ROOT.DS.'documents'.DS.'bibliotheques'.DS.$fileName;

                // Le nom du document dans la base
                $bibliotheque->document = $fileName;
                
            }else $bibliotheque->document = null;

            if ($this->Bibliotheques->save($bibliotheque)) {
                if($temp['document']['size'] != 0) {
                    $image = $temp['document'];
                    $file_tmp = $image['tmp_name'];
                    move_uploaded_file($file_tmp, $filepath);    // Déplacement du document dans le dossier voulu
                }
                $this->Flash->success(__("Le document a été bien enregistré."));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__("Impossible d'enregistrer le document. Merci de réessayer !"));
        }
        $this->set(compact('bibliotheque'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Bibliotheque id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit() {
        if ($this->request->is('post')) {
            $temp = $this->request->getData();
            $bibliotheque = $this->Bibliotheques->get($temp['id_pj']);
            $bibliotheque = $this->Bibliotheques->patchEntity($bibliotheque, $temp);

            $oldDocument = $bibliotheque->document;

            $filepath = null; $fileName = null;
            if($temp['document']['size'] != 0){
                
                @unlink(WWW_ROOT.DS.'documents'.DS.'bibliotheques'.DS.$bibliotheque->document); // Suppression de l'ancien document
                
                $image = $temp['document'];
                $fileName = $image['name'];
                $fileName = $this->sanitize($fileName);  // Pour éliminer les mauvais caractères 
                $fileName = time().$fileName;        // On concaténe le nom avec une chaine générée    
                $filepath = WWW_ROOT.DS.'documents'.DS.'bibliotheques'.DS.$fileName;  // Le chemin où on veut stocker le document

                // Le nom du document dans la base
                $bibliotheque->document = $fileName;
                
            }
            if($temp['document']['size'] == 0) $bibliotheque->document = $oldDocument;

            if ($this->Bibliotheques->save($bibliotheque)) {
                if($temp['document']['size'] != 0) {
                    $image = $temp['document'];
                    $file_tmp = $image['tmp_name'];
                    move_uploaded_file($file_tmp, $filepath);    // Déplacement du document dans le dossier voulu
                }

                $this->Flash->success(__('Enregistrement bien effectué !'));

                return $this->redirect($this->referer());
            }else $this->Flash->error(__("Impossible de faire l'enregistrement."));


        }
    }

    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $bibliotheque = $this->Bibliotheques->get($id);
        if ($this->Bibliotheques->delete($bibliotheque)) {
            @unlink(WWW_ROOT.DS.'documents'.DS.'bibliotheques'.DS.$bibliotheque->document);
            $this->Flash->success(__("Ce document a été bien supprimé."));
        } else {
            $this->Flash->error(__("Erreur de suppression du docuemnt. Merci de réessayer !"));
        }

        return $this->redirect($this->referer());

    }

    //make sure to sanitize the uploaded file name because sometimes it causes major problems.
    //Never trust on the user input. This sanitize() function will remove the extra characters 
    //(you can say bad characters) and replace them with dash(-)
    function sanitize($string, $force_lowercase = true, $anal = false) {
        $strip = array("~", "`", "!", "@", "#", "$", "%", "^", "&", "*", "(", ")", "_", "=", "+", "[", "{", "]","}", "\\", "|", ";", ":", "\"", "'", "&#8216;", "&#8217;", "&#8220;", "&#8221;", "&#8211;", "&#8212;","â€”", "â€“", ",", "<",">", "/", "?");
        $clean = trim(str_replace($strip, "", strip_tags($string)));
        $clean = preg_replace('/\s+/', "-", $clean);
        $clean = ($anal) ? preg_replace("/[^a-zA-Z0-9]/", "", $clean) : $clean ;
        return ($force_lowercase) ?
            (function_exists('mb_strtolower')) ?
                mb_strtolower($clean, 'UTF-8') :
                strtolower($clean) :
            $clean;
    }
}
