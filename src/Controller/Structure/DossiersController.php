<?php
namespace App\Controller\Structure;

use App\Controller\AppController;

/**
 * Dossiers Controller
 *
 * @property \App\Model\Table\DossiersTable $Dossiers
 *
 * @method \App\Model\Entity\Dossier[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class DossiersController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['NatureDemandes', 'Porteurs', 'Filieres', 'Communes', 'Agents']
        ];
        $dossiers = $this->paginate($this->Dossiers);

        $this->set(compact('dossiers'));
    }

    /**
     * View method
     *
     * @param string|null $id Dossier id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null) {

        $dossier = $this->Dossiers->get($id, [
            'contain' => ['Porteurs', 'Filieres',
                'Communes.Departements.Regions','Porteurs.Entreprises.TypeEntreprises', 
                'Porteurs.Entreprises.Communes.Departements.Regions', 'StatutDossiers'
            ]
        ]);
        $checkEntreprise = false;
        if($dossier->porteur->entreprise) $checkEntreprise = true;

        $checkAssociation = false;
        if($dossier->porteur->entreprise && $dossier->porteur->entreprise->association) $checkAssociation = true;

        $this->loadModel('DossiersFilieres');
        $dossiersFilieres = $this->DossiersFilieres->find('all', [
            'contain' => ['Filieres'],
            'conditions' => ['dossiers_id' => $dossier->id]
        ])->toArray();

        $this->loadModel('DossiersImpacts');
        $dossiersImpacts = $this->DossiersImpacts->find('all', [
            'contain' => ['TypesImpacts.Impactes'],
            'conditions' => ['dossiers_id' => $dossier->id]
        ])->toArray();

        $this->loadModel('PieceJointes');
        $pieces = $this->PieceJointes->find('all', [
            'conditions' => ['dossier_id' => $dossier->id]
        ])->toArray();
        

        $this->set(compact('dossier', 'checkEntreprise', 'checkAssociation', 'dossiersFilieres', 'dossiersImpacts', 'pieces'));

    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $dossier = $this->Dossiers->newEntity();
        if ($this->request->is('post')) {
            $dossier = $this->Dossiers->patchEntity($dossier, $this->request->getData());
            if ($this->Dossiers->save($dossier)) {
                $this->Flash->success(__('The dossier has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The dossier could not be saved. Please, try again.'));
        }
        $natureDemandes = $this->Dossiers->NatureDemandes->find('list', ['limit' => 200]);
        $porteurs = $this->Dossiers->Porteurs->find('list', ['limit' => 200]);
        $filieres = $this->Dossiers->Filieres->find('list', ['limit' => 200]);
        $communes = $this->Dossiers->Communes->find('list', ['limit' => 200]);
        $agents = $this->Dossiers->Agents->find('list', ['limit' => 200]);
        $this->set(compact('dossier', 'natureDemandes', 'porteurs', 'filieres', 'communes', 'agents'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Dossier id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $dossier = $this->Dossiers->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $dossier = $this->Dossiers->patchEntity($dossier, $this->request->getData());
            if ($this->Dossiers->save($dossier)) {
                $this->Flash->success(__('The dossier has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The dossier could not be saved. Please, try again.'));
        }
        $natureDemandes = $this->Dossiers->NatureDemandes->find('list', ['limit' => 200]);
        $porteurs = $this->Dossiers->Porteurs->find('list', ['limit' => 200]);
        $filieres = $this->Dossiers->Filieres->find('list', ['limit' => 200]);
        $communes = $this->Dossiers->Communes->find('list', ['limit' => 200]);
        $agents = $this->Dossiers->Agents->find('list', ['limit' => 200]);
        $this->set(compact('dossier', 'natureDemandes', 'porteurs', 'filieres', 'communes', 'agents'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Dossier id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $dossier = $this->Dossiers->get($id);
        if ($this->Dossiers->delete($dossier)) {
            $this->Flash->success(__('The dossier has been deleted.'));
        } else {
            $this->Flash->error(__('The dossier could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
