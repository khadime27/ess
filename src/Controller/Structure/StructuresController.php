<?php
namespace App\Controller\Structure;

use App\Controller\AppController;

/**
 * Structures Controller
 *
 * @property \App\Model\Table\StructuresTable $Structures
 *
 * @method \App\Model\Entity\Structure[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class StructuresController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $structures = $this->paginate($this->Structures);

        $this->set(compact('structures'));
    }

    /**
     * View method
     *
     * @param string|null $id Structure id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $structure = $this->Structures->get($id, [
            'contain' => ['Agences', 'Agents', 'Assignations', 'Ots']
        ]);

        $this->set('structure', $structure);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $structure = $this->Structures->newEntity();
        if ($this->request->is('post')) {
            $structure = $this->Structures->patchEntity($structure, $this->request->getData());
            if ($this->Structures->save($structure)) {
                $this->Flash->success(__('The structure has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The structure could not be saved. Please, try again.'));
        }
        $this->set(compact('structure'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Structure id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $structure = $this->Structures->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $structure = $this->Structures->patchEntity($structure, $this->request->getData());
            if ($this->Structures->save($structure)) {
                $this->Flash->success(__('The structure has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The structure could not be saved. Please, try again.'));
        }
        $this->set(compact('structure'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Structure id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $structure = $this->Structures->get($id);
        if ($this->Structures->delete($structure)) {
            $this->Flash->success(__('The structure has been deleted.'));
        } else {
            $this->Flash->error(__('The structure could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
