<?php
namespace App\Controller\Structure;

use App\Controller\AppController;


class ParametresController extends AppController
{
    public function index() {

    }


    public function filieres() {
        $this->loadModel('Filieres');
        $elements = $this->Filieres->find('all');

        $this->set(compact('elements'));
    }

    public function addFiliere() {
        $this->loadModel('Filieres');
        $filiere = $this->Filieres->newEntity();

        if ($this->request->is('post')) {
            $temp = $this->request->getData();
            $filiere = $this->Filieres->patchEntity($filiere, $temp);

            $filepath = null; $fileName = null;
            if($temp['photo']['size'] != 0){

                $image = $temp['photo'];
            
                $fileName = $image['name'];
                $fileName = $this->sanitize($fileName);  // Pour éliminer les mauvais caractères 
                $fileName = time().$fileName;        // On concaténe le nom avec une chaine générée
                $filepath = WWW_ROOT.DS.'img'.DS.'filieres'.DS.$fileName;

                // Le nom de l'image dans la base
                $filiere->photo = $fileName;
                
            }else $filiere->photo = null;
            $filiere->statut = true;
            
            if ($this->Filieres->save($filiere)) {
                if($temp['photo']['size'] != 0) {
                    $image = $temp['photo'];
                    $file_tmp = $image['tmp_name'];
                    move_uploaded_file($file_tmp, $filepath);    // Déplacement de l'image dans le dossier voulu
                }

                $this->Flash->success(__('Enregistrement bien effectué !'));

                return $this->redirect($this->referer());
            }else $this->Flash->error(__("Impossible de faire l'enregistrement."));


        }
    }

    public function editFiliere() {
        $this->loadModel('Filieres');
    
        if ($this->request->is('post')) {
            $temp = $this->request->getData();
            $filiere = $this->Filieres->get($temp['idElement']);
            $filiere = $this->Filieres->patchEntity($filiere, $temp);

            $oldImage = $filiere->photo;

            $filepath = null; $fileName = null;
            if($temp['photo']['size'] != 0){

                if($filiere->photo != null){
                    // Le fichier image
                    $fileName = $filiere->photo;      // On concaténe le nom avec une chaine générée
                    $filepath = WWW_ROOT.DS.'img'.DS.'filieres'.DS.$fileName;  // Le chemin où on veut stocker l'image
                    // Fichier image
                }else {
                    $image = $temp['photo'];
            
                    $fileName = $image['name'];
                    $fileName = $this->sanitize($fileName);  // Pour éliminer les mauvais caractères 
                    $fileName = time().$fileName;        // On concaténe le nom avec une chaine générée
                    $filepath = WWW_ROOT.DS.'img'.DS.'filieres'.DS.$fileName;
                }

                // Le nom de l'image dans la base
                $filiere->photo = $fileName;
                
            }
            if($temp['photo']['size'] == 0) $filiere->photo = $oldImage;

            if ($this->Filieres->save($filiere)) {
                if($temp['photo']['size'] != 0) {
                    $image = $temp['photo'];
                    $file_tmp = $image['tmp_name'];
                    move_uploaded_file($file_tmp, $filepath);    // Déplacement de l'image dans le dossier voulu
                }

                $this->Flash->success(__('Enregistrement bien effectué !'));

                return $this->redirect($this->referer());
            }else $this->Flash->error(__("Impossible de faire l'enregistrement."));


        }
    }

    public function changeStatutFiliere($id = null, $statut = null) {
        $this->loadModel('Filieres');
        $filiere = $this->Filieres->get($id);
        $filiere->statut = $statut;
        $this->Filieres->save($filiere);
        if($statut) $this->Flash->success(__("Ce secteur a été bien activé."));
        else $this->Flash->success(__("Ce secteur a été désactivé avec succès."));

        return $this->redirect($this->referer());
    }

    public function deleteFiliere($id = null) {
        $this->loadModel('Filieres');
        $this->request->allowMethod(['post', 'delete']);
        $filiere = $this->Filieres->get($id);
        if ($this->Filieres->delete($filiere)) {
            @unlink(WWW_ROOT.DS.'img'.DS.'filieres'.DS.$filiere->photo);
            $this->Flash->success(__("Le secteur a été bien supprimé."));
        } else {
            $this->Flash->error(__("Erreur de suppression du secteur. Merci de réessayer !"));
        }

        return $this->redirect($this->referer());

    }

    //make sure to sanitize the uploaded file name because sometimes it causes major problems.
    //Never trust on the user input. This sanitize() function will remove the extra characters 
    //(you can say bad characters) and replace them with dash(-)
    function sanitize($string, $force_lowercase = true, $anal = false) {
        $strip = array("~", "`", "!", "@", "#", "$", "%", "^", "&", "*", "(", ")", "_", "=", "+", "[", "{", "]","}", "\\", "|", ";", ":", "\"", "'", "&#8216;", "&#8217;", "&#8220;", "&#8221;", "&#8211;", "&#8212;","â€”", "â€“", ",", "<",">", "/", "?");
        $clean = trim(str_replace($strip, "", strip_tags($string)));
        $clean = preg_replace('/\s+/', "-", $clean);
        $clean = ($anal) ? preg_replace("/[^a-zA-Z0-9]/", "", $clean) : $clean ;
        return ($force_lowercase) ?
            (function_exists('mb_strtolower')) ?
                mb_strtolower($clean, 'UTF-8') :
                strtolower($clean) :
            $clean;
    }

}