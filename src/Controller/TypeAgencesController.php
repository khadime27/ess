<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * TypeAgences Controller
 *
 * @property \App\Model\Table\TypeAgencesTable $TypeAgences
 *
 * @method \App\Model\Entity\TypeAgence[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class TypeAgencesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $typeAgences = $this->paginate($this->TypeAgences);

        $this->set(compact('typeAgences'));
    }

    /**
     * View method
     *
     * @param string|null $id Type Agence id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $typeAgence = $this->TypeAgences->get($id, [
            'contain' => []
        ]);

        $this->set('typeAgence', $typeAgence);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $typeAgence = $this->TypeAgences->newEntity();
        if ($this->request->is('post')) {
            $typeAgence = $this->TypeAgences->patchEntity($typeAgence, $this->request->getData());
            if ($this->TypeAgences->save($typeAgence)) {
                $this->Flash->success(__('The type agence has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The type agence could not be saved. Please, try again.'));
        }
        $this->set(compact('typeAgence'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Type Agence id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $typeAgence = $this->TypeAgences->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $typeAgence = $this->TypeAgences->patchEntity($typeAgence, $this->request->getData());
            if ($this->TypeAgences->save($typeAgence)) {
                $this->Flash->success(__('The type agence has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The type agence could not be saved. Please, try again.'));
        }
        $this->set(compact('typeAgence'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Type Agence id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $typeAgence = $this->TypeAgences->get($id);
        if ($this->TypeAgences->delete($typeAgence)) {
            $this->Flash->success(__('The type agence has been deleted.'));
        } else {
            $this->Flash->error(__('The type agence could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
