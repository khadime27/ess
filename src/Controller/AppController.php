<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;
use cake\Routing\Router;
use \Mailjet\Resources;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link https://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('RequestHandler', [
            'enableBeforeRedirect' => false,
        ]);
        $this->loadComponent('Flash');

        $this->loadComponent('Auth', [
            'authenticate' => [
                'Form' => [
                    'fields' => [
                        'username' => 'email',
                        'password' => 'password'
                    ]
                ]
            ],
            'loginAction' => [
                'prefix'=> false,
                'controller' => 'Users',
                'action' => 'connecter'
            ],
            'loginRedirect' => '/',
            'authError' => false,
            'unauthorizedRedirect' => $this->referer(), // If unauthorized, return them to page they were just on
            //'authorize'=> 'Controller'
        ]);
        // Allow the display action so our pages controller
        // continues to work.
        $this->Auth->allow(['display','resetPassword', 'deconnexion', 'connecter', 'projectBySecteur']);

        /*
         * Enable the following component for recommended CakePHP security settings.
         * see https://book.cakephp.org/3.0/en/controllers/components/security.html
         */
        //$this->loadComponent('Security');

        $userConnect = $this->getUser();
        $this->getStatsVitrine();

        $this->set(compact('userConnect'));
    }


    public function getUser(){
        $this->loadModel('Users');
        $id = $this->Auth->user('id');
        $user = $this->Users->find('all',[
            'contain' => ['Porteurs', 'Porteurs.Entreprises', 'Profiles', 'Agents.Agences'],
            'conditions'=>['Users.id'=>$id]
        ])->first();
        return $user;
    }


    public function beforeRender(Event $event) {
        $this->viewBuilder()->setLayout('ajax');  // Layout
    }

    public function genererCode(int $taille) {
        $length = $taille;
        // debug($length);
        $char = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $taillechar = strlen($char);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $char[rand(0, $taillechar - 1)];
        }
        return $randomString;
    }

    public function sendEmail($subject,$emails,$message_user){
        try{
  
          $apikey = '3b795f369165a7a108363397cb73a61c';
          $apisecret = '00abdc94e2f7abdee5ff8ef6f65abcf8';
  
          $mj = new \Mailjet\Client($apikey, $apisecret);
          
          $msg = $message_user."<br><br>Cordialement.<br>";
  
          $recipients = [];
  
          foreach($emails as $email){
            $recipients[] = ['Email' => $email];
          }
          
          $body = [
              'FromEmail' => "contact@afridev-group.com",
              'FromName' => "Afridev-Group",
              'Subject' => $subject,
              //'Text-part' => "Dear tester, welcome to Mailjet! May the delivery force be with you!",
              'Html-part' => $msg,
              'Recipients' => $recipients
          ];
          $mj->setTimeout(8);
          $response = $mj->post(Resources::$Email, ['body' => $body]);
          //$response->success() && var_dump($response->getData());

        }catch(\Exception $e){
          dd($e);
        }        
    }

    public function getStatsVitrine() {
        $this->loadModel('Dossiers');
        $this->loadModel('Entreprises');
        $structures = $this->Entreprises->find('all');
        $nbEmployeTotal = 0;
        foreach($structures as $structure) {
            $nbEmployeTotal += $structure->nb_employe;
        }
        $dossiers = $this->Dossiers->find('all')->toArray();
        $nbProjet = count($dossiers);
        $dossiersLabelises = $this->Dossiers->find('all', [
            'conditions' => ['emission' => true]
        ])->toArray();
        $nbDossiersLabelises = count($dossiersLabelises);

        $dossiersFinances = $this->Dossiers->find('all', [
            'conditions' => ['emission_finance' => true]
        ])->toArray();
        $nbDossiersFinances = count($dossiersFinances);

        $this->loadModel('TypeEntreprises');
        $typesEntreprises = $this->TypeEntreprises->find('all')->toArray();
        $this->loadModel('Entreprises');
        $entreprises = $this->Entreprises->find('all')->toArray();
        /*
        foreach($typesEntreprises as $typeEntreprise): 
            $nbEntreprise = 0;
            foreach($entreprises as $entreprise):
                if($entreprise->type_entreprise_id == $typeEntreprise->id) $nbEntreprise += 1;

            endforeach;
        endforeach;
        */

        $this->loadModel('Filieres');
        $filieres = $this->Filieres->find('all', [
            'conditions' => ['photo IS NOT' => null]
        ])->limit(4);
        $this->set('background', '/img/real-bg1.jpg');

        $this->set(compact('nbEmployeTotal', 'filieres', 'nbProjet', 'nbDossiersLabelises', 'nbDossiersFinances', 'typesEntreprises', 'entreprises'));
    }

}
