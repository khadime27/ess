<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * FinancesParametres Controller
 *
 * @property \App\Model\Table\FinancesParametresTable $FinancesParametres
 *
 * @method \App\Model\Entity\FinancesParametre[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class FinancesParametresController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $financesParametres = $this->paginate($this->FinancesParametres);

        $this->set(compact('financesParametres'));
    }

    /**
     * View method
     *
     * @param string|null $id Finances Parametre id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $financesParametre = $this->FinancesParametres->get($id, [
            'contain' => []
        ]);

        $this->set('financesParametre', $financesParametre);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $financesParametre = $this->FinancesParametres->newEntity();
        if ($this->request->is('post')) {
            $financesParametre = $this->FinancesParametres->patchEntity($financesParametre, $this->request->getData());
            if ($this->FinancesParametres->save($financesParametre)) {
                $this->Flash->success(__('The finances parametre has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The finances parametre could not be saved. Please, try again.'));
        }
        $this->set(compact('financesParametre'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Finances Parametre id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $financesParametre = $this->FinancesParametres->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $financesParametre = $this->FinancesParametres->patchEntity($financesParametre, $this->request->getData());
            if ($this->FinancesParametres->save($financesParametre)) {
                $this->Flash->success(__('The finances parametre has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The finances parametre could not be saved. Please, try again.'));
        }
        $this->set(compact('financesParametre'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Finances Parametre id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $financesParametre = $this->FinancesParametres->get($id);
        if ($this->FinancesParametres->delete($financesParametre)) {
            $this->Flash->success(__('The finances parametre has been deleted.'));
        } else {
            $this->Flash->error(__('The finances parametre could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
