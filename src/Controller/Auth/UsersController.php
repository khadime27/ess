<?php
namespace App\Controller\Auth;

use App\Controller\AppController;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 *
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UsersController extends AppController
{
    

    public function connecter()
    {
        if ($this->request->is('post')) {
            $user = $this->Auth->identify();
            $this->loadModel('Roles');
            $infUser = $this->getUser();
            if ($user) {
                if($user['statut']==1){
                    $this->Auth->setUser($user);
                    return $this->redirect(['prefix'=>'admin','controller' => 'Users', 'action' => 'test']);
                }else $this->Flash->error('Vos accès ont été désactivés. Merci de contacter votre administrateur.');
            } else $this->Flash->error('Votre email ou mot de passe est incorrect. Merci de réessayer.');
        }

    }

    public function loginRedirect($profil){
        if($profil=='admin') return $this->redirect(['prefix'=>'admin','controller' => 'Users', 'action' => 'test']); 
        //else return $this->redirect(['controller' => 'Pages', 'action' => 'home']); 
    }

    
    public function deconnexion()
    {
        return $this->redirect($this->Auth->logout());
    }
    
    public function test(){}

}
