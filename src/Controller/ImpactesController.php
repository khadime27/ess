<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Impactes Controller
 *
 * @property \App\Model\Table\ImpactesTable $Impactes
 *
 * @method \App\Model\Entity\Impacte[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ImpactesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $impactes = $this->paginate($this->Impactes);

        $this->set(compact('impactes'));
    }

    /**
     * View method
     *
     * @param string|null $id Impacte id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $impacte = $this->Impactes->get($id, [
            'contain' => []
        ]);

        $this->set('impacte', $impacte);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $impacte = $this->Impactes->newEntity();
        if ($this->request->is('post')) {
            $impacte = $this->Impactes->patchEntity($impacte, $this->request->getData());
            if ($this->Impactes->save($impacte)) {
                $this->Flash->success(__('The impacte has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The impacte could not be saved. Please, try again.'));
        }
        $this->set(compact('impacte'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Impacte id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $impacte = $this->Impactes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $impacte = $this->Impactes->patchEntity($impacte, $this->request->getData());
            if ($this->Impactes->save($impacte)) {
                $this->Flash->success(__('The impacte has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The impacte could not be saved. Please, try again.'));
        }
        $this->set(compact('impacte'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Impacte id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $impacte = $this->Impactes->get($id);
        if ($this->Impactes->delete($impacte)) {
            $this->Flash->success(__('The impacte has been deleted.'));
        } else {
            $this->Flash->error(__('The impacte could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
