<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * FinancesEngagement Entity
 *
 * @property int $id
 * @property string|null $libelle
 * @property float|null $montant
 * @property float|null $encours
 * @property int|null $differe_mois
 * @property \Cake\I18n\FrozenDate|null $date_last_echeance
 * @property int|null $duree_mois
 * @property string|null $banque
 * @property float|null $taux_interet
 * @property int $dossiers_id
 *
 * @property \App\Model\Entity\Dossier $dossier
 */
class FinancesEngagement extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'libelle' => true,
        'montant' => true,
        'encours' => true,
        'differe_mois' => true,
        'date_last_echeance' => true,
        'duree_mois' => true,
        'banque' => true,
        'taux_interet' => true,
        'dossiers_id' => true,
        'dossier' => true
    ];
}
