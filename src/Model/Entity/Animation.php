<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Animation Entity
 *
 * @property int $id
 * @property string $titre
 * @property string $description
 * @property \Cake\I18n\FrozenTime $created
 * @property int $type_animation_id
 * @property int|null $agent_id
 * @property string|null $lieu
 *
 * @property \App\Model\Entity\TypeAnimation $type_animation
 * @property \App\Model\Entity\Agent $agent
 * @property \App\Model\Entity\Participant[] $participants
 * @property \App\Model\Entity\PieceJointe[] $piece_jointes
 */
class Animation extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'titre' => true,
        'description' => true,
        'created' => true,
        'type_animation_id' => true,
        'agent_id' => true,
        'lieu' => true,
        'type_animation' => true,
        'agent' => true,
        'participants' => true,
        'piece_jointes' => true
    ];
}
