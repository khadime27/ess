<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Notation Entity
 *
 * @property int $id
 * @property int $dossiers_id
 * @property int $agents_id
 * @property string|null $code
 * @property int|null $nat_struc_juridique
 * @property int|null $eligibilite_filiere
 * @property int|null $partenariat_relation
 * @property int|null $pertinence_activite
 * @property int|null $coherence_activite
 * @property int|null $positionnement_activite
 * @property string|null $comment_aspect_tech
 * @property int|null $aspect_technique
 * @property int|null $sante_financiere
 * @property int|null $croissance_reultat
 * @property int|null $situation_actif
 * @property int|null $situation_passif
 * @property int|null $croissance_en_vue
 * @property int|null $engagement_actuel
 * @property int|null $situation_compte
 * @property string|null $comment_aspect_financ
 * @property int|null $aspect_financier
 * @property int|null $eligibilite_ess
 * @property int|null $impact_social_env
 * @property int|null $influence_territoriale
 * @property int|null $autre_critere_ess1
 * @property int|null $autre_critere_ess2
 * @property int|null $pourcentage_revenu_dirigeants
 * @property string|null $comment_aspect_env
 * @property int|null $aspect_env
 * @property string|null $raison_rejet
 * @property string|null $type_demande
 *
 * @property \App\Model\Entity\Dossier $dossier
 * @property \App\Model\Entity\Agent $agent
 */
class Notation extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'dossiers_id' => true,
        'agents_id' => true,
        'code' => true,
        'nat_struc_juridique' => true,
        'eligibilite_filiere' => true,
        'partenariat_relation' => true,
        'pertinence_activite' => true,
        'coherence_activite' => true,
        'positionnement_activite' => true,
        'comment_aspect_tech' => true,
        'aspect_technique' => true,
        'sante_financiere' => true,
        'croissance_reultat' => true,
        'situation_actif' => true,
        'situation_passif' => true,
        'croissance_en_vue' => true,
        'engagement_actuel' => true,
        'situation_compte' => true,
        'comment_aspect_financ' => true,
        'aspect_financier' => true,
        'eligibilite_ess' => true,
        'impact_social_env' => true,
        'influence_territoriale' => true,
        'autre_critere_ess1' => true,
        'autre_critere_ess2' => true,
        'pourcentage_revenu_dirigeants' => true,
        'comment_aspect_env' => true,
        'aspect_env' => true,
        'raison_rejet' => true,
        'type_demande' => true,
        'dossier' => true,
        'agent' => true
    ];
}
