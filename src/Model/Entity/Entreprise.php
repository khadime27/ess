<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Entreprise Entity
 *
 * @property int $id
 * @property string|null $ninea
 * @property string|null $nom
 * @property int|null $type_entreprise_id
 * @property int|null $nb_employe
 * @property \Cake\I18n\FrozenDate|null $date_creation
 * @property string|null $immatriculation
 * @property string|null $type_social
 * @property string|null $social_link
 * @property string|null $tel_entite
 * @property int|null $communes_id
 * @property int|null $nb_salarie
 * @property int|null $nb_stageaire
 * @property int|null $nb_saisonnier
 * @property bool|null $association
 * @property bool|null $formel
 *
 * @property \App\Model\Entity\TypeEntreprise $type_entreprise
 * @property \App\Model\Entity\Commune $commune
 * @property \App\Model\Entity\Porteur[] $porteurs
 */
class Entreprise extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'ninea' => true,
        'nom' => true,
        'type_entreprise_id' => true,
        'nb_employe' => true,
        'date_creation' => true,
        'immatriculation' => true,
        'type_social' => true,
        'social_link' => true,
        'tel_entite' => true,
        'communes_id' => true,
        'nb_salarie' => true,
        'nb_stageaire' => true,
        'nb_saisonnier' => true,
        'association' => true,
        'formel' => true,
        'type_entreprise' => true,
        'commune' => true,
        'porteurs' => true
    ];
}
