<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Agent Entity
 *
 * @property int $id
 * @property string|null $prenom
 * @property string|null $nom
 * @property string|null $telephone
 * @property string|null $adresse
 * @property int $user_id
 * @property int|null $agence_id
 * @property int|null $structure_id
 * @property bool|null $admin
 * @property bool $superviseur
 * @property bool|null $etat
 * @property int|null $comites_id
 * @property int|null $departements_id
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Agence $agence
 * @property \App\Model\Entity\Structure $structure
 * @property \App\Model\Entity\Comite $comite
 * @property \App\Model\Entity\Animation[] $animations
 * @property \App\Model\Entity\Assignation[] $assignations
 * @property \App\Model\Entity\Dossier[] $dossiers
 * @property \App\Model\Entity\Message[] $messages
 * @property \App\Model\Entity\SuiviEvaluation[] $suivi_evaluations
 */
class Agent extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'prenom' => true,
        'nom' => true,
        'telephone' => true,
        'adresse' => true,
        'user_id' => true,
        'agence_id' => true,
        'structure_id' => true,
        'admin' => true,
        'superviseur' => true,
        'etat' => true,
        'comites_id' => true,
        'departements_id' => true,
        'user' => true,
        'agence' => true,
        'structure' => true,
        'comite' => true,
        'animations' => true,
        'assignations' => true,
        'dossiers' => true,
        'messages' => true,
        'suivi_evaluations' => true
    ];
}
