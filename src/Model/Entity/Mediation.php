<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Mediation Entity
 *
 * @property int $id
 * @property string $description
 * @property int $financement_credit_id
 * @property \Cake\I18n\FrozenTime $created
 * @property string $titre
 * @property int|null $statu_id
 *
 * @property \App\Model\Entity\FinancementCredit $financement_credit
 * @property \App\Model\Entity\Status $status
 * @property \App\Model\Entity\PieceJointe[] $piece_jointes
 */
class Mediation extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'description' => true,
        'financement_credit_id' => true,
        'created' => true,
        'titre' => true,
        'statu_id' => true,
        'financement_credit' => true,
        'status' => true,
        'piece_jointes' => true
    ];
}
