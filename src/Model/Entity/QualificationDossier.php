<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * QualificationDossier Entity
 *
 * @property int $id
 * @property int $dossiers_id
 * @property int|null $agents_id
 * @property string|null $desc_etat_civil
 * @property string|null $desc_dossier
 * @property string|null $desc_impact
 * @property string|null $raison_rejet
 * @property \Cake\I18n\FrozenTime $created
 *
 * @property \App\Model\Entity\Dossier $dossier
 * @property \App\Model\Entity\Agent $agent
 */
class QualificationDossier extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'dossiers_id' => true,
        'agents_id' => true,
        'desc_etat_civil' => true,
        'desc_dossier' => true,
        'desc_impact' => true,
        'raison_rejet' => true,
        'created' => true,
        'code' => true,
        'type_demande' => true,
        'dossier' => true,
        'agent' => true
    ];
}
