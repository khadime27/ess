<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Porteur Entity
 *
 * @property int $id
 * @property int $user_id
 * @property int|null $entreprise_id
 * @property string|null $nom
 * @property string|null $cni
 * @property string|null $telephone
 * @property string|null $prenom
 * @property string|null $diplome
 * @property string|null $niveau_etude
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Entreprise $entreprise
 * @property \App\Model\Entity\Dossier[] $dossiers
 * @property \App\Model\Entity\Message[] $messages
 */
class Porteur extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'entreprise_id' => true,
        'nom' => true,
        'cni' => true,
        'telephone' => true,
        'prenom' => true,
        'diplome' => true,
        'niveau_etude' => true,
        'user' => true,
        'entreprise' => true,
        'dossiers' => true,
        'messages' => true
    ];
}
