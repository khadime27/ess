<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * StatutDossier Entity
 *
 * @property int $id
 * @property int $dossier_id
 * @property int $agent_id
 * @property \Cake\I18n\FrozenTime|null $created
 * @property string|null $etat
 *
 * @property \App\Model\Entity\Dossier $dossier
 * @property \App\Model\Entity\Agent $agent
 */
class StatutDossier extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'dossier_id' => true,
        'agent_id' => true,
        'created' => true,
        'etat' => true,
        'dossier' => true,
        'agent' => true
    ];
}
