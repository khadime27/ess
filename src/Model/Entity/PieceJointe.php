<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * PieceJointe Entity
 *
 * @property int $id
 * @property string|null $url
 * @property int|null $message_id
 * @property int|null $dossier_id
 * @property string|null $titre
 * @property int|null $mediation_id
 * @property int|null $animation_id
 * @property int|null $suivi_evaluation_id
 *
 * @property \App\Model\Entity\Message $message
 * @property \App\Model\Entity\Dossier $dossier
 * @property \App\Model\Entity\Mediation $mediation
 * @property \App\Model\Entity\Animation $animation
 * @property \App\Model\Entity\SuiviEvaluation $suivi_evaluation
 */
class PieceJointe extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'url' => true,
        'message_id' => true,
        'dossier_id' => true,
        'titre' => true,
        'mediation_id' => true,
        'animation_id' => true,
        'suivi_evaluation_id' => true,
        'message' => true,
        'dossier' => true,
        'mediation' => true,
        'animation' => true,
        'suivi_evaluation' => true
    ];
}
