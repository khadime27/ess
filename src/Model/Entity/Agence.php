<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Agence Entity
 *
 * @property int $id
 * @property string|null $nom
 * @property int $structure_id
 * @property string|null $telephone
 * @property string|null $adresse
 * @property int|null $types_agences_id
 * @property int|null $regions_id
 * @property bool|null $etat
 *
 * @property \App\Model\Entity\Structure $structure
 * @property \App\Model\Entity\TypeAgence $type_agence
 * @property \App\Model\Entity\Agent[] $agents
 */
class Agence extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'nom' => true,
        'structure_id' => true,
        'telephone' => true,
        'adresse' => true,
        'types_agences_id' => true,
        'regions_id' => true,
        'etat' => true,
        'structure' => true,
        'type_agence' => true,
        'agents' => true
    ];
}
