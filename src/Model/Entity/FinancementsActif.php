<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * FinancementsActif Entity
 *
 * @property int $id
 * @property string|null $libelle
 * @property float|null $annee_moins_trois
 * @property float|null $annee_moins_deux
 * @property float|null $annee_moins_un
 * @property float|null $annee
 * @property float|null $prevision
 * @property float|null $variation
 * @property int $dossiers_id
 *
 * @property \App\Model\Entity\Dossier $dossier
 */
class FinancementsActif extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'libelle' => true,
        'annee_moins_trois' => true,
        'annee_moins_deux' => true,
        'annee_moins_un' => true,
        'annee' => true,
        'prevision' => true,
        'variation' => true,
        'dossiers_id' => true,
        'dossier' => true
    ];
}
