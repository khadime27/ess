<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * SuiviEvaluation Entity
 *
 * @property int $id
 * @property string|null $titre
 * @property string|null $description
 * @property \Cake\I18n\FrozenTime $created
 * @property int|null $type_evaluation_id
 * @property int|null $agent_id
 *
 * @property \App\Model\Entity\TypeEvaluation $type_evaluation
 * @property \App\Model\Entity\Agent $agent
 * @property \App\Model\Entity\PieceJointe[] $piece_jointes
 */
class SuiviEvaluation extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'titre' => true,
        'description' => true,
        'created' => true,
        'type_evaluation_id' => true,
        'agent_id' => true,
        'type_evaluation' => true,
        'agent' => true,
        'piece_jointes' => true
    ];
}
