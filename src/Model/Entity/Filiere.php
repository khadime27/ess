<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Filiere Entity
 *
 * @property int $id
 * @property string|null $nom
 * @property string|null $photo
 *
 * @property \App\Model\Entity\Dossier[] $dossiers
 */
class Filiere extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'nom' => true,
        'photo' => true,
        'dossiers' => true
    ];
}
