<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * DossiersFiliere Entity
 *
 * @property int $id
 * @property int|null $dossiers_id
 * @property int|null $filieres_id
 * @property string|null $link_type
 *
 * @property \App\Model\Entity\Dossier $dossier
 * @property \App\Model\Entity\Filiere $filiere
 */
class DossiersFiliere extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'dossiers_id' => true,
        'filieres_id' => true,
        'link_type' => true,
        'dossier' => true,
        'filiere' => true
    ];
}
