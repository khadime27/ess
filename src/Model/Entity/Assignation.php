<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Assignation Entity
 *
 * @property int $id
 * @property int $agent_id
 * @property int $dossier_id
 * @property int|null $rang
 * @property int|null $status_validations_id
 * @property string|null $type
 * @property string|null $code
 * @property string|null $type_demande
 * @property \Cake\I18n\FrozenTime $created
 *
 * @property \App\Model\Entity\Agent $agent
 * @property \App\Model\Entity\Dossier $dossier
 * @property \App\Model\Entity\StatusValidation $status_validation
 */
class Assignation extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'agent_id' => true,
        'dossier_id' => true,
        'rang' => true,
        'status_validations_id' => true,
        'type' => true,
        'code' => true,
        'type_demande' => true,
        'created' => true,
        'agent' => true,
        'dossier' => true,
        'status_validation' => true
    ];
}
