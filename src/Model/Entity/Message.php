<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Message Entity
 *
 * @property int $id
 * @property \Cake\I18n\FrozenTime|null $date
 * @property string|null $resume
 * @property int|null $agent_id
 * @property int|null $dossier_id
 * @property int|null $porteur_id
 * @property string|null $url
 *
 * @property \App\Model\Entity\Agent $agent
 * @property \App\Model\Entity\Dossier $dossier
 * @property \App\Model\Entity\Porteur $porteur
 * @property \App\Model\Entity\PieceJointe[] $piece_jointes
 */
class Message extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'date' => true,
        'resume' => true,
        'agent_id' => true,
        'dossier_id' => true,
        'porteur_id' => true,
        'url' => true,
        'agent' => true,
        'dossier' => true,
        'porteur' => true,
        'piece_jointes' => true
    ];
}
