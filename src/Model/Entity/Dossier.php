<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Dossier Entity
 *
 * @property int $id
 * @property string|null $intitule
 * @property string|null $numero
 * @property int|null $statut_dossiers_id
 * @property \Cake\I18n\FrozenTime|null $created
 * @property string|null $descrip_projet
 * @property int|null $nature_demande_id
 * @property string|null $descrip_marche
 * @property string|null $demarche_cmerc
 * @property int|null $porteur_id
 * @property int|null $filiere_id
 * @property int|null $commune_id
 * @property string|null $soumis
 * @property int|null $agent_id
 * @property bool|null $demande_label
 * @property int|null $nb_perso_impact
 * @property bool|null $label
 * @property string|null $type_label
 * @property bool|null $emission
 * @property int|null $statut_financements_id
 *
 * @property \App\Model\Entity\StatutDossier $statut_dossier
 * @property \App\Model\Entity\NatureDemande $nature_demande
 * @property \App\Model\Entity\Porteur $porteur
 * @property \App\Model\Entity\Filiere $filiere
 * @property \App\Model\Entity\Commune $commune
 * @property \App\Model\Entity\Agent $agent
 * @property \App\Model\Entity\Assignation[] $assignations
 * @property \App\Model\Entity\FinancementCredit[] $financement_credits
 * @property \App\Model\Entity\Imputation[] $imputations
 * @property \App\Model\Entity\Message[] $messages
 * @property \App\Model\Entity\Notation[] $notations
 * @property \App\Model\Entity\PieceJointe[] $piece_jointes
 * @property \App\Model\Entity\Surete[] $suretes
 */
class Dossier extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'intitule' => true,
        'numero' => true,
        'statut_dossiers_id' => true,
        'created' => true,
        'descrip_projet' => true,
        'nature_demande_id' => true,
        'descrip_marche' => true,
        'demarche_cmerc' => true,
        'porteur_id' => true,
        'filiere_id' => true,
        'commune_id' => true,
        'soumis' => true,
        'agent_id' => true,
        'demande_label' => true,
        'nb_perso_impact' => true,
        'nb_femme_impact' => true,
        'nb_homme_impact' => true,
        'label' => true,
        'type_label' => true,
        'note_final' => true,
        'emission' => true,
        'statut_financements_id' => true,
        'emission_finance' => true,
        'note_financement' => true,
        'statut_dossier' => true,
        'nature_demande' => true,
        'porteur' => true,
        'filiere' => true,
        'commune' => true,
        'agent' => true,
        'assignations' => true,
        'financement_credits' => true,
        'imputations' => true,
        'messages' => true,
        'notations' => true,
        'piece_jointes' => true,
        'suretes' => true
    ];
}
