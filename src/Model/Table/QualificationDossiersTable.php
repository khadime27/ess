<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * QualificationDossiers Model
 *
 * @property \App\Model\Table\DossiersTable|\Cake\ORM\Association\BelongsTo $Dossiers
 * @property \App\Model\Table\AgentsTable|\Cake\ORM\Association\BelongsTo $Agents
 *
 * @method \App\Model\Entity\QualificationDossier get($primaryKey, $options = [])
 * @method \App\Model\Entity\QualificationDossier newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\QualificationDossier[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\QualificationDossier|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\QualificationDossier|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\QualificationDossier patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\QualificationDossier[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\QualificationDossier findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class QualificationDossiersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('qualification_dossiers');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Dossiers', [
            'foreignKey' => 'dossiers_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Agents', [
            'foreignKey' => 'agents_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        $validator
            ->scalar('desc_etat_civil')
            ->allowEmptyString('desc_etat_civil');

        $validator
            ->scalar('desc_dossier')
            ->allowEmptyString('desc_dossier');

        $validator
            ->scalar('desc_impact')
            ->allowEmptyString('desc_impact');

        $validator
            ->scalar('raison_rejet')
            ->allowEmptyString('raison_rejet');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['dossiers_id'], 'Dossiers'));
        $rules->add($rules->existsIn(['agents_id'], 'Agents'));

        return $rules;
    }
}
