<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Agences Model
 *
 * @property \App\Model\Table\StructuresTable|\Cake\ORM\Association\BelongsTo $Structures
 * @property \App\Model\Table\TypeAgencesTable|\Cake\ORM\Association\BelongsTo $TypeAgences
 * @property |\Cake\ORM\Association\BelongsTo $Regions
 * @property \App\Model\Table\AgentsTable|\Cake\ORM\Association\HasMany $Agents
 *
 * @method \App\Model\Entity\Agence get($primaryKey, $options = [])
 * @method \App\Model\Entity\Agence newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Agence[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Agence|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Agence|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Agence patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Agence[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Agence findOrCreate($search, callable $callback = null, $options = [])
 */
class AgencesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('agences');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Structures', [
            'foreignKey' => 'structure_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('TypeAgences', [
            'foreignKey' => 'types_agences_id'
        ]);
        $this->belongsTo('Regions', [
            'foreignKey' => 'regions_id'
        ]);
        $this->hasMany('Agents', [
            'foreignKey' => 'agence_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        $validator
            ->scalar('nom')
            ->maxLength('nom', 250)
            ->allowEmptyString('nom');

        $validator
            ->scalar('telephone')
            ->maxLength('telephone', 50)
            ->allowEmptyString('telephone');

        $validator
            ->scalar('adresse')
            ->maxLength('adresse', 250)
            ->allowEmptyString('adresse');

        $validator
            ->boolean('etat')
            ->allowEmptyString('etat');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['structure_id'], 'Structures'));
        $rules->add($rules->existsIn(['types_agences_id'], 'TypeAgences'));
        $rules->add($rules->existsIn(['regions_id'], 'Regions'));

        return $rules;
    }
}
