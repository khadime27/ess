<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Notations Model
 *
 * @property \App\Model\Table\DossiersTable|\Cake\ORM\Association\BelongsTo $Dossiers
 * @property \App\Model\Table\AgentsTable|\Cake\ORM\Association\BelongsTo $Agents
 *
 * @method \App\Model\Entity\Notation get($primaryKey, $options = [])
 * @method \App\Model\Entity\Notation newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Notation[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Notation|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Notation|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Notation patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Notation[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Notation findOrCreate($search, callable $callback = null, $options = [])
 */
class NotationsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('notations');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Dossiers', [
            'foreignKey' => 'dossiers_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Agents', [
            'foreignKey' => 'agents_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        $validator
            ->scalar('code')
            ->maxLength('code', 255)
            ->allowEmptyString('code');

        $validator
            ->integer('nat_struc_juridique')
            ->allowEmptyString('nat_struc_juridique');

        $validator
            ->integer('eligibilite_filiere')
            ->allowEmptyString('eligibilite_filiere');

        $validator
            ->integer('partenariat_relation')
            ->allowEmptyString('partenariat_relation');

        $validator
            ->integer('pertinence_activite')
            ->allowEmptyString('pertinence_activite');

        $validator
            ->integer('coherence_activite')
            ->allowEmptyString('coherence_activite');

        $validator
            ->integer('positionnement_activite')
            ->allowEmptyString('positionnement_activite');

        $validator
            ->scalar('comment_aspect_tech')
            ->allowEmptyString('comment_aspect_tech');

        $validator
            ->integer('aspect_technique')
            ->allowEmptyString('aspect_technique');

        $validator
            ->integer('sante_financiere')
            ->allowEmptyString('sante_financiere');

        $validator
            ->integer('croissance_reultat')
            ->allowEmptyString('croissance_reultat');

        $validator
            ->integer('situation_actif')
            ->allowEmptyString('situation_actif');

        $validator
            ->integer('situation_passif')
            ->allowEmptyString('situation_passif');

        $validator
            ->integer('croissance_en_vue')
            ->allowEmptyString('croissance_en_vue');

        $validator
            ->integer('engagement_actuel')
            ->allowEmptyString('engagement_actuel');

        $validator
            ->integer('situation_compte')
            ->allowEmptyString('situation_compte');

        $validator
            ->scalar('comment_aspect_financ')
            ->allowEmptyString('comment_aspect_financ');

        $validator
            ->integer('aspect_financier')
            ->allowEmptyString('aspect_financier');

        $validator
            ->integer('eligibilite_ess')
            ->allowEmptyString('eligibilite_ess');

        $validator
            ->integer('impact_social_env')
            ->allowEmptyString('impact_social_env');

        $validator
            ->integer('influence_territoriale')
            ->allowEmptyString('influence_territoriale');

        $validator
            ->integer('autre_critere_ess1')
            ->allowEmptyString('autre_critere_ess1');

        $validator
            ->integer('autre_critere_ess2')
            ->allowEmptyString('autre_critere_ess2');

        $validator
            ->integer('pourcentage_revenu_dirigeants')
            ->allowEmptyString('pourcentage_revenu_dirigeants');

        $validator
            ->scalar('comment_aspect_env')
            ->allowEmptyString('comment_aspect_env');

        $validator
            ->integer('aspect_env')
            ->allowEmptyString('aspect_env');

        $validator
            ->scalar('raison_rejet')
            ->allowEmptyString('raison_rejet');

        $validator
            ->scalar('type_demande')
            ->maxLength('type_demande', 255)
            ->allowEmptyString('type_demande');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['dossiers_id'], 'Dossiers'));
        $rules->add($rules->existsIn(['agents_id'], 'Agents'));

        return $rules;
    }
}
