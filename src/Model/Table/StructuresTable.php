<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Structures Model
 *
 * @property \App\Model\Table\AgencesTable|\Cake\ORM\Association\HasMany $Agences
 * @property \App\Model\Table\AgentsTable|\Cake\ORM\Association\HasMany $Agents
 * @property \App\Model\Table\AssignationsTable|\Cake\ORM\Association\HasMany $Assignations
 * @property \App\Model\Table\OtsTable|\Cake\ORM\Association\HasMany $Ots
 *
 * @method \App\Model\Entity\Structure get($primaryKey, $options = [])
 * @method \App\Model\Entity\Structure newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Structure[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Structure|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Structure|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Structure patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Structure[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Structure findOrCreate($search, callable $callback = null, $options = [])
 */
class StructuresTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('structures');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->hasMany('Agences', [
            'foreignKey' => 'structure_id'
        ]);
        $this->hasMany('Agents', [
            'foreignKey' => 'structure_id'
        ]);
        $this->hasMany('Assignations', [
            'foreignKey' => 'structure_id'
        ]);
        $this->hasMany('Ots', [
            'foreignKey' => 'structure_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        $validator
            ->scalar('nom')
            ->maxLength('nom', 500)
            ->requirePresence('nom', 'create')
            ->allowEmptyString('nom', false);

        $validator
            ->scalar('telephone')
            ->maxLength('telephone', 45)
            ->requirePresence('telephone', 'create')
            ->allowEmptyString('telephone', false);

        $validator
            ->scalar('adresse')
            ->maxLength('adresse', 250)
            ->requirePresence('adresse', 'create')
            ->allowEmptyString('adresse', false);

        return $validator;
    }
}
