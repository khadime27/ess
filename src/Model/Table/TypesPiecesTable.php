<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * TypesPieces Model
 *
 * @method \App\Model\Entity\TypesPiece get($primaryKey, $options = [])
 * @method \App\Model\Entity\TypesPiece newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\TypesPiece[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\TypesPiece|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\TypesPiece|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\TypesPiece patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\TypesPiece[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\TypesPiece findOrCreate($search, callable $callback = null, $options = [])
 */
class TypesPiecesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('types_pieces');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        $validator
            ->scalar('libelle')
            ->maxLength('libelle', 255)
            ->allowEmptyString('libelle');

        return $validator;
    }
}
