<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Bibliotheques Model
 *
 * @method \App\Model\Entity\Bibliotheque get($primaryKey, $options = [])
 * @method \App\Model\Entity\Bibliotheque newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Bibliotheque[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Bibliotheque|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Bibliotheque|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Bibliotheque patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Bibliotheque[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Bibliotheque findOrCreate($search, callable $callback = null, $options = [])
 */
class BibliothequesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('bibliotheques');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        $validator
            ->scalar('libelle')
            ->maxLength('libelle', 255)
            ->allowEmptyString('libelle');

        $validator
            ->scalar('document')
            ->maxLength('document', 255)
            ->allowEmptyString('document');

        return $validator;
    }
}
