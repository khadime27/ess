<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Agents Model
 *
 * @property \App\Model\Table\UsersTable|\Cake\ORM\Association\BelongsTo $Users
 * @property \App\Model\Table\AgencesTable|\Cake\ORM\Association\BelongsTo $Agences
 * @property \App\Model\Table\StructuresTable|\Cake\ORM\Association\BelongsTo $Structures
 * @property \App\Model\Table\ComitesTable|\Cake\ORM\Association\BelongsTo $Comites
 * @property |\Cake\ORM\Association\BelongsTo $Departements
 * @property \App\Model\Table\AnimationsTable|\Cake\ORM\Association\HasMany $Animations
 * @property \App\Model\Table\AssignationsTable|\Cake\ORM\Association\HasMany $Assignations
 * @property \App\Model\Table\DossiersTable|\Cake\ORM\Association\HasMany $Dossiers
 * @property \App\Model\Table\MessagesTable|\Cake\ORM\Association\HasMany $Messages
 * @property \App\Model\Table\SuiviEvaluationsTable|\Cake\ORM\Association\HasMany $SuiviEvaluations
 *
 * @method \App\Model\Entity\Agent get($primaryKey, $options = [])
 * @method \App\Model\Entity\Agent newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Agent[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Agent|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Agent|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Agent patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Agent[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Agent findOrCreate($search, callable $callback = null, $options = [])
 */
class AgentsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('agents');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Agences', [
            'foreignKey' => 'agence_id'
        ]);
        $this->belongsTo('Structures', [
            'foreignKey' => 'structure_id'
        ]);
        $this->belongsTo('Comites', [
            'foreignKey' => 'comites_id'
        ]);
        $this->belongsTo('Departements', [
            'foreignKey' => 'departements_id'
        ]);
        $this->hasMany('Animations', [
            'foreignKey' => 'agent_id'
        ]);
        $this->hasMany('Assignations', [
            'foreignKey' => 'agent_id'
        ]);
        $this->hasMany('Dossiers', [
            'foreignKey' => 'agent_id'
        ]);
        $this->hasMany('Messages', [
            'foreignKey' => 'agent_id'
        ]);
        $this->hasMany('SuiviEvaluations', [
            'foreignKey' => 'agent_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        $validator
            ->scalar('prenom')
            ->maxLength('prenom', 45)
            ->allowEmptyString('prenom');

        $validator
            ->scalar('nom')
            ->maxLength('nom', 45)
            ->allowEmptyString('nom');

        $validator
            ->scalar('telephone')
            ->maxLength('telephone', 45)
            ->allowEmptyString('telephone');

        $validator
            ->scalar('adresse')
            ->maxLength('adresse', 45)
            ->allowEmptyString('adresse');

        $validator
            ->boolean('admin')
            ->allowEmptyString('admin');

        $validator
            ->boolean('superviseur')
            ->requirePresence('superviseur', 'create')
            ->allowEmptyString('superviseur', false);

        $validator
            ->boolean('etat')
            ->allowEmptyString('etat');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        $rules->add($rules->existsIn(['agence_id'], 'Agences'));
        $rules->add($rules->existsIn(['structure_id'], 'Structures'));
        $rules->add($rules->existsIn(['comites_id'], 'Comites'));
        $rules->add($rules->existsIn(['departements_id'], 'Departements'));

        return $rules;
    }
}
