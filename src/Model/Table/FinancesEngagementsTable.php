<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * FinancesEngagements Model
 *
 * @property \App\Model\Table\DossiersTable|\Cake\ORM\Association\BelongsTo $Dossiers
 *
 * @method \App\Model\Entity\FinancesEngagement get($primaryKey, $options = [])
 * @method \App\Model\Entity\FinancesEngagement newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\FinancesEngagement[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\FinancesEngagement|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\FinancesEngagement|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\FinancesEngagement patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\FinancesEngagement[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\FinancesEngagement findOrCreate($search, callable $callback = null, $options = [])
 */
class FinancesEngagementsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('finances_engagements');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Dossiers', [
            'foreignKey' => 'dossiers_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        $validator
            ->scalar('libelle')
            ->maxLength('libelle', 255)
            ->allowEmptyString('libelle');

        $validator
            ->numeric('montant')
            ->allowEmptyString('montant');

        $validator
            ->numeric('encours')
            ->allowEmptyString('encours');

        $validator
            ->integer('differe_mois')
            ->allowEmptyString('differe_mois');

        $validator
            ->date('date_last_echeance')
            ->allowEmptyDate('date_last_echeance');

        $validator
            ->integer('duree_mois')
            ->allowEmptyString('duree_mois');

        $validator
            ->scalar('banque')
            ->maxLength('banque', 255)
            ->allowEmptyString('banque');

        $validator
            ->numeric('taux_interet')
            ->allowEmptyString('taux_interet');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['dossiers_id'], 'Dossiers'));

        return $rules;
    }
}
