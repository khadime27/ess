<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * TypesImpacts Model
 *
 * @property |\Cake\ORM\Association\BelongsTo $Impactes
 *
 * @method \App\Model\Entity\TypesImpact get($primaryKey, $options = [])
 * @method \App\Model\Entity\TypesImpact newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\TypesImpact[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\TypesImpact|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\TypesImpact|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\TypesImpact patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\TypesImpact[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\TypesImpact findOrCreate($search, callable $callback = null, $options = [])
 */
class TypesImpactsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('types_impacts');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Impactes', [
            'foreignKey' => 'impactes_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        $validator
            ->scalar('libelle')
            ->maxLength('libelle', 255)
            ->allowEmptyString('libelle');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['impactes_id'], 'Impactes'));

        return $rules;
    }
}
