<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * NatureDemandes Model
 *
 * @property \App\Model\Table\DossiersTable|\Cake\ORM\Association\HasMany $Dossiers
 *
 * @method \App\Model\Entity\NatureDemande get($primaryKey, $options = [])
 * @method \App\Model\Entity\NatureDemande newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\NatureDemande[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\NatureDemande|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\NatureDemande|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\NatureDemande patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\NatureDemande[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\NatureDemande findOrCreate($search, callable $callback = null, $options = [])
 */
class NatureDemandesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('nature_demandes');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->hasMany('Dossiers', [
            'foreignKey' => 'nature_demande_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        $validator
            ->scalar('nom')
            ->maxLength('nom', 45)
            ->allowEmptyString('nom');

        return $validator;
    }
}
