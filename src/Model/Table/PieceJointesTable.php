<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * PieceJointes Model
 *
 * @property \App\Model\Table\MessagesTable|\Cake\ORM\Association\BelongsTo $Messages
 * @property \App\Model\Table\DossiersTable|\Cake\ORM\Association\BelongsTo $Dossiers
 * @property \App\Model\Table\MediationsTable|\Cake\ORM\Association\BelongsTo $Mediations
 * @property \App\Model\Table\AnimationsTable|\Cake\ORM\Association\BelongsTo $Animations
 * @property \App\Model\Table\SuiviEvaluationsTable|\Cake\ORM\Association\BelongsTo $SuiviEvaluations
 *
 * @method \App\Model\Entity\PieceJointe get($primaryKey, $options = [])
 * @method \App\Model\Entity\PieceJointe newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\PieceJointe[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\PieceJointe|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\PieceJointe|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\PieceJointe patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\PieceJointe[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\PieceJointe findOrCreate($search, callable $callback = null, $options = [])
 */
class PieceJointesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('piece_jointes');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Messages', [
            'foreignKey' => 'message_id'
        ]);
        $this->belongsTo('Dossiers', [
            'foreignKey' => 'dossier_id'
        ]);
        $this->belongsTo('Mediations', [
            'foreignKey' => 'mediation_id'
        ]);
        $this->belongsTo('Animations', [
            'foreignKey' => 'animation_id'
        ]);
        $this->belongsTo('SuiviEvaluations', [
            'foreignKey' => 'suivi_evaluation_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        $validator
            ->scalar('url')
            ->maxLength('url', 100)
            ->allowEmptyString('url');

        $validator
            ->scalar('titre')
            ->maxLength('titre', 500)
            ->allowEmptyString('titre');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['message_id'], 'Messages'));
        $rules->add($rules->existsIn(['dossier_id'], 'Dossiers'));
        $rules->add($rules->existsIn(['mediation_id'], 'Mediations'));
        $rules->add($rules->existsIn(['animation_id'], 'Animations'));
        $rules->add($rules->existsIn(['suivi_evaluation_id'], 'SuiviEvaluations'));

        return $rules;
    }
}
