<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * SuiviEvaluations Model
 *
 * @property \App\Model\Table\TypeEvaluationsTable|\Cake\ORM\Association\BelongsTo $TypeEvaluations
 * @property \App\Model\Table\AgentsTable|\Cake\ORM\Association\BelongsTo $Agents
 * @property \App\Model\Table\PieceJointesTable|\Cake\ORM\Association\HasMany $PieceJointes
 *
 * @method \App\Model\Entity\SuiviEvaluation get($primaryKey, $options = [])
 * @method \App\Model\Entity\SuiviEvaluation newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\SuiviEvaluation[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\SuiviEvaluation|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\SuiviEvaluation|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\SuiviEvaluation patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\SuiviEvaluation[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\SuiviEvaluation findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class SuiviEvaluationsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('suivi_evaluations');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('TypeEvaluations', [
            'foreignKey' => 'type_evaluation_id'
        ]);
        $this->belongsTo('Agents', [
            'foreignKey' => 'agent_id'
        ]);
        $this->hasMany('PieceJointes', [
            'foreignKey' => 'suivi_evaluation_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        $validator
            ->scalar('titre')
            ->maxLength('titre', 500)
            ->allowEmptyString('titre');

        $validator
            ->scalar('description')
            ->allowEmptyString('description');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['type_evaluation_id'], 'TypeEvaluations'));
        $rules->add($rules->existsIn(['agent_id'], 'Agents'));

        return $rules;
    }
}
