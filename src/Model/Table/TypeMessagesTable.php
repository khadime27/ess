<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * TypeMessages Model
 *
 * @property \App\Model\Table\MessagesTable|\Cake\ORM\Association\HasMany $Messages
 *
 * @method \App\Model\Entity\TypeMessage get($primaryKey, $options = [])
 * @method \App\Model\Entity\TypeMessage newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\TypeMessage[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\TypeMessage|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\TypeMessage|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\TypeMessage patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\TypeMessage[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\TypeMessage findOrCreate($search, callable $callback = null, $options = [])
 */
class TypeMessagesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('type_messages');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->hasMany('Messages', [
            'foreignKey' => 'type_message_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        $validator
            ->scalar('nom')
            ->maxLength('nom', 250)
            ->allowEmptyString('nom');

        return $validator;
    }
}
