<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * FinancementsActifs Model
 *
 * @property \App\Model\Table\DossiersTable|\Cake\ORM\Association\BelongsTo $Dossiers
 *
 * @method \App\Model\Entity\FinancementsActif get($primaryKey, $options = [])
 * @method \App\Model\Entity\FinancementsActif newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\FinancementsActif[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\FinancementsActif|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\FinancementsActif|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\FinancementsActif patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\FinancementsActif[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\FinancementsActif findOrCreate($search, callable $callback = null, $options = [])
 */
class FinancementsActifsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('financements_actifs');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Dossiers', [
            'foreignKey' => 'dossiers_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        $validator
            ->scalar('libelle')
            ->maxLength('libelle', 255)
            ->allowEmptyString('libelle');

        $validator
            ->numeric('annee_moins_trois')
            ->allowEmptyString('annee_moins_trois');

        $validator
            ->numeric('annee_moins_deux')
            ->allowEmptyString('annee_moins_deux');

        $validator
            ->numeric('annee_moins_un')
            ->allowEmptyString('annee_moins_un');

        $validator
            ->numeric('annee')
            ->allowEmptyString('annee');

        $validator
            ->numeric('prevision')
            ->allowEmptyString('prevision');

        $validator
            ->numeric('variation')
            ->allowEmptyString('variation');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['dossiers_id'], 'Dossiers'));

        return $rules;
    }
}
