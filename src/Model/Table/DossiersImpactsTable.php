<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * DossiersImpacts Model
 *
 * @property \App\Model\Table\DossiersTable|\Cake\ORM\Association\BelongsTo $Dossiers
 * @property \App\Model\Table\TypesImpactsTable|\Cake\ORM\Association\BelongsTo $TypesImpacts
 *
 * @method \App\Model\Entity\DossiersImpact get($primaryKey, $options = [])
 * @method \App\Model\Entity\DossiersImpact newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\DossiersImpact[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\DossiersImpact|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\DossiersImpact|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\DossiersImpact patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\DossiersImpact[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\DossiersImpact findOrCreate($search, callable $callback = null, $options = [])
 */
class DossiersImpactsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('dossiers_impacts');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Dossiers', [
            'foreignKey' => 'dossiers_id'
        ]);
        $this->belongsTo('TypesImpacts', [
            'foreignKey' => 'types_impacts_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['dossiers_id'], 'Dossiers'));
        $rules->add($rules->existsIn(['types_impacts_id'], 'TypesImpacts'));

        return $rules;
    }
}
