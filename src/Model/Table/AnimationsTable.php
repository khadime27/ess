<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Animations Model
 *
 * @property \App\Model\Table\TypeAnimationsTable|\Cake\ORM\Association\BelongsTo $TypeAnimations
 * @property \App\Model\Table\AgentsTable|\Cake\ORM\Association\BelongsTo $Agents
 * @property \App\Model\Table\ParticipantsTable|\Cake\ORM\Association\HasMany $Participants
 * @property \App\Model\Table\PieceJointesTable|\Cake\ORM\Association\HasMany $PieceJointes
 *
 * @method \App\Model\Entity\Animation get($primaryKey, $options = [])
 * @method \App\Model\Entity\Animation newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Animation[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Animation|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Animation|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Animation patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Animation[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Animation findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class AnimationsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('animations');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('TypeAnimations', [
            'foreignKey' => 'type_animation_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Agents', [
            'foreignKey' => 'agent_id'
        ]);
        $this->hasMany('Participants', [
            'foreignKey' => 'animation_id'
        ]);
        $this->hasMany('PieceJointes', [
            'foreignKey' => 'animation_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        $validator
            ->scalar('titre')
            ->maxLength('titre', 500)
            ->requirePresence('titre', 'create')
            ->allowEmptyString('titre', false);

        $validator
            ->scalar('description')
            ->maxLength('description', 4294967295)
            ->requirePresence('description', 'create')
            ->allowEmptyString('description', false);

        $validator
            ->scalar('lieu')
            ->maxLength('lieu', 250)
            ->allowEmptyString('lieu');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['type_animation_id'], 'TypeAnimations'));
        $rules->add($rules->existsIn(['agent_id'], 'Agents'));

        return $rules;
    }
}
