<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * TypeEntreprises Model
 *
 * @property \App\Model\Table\EntreprisesTable|\Cake\ORM\Association\HasMany $Entreprises
 *
 * @method \App\Model\Entity\TypeEntreprise get($primaryKey, $options = [])
 * @method \App\Model\Entity\TypeEntreprise newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\TypeEntreprise[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\TypeEntreprise|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\TypeEntreprise|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\TypeEntreprise patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\TypeEntreprise[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\TypeEntreprise findOrCreate($search, callable $callback = null, $options = [])
 */
class TypeEntreprisesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('type_entreprises');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->hasMany('Entreprises', [
            'foreignKey' => 'type_entreprise_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        $validator
            ->scalar('nom')
            ->maxLength('nom', 250)
            ->allowEmptyString('nom');

        return $validator;
    }
}
