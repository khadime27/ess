<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * TypeAgences Model
 *
 * @method \App\Model\Entity\TypeAgence get($primaryKey, $options = [])
 * @method \App\Model\Entity\TypeAgence newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\TypeAgence[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\TypeAgence|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\TypeAgence|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\TypeAgence patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\TypeAgence[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\TypeAgence findOrCreate($search, callable $callback = null, $options = [])
 */
class TypeAgencesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('type_agences');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        $validator
            ->scalar('libelle')
            ->maxLength('libelle', 255)
            ->allowEmptyString('libelle');

        return $validator;
    }
}
