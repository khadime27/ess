<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Dossiers Model
 *
 * @property \App\Model\Table\StatutDossiersTable|\Cake\ORM\Association\BelongsTo $StatutDossiers
 * @property \App\Model\Table\NatureDemandesTable|\Cake\ORM\Association\BelongsTo $NatureDemandes
 * @property \App\Model\Table\PorteursTable|\Cake\ORM\Association\BelongsTo $Porteurs
 * @property \App\Model\Table\FilieresTable|\Cake\ORM\Association\BelongsTo $Filieres
 * @property \App\Model\Table\CommunesTable|\Cake\ORM\Association\BelongsTo $Communes
 * @property \App\Model\Table\AgentsTable|\Cake\ORM\Association\BelongsTo $Agents
 * @property |\Cake\ORM\Association\BelongsTo $StatutFinancements
 * @property \App\Model\Table\AssignationsTable|\Cake\ORM\Association\HasMany $Assignations
 * @property \App\Model\Table\FinancementCreditsTable|\Cake\ORM\Association\HasMany $FinancementCredits
 * @property \App\Model\Table\ImputationsTable|\Cake\ORM\Association\HasMany $Imputations
 * @property \App\Model\Table\MessagesTable|\Cake\ORM\Association\HasMany $Messages
 * @property \App\Model\Table\PieceJointesTable|\Cake\ORM\Association\HasMany $PieceJointes
 * @property \App\Model\Table\SuretesTable|\Cake\ORM\Association\HasMany $Suretes
 * @property \App\Model\Table\FilieresTable|\Cake\ORM\Association\BelongsToMany $Filieres
 *
 * @method \App\Model\Entity\Dossier get($primaryKey, $options = [])
 * @method \App\Model\Entity\Dossier newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Dossier[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Dossier|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Dossier|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Dossier patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Dossier[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Dossier findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class DossiersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('dossiers');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('StatutDossiers', [
            'foreignKey' => 'statut_dossiers_id'
        ]);
        $this->belongsTo('NatureDemandes', [
            'foreignKey' => 'nature_demande_id'
        ]);
        $this->belongsTo('Porteurs', [
            'foreignKey' => 'porteur_id'
        ]);
        $this->belongsTo('Filieres', [
            'foreignKey' => 'filiere_id'
        ]);
        $this->belongsTo('Communes', [
            'foreignKey' => 'commune_id'
        ]);
        $this->belongsTo('Agents', [
            'foreignKey' => 'agent_id'
        ]);
        $this->belongsTo('StatutFinancements', [
            'foreignKey' => 'statut_financements_id'
        ]);
        $this->hasMany('Assignations', [
            'foreignKey' => 'dossier_id'
        ]);
        $this->hasMany('FinancementCredits', [
            'foreignKey' => 'dossier_id'
        ]);
        $this->hasMany('Imputations', [
            'foreignKey' => 'dossier_id'
        ]);
        $this->hasMany('Messages', [
            'foreignKey' => 'dossier_id'
        ]);
        $this->hasMany('PieceJointes', [
            'foreignKey' => 'dossier_id'
        ]);
        $this->hasMany('Suretes', [
            'foreignKey' => 'dossier_id'
        ]);
        /*
        $this->belongsToMany('Filieres', [
            'foreignKey' => 'dossier_id',
            'targetForeignKey' => 'filiere_id',
            'joinTable' => 'dossiers_filieres'
        ]);
        */
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        $validator
            ->scalar('intitule')
            ->maxLength('intitule', 255)
            ->allowEmptyString('intitule');

        $validator
            ->scalar('numero')
            ->maxLength('numero', 45)
            ->allowEmptyString('numero');

        $validator
            ->scalar('descrip_projet')
            ->maxLength('descrip_projet', 16777215)
            ->allowEmptyString('descrip_projet');

        $validator
            ->scalar('descrip_marche')
            ->maxLength('descrip_marche', 16777215)
            ->allowEmptyString('descrip_marche');

        $validator
            ->scalar('demarche_cmerc')
            ->allowEmptyString('demarche_cmerc');

        $validator
            ->scalar('soumis')
            ->maxLength('soumis', 45)
            ->allowEmptyString('soumis');

        $validator
            ->boolean('demande_label')
            ->allowEmptyString('demande_label');

        $validator
            ->integer('nb_perso_impact')
            ->allowEmptyString('nb_perso_impact');

        $validator
            ->boolean('label')
            ->allowEmptyString('label');

        $validator
            ->scalar('type_label')
            ->maxLength('type_label', 255)
            ->allowEmptyString('type_label');

        $validator
            ->boolean('emission')
            ->allowEmptyString('emission');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['statut_dossiers_id'], 'StatutDossiers'));
        $rules->add($rules->existsIn(['nature_demande_id'], 'NatureDemandes'));
        $rules->add($rules->existsIn(['porteur_id'], 'Porteurs'));
        $rules->add($rules->existsIn(['filiere_id'], 'Filieres'));
        $rules->add($rules->existsIn(['commune_id'], 'Communes'));
        $rules->add($rules->existsIn(['agent_id'], 'Agents'));
        $rules->add($rules->existsIn(['statut_financements_id'], 'StatutFinancements'));

        return $rules;
    }
}
