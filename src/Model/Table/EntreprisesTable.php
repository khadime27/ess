<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Entreprises Model
 *
 * @property \App\Model\Table\TypeEntreprisesTable|\Cake\ORM\Association\BelongsTo $TypeEntreprises
 * @property \App\Model\Table\CommunesTable|\Cake\ORM\Association\BelongsTo $Communes
 * @property \App\Model\Table\PorteursTable|\Cake\ORM\Association\HasMany $Porteurs
 *
 * @method \App\Model\Entity\Entreprise get($primaryKey, $options = [])
 * @method \App\Model\Entity\Entreprise newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Entreprise[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Entreprise|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Entreprise|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Entreprise patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Entreprise[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Entreprise findOrCreate($search, callable $callback = null, $options = [])
 */
class EntreprisesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('entreprises');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('TypeEntreprises', [
            'foreignKey' => 'type_entreprise_id'
        ]);
        $this->belongsTo('Communes', [
            'foreignKey' => 'communes_id'
        ]);
        $this->hasMany('Porteurs', [
            'foreignKey' => 'entreprise_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        $validator
            ->scalar('ninea')
            ->maxLength('ninea', 45)
            ->allowEmptyString('ninea');

        $validator
            ->scalar('nom')
            ->maxLength('nom', 45)
            ->allowEmptyString('nom');

        $validator
            ->integer('nb_employe')
            ->allowEmptyString('nb_employe');

        $validator
            ->date('date_creation')
            ->allowEmptyDate('date_creation');

        $validator
            ->scalar('immatriculation')
            ->maxLength('immatriculation', 50)
            ->allowEmptyString('immatriculation');

        $validator
            ->scalar('type_social')
            ->maxLength('type_social', 50)
            ->allowEmptyString('type_social');

        $validator
            ->scalar('social_link')
            ->maxLength('social_link', 255)
            ->allowEmptyString('social_link');

        $validator
            ->scalar('tel_entite')
            ->maxLength('tel_entite', 255)
            ->allowEmptyString('tel_entite');

        $validator
            ->integer('nb_salarie')
            ->allowEmptyString('nb_salarie');

        $validator
            ->integer('nb_stageaire')
            ->allowEmptyString('nb_stageaire');

        $validator
            ->integer('nb_saisonnier')
            ->allowEmptyString('nb_saisonnier');

        $validator
            ->boolean('association')
            ->allowEmptyString('association');

        $validator
            ->boolean('formel')
            ->allowEmptyString('formel');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['type_entreprise_id'], 'TypeEntreprises'));
        $rules->add($rules->existsIn(['communes_id'], 'Communes'));

        return $rules;
    }
}
