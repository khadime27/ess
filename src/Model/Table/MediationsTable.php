<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Mediations Model
 *
 * @property \App\Model\Table\FinancementCreditsTable|\Cake\ORM\Association\BelongsTo $FinancementCredits
 * @property \App\Model\Table\StatusTable|\Cake\ORM\Association\BelongsTo $Status
 * @property \App\Model\Table\PieceJointesTable|\Cake\ORM\Association\HasMany $PieceJointes
 *
 * @method \App\Model\Entity\Mediation get($primaryKey, $options = [])
 * @method \App\Model\Entity\Mediation newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Mediation[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Mediation|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Mediation|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Mediation patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Mediation[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Mediation findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class MediationsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('mediations');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('FinancementCredits', [
            'foreignKey' => 'financement_credit_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Status', [
            'foreignKey' => 'statu_id'
        ]);
        $this->hasMany('PieceJointes', [
            'foreignKey' => 'mediation_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        $validator
            ->scalar('description')
            ->maxLength('description', 16777215)
            ->requirePresence('description', 'create')
            ->allowEmptyString('description', false);

        $validator
            ->scalar('titre')
            ->maxLength('titre', 500)
            ->requirePresence('titre', 'create')
            ->allowEmptyString('titre', false);

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['financement_credit_id'], 'FinancementCredits'));
        $rules->add($rules->existsIn(['statu_id'], 'Status'));

        return $rules;
    }
}
