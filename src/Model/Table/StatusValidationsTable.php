<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * StatusValidations Model
 *
 * @method \App\Model\Entity\StatusValidation get($primaryKey, $options = [])
 * @method \App\Model\Entity\StatusValidation newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\StatusValidation[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\StatusValidation|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\StatusValidation|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\StatusValidation patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\StatusValidation[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\StatusValidation findOrCreate($search, callable $callback = null, $options = [])
 */
class StatusValidationsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('status_validations');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        $validator
            ->scalar('nom')
            ->maxLength('nom', 250)
            ->requirePresence('nom', 'create')
            ->allowEmptyString('nom', false);

        return $validator;
    }
}
