<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * TypeEvaluations Model
 *
 * @property \App\Model\Table\SuiviEvaluationsTable|\Cake\ORM\Association\HasMany $SuiviEvaluations
 *
 * @method \App\Model\Entity\TypeEvaluation get($primaryKey, $options = [])
 * @method \App\Model\Entity\TypeEvaluation newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\TypeEvaluation[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\TypeEvaluation|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\TypeEvaluation|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\TypeEvaluation patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\TypeEvaluation[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\TypeEvaluation findOrCreate($search, callable $callback = null, $options = [])
 */
class TypeEvaluationsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('type_evaluations');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->hasMany('SuiviEvaluations', [
            'foreignKey' => 'type_evaluation_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        $validator
            ->scalar('nom')
            ->maxLength('nom', 255)
            ->requirePresence('nom', 'create')
            ->allowEmptyString('nom', false);

        return $validator;
    }
}
