/*
SQLyog Community v13.1.5  (64 bit)
MySQL - 5.7.14 : Database - ess
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`ess` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `ess`;

/*Table structure for table `agences` */

DROP TABLE IF EXISTS `agences`;

CREATE TABLE `agences` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(250) DEFAULT NULL,
  `structure_id` int(11) NOT NULL,
  `telephone` varchar(50) DEFAULT NULL,
  `adresse` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_agences_structures1_idx` (`structure_id`),
  CONSTRAINT `fk_agences_structures1` FOREIGN KEY (`structure_id`) REFERENCES `structures` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Table structure for table `agents` */

DROP TABLE IF EXISTS `agents`;

CREATE TABLE `agents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `prenom` varchar(45) DEFAULT NULL,
  `nom` varchar(45) DEFAULT NULL,
  `telephone` varchar(45) DEFAULT NULL,
  `adresse` varchar(45) DEFAULT NULL,
  `ot_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `agent_id` int(11) DEFAULT NULL,
  `agence_id` int(11) DEFAULT NULL,
  `structure_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_agents_Ots1_idx` (`ot_id`),
  KEY `fk_agents_Users1_idx` (`user_id`),
  KEY `fk_agents_agents1_idx` (`agent_id`),
  KEY `fk_agents_agences1_idx` (`agence_id`),
  KEY `fk_agents_structures1_idx` (`structure_id`),
  CONSTRAINT `fk_agents_Ots1` FOREIGN KEY (`ot_id`) REFERENCES `ots` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_agents_Users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_agents_agences1` FOREIGN KEY (`agence_id`) REFERENCES `agences` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_agents_agents1` FOREIGN KEY (`agent_id`) REFERENCES `agents` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_agents_structures1` FOREIGN KEY (`structure_id`) REFERENCES `structures` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

/*Table structure for table `animations` */

DROP TABLE IF EXISTS `animations`;

CREATE TABLE `animations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titre` varchar(500) NOT NULL,
  `description` longtext NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `type_animation_id` int(11) NOT NULL,
  `agent_id` int(11) DEFAULT NULL,
  `lieu` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `type_animation_id` (`type_animation_id`),
  KEY `agent_id` (`agent_id`),
  CONSTRAINT `fk_agent2_id` FOREIGN KEY (`agent_id`) REFERENCES `agents` (`id`),
  CONSTRAINT `fk_type_animation_id` FOREIGN KEY (`type_animation_id`) REFERENCES `animations` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

/*Table structure for table `articles` */

DROP TABLE IF EXISTS `articles`;

CREATE TABLE `articles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titre` varchar(500) DEFAULT NULL,
  `contenu` longtext,
  `categorie_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `image` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_articles_categories1_idx` (`categorie_id`),
  KEY `fk_article_users1_idx` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Table structure for table `assignations` */

DROP TABLE IF EXISTS `assignations`;

CREATE TABLE `assignations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ot_id` int(11) DEFAULT NULL,
  `agence_id` int(11) DEFAULT NULL,
  `structure_id` int(11) DEFAULT NULL,
  `agent_id` int(11) NOT NULL,
  `dossier_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_assignations_ots1_idx` (`ot_id`),
  KEY `fk_assignations_agences1_idx` (`agence_id`),
  KEY `fk_assignations_structures1_idx` (`structure_id`),
  KEY `fk_assignations_agents1_idx` (`agent_id`),
  KEY `dossier_id` (`dossier_id`),
  CONSTRAINT `fk_assignation_dossier` FOREIGN KEY (`dossier_id`) REFERENCES `dossiers` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_assignations_agences1` FOREIGN KEY (`agence_id`) REFERENCES `agences` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_assignations_agents1` FOREIGN KEY (`agent_id`) REFERENCES `agents` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_assignations_ots1` FOREIGN KEY (`ot_id`) REFERENCES `ots` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_assignations_structures1` FOREIGN KEY (`structure_id`) REFERENCES `structures` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Table structure for table `categories` */

DROP TABLE IF EXISTS `categories`;

CREATE TABLE `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Table structure for table `communes` */

DROP TABLE IF EXISTS `communes`;

CREATE TABLE `communes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(45) DEFAULT NULL,
  `departement_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_communes_departements1_idx` (`departement_id`)
) ENGINE=InnoDB AUTO_INCREMENT=626 DEFAULT CHARSET=latin1;

/*Table structure for table `departements` */

DROP TABLE IF EXISTS `departements`;

CREATE TABLE `departements` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `departementscol` varchar(45) DEFAULT NULL,
  `region_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_departements_regions1_idx` (`region_id`)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=latin1;

/*Table structure for table `dossiers` */

DROP TABLE IF EXISTS `dossiers`;

CREATE TABLE `dossiers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `intitule` varchar(255) DEFAULT NULL,
  `numero` varchar(45) DEFAULT NULL,
  `created` timestamp NULL DEFAULT NULL,
  `descrip_projet` mediumtext,
  `nature_demande_id` int(11) DEFAULT NULL,
  `descrip_marche` mediumtext,
  `demarche_cmerc` text,
  `porteur_id` int(11) NOT NULL,
  `filiere_id` int(11) DEFAULT NULL,
  `commune_id` int(11) NOT NULL,
  `soumis` varchar(45) DEFAULT NULL,
  `agent_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_dossiers_natureDemandes1_idx` (`nature_demande_id`),
  KEY `fk_dossiers_porteurs1_idx` (`porteur_id`),
  KEY `fk_dossiers_filieres1_idx` (`filiere_id`),
  KEY `fk_dossiers_communes1_idx` (`commune_id`),
  KEY `agent_id` (`agent_id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

/*Table structure for table `entreprises` */

DROP TABLE IF EXISTS `entreprises`;

CREATE TABLE `entreprises` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ninea` varchar(45) DEFAULT NULL,
  `nom` varchar(45) DEFAULT NULL,
  `type_entreprise_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `type_entreprise_id` (`type_entreprise_id`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=latin1;

/*Table structure for table `filieres` */

DROP TABLE IF EXISTS `filieres`;

CREATE TABLE `filieres` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

/*Table structure for table `financement_credits` */

DROP TABLE IF EXISTS `financement_credits`;

CREATE TABLE `financement_credits` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `investissement` float DEFAULT NULL,
  `fond_roulement` float DEFAULT NULL,
  `apport_valeur` float DEFAULT NULL,
  `mtnt_solicite` float DEFAULT NULL,
  `intitution_financiere_id` int(11) DEFAULT NULL,
  `apport_prcent` float DEFAULT NULL,
  `dossier_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_financement_credits_intitution_financieres1_idx` (`intitution_financiere_id`),
  KEY `dossier_id` (`dossier_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Table structure for table `imputations` */

DROP TABLE IF EXISTS `imputations`;

CREATE TABLE `imputations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dossier_id` int(11) NOT NULL,
  `agent_imput_id` int(11) NOT NULL,
  `agent_imputateur_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_Imputations_Dossiers1_idx` (`dossier_id`),
  KEY `fk_imputations_agents1_idx` (`agent_imput_id`),
  KEY `agent_imputateur_id` (`agent_imputateur_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Table structure for table `intitution_financieres` */

DROP TABLE IF EXISTS `intitution_financieres`;

CREATE TABLE `intitution_financieres` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Table structure for table `mediations` */

DROP TABLE IF EXISTS `mediations`;

CREATE TABLE `mediations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` mediumtext NOT NULL,
  `financement_credit_id` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `titre` varchar(500) NOT NULL,
  `statu_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `financement_credit_id` (`financement_credit_id`),
  KEY `statu_id` (`statu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Table structure for table `messages` */

DROP TABLE IF EXISTS `messages`;

CREATE TABLE `messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` date DEFAULT NULL,
  `resume` mediumtext,
  `type_message_id` int(11) NOT NULL,
  `agent_id` int(11) DEFAULT NULL,
  `dossier_id` int(11) DEFAULT NULL,
  `porteur_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_messages_type_messages1_idx` (`type_message_id`),
  KEY `fk_messages_agents1_idx` (`agent_id`),
  KEY `fk_messages_dossiers1_idx` (`dossier_id`),
  KEY `fk_messages_porteurs1_idx` (`porteur_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `nature_demandes` */

DROP TABLE IF EXISTS `nature_demandes`;

CREATE TABLE `nature_demandes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Table structure for table `notations` */

DROP TABLE IF EXISTS `notations`;

CREATE TABLE `notations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sect_filier` int(45) DEFAULT NULL,
  `prod_serv` int(45) DEFAULT NULL,
  `imp_emploi` int(45) DEFAULT NULL,
  `imp_terri` int(45) DEFAULT NULL,
  `imp_filier` int(45) DEFAULT NULL,
  `formation` int(45) DEFAULT NULL,
  `experience` int(45) DEFAULT NULL,
  `rep_bank` int(45) DEFAULT NULL,
  `bp` int(45) DEFAULT NULL,
  `stad_avanc` int(45) DEFAULT NULL,
  `equi_ct` int(11) DEFAULT NULL,
  `dossier_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `dossier_id` (`dossier_id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

/*Table structure for table `ots` */

DROP TABLE IF EXISTS `ots`;

CREATE TABLE `ots` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(250) NOT NULL,
  `adresse` varchar(250) DEFAULT NULL,
  `telephone` varchar(255) DEFAULT NULL,
  `structure_id` int(11) DEFAULT NULL,
  `type` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `structure_id` (`structure_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Table structure for table `participants` */

DROP TABLE IF EXISTS `participants`;

CREATE TABLE `participants` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(250) NOT NULL,
  `prenom` varchar(250) NOT NULL,
  `adresse` varchar(250) NOT NULL,
  `telephone` varchar(250) DEFAULT NULL,
  `cni` int(50) NOT NULL,
  `animation_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `animation_id` (`animation_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `piece_jointes` */

DROP TABLE IF EXISTS `piece_jointes`;

CREATE TABLE `piece_jointes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(100) DEFAULT NULL,
  `message_id` int(11) DEFAULT NULL,
  `dossier_id` int(11) DEFAULT NULL,
  `titre` varchar(500) DEFAULT NULL,
  `mediation_id` int(11) DEFAULT NULL,
  `animation_id` int(11) DEFAULT NULL,
  `suivi_evaluation_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_documents_messages1_idx` (`message_id`),
  KEY `fk_piece_jointes_dossiers1_idx` (`dossier_id`),
  KEY `mediation_id` (`mediation_id`),
  KEY `animation_id` (`animation_id`),
  KEY `suivi_evaluation_id` (`suivi_evaluation_id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;

/*Table structure for table `porteurs` */

DROP TABLE IF EXISTS `porteurs`;

CREATE TABLE `porteurs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `entreprise_id` int(11) DEFAULT NULL,
  `nom` varchar(45) DEFAULT NULL,
  `cni` varchar(45) DEFAULT NULL,
  `telephone` varchar(45) DEFAULT NULL,
  `prenom` varchar(45) DEFAULT NULL,
  `diplome` varchar(100) DEFAULT NULL,
  `niveau_etude` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_demandeurs_users1_idx` (`user_id`),
  KEY `fk_porteurs_entreprises1_idx` (`entreprise_id`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=latin1;

/*Table structure for table `regions` */

DROP TABLE IF EXISTS `regions`;

CREATE TABLE `regions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

/*Table structure for table `status` */

DROP TABLE IF EXISTS `status`;

CREATE TABLE `status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Table structure for table `statut_dossiers` */

DROP TABLE IF EXISTS `statut_dossiers`;

CREATE TABLE `statut_dossiers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dossier_id` int(11) NOT NULL,
  `agent_id` int(11) NOT NULL,
  `created` timestamp NULL DEFAULT NULL,
  `etat` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_statutsDossiers_dossiers1_idx` (`dossier_id`),
  KEY `fk_statutsDossiers_agents1_idx` (`agent_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

/*Table structure for table `structures` */

DROP TABLE IF EXISTS `structures`;

CREATE TABLE `structures` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(500) NOT NULL,
  `telephone` varchar(45) NOT NULL,
  `adresse` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Table structure for table `suivi_evaluations` */

DROP TABLE IF EXISTS `suivi_evaluations`;

CREATE TABLE `suivi_evaluations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titre` varchar(500) DEFAULT NULL,
  `description` text,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `type_evaluation_id` int(11) DEFAULT NULL,
  `agent_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`),
  KEY `type_evaluation_id` (`type_evaluation_id`),
  KEY `agent_id` (`agent_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Table structure for table `suretes` */

DROP TABLE IF EXISTS `suretes`;

CREATE TABLE `suretes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `libelle` varchar(250) DEFAULT NULL,
  `valeur` float DEFAULT NULL,
  `cotation` int(11) DEFAULT NULL,
  `type_id` int(11) DEFAULT NULL,
  `dossier_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_suretes_types1_idx` (`type_id`),
  KEY `dossier_id` (`dossier_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Table structure for table `type_animations` */

DROP TABLE IF EXISTS `type_animations`;

CREATE TABLE `type_animations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Table structure for table `type_entreprises` */

DROP TABLE IF EXISTS `type_entreprises`;

CREATE TABLE `type_entreprises` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Table structure for table `type_evaluations` */

DROP TABLE IF EXISTS `type_evaluations`;

CREATE TABLE `type_evaluations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Table structure for table `type_messages` */

DROP TABLE IF EXISTS `type_messages`;

CREATE TABLE `type_messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `types` */

DROP TABLE IF EXISTS `types`;

CREATE TABLE `types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(45) DEFAULT NULL,
  `password` varchar(250) DEFAULT NULL,
  `profil` varchar(45) DEFAULT NULL,
  `photo` varchar(45) DEFAULT NULL,
  `type` varchar(500) DEFAULT NULL,
  `codeIdentification` varchar(250) DEFAULT NULL,
  `actif` varchar(10) DEFAULT NULL,
  `cle` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=114 DEFAULT CHARSET=latin1;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
