/*
SQLyog Community v13.1.5  (64 bit)
MySQL - 5.7.14 : Database - ess
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`ess` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `ess`;

/*Table structure for table `admin` */

DROP TABLE IF EXISTS `admin`;

CREATE TABLE `admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `libelle` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `admin` */

/*Table structure for table `agences` */

DROP TABLE IF EXISTS `agences`;

CREATE TABLE `agences` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(250) DEFAULT NULL,
  `structure_id` int(11) NOT NULL,
  `telephone` varchar(50) DEFAULT NULL,
  `adresse` varchar(250) DEFAULT NULL,
  `types_agences_id` int(11) DEFAULT NULL,
  `regions_id` int(11) DEFAULT NULL,
  `etat` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_agences_structures1_idx` (`structure_id`),
  KEY `types_agences_id` (`types_agences_id`),
  KEY `fk_agences_regions1` (`regions_id`),
  CONSTRAINT `fk_agences_regions1` FOREIGN KEY (`regions_id`) REFERENCES `regions` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_agences_structures1` FOREIGN KEY (`structure_id`) REFERENCES `structures` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_agences_types_agences1` FOREIGN KEY (`types_agences_id`) REFERENCES `type_agences` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `agences` */

insert  into `agences`(`id`,`nom`,`structure_id`,`telephone`,`adresse`,`types_agences_id`,`regions_id`,`etat`) values 
(1,'Ministère de ESS',1,'338003030','Dakar, Sénégal',1,NULL,1),
(3,'Ministère du travail',1,'781232269','66 Avenue G. Pompidou',1,1,1),
(4,'Comité de Dakar',1,'338505563','Dakar plateau',2,1,1),
(5,'Comité de Thiès',1,'338506675','Grand Thiès',2,15,1),
(6,'Comité de Sédhiou',1,'338559926','Sédhiou, Sénégal',2,13,1);

/*Table structure for table `agents` */

DROP TABLE IF EXISTS `agents`;

CREATE TABLE `agents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `prenom` varchar(45) DEFAULT NULL,
  `nom` varchar(45) DEFAULT NULL,
  `telephone` varchar(45) DEFAULT NULL,
  `adresse` varchar(45) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `agence_id` int(11) DEFAULT NULL,
  `structure_id` int(11) DEFAULT NULL,
  `admin` tinyint(1) DEFAULT NULL,
  `superviseur` tinyint(1) DEFAULT NULL,
  `etat` tinyint(1) DEFAULT NULL,
  `comites_id` int(11) DEFAULT NULL,
  `departements_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_agents_Users1_idx` (`user_id`),
  KEY `fk_agents_agences1_idx` (`agence_id`),
  KEY `fk_agents_structures1_idx` (`structure_id`),
  KEY `comites_id` (`comites_id`),
  KEY `departements_id` (`departements_id`),
  CONSTRAINT `fk_agents_Users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_agents_agences1` FOREIGN KEY (`agence_id`) REFERENCES `agences` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_agents_departements1` FOREIGN KEY (`departements_id`) REFERENCES `departements` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_agents_structures1` FOREIGN KEY (`structure_id`) REFERENCES `structures` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_comites_agents1` FOREIGN KEY (`comites_id`) REFERENCES `comites` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

/*Data for the table `agents` */

insert  into `agents`(`id`,`prenom`,`nom`,`telephone`,`adresse`,`user_id`,`agence_id`,`structure_id`,`admin`,`superviseur`,`etat`,`comites_id`,`departements_id`) values 
(1,'Khadime','Wade','338003030','Ouest foire, Dakar',18,1,1,1,0,1,1,1),
(2,'Abdoulaye','Ndiaye','775005555','Sicap foire, Dakar',19,1,1,0,0,1,1,NULL),
(3,'Ababacar Sadikh','Faye','785942879','Dakar, Sénégal',20,NULL,1,1,0,NULL,NULL,NULL),
(4,'Ndeye Aida','Gueye','778005554','Ouest foire, Dakar',21,4,1,0,0,1,1,NULL),
(5,'Elhadj','Diop','785558432','Hlm Grd Yoff, Dakar',25,3,1,0,0,1,2,NULL),
(6,'Laye 1','Ndiaye','774531254','Dakar, Sénégal',28,1,1,1,0,1,2,1),
(7,'Abdoulaye','Ndiaye','785554466','Dakar, Sénégal',29,3,1,0,0,1,2,NULL),
(8,'Fatou','Ndiaye','775002244','Dieupeul derkelé',30,4,1,1,0,1,NULL,NULL),
(9,'Ousmane','Diop','787541264','Diakhao, Thiès',31,5,1,1,0,1,NULL,NULL),
(10,'Oumar','Cissé','774645879','Sacré coeur, Dakar',32,4,1,0,0,1,NULL,NULL),
(12,'Zahra Lyane','Thiam','785942248','Dakar, Sénégal',34,NULL,1,0,1,1,NULL,NULL),
(13,'Kiné','Diouf','785942933','Sédhiou, Sénégal',36,6,1,1,0,1,NULL,NULL);

/*Table structure for table `animations` */

DROP TABLE IF EXISTS `animations`;

CREATE TABLE `animations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titre` varchar(500) NOT NULL,
  `description` longtext NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `type_animation_id` int(11) NOT NULL,
  `agent_id` int(11) DEFAULT NULL,
  `lieu` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `type_animation_id` (`type_animation_id`),
  KEY `agent_id` (`agent_id`),
  CONSTRAINT `fk_agent2_id` FOREIGN KEY (`agent_id`) REFERENCES `agents` (`id`),
  CONSTRAINT `fk_type_animation_id` FOREIGN KEY (`type_animation_id`) REFERENCES `animations` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `animations` */

/*Table structure for table `articles` */

DROP TABLE IF EXISTS `articles`;

CREATE TABLE `articles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titre` varchar(500) DEFAULT NULL,
  `contenu` longtext,
  `categorie_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `image` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_articles_categories1_idx` (`categorie_id`),
  KEY `fk_article_users1_idx` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `articles` */

/*Table structure for table `assignations` */

DROP TABLE IF EXISTS `assignations`;

CREATE TABLE `assignations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `agent_id` int(11) NOT NULL,
  `dossier_id` int(11) NOT NULL,
  `rang` int(11) DEFAULT NULL,
  `status_validations_id` int(11) DEFAULT NULL,
  `type` varchar(45) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `type_demande` varchar(255) DEFAULT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fk_assignations_agents1_idx` (`agent_id`),
  KEY `dossier_id` (`dossier_id`),
  KEY `status_validations_id` (`status_validations_id`),
  CONSTRAINT `fk_assignation_dossier` FOREIGN KEY (`dossier_id`) REFERENCES `dossiers` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_assignations_agents1` FOREIGN KEY (`agent_id`) REFERENCES `agents` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_status_validations_assignation11` FOREIGN KEY (`status_validations_id`) REFERENCES `status_validations` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=latin1;

/*Data for the table `assignations` */

insert  into `assignations`(`id`,`agent_id`,`dossier_id`,`rang`,`status_validations_id`,`type`,`code`,`type_demande`,`created`) values 
(3,2,2,NULL,NULL,NULL,NULL,'label','2020-09-20 15:30:46'),
(4,2,9,NULL,NULL,NULL,NULL,'label','2020-10-01 14:04:15'),
(5,5,9,1,6,'analyse','aak5','label','2020-10-12 18:14:09'),
(6,7,9,2,6,'analyse','aak5','label','2020-10-12 18:14:09'),
(7,5,9,3,6,'analyse','aak5','label','2020-10-12 18:14:09'),
(9,2,3,1,6,NULL,'OyC6','label','2020-10-15 14:03:32'),
(10,5,3,1,6,'analyse','wwm8','label','2020-10-15 15:50:48'),
(11,7,3,2,6,'analyse','wwm8','label','2020-10-15 15:50:48'),
(12,5,3,3,6,'analyse','wwm8','label','2020-10-15 15:50:48'),
(13,2,2,1,6,NULL,'5Cq5','label','2020-11-04 13:37:54'),
(14,2,9,1,6,NULL,'YQMH','finance','2020-12-01 10:12:44'),
(15,5,9,1,6,'analyse','X5Lp','finance','2020-12-01 12:11:46'),
(16,7,9,2,6,'analyse','X5Lp','finance','2020-12-01 12:11:46'),
(17,4,10,1,6,NULL,'R0RO','label','2021-01-25 15:11:51'),
(20,5,10,1,6,'analyse','VMz7','label','2021-01-26 10:12:36'),
(21,7,10,2,6,'analyse','VMz7','label','2021-01-26 10:12:36'),
(27,4,11,1,6,NULL,'NuCM','label','2021-01-29 15:26:02'),
(28,5,2,1,6,'analyse','CXW5','label','2021-01-29 15:27:17'),
(29,7,2,2,6,'analyse','CXW5','label','2021-01-29 15:27:17'),
(30,5,11,1,6,'analyse','feBg','label','2021-01-29 15:27:24'),
(31,7,11,2,6,'analyse','feBg','label','2021-01-29 15:27:24');

/*Table structure for table `bannieres` */

DROP TABLE IF EXISTS `bannieres`;

CREATE TABLE `bannieres` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `photo` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `bannieres` */

/*Table structure for table `bibliotheques` */

DROP TABLE IF EXISTS `bibliotheques`;

CREATE TABLE `bibliotheques` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `libelle` varchar(255) DEFAULT NULL,
  `document` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

/*Data for the table `bibliotheques` */

insert  into `bibliotheques`(`id`,`libelle`,`document`) values 
(1,'Business plan','1615203792dashboard.pdf'),
(3,'Test','1615220360ticket.pdf'),
(5,'Modèle 1','1615223674note-de-frais-dec.xlsx'),
(6,'Modèle cahier de charge','1615223780devis001---copie-converted-converted.pdf'),
(7,'Modèle cahier de charge 2','1615223809certificat-e.s.s.pdf'),
(8,'Pièces de rechange automobile','1615223873certificat-ess-v1.docx');

/*Table structure for table `categories` */

DROP TABLE IF EXISTS `categories`;

CREATE TABLE `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `categories` */

/*Table structure for table `comites` */

DROP TABLE IF EXISTS `comites`;

CREATE TABLE `comites` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `comites` */

insert  into `comites`(`id`,`nom`) values 
(1,'Comité qualification'),
(2,'Comité d\'analyse');

/*Table structure for table `communes` */

DROP TABLE IF EXISTS `communes`;

CREATE TABLE `communes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(45) DEFAULT NULL,
  `departement_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_communes_departements1_idx` (`departement_id`)
) ENGINE=InnoDB AUTO_INCREMENT=626 DEFAULT CHARSET=latin1;

/*Data for the table `communes` */

insert  into `communes`(`id`,`nom`,`departement_id`) values 
(84,'Mermoz-Sacré-Cœur ',1),
(85,'Ngor',1),
(86,' Ouakam',1),
(87,'Yoff',1),
(88,'Dakar-Plateau',1),
(89,'Fann-Point E-Amitié',1),
(90,' Gueule Tapée-Fass-Colobane',1),
(91,'Médina',1),
(92,'Biscuiterie',1),
(93,'Dieuppeul-Derklé ',1),
(94,' Grand Dakar',1),
(95,'Hann Bel-Air',1),
(96,' HLM ',1),
(97,'Sicap-Liberté',1),
(98,'Cambérène ',1),
(99,'Grand Yoff',1),
(100,'Parcelles Assainies ',1),
(101,' Patte d\'Oie',1),
(102,'Golf Sud',2),
(103,'Médina Gounass',2),
(104,'Ndiarème Limamoulaye',2),
(105,'Sam Notaire',2),
(106,'Wakhinane Nimzatt',2),
(107,'Dalifort ',3),
(108,' Djidah Thiaroye Kaw',3),
(109,' Guinaw Rail Nord',3),
(110,' Guinaw Rail Sud',3),
(111,'Pikine Est ',3),
(112,'Pikine Nord',3),
(113,' Pikine Ouest',3),
(114,'Keur Massar',3),
(115,' Malika ',3),
(116,' Yeumbeul Nord ',3),
(117,'Yeumbeul Sud',3),
(118,'Tivaouane Diacksao',3),
(119,' Diamaguène Sicap Mbao',3),
(120,'Mbao',3),
(121,'Thiaroye-sur-Mer ',3),
(122,'Thiaroye Gare',3),
(123,'Rufisque Est',4),
(124,' Rufisque Nord',4),
(125,'Rufisque Ouest ',4),
(126,'Bargny ',4),
(127,'Sébikotane ',4),
(128,'Diamniadio',4),
(129,'Jaxaay-Parcelles-Niakoul Rab',4),
(130,'Sangalkam ',4),
(131,'Sendou',4),
(132,'Diourbel',9),
(133,'Ndoulo',9),
(134,'Ngohe',9),
(135,'Pattar',9),
(136,'Tocky  Gare',9),
(137,'Touré Mbondé',9),
(138,'Ndankh  Séne',9),
(139,'Gade  Escale',9),
(140,'Touba Lappé',9),
(141,'Keur  Ngalgou',9),
(142,'Ndindy',9),
(143,'Taïba Moutoupha',9),
(144,'Ngoye',8),
(145,'Thiakhar',8),
(146,'Ndondol',8),
(147,'Ndangalma',8),
(148,'Lambaye',8),
(149,'Réfane',8),
(150,'Gawane',8),
(151,'Ngogom',8),
(152,'Mbacké',10),
(153,'Touba Mosquée',10),
(154,'Dalla Ngabou',10),
(155,'Missirah',10),
(156,'Nghaye',10),
(157,'Touba Fall',10),
(158,'Darou Salam Typ',10),
(159,'Darou Nahim',10),
(160,'Kael',10),
(161,'Madina',10),
(162,'Touba Mboul',10),
(163,'Taïba Thièkène',10),
(164,'Dendèye Gouy Gui',10),
(165,'Ndioumane',10),
(166,'Taïf',10),
(167,'Sadio',10),
(168,'Fatick',11),
(169,'Dioffior',11),
(170,'Thiaré Ndialgui',11),
(171,'Diaoulé',11),
(172,'Mbéllacadiao',11),
(173,'Ndiop',11),
(174,'Diakhao',11),
(175,'Djilasse',11),
(176,'Fimela',11),
(177,'Loul  Sessène',11),
(178,'Palmarin Facao',11),
(179,'Niakhar',11),
(180,'Ngayokhème',11),
(181,'Patar',11),
(182,'Diarrère',11),
(183,'Diouroup',11),
(184,'Tattaguine',11),
(185,'Foundiougne',12),
(186,'Sokone',12),
(187,'Keur Saloum Diané',12),
(188,'Keur Samba Gueye',12),
(189,'Toubacouta',12),
(190,'Nioro Alassane Tall',12),
(191,'Karang Poste',12),
(192,'Passy',12),
(193,'Soum',12),
(194,'Diossong',12),
(195,'Djilor',12),
(196,'Niassène',12),
(197,'Diagane Barka',12),
(198,'Mbam',12),
(199,'Bassoul',12),
(200,'Dionewar',12),
(201,'Djirnda',12),
(202,'Gossas',13),
(203,'Colobane',13),
(204,'Mbar',13),
(205,'Ndiene Lagane',13),
(206,'Ouadiour',13),
(207,'Patar Lia',13),
(208,'Kaffrine',37),
(209,'Kathiotte',37),
(210,'Médinatoul Salam 2',37),
(211,'Gniby',37),
(212,'Boulel',37),
(213,'Kahi',37),
(214,'Birkelane',36),
(215,'Keur Mboucki',36),
(216,'Touba Mbella',36),
(217,'Diamal',36),
(218,'Mabo',36),
(219,'Ndiognick',36),
(220,'Ségré-gatta',36),
(221,'Mbeuleup',36),
(222,'Malème-Hodar',38),
(223,'Darou Miname II',38),
(224,'Khelcom',38),
(225,'Ndioum Ngainth',38),
(226,'Ndiobène Samba Lamo',38),
(227,'Sagna',38),
(228,'Dianké Souf',38),
(229,'Koungheul',39),
(230,'Missirah Wadène',39),
(231,'Maka Yop',39),
(232,'Ngainthe Pathé',39),
(233,'Fass Thièkène',39),
(234,'Saly Escale',39),
(235,'Ida Mouride',39),
(236,'Ribot Escale',39),
(237,'Lour Escale',39),
(238,'Kaolack',6),
(239,'Kahone',6),
(240,'Keur Baka',6),
(241,'Latmingué',6),
(242,'Thiaré',6),
(243,'Ndoffane',6),
(244,'Keur Socé',6),
(245,'Ndiaffate',6),
(246,'Ndiedieng',6),
(247,'Dya',6),
(248,'Ndiébel',6),
(249,'Thiomby',6),
(250,'Gandiaye',6),
(251,'Sibassor',6),
(252,'Guinguinéo',5),
(253,'Khelcom – Birane',5),
(254,'Mbadakhoune',5),
(255,'Ndiago',5),
(256,'Ngathie Naoudé',5),
(257,'Fass',5),
(258,'Gagnick',5),
(259,'Dara Mboss',5),
(260,'Nguélou',5),
(261,'Ourour',5),
(262,'Panal Ouolof',5),
(263,'Mboss',5),
(264,'Nioro du Rip',7),
(265,'Kayemor',7),
(266,'Médina Sabakh',7),
(267,'Ngayene',7),
(268,'Gainthe  Kaye',7),
(269,'Dabaly',7),
(270,'Darou Salam',7),
(271,'Paos Koto',7),
(272,'Porokhane',7),
(273,'Taïba Niassène',7),
(274,'Keur Maba Diakhou',7),
(275,'Keur Madongo',7),
(276,'Ndramé Escale',7),
(277,'Wack Ngouna',7),
(278,'Keur Madiabel',7),
(279,'Kédougou',40),
(280,'Ninéfécha',40),
(281,'Bandafassi',40),
(282,'Tomboroncoto',40),
(283,'Dindefelo',40),
(284,'Fongolimbi',40),
(285,'Dimboli',40),
(286,'Salémata',41),
(287,'Kévoye',41),
(288,'Dakatéli',41),
(289,'Ethiolo',41),
(290,'Oubadji',41),
(291,'Dar salam',41),
(292,'Saraya',42),
(293,'Bembou',42),
(294,'Médina Baffé',42),
(295,'Sabodala',42),
(296,'Khossanto',42),
(297,'Missirah Sirimana',42),
(298,'Kolda',14),
(299,'Dialambéré',14),
(300,'Médina Chérif',14),
(301,'Mampatim',14),
(302,'Coumbacara',14),
(303,'Bagadadji',14),
(304,'Dabo',14),
(305,'Thiétty',14),
(306,'Saré Bidji',14),
(307,'Guiro Yéro Bocar',14),
(308,'Dioulacolon',14),
(309,'Tankanto Escale',14),
(310,'Médina El hadj',14),
(311,'Salykégné',14),
(312,'Saré Yoba Diéga',14),
(313,'Médina Yoro Foulah',16),
(314,'Badion',16),
(315,'Fafacourou',16),
(316,'Bourouco',16),
(317,'Bignarabé',16),
(318,'Ndorna',16),
(319,'Koulinto',16),
(320,'Niaming',16),
(321,'Dinguiraye',16),
(322,'Pata',16),
(323,'Kéréwane',16),
(324,'Vélingara',15),
(325,'Diaobé-Kabendou',15),
(326,'Kounkané',15),
(327,'Kandia',15),
(328,'Saré Coly Sallé',15),
(329,'Kandiaye',15),
(330,'Némataba',15),
(331,'Pakour',15),
(332,'Paroumba',15),
(333,'Ouassadou',15),
(334,'Bonconto',15),
(335,'Linkering',15),
(336,'Médina Gounass',15),
(337,'Sinthiang Koundara',15),
(338,'Louga',19),
(339,'Coki',19),
(340,'Ndiagne',19),
(341,'Guet Ardo',19),
(342,'Thiamène Cayor',19),
(343,'Pété Ouarack',19),
(344,'Keur Momar Sarr',19),
(345,'Nguer Malal',19),
(346,'Syer',19),
(347,'Gande',19),
(348,'Mbédiene',19),
(349,'Niomré',19),
(350,'Nguidilé',19),
(351,'Kéle Gueye',19),
(352,'Léona',19),
(353,'Sakal',19),
(354,'Ngueune Sarr',19),
(355,'Kébémer',17),
(356,'Bandegne Ouolof',17),
(357,'Diokoul Diawrigne',17),
(358,'Kab Gaye',17),
(359,'Ndande',17),
(360,'Thieppe',17),
(361,'Guéoul',17),
(362,'Mbacké Cajor',17),
(363,'Darou Marnane',17),
(364,'Darou Mousty',17),
(365,'Mbadiane',17),
(366,'Ndoyene',17),
(367,'Sam Yabal',17),
(368,'Touba Mérina',17),
(369,'Ngourane Ouolof',17),
(370,'Thiolom Fall',17),
(371,'Sagatta Gueth',17),
(372,'Kanène Ndiob',17),
(373,'Loro',17),
(374,'Linguére',18),
(375,'Dahra',18),
(376,'Barkédji',18),
(377,'Gassane',18),
(378,'Thiargny',18),
(379,'Thiel',18),
(380,'Boulal',18),
(381,'Dealy',18),
(382,'Thiamène Pass',18),
(383,'Affé Djiolof',18),
(384,'Dodji',18),
(385,'Labgar',18),
(386,'Ouarkhokh',18),
(387,'Kamb',18),
(388,'Mboula',18),
(389,'Téssékéré Forage',18),
(390,'Yang-Yang',18),
(391,'Mbeuleukhé',18),
(395,'Matam',21),
(396,'Ourossogui',21),
(397,'Thilogne',21),
(398,'Nguidjilone',21),
(399,'Dabia',21),
(400,'Des Agnam',21),
(401,'Oréfondé',21),
(402,'Bokidiawé',21),
(403,'Nabadji Civol',21),
(404,'Ogo',21),
(405,'Kanel',20),
(406,'Odobéré',20),
(407,'Wouro Sidy',20),
(408,'Ndendory',20),
(409,'Sinthiou Bamambé',20),
(410,'Hamady Hounaré',20),
(411,'Aouré',20),
(412,'Bokiladji',20),
(413,'Orkadiéré',20),
(414,'Ouaoundé',20),
(415,'Semmé',20),
(416,'Dembancané',20),
(417,'Ranérou',22),
(418,'Lougré Thioly',22),
(419,'Oudalaye',22),
(420,'Vélingara',22),
(421,'Saint Louis',25),
(422,'Mpal',25),
(423,'Fass Ngom',25),
(424,'Ndiébène Gandiol',25),
(425,'Gandon',25),
(426,'Dagana',23),
(427,'Richard Toll',23),
(428,'Ross-Béthio',23),
(429,'Rosso-Sénégal',23),
(430,'Ngnith',23),
(431,'Diama',23),
(432,'Ronkh',23),
(433,'Ndombo Sandjiry',23),
(434,'Gae',23),
(435,'Bokhol',23),
(436,'Mbane',23),
(437,'Ndioum',23),
(438,'Podor',24),
(439,'Méry',24),
(440,'Doumga Lao',24),
(441,'Madina Diathbé',24),
(442,'Golléré',24),
(443,'Mboumba',24),
(444,'Walaldé',24),
(445,'Aéré Lao',24),
(446,'Gamadji Saré',24),
(447,'Dodel',24),
(448,'Guedé Village',24),
(449,'Guédé Chantier',24),
(450,'Démette',24),
(451,'Bodé Lao',24),
(452,'Fanaye',24),
(453,'Ndiayene Peindao',24),
(454,'Ndiandane',24),
(455,'Mbolo Birane',24),
(456,'Boké Dialloubé',24),
(457,'Galoya Toucouleur',24),
(458,'Pété',24),
(459,'Sédhiou',45),
(460,'Marssassoum',45),
(461,'Djiredji',45),
(462,'Bambaly',45),
(463,'Oudoucar',45),
(464,'Sama Kanta Peulh',45),
(465,'Diannah Ba',45),
(466,'Koussy',45),
(467,'Sakar',45),
(468,'Diendé',45),
(469,'Diannah Malary',45),
(470,'San Samba',45),
(471,'Bémet Bidjini',45),
(472,'Djibabouya',45),
(473,'Bounkiling',43),
(474,'Ndiamacouta',43),
(475,'Tankon',43),
(476,'Ndiamalathiel',43),
(477,'Djinany',43),
(478,'Diacounda',43),
(479,'Inor',43),
(480,'Kandion Mangana',43),
(481,'Bona',43),
(482,'Diambati',43),
(483,'Faoune',43),
(484,'Diaroumé',43),
(485,'Madina Wandifa',43),
(486,'Goudomp',44),
(487,'Diattacounda',44),
(488,'Samine',44),
(489,'Yarang Balante',44),
(490,'Mangaroungou Santo',44),
(491,'Simbandi Balante',44),
(492,'Djibanar',44),
(493,'Kaour',44),
(494,'Diouboudou',44),
(495,'Simbandi Brassou',44),
(496,'Baghère',44),
(497,'Niagha',44),
(498,'Tanaff',44),
(499,'Karantaba',44),
(500,'Kolibantang',44),
(501,'Tambacounda',28),
(502,'Niani Toucouleur',28),
(503,'Makacolibantang',28),
(504,'Ndoga Babacar',28),
(505,'Missirah',28),
(506,'Néttéboulou',28),
(507,'Dialacoto',28),
(508,'Sinthiou Malème',28),
(509,'Koussanar',28),
(510,'Kothiary',29),
(511,'Goudiry',29),
(512,'Boynguel Bamba',29),
(513,'Sinthiou Mamadou',29),
(514,'Koussan',29),
(515,'Dougué',29),
(516,'Dianké Makha',29),
(517,'Boutoucoufara',29),
(518,'Bani Israel',29),
(519,'Sinthiou Bocar Aly',29),
(520,'Koulor',29),
(521,'Komoti',29),
(522,'Bala',29),
(523,'Bakel',26),
(524,'Bélé',26),
(525,'Sinthiou Fissa',26),
(526,'Kidira',26),
(527,'Toumboura',26),
(528,'Sadatou',26),
(529,'Madina Foulbé',26),
(530,'Gathiary',26),
(531,'Moudery',26),
(532,'Ballou',26),
(533,'Gabou',26),
(534,'Diawara',26),
(535,'Koumpentoum',27),
(536,'Malem Niany',27),
(537,'Ndame',27),
(538,'Méréto',27),
(539,'Kahène',27),
(540,'Bamba Thialène',27),
(541,'Payar',27),
(542,'Kouthiaba Wolof',27),
(543,'Kouthia Gaydi',27),
(544,'Pass Coto',27),
(545,'Malem Niany',27),
(546,'Ville de Thiès',31),
(547,'Khombole',31),
(548,'Pout',31),
(549,'Thiès Ouest',31),
(550,'Thiès Est',31),
(551,'Thiès Nord',31),
(552,'Thiénaba',31),
(553,'Ngoudiane',31),
(554,'Ndiéyène Sirakh',31),
(555,'Touba Toul',31),
(556,'Keur Moussa',31),
(557,'Diender',31),
(558,'Fandène',31),
(559,'Kayar',31),
(560,'Notto',31),
(561,'Tassète',31),
(562,'Mbour',30),
(563,'Joal Fadiouth',30),
(564,'Fissel',30),
(565,'Ndiaganiao',30),
(566,'Sessene',30),
(567,'Sandiara',30),
(568,'Nguéniène',31),
(569,'Thiadiaye',31),
(570,'Sindia',30),
(571,'Malicounda',30),
(572,'Diass',30),
(573,'Nguékhokh',30),
(574,'Saly Portudal',30),
(575,'Ngaparou',30),
(576,'Somone',30),
(577,'Popenguine- Ndayane',30),
(578,'Popenguine- Ndayane',32),
(579,'Mékhé',32),
(580,'Mboro',32),
(581,'Méouane',32),
(582,'Darou Khoudoss',32),
(583,'Taïba Ndiaye',32),
(584,'Mérina  Dakhar',32),
(585,'Koul',32),
(586,'Pékèsse',32),
(587,'Niakhène',32),
(588,'Mbayène',32),
(589,'Thilmakha',32),
(590,'Ngandiouf',32),
(591,'Notto Gouye Diama',32),
(592,'Mont Rolland',32),
(593,'Pire Goureye',32),
(594,'Chérif Lo',32),
(595,'Pambal',32),
(596,'Ziguinchor',35),
(597,'Niaguis',35),
(598,'Adéane',35),
(599,'Boutoupa Camaracounda',35),
(600,'Niassia',35),
(601,'Enampore',35),
(602,'Bignona',33),
(603,'Thionck Essyl',33),
(604,'Kataba  1',33),
(605,'Djinaky',33),
(606,'Kafountine',33),
(607,'Diouloulou',33),
(608,'Tenghori',33),
(609,'Niamone',33),
(610,'Ouonck',33),
(611,'Coubalan',33),
(612,'Balinghore',33),
(613,'Diégoune',33),
(614,'Kartiack',33),
(615,'Mangagoulack',33),
(616,'Mlomp',33),
(617,'Djibidione',33),
(618,'Oulampane',33),
(619,'Sindian',33),
(620,'Suelle',33),
(621,'Oussouye',34),
(622,'Diembéring',34),
(623,'Santhiaba Manjack',34),
(624,'Oukout',34),
(625,'Mlomp',34);

/*Table structure for table `departements` */

DROP TABLE IF EXISTS `departements`;

CREATE TABLE `departements` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `departementscol` varchar(45) DEFAULT NULL,
  `region_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_departements_regions1_idx` (`region_id`)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=latin1;

/*Data for the table `departements` */

insert  into `departements`(`id`,`departementscol`,`region_id`) values 
(1,'Dakar',1),
(2,'Guédiawaye',1),
(3,'Pikine',1),
(4,'Rufisque',1),
(5,'Guinguino',4),
(6,'Kaolack',4),
(7,'Nioro du Rip',4),
(8,'Bambey',5),
(9,'Diourbel',5),
(10,'Mbacké',5),
(11,'Fatick',6),
(12,'Foundiougne',6),
(13,'Gossas',6),
(14,'Kolda',9),
(15,'Vélingara',9),
(16,'Médina Yoro Foulah',9),
(17,'Kébémer',10),
(18,'Linguère',10),
(19,'Louga',10),
(20,'Kanel',11),
(21,'Matam',11),
(22,'Ranérou',11),
(23,'Dagana',12),
(24,'Podor',12),
(25,'Saint-Louis',12),
(26,'Bakel',14),
(27,'Koumpentoum',14),
(28,'Tambacounda',14),
(29,'Goudiry',14),
(30,'M\'bour',15),
(31,'Thiès',15),
(32,'Tivaouane',15),
(33,'Bignogna',16),
(34,'Oussouye',16),
(35,'Ziguinchor',16),
(36,'Birkilane',7),
(37,'Kaffrine',7),
(38,'Malem-Hodar',7),
(39,'Kounghel',7),
(40,'Kédougou',8),
(41,'Salemata',8),
(42,'Saraya',8),
(43,'Bounkiling',13),
(44,'Goudomp',13),
(45,'Sédhiou',13),
(46,'Koar',29),
(47,'Goumbayel',29);

/*Table structure for table `dossiers` */

DROP TABLE IF EXISTS `dossiers`;

CREATE TABLE `dossiers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `intitule` varchar(255) DEFAULT NULL,
  `numero` varchar(45) DEFAULT NULL,
  `statut_dossiers_id` int(11) DEFAULT NULL,
  `created` timestamp NULL DEFAULT NULL,
  `descrip_projet` mediumtext,
  `nature_demande_id` int(11) DEFAULT NULL,
  `descrip_marche` mediumtext,
  `demarche_cmerc` text,
  `porteur_id` int(11) DEFAULT NULL,
  `filiere_id` int(11) DEFAULT NULL,
  `commune_id` int(11) DEFAULT NULL,
  `soumis` varchar(45) DEFAULT NULL,
  `agent_id` int(11) DEFAULT NULL,
  `demande_label` tinyint(1) DEFAULT NULL,
  `nb_perso_impact` int(11) DEFAULT NULL,
  `nb_femme_impact` int(11) DEFAULT NULL,
  `nb_homme_impact` int(11) DEFAULT NULL,
  `label` tinyint(1) DEFAULT NULL,
  `type_label` varchar(255) DEFAULT NULL,
  `note_final` varchar(255) DEFAULT NULL,
  `emission` tinyint(1) DEFAULT NULL,
  `statut_financements_id` int(11) DEFAULT NULL,
  `note_financement` double DEFAULT NULL,
  `emission_finance` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_dossiers_natureDemandes1_idx` (`nature_demande_id`),
  KEY `fk_dossiers_porteurs1_idx` (`porteur_id`),
  KEY `fk_dossiers_filieres1_idx` (`filiere_id`),
  KEY `fk_dossiers_communes1_idx` (`commune_id`),
  KEY `agent_id` (`agent_id`),
  KEY `statut_dossiers_id` (`statut_dossiers_id`),
  KEY `statut_financements_id` (`statut_financements_id`),
  CONSTRAINT `fk_dossiers_statut_dossiers1` FOREIGN KEY (`statut_dossiers_id`) REFERENCES `statut_dossiers` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_dossiers_statut_financements1` FOREIGN KEY (`statut_financements_id`) REFERENCES `statut_financements` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

/*Data for the table `dossiers` */

insert  into `dossiers`(`id`,`intitule`,`numero`,`statut_dossiers_id`,`created`,`descrip_projet`,`nature_demande_id`,`descrip_marche`,`demarche_cmerc`,`porteur_id`,`filiere_id`,`commune_id`,`soumis`,`agent_id`,`demande_label`,`nb_perso_impact`,`nb_femme_impact`,`nb_homme_impact`,`label`,`type_label`,`note_final`,`emission`,`statut_financements_id`,`note_financement`,`emission_finance`) values 
(2,'Site de e-commerce','F-yQWr',6,'2020-11-03 11:51:26','<p>Panierdakar est un site de vente en ligne bas&eacute; &agrave; Dakar.</p>\r\n',NULL,NULL,NULL,1,11,93,'oui',NULL,1,25,NULL,NULL,1,'gold','16.2',NULL,NULL,NULL,NULL),
(3,'Agence de multiservice','F-IpCC',4,'2020-11-05 14:06:10','<p>Lorem ipsum</p>\r\n',NULL,NULL,NULL,11,7,551,'oui',NULL,1,18,NULL,NULL,1,'argent','12.93',0,NULL,NULL,NULL),
(7,'Transformation de produits locaux','Modou-QFxN',NULL,'2020-09-26 12:30:19','<p>Transformation de produits locaux</p>\r\n',NULL,NULL,NULL,12,2,547,'non',NULL,1,15,NULL,NULL,0,NULL,NULL,0,NULL,NULL,NULL),
(8,'Réseaux des collectivités vertes','Reves-14mk',1,'2020-09-30 15:48:33','<p>R&eacute;seaux des collectivit&eacute;s vertes</p>\r\n',NULL,NULL,NULL,14,1,169,'oui',NULL,1,50,NULL,NULL,0,NULL,NULL,0,NULL,NULL,NULL),
(9,'Espace verte','REVES-QflN',6,'2020-11-05 16:00:01','<p>Am&eacute;nagement territorial</p>\r\n',NULL,NULL,NULL,14,3,84,'oui',NULL,1,100,NULL,NULL,1,'argent','14.6',1,6,15.1,0),
(10,'Boutique cosmétique','Moussa-zpUE',6,'2021-01-25 13:00:29','<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry.</p>\r\n',NULL,NULL,NULL,1,11,84,'oui',NULL,1,4,NULL,NULL,1,'gold','16.4',1,NULL,NULL,NULL),
(11,'Vente de vêtement','Moussa-MgOh',6,'2021-01-26 17:15:32','<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry.</p>\r\n',NULL,NULL,NULL,1,10,85,'oui',NULL,1,7,NULL,NULL,1,'gold','16.5',NULL,NULL,NULL,NULL);

/*Table structure for table `dossiers_filieres` */

DROP TABLE IF EXISTS `dossiers_filieres`;

CREATE TABLE `dossiers_filieres` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dossiers_id` int(11) DEFAULT NULL,
  `filieres_id` int(11) DEFAULT NULL,
  `link_type` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `dossiers_id` (`dossiers_id`,`filieres_id`),
  KEY `fk_filieres_dossiers_filieres1` (`filieres_id`),
  CONSTRAINT `fk_filieres_dossiers_filieres1` FOREIGN KEY (`filieres_id`) REFERENCES `filieres` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=latin1;

/*Data for the table `dossiers_filieres` */

insert  into `dossiers_filieres`(`id`,`dossiers_id`,`filieres_id`,`link_type`) values 
(17,8,2,'interesse'),
(18,8,3,'interesse'),
(19,8,4,'interesse'),
(20,8,1,'relation'),
(25,9,1,'interesse'),
(26,9,3,'interesse'),
(27,9,9,'interesse'),
(28,9,11,'relation'),
(29,7,2,'interesse'),
(30,7,3,'interesse'),
(31,7,11,'relation'),
(47,2,1,'interesse'),
(48,2,11,'interesse'),
(49,2,4,'relation'),
(50,2,10,'relation'),
(51,10,10,'interesse'),
(52,10,11,'interesse'),
(53,10,7,'relation'),
(54,11,10,'interesse'),
(55,11,11,'interesse'),
(56,11,4,'relation');

/*Table structure for table `dossiers_impacts` */

DROP TABLE IF EXISTS `dossiers_impacts`;

CREATE TABLE `dossiers_impacts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dossiers_id` int(11) DEFAULT NULL,
  `types_impacts_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `dossiers_id` (`dossiers_id`,`types_impacts_id`),
  KEY `impacts_types_impacts1` (`types_impacts_id`),
  CONSTRAINT `impacts_types_impacts1` FOREIGN KEY (`types_impacts_id`) REFERENCES `types_impacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `types_impacts_dossiers1` FOREIGN KEY (`dossiers_id`) REFERENCES `dossiers` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=67 DEFAULT CHARSET=latin1;

/*Data for the table `dossiers_impacts` */

insert  into `dossiers_impacts`(`id`,`dossiers_id`,`types_impacts_id`) values 
(60,2,1),
(58,2,3),
(59,2,5),
(45,7,1),
(43,7,3),
(44,7,4),
(32,8,1),
(30,8,3),
(31,8,4),
(41,9,1),
(42,9,2),
(38,9,3),
(39,9,4),
(40,9,6),
(63,10,2),
(61,10,3),
(62,10,6),
(66,11,2),
(64,11,3),
(65,11,6);

/*Table structure for table `entreprises` */

DROP TABLE IF EXISTS `entreprises`;

CREATE TABLE `entreprises` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ninea` varchar(45) DEFAULT NULL,
  `nom` varchar(45) DEFAULT NULL,
  `type_entreprise_id` int(11) DEFAULT NULL,
  `nb_employe` int(11) DEFAULT NULL,
  `date_creation` date DEFAULT NULL,
  `immatriculation` varchar(50) DEFAULT NULL,
  `type_social` varchar(50) DEFAULT NULL,
  `social_link` varchar(255) DEFAULT NULL,
  `tel_entite` varchar(255) DEFAULT NULL,
  `communes_id` int(11) DEFAULT NULL,
  `nb_salarie` int(11) DEFAULT NULL,
  `nb_stageaire` int(11) DEFAULT NULL,
  `nb_saisonnier` int(11) DEFAULT NULL,
  `association` tinyint(1) DEFAULT NULL,
  `formel` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `type_entreprise_id` (`type_entreprise_id`),
  KEY `communes_id` (`communes_id`),
  CONSTRAINT `fk_communes_entreprises1` FOREIGN KEY (`communes_id`) REFERENCES `communes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

/*Data for the table `entreprises` */

insert  into `entreprises`(`id`,`ninea`,`nom`,`type_entreprise_id`,`nb_employe`,`date_creation`,`immatriculation`,`type_social`,`social_link`,`tel_entite`,`communes_id`,`nb_salarie`,`nb_stageaire`,`nb_saisonnier`,`association`,`formel`) values 
(4,'A564a44','Panierdakar',2,5,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(8,'C154156','Multiservice Thièssois',2,4,'2020-06-01','CK8332','WhatsApp','','772332244',551,2,2,NULL,NULL,NULL),
(10,'G5456F','REVES',5,15,'2017-03-16','BC219F','Facebook','Reves','338645528',547,10,1,4,1,1);

/*Table structure for table `filieres` */

DROP TABLE IF EXISTS `filieres`;

CREATE TABLE `filieres` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(45) DEFAULT NULL,
  `photo` varchar(255) DEFAULT NULL,
  `statut` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

/*Data for the table `filieres` */

insert  into `filieres`(`id`,`nom`,`photo`,`statut`) values 
(1,'Logistique',NULL,1),
(2,'Agriculture','1614365124agriculture.jpg',1),
(3,'Elevage','1614365288elevage.jpg',1),
(4,'Industrie',NULL,1),
(6,'Pêche','1614365358peche.jpg',1),
(7,'Technologie',NULL,1),
(8,'Santé',NULL,1),
(9,'Artisanat','1614365329artisanat.jpg',1),
(10,'Mode / Culture',NULL,1),
(11,'Commerce',NULL,1);

/*Table structure for table `financements_actifs` */

DROP TABLE IF EXISTS `financements_actifs`;

CREATE TABLE `financements_actifs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `libelle` varchar(255) DEFAULT NULL,
  `annee_moins_trois` double DEFAULT NULL,
  `annee_moins_deux` double DEFAULT NULL,
  `annee_moins_un` double DEFAULT NULL,
  `annee` double DEFAULT NULL,
  `prevision` double DEFAULT NULL,
  `variation` double DEFAULT NULL,
  `dossiers_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `dossiers_id` (`dossiers_id`),
  CONSTRAINT `fk_actifs_dossiers1` FOREIGN KEY (`dossiers_id`) REFERENCES `dossiers` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Data for the table `financements_actifs` */

insert  into `financements_actifs`(`id`,`libelle`,`annee_moins_trois`,`annee_moins_deux`,`annee_moins_un`,`annee`,`prevision`,`variation`,`dossiers_id`) values 
(2,'Actifs immobilisés',1000,1544,46565,65656,565,565,9),
(3,'Stocks',566,565,65,66,6565,56,9),
(4,'Créances et emplois assimilés',656,48956,6565,65,5656,59565,9),
(5,'Actif circulant',656,5,5656,565656,56,565,9),
(6,'Trésorerie Actifs',565,65,565,656,5656,55,9),
(7,'Total Actifs',656565,65,65,656,5595,4876,9);

/*Table structure for table `financements_passifs` */

DROP TABLE IF EXISTS `financements_passifs`;

CREATE TABLE `financements_passifs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `libelle` varchar(255) DEFAULT NULL,
  `annee_moins_trois` double DEFAULT NULL,
  `annee_moins_deux` double DEFAULT NULL,
  `annee_moins_un` double DEFAULT NULL,
  `annee` double DEFAULT NULL,
  `prevision` double DEFAULT NULL,
  `variation` double DEFAULT NULL,
  `dossiers_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `dossiers_id` (`dossiers_id`),
  CONSTRAINT `fk_passifs_dossiers1` FOREIGN KEY (`dossiers_id`) REFERENCES `dossiers` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

/*Data for the table `financements_passifs` */

insert  into `financements_passifs`(`id`,`libelle`,`annee_moins_trois`,`annee_moins_deux`,`annee_moins_un`,`annee`,`prevision`,`variation`,`dossiers_id`) values 
(7,'Capital',64154,565546,46464,46546,464,6466,9),
(8,'Résultat de l\'exercice',656,5656,5656,565656,6664,6664,9),
(9,'Capitaux propres',6644,6565,565,656,6565,6565,9),
(10,'Dettes financières',65656,65656,656,6565,65656,6565,9),
(11,'Ressources stables',6565,656565,655656,32165,6565,3265,9),
(12,'Passif circulant',325,956,565565,5656,565,65,9),
(13,'Trésorerie Passif',6565,6565,6565,656655,595,8989,9),
(14,'Total Passif',5959,959589,9598,65968,9898,4587,9);

/*Table structure for table `finances_engagements` */

DROP TABLE IF EXISTS `finances_engagements`;

CREATE TABLE `finances_engagements` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `libelle` varchar(255) DEFAULT NULL,
  `montant` double DEFAULT NULL,
  `encours` double DEFAULT NULL,
  `differe_mois` int(11) DEFAULT NULL,
  `date_last_echeance` date DEFAULT NULL,
  `duree_mois` int(11) DEFAULT NULL,
  `banque` varchar(255) DEFAULT NULL,
  `taux_interet` double DEFAULT NULL,
  `dossiers_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `dossiers_id` (`dossiers_id`),
  CONSTRAINT `fk_engagements_dossiers1` FOREIGN KEY (`dossiers_id`) REFERENCES `dossiers` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `finances_engagements` */

insert  into `finances_engagements`(`id`,`libelle`,`montant`,`encours`,`differe_mois`,`date_last_echeance`,`duree_mois`,`banque`,`taux_interet`,`dossiers_id`) values 
(2,'Crédit Moyen Terme',200000,450000,4,'2020-11-28',1,'Banque islamique',2,9),
(3,'Découvert',150000,300000,2,'2020-11-30',1,'Banque islamique',1,9);

/*Table structure for table `finances_parametres` */

DROP TABLE IF EXISTS `finances_parametres`;

CREATE TABLE `finances_parametres` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `libelle` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;

/*Data for the table `finances_parametres` */

insert  into `finances_parametres`(`id`,`libelle`,`type`) values 
(1,'Crédit Moyen Terme','engagement'),
(2,'Découvert','engagement'),
(3,'Chiffres d\'affaires','situation compte'),
(4,'Résultat Net','situation compte'),
(5,'Fonds de roulement','situation compte'),
(6,'Charges financières','situation compte'),
(7,'Dettes financières','situation compte'),
(8,'Créances Clients','situation compte'),
(9,'Dettes Fournisseurs','situation compte'),
(10,'Capacité de remboursement','situation compte'),
(11,'Actifs immobilisés','actif'),
(12,'Stocks','actif'),
(13,'Créances et emplois assimilés','actif'),
(14,'Actif circulant','actif'),
(15,'Trésorerie Actifs','actif'),
(16,'Total Actifs','actif'),
(17,'Capital','passif'),
(18,'Résultat de l\'exercice','passif'),
(19,'Capitaux propres','passif'),
(20,'Dettes financières','passif'),
(21,'Ressources stables','passif'),
(22,'Passif circulant','passif'),
(23,'Trésorerie Passif','passif'),
(24,'Total Passif','passif');

/*Table structure for table `impactes` */

DROP TABLE IF EXISTS `impactes`;

CREATE TABLE `impactes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `libelle` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `impactes` */

insert  into `impactes`(`id`,`libelle`) values 
(1,'La société'),
(2,'L\'environnement'),
(3,'L\'individu'),
(4,'La politique'),
(5,'L\'économie');

/*Table structure for table `imputations` */

DROP TABLE IF EXISTS `imputations`;

CREATE TABLE `imputations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dossier_id` int(11) NOT NULL,
  `agent_imput_id` int(11) NOT NULL,
  `agent_imputateur_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_Imputations_Dossiers1_idx` (`dossier_id`),
  KEY `fk_imputations_agents1_idx` (`agent_imput_id`),
  KEY `agent_imputateur_id` (`agent_imputateur_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `imputations` */

/*Table structure for table `mediations` */

DROP TABLE IF EXISTS `mediations`;

CREATE TABLE `mediations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` mediumtext NOT NULL,
  `financement_credit_id` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `titre` varchar(500) NOT NULL,
  `statu_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `financement_credit_id` (`financement_credit_id`),
  KEY `statu_id` (`statu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `mediations` */

/*Table structure for table `messages` */

DROP TABLE IF EXISTS `messages`;

CREATE TABLE `messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` datetime DEFAULT CURRENT_TIMESTAMP,
  `resume` mediumtext,
  `agent_id` int(11) DEFAULT NULL,
  `dossier_id` int(11) DEFAULT NULL,
  `porteur_id` int(11) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_messages_agents1_idx` (`agent_id`),
  KEY `fk_messages_dossiers1_idx` (`dossier_id`),
  KEY `fk_messages_porteurs1_idx` (`porteur_id`),
  CONSTRAINT `fk_messages_agents1` FOREIGN KEY (`agent_id`) REFERENCES `agents` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_messages_dossiers1` FOREIGN KEY (`dossier_id`) REFERENCES `dossiers` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_messges_porteurs1` FOREIGN KEY (`porteur_id`) REFERENCES `porteurs` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=latin1;

/*Data for the table `messages` */

insert  into `messages`(`id`,`date`,`resume`,`agent_id`,`dossier_id`,`porteur_id`,`url`) values 
(1,'2020-11-18 10:53:42',' Salut',NULL,2,14,NULL),
(2,'2020-11-18 12:05:26',' Bonjour Babacar',NULL,8,1,NULL),
(4,'2020-11-18 13:17:06','Je voulais savoir un peu plus sur votre projet',NULL,2,14,NULL),
(5,'2020-11-18 13:30:59',' Ok ça marche',NULL,8,1,NULL),
(7,'2020-11-18 17:16:00',' Salut Khadime',1,NULL,1,NULL),
(8,'2020-11-18 17:22:25',' Cc cv',NULL,8,1,NULL),
(9,'2020-11-18 17:43:29','Oui Salut Mr Fall',1,2,1,NULL),
(10,'2020-11-18 18:07:56',' Salut comment ça se passe ?',1,NULL,1,NULL),
(16,'2020-11-19 13:20:56',' Salut',NULL,2,14,NULL),
(18,'2020-11-19 14:12:16',' Bonsoir',NULL,2,14,NULL),
(19,'2020-11-19 15:19:21',' Ok',1,NULL,1,NULL),
(20,'2020-11-19 15:19:25','Cv',1,NULL,1,NULL),
(21,'2020-11-19 15:19:35','Bonsoir',1,NULL,1,NULL),
(22,'2020-11-19 15:19:55','Hello',1,NULL,1,NULL),
(23,'2020-11-19 15:20:47',' Bonsoir Moussa',1,2,1,NULL),
(24,'2020-11-19 15:20:59','Comment cv',1,2,1,NULL),
(25,'2020-11-19 15:46:55',' Salut',NULL,7,1,NULL),
(26,'2020-11-19 15:47:01','çava',NULL,7,1,NULL),
(27,'2020-11-19 15:47:09','C\'est Moussa',NULL,7,1,NULL),
(28,'2020-11-19 15:47:27','Je voulais savoir un peu sur votre projet',NULL,7,1,NULL),
(29,'2020-11-19 15:48:11',' Salut Moussa ',NULL,2,12,NULL),
(30,'2020-11-19 15:48:18','Comment çava ',NULL,2,12,NULL),
(31,'2020-11-19 15:48:43','Oui c\'est bien possible',NULL,2,12,NULL),
(32,'2020-11-19 15:48:44','Oui c\'est bien possible',NULL,2,12,NULL),
(37,'2020-11-20 10:36:59',' Salut',NULL,8,1,'1605868619flyer.png'),
(39,'2020-11-20 13:31:21','',NULL,2,14,'1605879081logo.png'),
(41,'2020-11-20 13:33:40','C\'est ton logo ?',NULL,8,1,NULL),
(42,'2020-11-25 17:37:24','',NULL,2,14,'1606325843mon-cv.docx'),
(43,'2020-11-25 18:06:04','',NULL,2,14,'1606327564cv-moussa-fall.pdf');

/*Table structure for table `nature_demandes` */

DROP TABLE IF EXISTS `nature_demandes`;

CREATE TABLE `nature_demandes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `nature_demandes` */

/*Table structure for table `notations` */

DROP TABLE IF EXISTS `notations`;

CREATE TABLE `notations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dossiers_id` int(11) NOT NULL,
  `agents_id` int(11) NOT NULL,
  `code` varchar(255) DEFAULT NULL,
  `nat_struc_juridique` int(45) DEFAULT NULL,
  `eligibilite_filiere` int(45) DEFAULT NULL,
  `partenariat_relation` int(45) DEFAULT NULL,
  `pertinence_activite` int(45) DEFAULT NULL,
  `coherence_activite` int(45) DEFAULT NULL,
  `positionnement_activite` int(45) DEFAULT NULL,
  `comment_aspect_tech` text,
  `aspect_technique` int(45) DEFAULT NULL,
  `sante_financiere` int(45) DEFAULT NULL,
  `croissance_reultat` int(45) DEFAULT NULL,
  `situation_actif` int(45) DEFAULT NULL,
  `situation_passif` int(45) DEFAULT NULL,
  `croissance_en_vue` int(11) DEFAULT NULL,
  `engagement_actuel` int(11) DEFAULT NULL,
  `situation_compte` int(11) DEFAULT NULL,
  `comment_aspect_financ` text,
  `aspect_financier` int(11) DEFAULT NULL,
  `eligibilite_ess` int(11) DEFAULT NULL,
  `impact_social_env` int(11) DEFAULT NULL,
  `influence_territoriale` int(11) DEFAULT NULL,
  `autre_critere_ess1` int(11) DEFAULT NULL,
  `autre_critere_ess2` int(11) DEFAULT NULL,
  `pourcentage_revenu_dirigeants` int(11) DEFAULT NULL,
  `comment_aspect_env` text,
  `aspect_env` int(11) DEFAULT NULL,
  `raison_rejet` text,
  `type_demande` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `dossiers_id` (`dossiers_id`),
  KEY `agents_id` (`agents_id`),
  CONSTRAINT `fk_notations_agents1` FOREIGN KEY (`agents_id`) REFERENCES `agents` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_notations_dossiers1` FOREIGN KEY (`dossiers_id`) REFERENCES `dossiers` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;

/*Data for the table `notations` */

insert  into `notations`(`id`,`dossiers_id`,`agents_id`,`code`,`nat_struc_juridique`,`eligibilite_filiere`,`partenariat_relation`,`pertinence_activite`,`coherence_activite`,`positionnement_activite`,`comment_aspect_tech`,`aspect_technique`,`sante_financiere`,`croissance_reultat`,`situation_actif`,`situation_passif`,`croissance_en_vue`,`engagement_actuel`,`situation_compte`,`comment_aspect_financ`,`aspect_financier`,`eligibilite_ess`,`impact_social_env`,`influence_territoriale`,`autre_critere_ess1`,`autre_critere_ess2`,`pourcentage_revenu_dirigeants`,`comment_aspect_env`,`aspect_env`,`raison_rejet`,`type_demande`) values 
(2,9,5,'aak5',4,4,5,4,4,2,'<p>Aspect technique</p>\r\n',23,4,5,4,4,4,NULL,NULL,'<p>Aspect financier</p>\r\n',21,25,3,1,1,2,2,'<p>Aspect environnemental</p>\r\n',34,NULL,'label'),
(7,9,7,'aak5',5,3,3,4,3,3,'<p>Aspect technique</p>\r\n',21,4,5,2,4,4,NULL,NULL,'<p>Aspect financier</p>\r\n',19,25,2,2,1,1,1,'<p>Aspect environnemental</p>\r\n',32,NULL,'label'),
(9,9,5,'aak5',5,4,4,3,2,2,'',20,4,7,1,3,4,NULL,NULL,'',19,25,1,1,1,1,1,'',30,NULL,'label'),
(11,3,5,'wwm8',5,2,2,3,2,4,'<p>Ok</p>\r\n',18,5,6,3,1,1,NULL,NULL,'<p>Ok</p>\r\n',16,18,3,1,1,1,2,'<p>Ok</p>\r\n',26,NULL,'label'),
(12,3,7,'wwm8',4,2,1,1,3,3,'<p>Ok</p>\r\n',14,2,7,4,2,3,NULL,NULL,'<p>Ok</p>\r\n',18,25,1,1,1,1,2,'<p>Ok</p>\r\n',31,NULL,'label'),
(13,3,5,'wwm8',4,2,4,3,4,5,'<p>Ok</p>\r\n',22,4,5,3,4,3,NULL,NULL,'<p>Ok</p>\r\n',19,24,1,1,1,2,1,'<p>Ok</p>\r\n',30,NULL,'label'),
(15,9,5,'X5Lp',3,4,2,2,2,2,'<p>Cool!</p>\r\n',15,NULL,NULL,14,11,NULL,12,7,'<p>Ok</p>\r\n',44,8,2,2,1,1,2,'<p>Super</p>\r\n',16,NULL,'finance'),
(16,9,7,'X5Lp',4,2,2,4,1,2,'<p>Ok</p>\r\n',15,NULL,NULL,10,8,NULL,14,13,'<p>Super</p>\r\n',45,9,2,1,1,1,2,'<p>Cool</p>\r\n',16,NULL,'finance'),
(17,10,5,'VMz7',4,3,5,2,5,3,'<p>Ok !</p>\r\n',22,3,9,2,5,4,NULL,NULL,'<p>Super !</p>\r\n',23,25,1,2,1,1,2,'<p>Ok !</p>\r\n',32,NULL,'label'),
(18,10,7,'VMz7',5,4,5,3,4,4,'<p>Ok !</p>\r\n',25,5,8,4,4,5,NULL,NULL,'<p>Super !</p>\r\n',26,28,2,2,1,1,2,'<p>Ok !</p>\r\n',36,NULL,'label'),
(20,2,5,'CXW5',5,4,4,2,5,3,'<p>Ok</p>\r\n',23,5,8,5,4,4,NULL,NULL,'<p>Super</p>\r\n',26,26,2,2,1,1,0,'<p>Ok</p>\r\n',32,NULL,'label'),
(21,11,5,'feBg',5,3,5,5,4,3,'<p>Ok</p>\r\n',25,4,7,4,3,4,NULL,NULL,'<p>Super</p>\r\n',22,26,2,2,1,1,2,'<p>Ok</p>\r\n',34,NULL,'label'),
(22,2,7,'CXW5',5,3,4,4,5,3,'<p>Ok</p>\r\n',24,4,8,5,3,4,NULL,NULL,'<p>Super</p>\r\n',24,25,3,1,1,1,2,'<p>Ok</p>\r\n',33,NULL,'label'),
(23,11,7,'feBg',4,4,5,3,5,5,'<p>Ok</p>\r\n',26,4,4,5,4,5,NULL,NULL,'<p>Ok</p>\r\n',22,28,2,2,1,1,2,'<p>Super</p>\r\n',36,NULL,'label');

/*Table structure for table `ots` */

DROP TABLE IF EXISTS `ots`;

CREATE TABLE `ots` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(250) NOT NULL,
  `adresse` varchar(250) DEFAULT NULL,
  `telephone` varchar(255) DEFAULT NULL,
  `structure_id` int(11) DEFAULT NULL,
  `type` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `structure_id` (`structure_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `ots` */

/*Table structure for table `participants` */

DROP TABLE IF EXISTS `participants`;

CREATE TABLE `participants` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(250) NOT NULL,
  `prenom` varchar(250) NOT NULL,
  `adresse` varchar(250) NOT NULL,
  `telephone` varchar(250) DEFAULT NULL,
  `cni` int(50) NOT NULL,
  `animation_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `animation_id` (`animation_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `participants` */

/*Table structure for table `piece_jointes` */

DROP TABLE IF EXISTS `piece_jointes`;

CREATE TABLE `piece_jointes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(100) DEFAULT NULL,
  `message_id` int(11) DEFAULT NULL,
  `dossier_id` int(11) DEFAULT NULL,
  `titre` varchar(500) DEFAULT NULL,
  `mediation_id` int(11) DEFAULT NULL,
  `animation_id` int(11) DEFAULT NULL,
  `suivi_evaluation_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_documents_messages1_idx` (`message_id`),
  KEY `fk_piece_jointes_dossiers1_idx` (`dossier_id`),
  KEY `mediation_id` (`mediation_id`),
  KEY `animation_id` (`animation_id`),
  KEY `suivi_evaluation_id` (`suivi_evaluation_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

/*Data for the table `piece_jointes` */

insert  into `piece_jointes`(`id`,`url`,`message_id`,`dossier_id`,`titre`,`mediation_id`,`animation_id`,`suivi_evaluation_id`) values 
(1,'pape.jpg',NULL,2,'Etude de marché',NULL,NULL,NULL),
(2,'brand.jpg',NULL,3,'Ninea',NULL,NULL,NULL),
(3,'Fiche mission.pdf',NULL,7,'Business plan',NULL,NULL,NULL),
(4,'abidjan.jpeg',NULL,8,'Business plan',NULL,NULL,NULL),
(5,'libreville2.jpeg',NULL,9,'Etude de marché',NULL,NULL,NULL),
(6,'dakar2.jpeg',NULL,9,'Business plan',NULL,NULL,NULL),
(7,'cosmÉtique panierdakar.pdf',NULL,10,'',NULL,NULL,NULL),
(8,'Génrération clé Api.PNG',NULL,11,'Registre de commerce',NULL,NULL,NULL);

/*Table structure for table `porteurs` */

DROP TABLE IF EXISTS `porteurs`;

CREATE TABLE `porteurs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `entreprise_id` int(11) DEFAULT NULL,
  `nom` varchar(45) DEFAULT NULL,
  `telephone` varchar(45) DEFAULT NULL,
  `prenom` varchar(45) DEFAULT NULL,
  `genre` varchar(50) DEFAULT NULL,
  `date_naissance` date DEFAULT NULL,
  `lieu_naissance` varchar(255) DEFAULT NULL,
  `lieu_residence` varchar(255) DEFAULT NULL,
  `type_identification` varchar(50) DEFAULT NULL,
  `num_identification` varchar(255) DEFAULT NULL,
  `diplome` varchar(100) DEFAULT NULL,
  `niveau_etude` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_demandeurs_users1_idx` (`user_id`),
  KEY `fk_porteurs_entreprises1_idx` (`entreprise_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

/*Data for the table `porteurs` */

insert  into `porteurs`(`id`,`user_id`,`entreprise_id`,`nom`,`telephone`,`prenom`,`genre`,`date_naissance`,`lieu_naissance`,`lieu_residence`,`type_identification`,`num_identification`,`diplome`,`niveau_etude`) values 
(1,6,NULL,'Fall','773976458','Moussa','homme','1994-01-07','Dakar','HLM Grd Yoff','CNI','1458795206699',NULL,NULL),
(11,17,8,'Faye',NULL,'Sadikh',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(12,22,NULL,'Diop','775234479','Modou','homme','1995-01-26','Dakar','Dakar','CNI','1456578941548',NULL,NULL),
(14,24,10,'Bathiéry','785947863','Babacar',NULL,'1984-03-05','Dakar','Thiès',NULL,NULL,NULL,NULL);

/*Table structure for table `profiles` */

DROP TABLE IF EXISTS `profiles`;

CREATE TABLE `profiles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `libelle` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `profiles` */

insert  into `profiles`(`id`,`libelle`) values 
(1,'Porteur'),
(2,'Admin'),
(3,'Operateur'),
(4,'Validateur'),
(5,'Comité'),
(6,'Superviseur');

/*Table structure for table `qualification_dossiers` */

DROP TABLE IF EXISTS `qualification_dossiers`;

CREATE TABLE `qualification_dossiers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dossiers_id` int(11) NOT NULL,
  `agents_id` int(11) DEFAULT NULL,
  `desc_etat_civil` text,
  `desc_dossier` text,
  `desc_impact` text,
  `raison_rejet` text,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `code` varchar(45) DEFAULT NULL,
  `type_demande` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `dossiers_id` (`dossiers_id`),
  KEY `agents_id` (`agents_id`),
  CONSTRAINT `fk_agents_qualifications_dossiers1` FOREIGN KEY (`agents_id`) REFERENCES `agents` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_qualifications_dossiers1` FOREIGN KEY (`dossiers_id`) REFERENCES `dossiers` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

/*Data for the table `qualification_dossiers` */

insert  into `qualification_dossiers`(`id`,`dossiers_id`,`agents_id`,`desc_etat_civil`,`desc_dossier`,`desc_impact`,`raison_rejet`,`created`,`code`,`type_demande`) values 
(1,9,2,'<p>Etat civil complet !</p>\r\n','<p>Dossier complet !</p>\r\n','<p>C&#39;est Ok !</p>\r\n',NULL,'2020-10-02 13:33:23',NULL,'label'),
(6,3,2,'<p>Ok cool !</p>\r\n','<p>Les documents sont complets</p>\r\n','<p>Ok !</p>\r\n',NULL,'2020-10-15 15:47:28','OyC6','label'),
(7,2,2,'<p>Ok super !</p>\r\n','<p>Excellent !</p>\r\n','<p>Ok&nbsp;</p>\r\n',NULL,'2020-11-04 13:39:04','5Cq5','label'),
(10,9,2,'<p>Ok cool</p>\r\n','<p>Super</p>\r\n','<p>Super</p>\r\n',NULL,'2020-12-01 12:06:20','YQMH','finance'),
(11,10,4,'<p>C&#39;est fantastique !</p>\r\n','<p>Super !</p>\r\n','<p>Parfait !</p>\r\n',NULL,'2021-01-25 15:41:06','R0RO','label'),
(18,11,4,'<p>Ok</p>\r\n','<p>Ok</p>\r\n','<p>Super</p>\r\n',NULL,'2021-01-29 15:26:50','NuCM','label');

/*Table structure for table `regions` */

DROP TABLE IF EXISTS `regions`;

CREATE TABLE `regions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

/*Data for the table `regions` */

insert  into `regions`(`id`,`nom`) values 
(1,'Dakar'),
(4,'Koalack'),
(5,'Diourbel'),
(6,'Fatick'),
(7,'Kaffrine'),
(8,'Kédougou'),
(9,'Kolda'),
(10,'Louga'),
(11,'Matam'),
(12,'Saint-Louis'),
(13,'Sédhiou'),
(14,'Tambacounda'),
(15,'Thiès'),
(16,'Ziguinchor');

/*Table structure for table `situations_comptes` */

DROP TABLE IF EXISTS `situations_comptes`;

CREATE TABLE `situations_comptes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `libelle` varchar(255) DEFAULT NULL,
  `annee_moins_trois` double DEFAULT NULL,
  `annee_moins_deux` double DEFAULT NULL,
  `annee_moins_un` double DEFAULT NULL,
  `annee` double DEFAULT NULL,
  `prevision` double DEFAULT NULL,
  `dossiers_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `dossiers_id` (`dossiers_id`),
  CONSTRAINT `fk_situations_comptes_dossiers1` FOREIGN KEY (`dossiers_id`) REFERENCES `dossiers` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

/*Data for the table `situations_comptes` */

insert  into `situations_comptes`(`id`,`libelle`,`annee_moins_trois`,`annee_moins_deux`,`annee_moins_un`,`annee`,`prevision`,`dossiers_id`) values 
(1,'Chiffres d\'affaires',800000,1500000,2000000,3000000,4500000,9),
(2,'Résultat Net',700000,750000,850000,850000,1500000,9),
(3,'Fonds de roulement',500000,650000,800000,900000,1000000,9),
(4,'Charges financières',300000,350000,400000,450000,550000,9),
(5,'Dettes financières',0,0,0,0,0,9),
(6,'Créances Clients',250000,350000,450000,550000,1000000,9),
(7,'Dettes Fournisseurs',0,0,0,0,0,9),
(8,'Capacité de remboursement',150000,200000,200000,200000,300000,9);

/*Table structure for table `status_validations` */

DROP TABLE IF EXISTS `status_validations`;

CREATE TABLE `status_validations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `status_validations` */

insert  into `status_validations`(`id`,`nom`) values 
(1,'En cours'),
(2,'qualifié'),
(3,'En cours d\'analyse'),
(4,'labélisé'),
(5,'refusé'),
(6,'accepté');

/*Table structure for table `statut_dossiers` */

DROP TABLE IF EXISTS `statut_dossiers`;

CREATE TABLE `statut_dossiers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `libelle` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `statut_dossiers` */

insert  into `statut_dossiers`(`id`,`libelle`) values 
(1,'En cours'),
(2,'qualifié'),
(3,'En cours d\'analyse'),
(4,'labélisé'),
(5,'refusé'),
(6,'Confirmé labélisation');

/*Table structure for table `statut_financements` */

DROP TABLE IF EXISTS `statut_financements`;

CREATE TABLE `statut_financements` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `libelle` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Data for the table `statut_financements` */

insert  into `statut_financements`(`id`,`libelle`) values 
(1,'Soumis'),
(2,'En cours'),
(3,'Qualifié'),
(4,'En cours d\'analyse'),
(5,'Accepté'),
(6,'Confirmé'),
(7,'Rejeté');

/*Table structure for table `structures` */

DROP TABLE IF EXISTS `structures`;

CREATE TABLE `structures` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(500) NOT NULL,
  `telephone` varchar(45) NOT NULL,
  `adresse` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `structures` */

insert  into `structures`(`id`,`nom`,`telephone`,`adresse`) values 
(1,'Ess','338003030','Dakar, Sénégal');

/*Table structure for table `suivi_evaluations` */

DROP TABLE IF EXISTS `suivi_evaluations`;

CREATE TABLE `suivi_evaluations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titre` varchar(500) DEFAULT NULL,
  `description` text,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `type_evaluation_id` int(11) DEFAULT NULL,
  `agent_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`),
  KEY `type_evaluation_id` (`type_evaluation_id`),
  KEY `agent_id` (`agent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `suivi_evaluations` */

/*Table structure for table `suretes` */

DROP TABLE IF EXISTS `suretes`;

CREATE TABLE `suretes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `libelle` varchar(250) DEFAULT NULL,
  `valeur` float DEFAULT NULL,
  `cotation` int(11) DEFAULT NULL,
  `type_id` int(11) DEFAULT NULL,
  `dossier_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_suretes_types1_idx` (`type_id`),
  KEY `dossier_id` (`dossier_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `suretes` */

/*Table structure for table `type_agences` */

DROP TABLE IF EXISTS `type_agences`;

CREATE TABLE `type_agences` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `libelle` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `type_agences` */

insert  into `type_agences`(`id`,`libelle`) values 
(1,'Service'),
(2,'Comite');

/*Table structure for table `type_animations` */

DROP TABLE IF EXISTS `type_animations`;

CREATE TABLE `type_animations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `type_animations` */

/*Table structure for table `type_entreprises` */

DROP TABLE IF EXISTS `type_entreprises`;

CREATE TABLE `type_entreprises` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `type_entreprises` */

insert  into `type_entreprises`(`id`,`nom`) values 
(1,'PMI'),
(2,'PME'),
(3,'Groupement de jeunes '),
(4,'Groupement de femmes'),
(5,'Association de jeunes');

/*Table structure for table `type_evaluations` */

DROP TABLE IF EXISTS `type_evaluations`;

CREATE TABLE `type_evaluations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `type_evaluations` */

/*Table structure for table `type_messages` */

DROP TABLE IF EXISTS `type_messages`;

CREATE TABLE `type_messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `type_messages` */

/*Table structure for table `types` */

DROP TABLE IF EXISTS `types`;

CREATE TABLE `types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `types` */

/*Table structure for table `types_impacts` */

DROP TABLE IF EXISTS `types_impacts`;

CREATE TABLE `types_impacts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `libelle` varchar(255) DEFAULT NULL,
  `impactes_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `impactes_id` (`impactes_id`),
  CONSTRAINT `impactes_types_impacts1` FOREIGN KEY (`impactes_id`) REFERENCES `impactes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

/*Data for the table `types_impacts` */

insert  into `types_impacts`(`id`,`libelle`,`impactes_id`) values 
(1,'Education à l\'environnement',2),
(2,'Préservation de l\'environnement',2),
(3,'Lien social',1),
(4,'Citoyenneté',1),
(5,'Equipe térritoriale',1),
(6,'Diversité culturelle',1),
(7,'Egalité des chances',1),
(8,'Innovation',4),
(9,'Représentation citoyenne',4),
(10,'Création de richesse et de services épargnés à la collectivité',5),
(11,'Hausse de l\'emploi',5),
(12,'Développement du capital humain',5),
(13,'Caadre et conditions de vie',3),
(14,'Expression',3),
(15,'Autonomie / capabilités',3),
(16,'Santé',3);

/*Table structure for table `types_pieces` */

DROP TABLE IF EXISTS `types_pieces`;

CREATE TABLE `types_pieces` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `libelle` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

/*Data for the table `types_pieces` */

insert  into `types_pieces`(`id`,`libelle`) values 
(1,'Nature du document'),
(2,'Business plan'),
(3,'Registre de commerce'),
(4,'Facture proformat'),
(5,'Ninea'),
(6,'Etude de marché'),
(7,'Note conceptuelle'),
(8,'Autre');

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(45) DEFAULT NULL,
  `password` varchar(250) DEFAULT NULL,
  `profiles_id` int(11) NOT NULL,
  `photo` varchar(255) DEFAULT NULL,
  `type` varchar(50) DEFAULT NULL,
  `codeIdentification` varchar(250) DEFAULT NULL,
  `actif` tinyint(1) DEFAULT NULL,
  `cle` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `profiles_id` (`profiles_id`),
  CONSTRAINT `fk_profiles_users1` FOREIGN KEY (`profiles_id`) REFERENCES `profiles` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=latin1;

/*Data for the table `users` */

insert  into `users`(`id`,`email`,`password`,`profiles_id`,`photo`,`type`,`codeIdentification`,`actif`,`cle`) values 
(6,'musa@gmail.com','$2y$10$XsXz/i5.Tg8e2YzrJZP8p.uAX.qEwbTi7CQjCcNhassol1.uftee2',1,'1600190520profil.jpg',NULL,'ess-ugia',1,NULL),
(17,'sadikh@gmail.com','$2y$10$pS0hwtAnJQTD3Q21zaA7SeVh/L6i7lhmb7HbEGUvh.UGLK1reSJ8y',1,'1600468599pape.jpg',NULL,'ess-TB3Q',1,NULL),
(18,'khadime@gmail.com','$2y$10$w7WmsU18ddJZY.5wA8TiTeV7Xpdw/I3Tw984W8.NIaXpnfiSJ2t46',3,'1608036435pape.jpg',NULL,NULL,1,NULL),
(19,'laye@gmail.com','$2y$10$XsXz/i5.Tg8e2YzrJZP8p.uAX.qEwbTi7CQjCcNhassol1.uftee2',4,NULL,NULL,NULL,1,NULL),
(20,'contact@ess.com','$2y$10$XsXz/i5.Tg8e2YzrJZP8p.uAX.qEwbTi7CQjCcNhassol1.uftee2',2,'1608037198246x0w.png',NULL,NULL,1,NULL),
(21,'aida@gmail.com','$2y$10$c3ghrEjRgWGOH0uFrMuIZu9FDxGsjrbma1.S6LFaAG9fxngfbMRKq',4,NULL,NULL,NULL,1,NULL),
(22,'modou@gmail.com','$2y$10$vkmUhcLBKOFQIfMS.Dlz4.6UoP8yjqljqrRRsGp.s823zz05mSj26',1,'1601121417pape.jpg',NULL,'ess-Hr5A',1,NULL),
(24,'babacar@gmail.com','$2y$10$40DaBHYRZRtEVY6p4Qp3tOqanNc/yKpZp3gud7SJnD5yKR9TeCPae',1,'1601320206yaounde2.jpeg',NULL,'REVES-Dihu',1,NULL),
(25,'validateur1@gmail.com','$2y$10$U8Z6AxZQCE2yRqGo3NKeO.7tbqaAdGMyIYh01vwmAAb9M8PMfftfq',4,NULL,NULL,NULL,1,NULL),
(28,'admin@analyse.com','$2y$10$ZkWnLQBdHRvT6yO6GqYEoe377hjoud1y./7X0dlibP2K2E.EOzf.O',3,NULL,NULL,NULL,1,NULL),
(29,'validateur2@gmail.com','$2y$10$z1EHZOrjwg94BDD.RDkPIurHSsGaAkS8A2CDJKG1bshQ6dtNgV4e6',4,NULL,NULL,NULL,1,NULL),
(30,'comite.dakar@gmail.com','$2y$10$Ab/sbt8oi5lO7nqgB6LHmuBxBHUpF29PC5dl0a6pZTq57uvkFT0xK',5,NULL,NULL,NULL,1,NULL),
(31,'comite.thies@gmail.com','$2y$10$yoOWv59QVImAbVMvnhHkWOuhQalgQ4XoSB3Pw1dalnDFHxKpxDfIO',5,NULL,NULL,NULL,1,NULL),
(32,'agent@comitedk.com','$2y$10$/A7wv5M8O7Br5EkPDX1dI.IRsmCkaVuq5r8P1c9WDJMpgdIarMLgu',5,NULL,NULL,NULL,1,NULL),
(34,'zahra.thiam@ess.com','$2y$10$TNChrQyPvWTBUIJHzGLPW.J8BOyoP/Jiq8VMuZ8CSlBvRcZu4yKBG',6,NULL,NULL,NULL,1,NULL),
(36,'comite.sedhiou@gmail.com','$2y$10$fMmLsOJMhIcdUpvL5c523OJ8SxOxLcgMcEoygFAd3v57ATnS110du',5,NULL,NULL,NULL,1,NULL);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
