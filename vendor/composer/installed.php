<?php return array (
  'root' => 
  array (
    'pretty_version' => 'dev-master',
    'version' => 'dev-master',
    'aliases' => 
    array (
    ),
    'reference' => '9d7eea441e0ed9639c99070a2c2163b2b9bb7cd1',
    'name' => 'cakephp/app',
  ),
  'versions' => 
  array (
    'ajgl/breakpoint-twig-extension' => 
    array (
      'pretty_version' => '0.3.2',
      'version' => '0.3.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'bb419d13775d8fb5cd03b31012eaf496b31b444f',
    ),
    'aptoma/twig-markdown' => 
    array (
      'pretty_version' => '2.0.0',
      'version' => '2.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '64a9c5c7418c08faf91c4410b34bdb65fb25c23d',
    ),
    'asm89/twig-cache-extension' => 
    array (
      'pretty_version' => '1.3.2',
      'version' => '1.3.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '630ea7abdc3fc62ba6786c02590a1560e449cf55',
    ),
    'aura/intl' => 
    array (
      'pretty_version' => '3.0.0',
      'version' => '3.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '7fce228980b19bf4dee2d7bbd6202a69b0dde926',
    ),
    'cakephp/app' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
      ),
      'reference' => '9d7eea441e0ed9639c99070a2c2163b2b9bb7cd1',
    ),
    'cakephp/bake' => 
    array (
      'pretty_version' => '1.9.2',
      'version' => '1.9.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '9949a6a867a17cf30996dfc9daceab1c63a7a9c1',
    ),
    'cakephp/cache' => 
    array (
      'replaced' => 
      array (
        0 => '3.7.4',
      ),
    ),
    'cakephp/cakephp' => 
    array (
      'pretty_version' => '3.7.4',
      'version' => '3.7.4.0',
      'aliases' => 
      array (
      ),
      'reference' => '5c78a61c0ed7fbd56ce9d7cee3c6d6f9b27af46b',
    ),
    'cakephp/cakephp-codesniffer' => 
    array (
      'pretty_version' => '3.1.1',
      'version' => '3.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '682e79fda294c4383e094a2a881e16dcf1130750',
    ),
    'cakephp/chronos' => 
    array (
      'pretty_version' => '1.2.4',
      'version' => '1.2.4.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ebda7326d4a65e53bc5bb915ebbbeee98f1926b0',
    ),
    'cakephp/collection' => 
    array (
      'replaced' => 
      array (
        0 => '3.7.4',
      ),
    ),
    'cakephp/core' => 
    array (
      'replaced' => 
      array (
        0 => '3.7.4',
      ),
    ),
    'cakephp/database' => 
    array (
      'replaced' => 
      array (
        0 => '3.7.4',
      ),
    ),
    'cakephp/datasource' => 
    array (
      'replaced' => 
      array (
        0 => '3.7.4',
      ),
    ),
    'cakephp/debug_kit' => 
    array (
      'pretty_version' => '3.17.1',
      'version' => '3.17.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b850256dd6a15af21f02c188b9ca9a4c8e197c79',
    ),
    'cakephp/event' => 
    array (
      'replaced' => 
      array (
        0 => '3.7.4',
      ),
    ),
    'cakephp/filesystem' => 
    array (
      'replaced' => 
      array (
        0 => '3.7.4',
      ),
    ),
    'cakephp/form' => 
    array (
      'replaced' => 
      array (
        0 => '3.7.4',
      ),
    ),
    'cakephp/i18n' => 
    array (
      'replaced' => 
      array (
        0 => '3.7.4',
      ),
    ),
    'cakephp/log' => 
    array (
      'replaced' => 
      array (
        0 => '3.7.4',
      ),
    ),
    'cakephp/migrations' => 
    array (
      'pretty_version' => '2.1.0',
      'version' => '2.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b5148180a59b991dac3a74c3058dd5891dfc8961',
    ),
    'cakephp/orm' => 
    array (
      'replaced' => 
      array (
        0 => '3.7.4',
      ),
    ),
    'cakephp/plugin-installer' => 
    array (
      'pretty_version' => '1.1.0',
      'version' => '1.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '41373d0678490502f45adc7be88aa22d24ac1843',
    ),
    'cakephp/utility' => 
    array (
      'replaced' => 
      array (
        0 => '3.7.4',
      ),
    ),
    'cakephp/validation' => 
    array (
      'replaced' => 
      array (
        0 => '3.7.4',
      ),
    ),
    'composer/ca-bundle' => 
    array (
      'pretty_version' => '1.1.4',
      'version' => '1.1.4.0',
      'aliases' => 
      array (
      ),
      'reference' => '558f321c52faeb4828c03e7dc0cfe39a09e09a2d',
    ),
    'composer/composer' => 
    array (
      'pretty_version' => '1.8.4',
      'version' => '1.8.4.0',
      'aliases' => 
      array (
      ),
      'reference' => 'bc364c2480c17941e2135cfc568fa41794392534',
    ),
    'composer/semver' => 
    array (
      'pretty_version' => '1.4.2',
      'version' => '1.4.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c7cb9a2095a074d131b65a8a0cd294479d785573',
    ),
    'composer/spdx-licenses' => 
    array (
      'pretty_version' => '1.5.0',
      'version' => '1.5.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '7a9556b22bd9d4df7cad89876b00af58ef20d3a2',
    ),
    'composer/xdebug-handler' => 
    array (
      'pretty_version' => '1.3.2',
      'version' => '1.3.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd17708133b6c276d6e42ef887a877866b909d892',
    ),
    'dnoegel/php-xdg-base-dir' => 
    array (
      'pretty_version' => '0.1',
      'version' => '0.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '265b8593498b997dc2d31e75b89f053b5cc9621a',
    ),
    'doctrine/instantiator' => 
    array (
      'pretty_version' => '1.1.0',
      'version' => '1.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '185b8868aa9bf7159f5f953ed5afb2d7fcdc3bda',
    ),
    'guzzlehttp/guzzle' => 
    array (
      'pretty_version' => '6.3.3',
      'version' => '6.3.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '407b0cb880ace85c9b63c5f9551db498cb2d50ba',
    ),
    'guzzlehttp/promises' => 
    array (
      'pretty_version' => 'v1.3.1',
      'version' => '1.3.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a59da6cf61d80060647ff4d3eb2c03a2bc694646',
    ),
    'guzzlehttp/psr7' => 
    array (
      'pretty_version' => '1.5.2',
      'version' => '1.5.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '9f83dded91781a01c63574e387eaa769be769115',
    ),
    'jakub-onderka/php-console-color' => 
    array (
      'pretty_version' => 'v0.2',
      'version' => '0.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd5deaecff52a0d61ccb613bb3804088da0307191',
    ),
    'jakub-onderka/php-console-highlighter' => 
    array (
      'pretty_version' => 'v0.4',
      'version' => '0.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '9f7a229a69d52506914b4bc61bfdb199d90c5547',
    ),
    'jasny/twig-extensions' => 
    array (
      'pretty_version' => 'v1.2.0',
      'version' => '1.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '30bdf3a3903c021544f36332c9d5d4d563527da4',
    ),
    'jdorn/sql-formatter' => 
    array (
      'pretty_version' => 'v1.2.17',
      'version' => '1.2.17.0',
      'aliases' => 
      array (
      ),
      'reference' => '64990d96e0959dff8e059dfcdc1af130728d92bc',
    ),
    'josegonzalez/dotenv' => 
    array (
      'pretty_version' => '3.2.0',
      'version' => '3.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f19174d9d7213a6c20e8e5e268aa7dd042d821ca',
    ),
    'justinrainbow/json-schema' => 
    array (
      'pretty_version' => '5.2.8',
      'version' => '5.2.8.0',
      'aliases' => 
      array (
      ),
      'reference' => 'dcb6e1006bb5fd1e392b4daa68932880f37550d4',
    ),
    'm1/env' => 
    array (
      'pretty_version' => '2.1.2',
      'version' => '2.1.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '294addeedf15e1149eeb96ec829f2029d2017d39',
    ),
    'mailjet/mailjet-apiv3-php' => 
    array (
      'pretty_version' => 'v1.4.1',
      'version' => '1.4.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '9d89b9a424a9631bff8e499cbbe34058481e8102',
    ),
    'mobiledetect/mobiledetectlib' => 
    array (
      'pretty_version' => '2.8.33',
      'version' => '2.8.33.0',
      'aliases' => 
      array (
      ),
      'reference' => 'cd385290f9a0d609d2eddd165a1e44ec1bf12102',
    ),
    'myclabs/deep-copy' => 
    array (
      'pretty_version' => '1.8.1',
      'version' => '1.8.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '3e01bdad3e18354c3dce54466b7fbe33a9f9f7f8',
      'replaced' => 
      array (
        0 => '1.8.1',
      ),
    ),
    'nikic/php-parser' => 
    array (
      'pretty_version' => 'v4.2.1',
      'version' => '4.2.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '5221f49a608808c1e4d436df32884cbc1b821ac0',
    ),
    'pdfcrowd/pdfcrowd' => 
    array (
      'pretty_version' => '4.12.0',
      'version' => '4.12.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'fbeafc899a479f6a31498e18190ee62717299846',
    ),
    'phar-io/manifest' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '2df402786ab5368a0169091f61a7c1e0eb6852d0',
    ),
    'phar-io/version' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a70c0ced4be299a63d32fa96d9281d03e94041df',
    ),
    'phpdocumentor/reflection-common' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '21bdeb5f65d7ebf9f43b1b25d404f87deab5bfb6',
    ),
    'phpdocumentor/reflection-docblock' => 
    array (
      'pretty_version' => '4.3.0',
      'version' => '4.3.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '94fd0001232e47129dd3504189fa1c7225010d08',
    ),
    'phpdocumentor/type-resolver' => 
    array (
      'pretty_version' => '0.4.0',
      'version' => '0.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '9c977708995954784726e25d0cd1dddf4e65b0f7',
    ),
    'phpspec/prophecy' => 
    array (
      'pretty_version' => '1.8.0',
      'version' => '1.8.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '4ba436b55987b4bf311cb7c6ba82aa528aac0a06',
    ),
    'phpunit/php-code-coverage' => 
    array (
      'pretty_version' => '5.3.2',
      'version' => '5.3.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c89677919c5dd6d3b3852f230a663118762218ac',
    ),
    'phpunit/php-file-iterator' => 
    array (
      'pretty_version' => '1.4.5',
      'version' => '1.4.5.0',
      'aliases' => 
      array (
      ),
      'reference' => '730b01bc3e867237eaac355e06a36b85dd93a8b4',
    ),
    'phpunit/php-text-template' => 
    array (
      'pretty_version' => '1.2.1',
      'version' => '1.2.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '31f8b717e51d9a2afca6c9f046f5d69fc27c8686',
    ),
    'phpunit/php-timer' => 
    array (
      'pretty_version' => '1.0.9',
      'version' => '1.0.9.0',
      'aliases' => 
      array (
      ),
      'reference' => '3dcf38ca72b158baf0bc245e9184d3fdffa9c46f',
    ),
    'phpunit/php-token-stream' => 
    array (
      'pretty_version' => '2.0.2',
      'version' => '2.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '791198a2c6254db10131eecfe8c06670700904db',
    ),
    'phpunit/phpunit' => 
    array (
      'pretty_version' => '6.5.14',
      'version' => '6.5.14.0',
      'aliases' => 
      array (
      ),
      'reference' => 'bac23fe7ff13dbdb461481f706f0e9fe746334b7',
    ),
    'phpunit/phpunit-mock-objects' => 
    array (
      'pretty_version' => '5.0.10',
      'version' => '5.0.10.0',
      'aliases' => 
      array (
      ),
      'reference' => 'cd1cf05c553ecfec36b170070573e540b67d3f1f',
    ),
    'psr/http-message' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f6561bf28d520154e4b0ec72be95418abe6d9363',
    ),
    'psr/http-message-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'psr/log' => 
    array (
      'pretty_version' => '1.1.0',
      'version' => '1.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '6c001f1daafa3a3ac1d8ff69ee4db8e799a654dd',
    ),
    'psr/log-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'psr/simple-cache' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '408d5eafb83c57f6365a3ca330ff23aa4a5fa39b',
    ),
    'psy/psysh' => 
    array (
      'pretty_version' => 'v0.9.9',
      'version' => '0.9.9.0',
      'aliases' => 
      array (
      ),
      'reference' => '9aaf29575bb8293206bb0420c1e1c87ff2ffa94e',
    ),
    'ralouphie/getallheaders' => 
    array (
      'pretty_version' => '2.0.5',
      'version' => '2.0.5.0',
      'aliases' => 
      array (
      ),
      'reference' => '5601c8a83fbba7ef674a7369456d12f1e0d0eafa',
    ),
    'robmorgan/phinx' => 
    array (
      'pretty_version' => '0.10.6',
      'version' => '0.10.6.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f28a1c6ab1fa1f0295cddade9aea05eeb303bd2b',
    ),
    'sebastian/code-unit-reverse-lookup' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '4419fcdb5eabb9caa61a27c7a1db532a6b55dd18',
    ),
    'sebastian/comparator' => 
    array (
      'pretty_version' => '2.1.3',
      'version' => '2.1.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '34369daee48eafb2651bea869b4b15d75ccc35f9',
    ),
    'sebastian/diff' => 
    array (
      'pretty_version' => '2.0.1',
      'version' => '2.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '347c1d8b49c5c3ee30c7040ea6fc446790e6bddd',
    ),
    'sebastian/environment' => 
    array (
      'pretty_version' => '3.1.0',
      'version' => '3.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'cd0871b3975fb7fc44d11314fd1ee20925fce4f5',
    ),
    'sebastian/exporter' => 
    array (
      'pretty_version' => '3.1.0',
      'version' => '3.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '234199f4528de6d12aaa58b612e98f7d36adb937',
    ),
    'sebastian/global-state' => 
    array (
      'pretty_version' => '2.0.0',
      'version' => '2.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e8ba02eed7bbbb9e59e43dedd3dddeff4a56b0c4',
    ),
    'sebastian/object-enumerator' => 
    array (
      'pretty_version' => '3.0.3',
      'version' => '3.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '7cfd9e65d11ffb5af41198476395774d4c8a84c5',
    ),
    'sebastian/object-reflector' => 
    array (
      'pretty_version' => '1.1.1',
      'version' => '1.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '773f97c67f28de00d397be301821b06708fca0be',
    ),
    'sebastian/recursion-context' => 
    array (
      'pretty_version' => '3.0.0',
      'version' => '3.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '5b0cd723502bac3b006cbf3dbf7a1e3fcefe4fa8',
    ),
    'sebastian/resource-operations' => 
    array (
      'pretty_version' => '1.0.0',
      'version' => '1.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ce990bb21759f94aeafd30209e8cfcdfa8bc3f52',
    ),
    'sebastian/version' => 
    array (
      'pretty_version' => '2.0.1',
      'version' => '2.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '99732be0ddb3361e16ad77b68ba41efc8e979019',
    ),
    'seld/jsonlint' => 
    array (
      'pretty_version' => '1.7.1',
      'version' => '1.7.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd15f59a67ff805a44c50ea0516d2341740f81a38',
    ),
    'seld/phar-utils' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '7009b5139491975ef6486545a39f3e6dad5ac30a',
    ),
    'squizlabs/php_codesniffer' => 
    array (
      'pretty_version' => '3.4.0',
      'version' => '3.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '379deb987e26c7cd103a7b387aea178baec96e48',
    ),
    'symfony/config' => 
    array (
      'pretty_version' => 'v4.2.4',
      'version' => '4.2.4.0',
      'aliases' => 
      array (
      ),
      'reference' => '7f70d79c7a24a94f8e98abb988049403a53d7b31',
    ),
    'symfony/console' => 
    array (
      'pretty_version' => 'v4.2.4',
      'version' => '4.2.4.0',
      'aliases' => 
      array (
      ),
      'reference' => '9dc2299a016497f9ee620be94524e6c0af0280a9',
    ),
    'symfony/contracts' => 
    array (
      'pretty_version' => 'v1.0.2',
      'version' => '1.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '1aa7ab2429c3d594dd70689604b5cf7421254cdf',
    ),
    'symfony/filesystem' => 
    array (
      'pretty_version' => 'v4.2.4',
      'version' => '4.2.4.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e16b9e471703b2c60b95f14d31c1239f68f11601',
    ),
    'symfony/finder' => 
    array (
      'pretty_version' => 'v4.2.4',
      'version' => '4.2.4.0',
      'aliases' => 
      array (
      ),
      'reference' => '267b7002c1b70ea80db0833c3afe05f0fbde580a',
    ),
    'symfony/polyfill-ctype' => 
    array (
      'pretty_version' => 'v1.10.0',
      'version' => '1.10.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e3d826245268269cd66f8326bd8bc066687b4a19',
    ),
    'symfony/polyfill-mbstring' => 
    array (
      'pretty_version' => 'v1.10.0',
      'version' => '1.10.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c79c051f5b3a46be09205c73b80b346e4153e494',
    ),
    'symfony/polyfill-php72' => 
    array (
      'pretty_version' => 'v1.10.0',
      'version' => '1.10.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '9050816e2ca34a8e916c3a0ae8b9c2fccf68b631',
    ),
    'symfony/process' => 
    array (
      'pretty_version' => 'v4.2.4',
      'version' => '4.2.4.0',
      'aliases' => 
      array (
      ),
      'reference' => '6c05edb11fbeff9e2b324b4270ecb17911a8b7ad',
    ),
    'symfony/var-dumper' => 
    array (
      'pretty_version' => 'v4.2.4',
      'version' => '4.2.4.0',
      'aliases' => 
      array (
      ),
      'reference' => '9f87189ac10b42edf7fb8edc846f1937c6d157cf',
    ),
    'symfony/yaml' => 
    array (
      'pretty_version' => 'v4.2.4',
      'version' => '4.2.4.0',
      'aliases' => 
      array (
      ),
      'reference' => '761fa560a937fd7686e5274ff89dcfa87a5047df',
    ),
    'theseer/tokenizer' => 
    array (
      'pretty_version' => '1.1.0',
      'version' => '1.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'cb2f008f3f05af2893a87208fe6a6c4985483f8b',
    ),
    'twig/twig' => 
    array (
      'pretty_version' => 'v1.37.1',
      'version' => '1.37.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '66be9366c76cbf23e82e7171d47cbfa54a057a62',
    ),
    'umpirsky/twig-php-function' => 
    array (
      'pretty_version' => 'v0.1',
      'version' => '0.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '53b4b1eb0c5eacbd7d66c504b7d809c79b4bedbc',
    ),
    'webmozart/assert' => 
    array (
      'pretty_version' => '1.4.0',
      'version' => '1.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '83e253c8e0be5b0257b881e1827274667c5c17a9',
    ),
    'wyrihaximus/twig-view' => 
    array (
      'pretty_version' => '4.3.8',
      'version' => '4.3.8.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a5ec66690aa045d6eda17ab1c8a5baf0efdcfa45',
    ),
    'zendframework/zend-diactoros' => 
    array (
      'pretty_version' => '1.8.6',
      'version' => '1.8.6.0',
      'aliases' => 
      array (
      ),
      'reference' => '20da13beba0dde8fb648be3cc19765732790f46e',
    ),
  ),
);
