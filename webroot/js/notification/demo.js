$(function(){
    $.notify("BOOM!", "error");
    $.notify("Warning: Self-destruct in 3.. 2..", "warn");
    $.notify("Do not press this button", "info");
    $.notify("Access granted", "success");

    // Click for notification
    $('.pos-demo').click(function(){
        $(".pos-demo").notify(
            "I'm to the right of this box",
            { position: "right" }
        );
    });
   
});