$(document).ready(function () {
    $('#table_id').DataTable({
        select: true,
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": false,
        "loadingRecords": "Chargement en cours...",
        "processing": "En traitement...",
        "language": {
            "zeroRecords": "Rien trouvé - désolé",
            "lengthMenu": "Afficher les enregistrements _MENU_ par page",
            "info": "Afficher la page _PAGE_ de _PAGES_",
            "infoEmpty": "Aucun résultat",
            "emptyTable": "Aucune donnée disponible",
            "infoFiltered": "(filtré à partir du nombre total d'enregistrements)",
            "search": "Rechercher : ",
            "paginate": {
                "first": "Premier",
                "last": "Dernier",
                "previous": "<",
                "next": ">"
            },
            "aria": {
                "sortAscending": ": Activer pour trier la colonne par ordre croissant",
                "sortDescending": ": Activer pour trier la colonne par ordre décroissant"
            }
        }
    });
    $('.dataTables_length').addClass('bs-select');

    // Ajout de quelques classes styling.
    //$('#table_id').addClass('stripe').addClass('cell-border').addClass('display').addClass('compact').addClass('order-column');


});