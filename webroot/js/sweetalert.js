$(function () {
    // Bouton alert success
    $('#alertSuccess').on('click', function () {
        swal({
            title: "Good job!",
            text: "You clicked the button!",
            icon: "success",
        });
    });

    // Bouton alert warning
    $('#alertDanger').on('click', function () {
        swal({
            title: "Deleting",
            text: "Are you sure you want to delete ?",
            icon: "warning",
            buttons: {
                cancel: "Annuler",
                catch: {
                    text: "Oui",
                    value: "catch",
                },
            },
            dangerMode: true,
        })
            .then((value) => {
                switch (value) {

                    case "catch":
                        swal("Good job!", "Suppression bien effectuée !", "success");
                        break;

                    default:
                        swal("Vous avez annuler la suppression.");
                }
            });

    });

});