<?php
namespace App\Test\TestCase\Controller;

use App\Controller\DossiersController;
use Cake\TestSuite\IntegrationTestTrait;
use Cake\TestSuite\TestCase;

/**
 * App\Controller\DossiersController Test Case
 */
class DossiersControllerTest extends TestCase
{
    use IntegrationTestTrait;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.Dossiers',
        'app.NatureDemandes',
        'app.Porteurs',
        'app.Filieres',
        'app.Communes',
        'app.Agents',
        'app.Assignations',
        'app.FinancementCredits',
        'app.Imputations',
        'app.Messages',
        'app.Notations',
        'app.PieceJointes',
        'app.StatutDossiers',
        'app.Suretes'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
