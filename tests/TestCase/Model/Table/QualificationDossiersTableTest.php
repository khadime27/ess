<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\QualificationDossiersTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\QualificationDossiersTable Test Case
 */
class QualificationDossiersTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\QualificationDossiersTable
     */
    public $QualificationDossiers;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.QualificationDossiers',
        'app.Dossiers',
        'app.Agents'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('QualificationDossiers') ? [] : ['className' => QualificationDossiersTable::class];
        $this->QualificationDossiers = TableRegistry::getTableLocator()->get('QualificationDossiers', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->QualificationDossiers);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
