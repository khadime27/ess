<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SuiviEvaluationsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SuiviEvaluationsTable Test Case
 */
class SuiviEvaluationsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\SuiviEvaluationsTable
     */
    public $SuiviEvaluations;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.SuiviEvaluations',
        'app.TypeEvaluations',
        'app.Agents',
        'app.PieceJointes'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('SuiviEvaluations') ? [] : ['className' => SuiviEvaluationsTable::class];
        $this->SuiviEvaluations = TableRegistry::getTableLocator()->get('SuiviEvaluations', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->SuiviEvaluations);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
