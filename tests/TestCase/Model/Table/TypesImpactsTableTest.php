<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\TypesImpactsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\TypesImpactsTable Test Case
 */
class TypesImpactsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\TypesImpactsTable
     */
    public $TypesImpacts;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.TypesImpacts'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('TypesImpacts') ? [] : ['className' => TypesImpactsTable::class];
        $this->TypesImpacts = TableRegistry::getTableLocator()->get('TypesImpacts', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->TypesImpacts);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
