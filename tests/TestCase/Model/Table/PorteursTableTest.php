<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\PorteursTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\PorteursTable Test Case
 */
class PorteursTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\PorteursTable
     */
    public $Porteurs;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.Porteurs',
        'app.Users',
        'app.Entreprises',
        'app.Dossiers',
        'app.Messages'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Porteurs') ? [] : ['className' => PorteursTable::class];
        $this->Porteurs = TableRegistry::getTableLocator()->get('Porteurs', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Porteurs);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
