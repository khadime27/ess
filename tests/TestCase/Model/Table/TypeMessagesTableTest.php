<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\TypeMessagesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\TypeMessagesTable Test Case
 */
class TypeMessagesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\TypeMessagesTable
     */
    public $TypeMessages;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.TypeMessages',
        'app.Messages'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('TypeMessages') ? [] : ['className' => TypeMessagesTable::class];
        $this->TypeMessages = TableRegistry::getTableLocator()->get('TypeMessages', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->TypeMessages);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
