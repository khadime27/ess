<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\BannieresTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\BannieresTable Test Case
 */
class BannieresTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\BannieresTable
     */
    public $Bannieres;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.Bannieres'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Bannieres') ? [] : ['className' => BannieresTable::class];
        $this->Bannieres = TableRegistry::getTableLocator()->get('Bannieres', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Bannieres);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
