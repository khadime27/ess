<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\TypeEntreprisesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\TypeEntreprisesTable Test Case
 */
class TypeEntreprisesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\TypeEntreprisesTable
     */
    public $TypeEntreprises;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.TypeEntreprises',
        'app.Entreprises'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('TypeEntreprises') ? [] : ['className' => TypeEntreprisesTable::class];
        $this->TypeEntreprises = TableRegistry::getTableLocator()->get('TypeEntreprises', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->TypeEntreprises);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
