<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\MediationsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\MediationsTable Test Case
 */
class MediationsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\MediationsTable
     */
    public $Mediations;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.Mediations',
        'app.FinancementCredits',
        'app.Status',
        'app.PieceJointes'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Mediations') ? [] : ['className' => MediationsTable::class];
        $this->Mediations = TableRegistry::getTableLocator()->get('Mediations', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Mediations);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
