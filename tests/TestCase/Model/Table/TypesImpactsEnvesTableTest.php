<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\TypesImpactsEnvesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\TypesImpactsEnvesTable Test Case
 */
class TypesImpactsEnvesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\TypesImpactsEnvesTable
     */
    public $TypesImpactsEnves;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.TypesImpactsEnves'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('TypesImpactsEnves') ? [] : ['className' => TypesImpactsEnvesTable::class];
        $this->TypesImpactsEnves = TableRegistry::getTableLocator()->get('TypesImpactsEnves', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->TypesImpactsEnves);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
