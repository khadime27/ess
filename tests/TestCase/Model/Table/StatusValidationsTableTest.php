<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\StatusValidationsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\StatusValidationsTable Test Case
 */
class StatusValidationsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\StatusValidationsTable
     */
    public $StatusValidations;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.StatusValidations'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('StatusValidations') ? [] : ['className' => StatusValidationsTable::class];
        $this->StatusValidations = TableRegistry::getTableLocator()->get('StatusValidations', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->StatusValidations);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
