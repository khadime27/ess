<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\FinancementsPassifsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\FinancementsPassifsTable Test Case
 */
class FinancementsPassifsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\FinancementsPassifsTable
     */
    public $FinancementsPassifs;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.FinancementsPassifs',
        'app.Dossiers'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('FinancementsPassifs') ? [] : ['className' => FinancementsPassifsTable::class];
        $this->FinancementsPassifs = TableRegistry::getTableLocator()->get('FinancementsPassifs', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->FinancementsPassifs);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
