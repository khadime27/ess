<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ImpactesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ImpactesTable Test Case
 */
class ImpactesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ImpactesTable
     */
    public $Impactes;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.Impactes'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Impactes') ? [] : ['className' => ImpactesTable::class];
        $this->Impactes = TableRegistry::getTableLocator()->get('Impactes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Impactes);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
