<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\TypeAgencesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\TypeAgencesTable Test Case
 */
class TypeAgencesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\TypeAgencesTable
     */
    public $TypeAgences;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.TypeAgences'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('TypeAgences') ? [] : ['className' => TypeAgencesTable::class];
        $this->TypeAgences = TableRegistry::getTableLocator()->get('TypeAgences', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->TypeAgences);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
