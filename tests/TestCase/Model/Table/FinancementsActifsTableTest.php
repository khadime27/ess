<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\FinancementsActifsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\FinancementsActifsTable Test Case
 */
class FinancementsActifsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\FinancementsActifsTable
     */
    public $FinancementsActifs;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.FinancementsActifs',
        'app.Dossiers'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('FinancementsActifs') ? [] : ['className' => FinancementsActifsTable::class];
        $this->FinancementsActifs = TableRegistry::getTableLocator()->get('FinancementsActifs', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->FinancementsActifs);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
