<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\AnimationsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\AnimationsTable Test Case
 */
class AnimationsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\AnimationsTable
     */
    public $Animations;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.Animations',
        'app.TypeAnimations',
        'app.Agents',
        'app.Participants',
        'app.PieceJointes'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Animations') ? [] : ['className' => AnimationsTable::class];
        $this->Animations = TableRegistry::getTableLocator()->get('Animations', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Animations);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
