<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\StatutDossiersTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\StatutDossiersTable Test Case
 */
class StatutDossiersTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\StatutDossiersTable
     */
    public $StatutDossiers;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.StatutDossiers',
        'app.Dossiers',
        'app.Agents'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('StatutDossiers') ? [] : ['className' => StatutDossiersTable::class];
        $this->StatutDossiers = TableRegistry::getTableLocator()->get('StatutDossiers', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->StatutDossiers);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
