<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\FinancesParametresTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\FinancesParametresTable Test Case
 */
class FinancesParametresTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\FinancesParametresTable
     */
    public $FinancesParametres;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.FinancesParametres'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('FinancesParametres') ? [] : ['className' => FinancesParametresTable::class];
        $this->FinancesParametres = TableRegistry::getTableLocator()->get('FinancesParametres', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->FinancesParametres);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
