<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\NatureDemandesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\NatureDemandesTable Test Case
 */
class NatureDemandesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\NatureDemandesTable
     */
    public $NatureDemandes;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.NatureDemandes',
        'app.Dossiers'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('NatureDemandes') ? [] : ['className' => NatureDemandesTable::class];
        $this->NatureDemandes = TableRegistry::getTableLocator()->get('NatureDemandes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->NatureDemandes);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
