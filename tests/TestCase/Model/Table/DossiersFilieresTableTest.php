<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\DossiersFilieresTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\DossiersFilieresTable Test Case
 */
class DossiersFilieresTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\DossiersFilieresTable
     */
    public $DossiersFilieres;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.DossiersFilieres',
        'app.Dossiers',
        'app.Filieres'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('DossiersFilieres') ? [] : ['className' => DossiersFilieresTable::class];
        $this->DossiersFilieres = TableRegistry::getTableLocator()->get('DossiersFilieres', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->DossiersFilieres);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
