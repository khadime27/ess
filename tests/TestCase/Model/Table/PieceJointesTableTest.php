<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\PieceJointesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\PieceJointesTable Test Case
 */
class PieceJointesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\PieceJointesTable
     */
    public $PieceJointes;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.PieceJointes',
        'app.Messages',
        'app.Dossiers',
        'app.Mediations',
        'app.Animations',
        'app.SuiviEvaluations'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('PieceJointes') ? [] : ['className' => PieceJointesTable::class];
        $this->PieceJointes = TableRegistry::getTableLocator()->get('PieceJointes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->PieceJointes);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
