<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\DossiersImpactsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\DossiersImpactsTable Test Case
 */
class DossiersImpactsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\DossiersImpactsTable
     */
    public $DossiersImpacts;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.DossiersImpacts',
        'app.Dossiers',
        'app.TypesImpacts'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('DossiersImpacts') ? [] : ['className' => DossiersImpactsTable::class];
        $this->DossiersImpacts = TableRegistry::getTableLocator()->get('DossiersImpacts', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->DossiersImpacts);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
