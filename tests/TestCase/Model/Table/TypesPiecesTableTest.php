<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\TypesPiecesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\TypesPiecesTable Test Case
 */
class TypesPiecesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\TypesPiecesTable
     */
    public $TypesPieces;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.TypesPieces'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('TypesPieces') ? [] : ['className' => TypesPiecesTable::class];
        $this->TypesPieces = TableRegistry::getTableLocator()->get('TypesPieces', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->TypesPieces);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
