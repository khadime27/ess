<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\NotationsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\NotationsTable Test Case
 */
class NotationsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\NotationsTable
     */
    public $Notations;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.Notations',
        'app.Dossiers',
        'app.Agents'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Notations') ? [] : ['className' => NotationsTable::class];
        $this->Notations = TableRegistry::getTableLocator()->get('Notations', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Notations);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
