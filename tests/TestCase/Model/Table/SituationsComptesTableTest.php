<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SituationsComptesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SituationsComptesTable Test Case
 */
class SituationsComptesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\SituationsComptesTable
     */
    public $SituationsComptes;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.SituationsComptes',
        'app.Dossiers'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('SituationsComptes') ? [] : ['className' => SituationsComptesTable::class];
        $this->SituationsComptes = TableRegistry::getTableLocator()->get('SituationsComptes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->SituationsComptes);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
