<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\EntreprisesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\EntreprisesTable Test Case
 */
class EntreprisesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\EntreprisesTable
     */
    public $Entreprises;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.Entreprises',
        'app.TypeEntreprises',
        'app.Communes',
        'app.Porteurs'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Entreprises') ? [] : ['className' => EntreprisesTable::class];
        $this->Entreprises = TableRegistry::getTableLocator()->get('Entreprises', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Entreprises);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
