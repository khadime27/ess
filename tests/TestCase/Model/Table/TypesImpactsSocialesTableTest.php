<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\TypesImpactsSocialesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\TypesImpactsSocialesTable Test Case
 */
class TypesImpactsSocialesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\TypesImpactsSocialesTable
     */
    public $TypesImpactsSociales;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.TypesImpactsSociales'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('TypesImpactsSociales') ? [] : ['className' => TypesImpactsSocialesTable::class];
        $this->TypesImpactsSociales = TableRegistry::getTableLocator()->get('TypesImpactsSociales', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->TypesImpactsSociales);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
