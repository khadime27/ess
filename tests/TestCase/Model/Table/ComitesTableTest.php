<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ComitesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ComitesTable Test Case
 */
class ComitesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ComitesTable
     */
    public $Comites;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.Comites'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Comites') ? [] : ['className' => ComitesTable::class];
        $this->Comites = TableRegistry::getTableLocator()->get('Comites', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Comites);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
