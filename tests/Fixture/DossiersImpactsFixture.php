<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * DossiersImpactsFixture
 *
 */
class DossiersImpactsFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'dossiers_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'types_impacts_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        '_indexes' => [
            'dossiers_id' => ['type' => 'index', 'columns' => ['dossiers_id', 'types_impacts_id'], 'length' => []],
            'impacts_types_impacts1' => ['type' => 'index', 'columns' => ['types_impacts_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'impacts_types_impacts1' => ['type' => 'foreign', 'columns' => ['types_impacts_id'], 'references' => ['types_impacts', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'types_impacts_dossiers1' => ['type' => 'foreign', 'columns' => ['dossiers_id'], 'references' => ['dossiers', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'latin1_swedish_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Init method
     *
     * @return void
     */
    public function init()
    {
        $this->records = [
            [
                'id' => 1,
                'dossiers_id' => 1,
                'types_impacts_id' => 1
            ],
        ];
        parent::init();
    }
}
